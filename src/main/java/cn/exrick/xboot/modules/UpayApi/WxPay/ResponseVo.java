package cn.exrick.xboot.modules.UpayApi.WxPay;

import lombok.Data;
import lombok.experimental.Accessors;

/**
 * @Author : xiaofei
 * @Date: 2019/9/6
 */
@Data
@Accessors(chain = true)
public class ResponseVo {
    private String prepay_id;
    private String paySign;
    private String appId;
    private String signType;
    private String nonceStr;
    private String timeStamp;
    private String packagex;

}
