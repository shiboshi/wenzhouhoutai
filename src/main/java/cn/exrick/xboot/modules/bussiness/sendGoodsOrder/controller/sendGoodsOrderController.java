package cn.exrick.xboot.modules.bussiness.sendGoodsOrder.controller;

import cn.exrick.xboot.common.enums.IsComplete;
import cn.exrick.xboot.common.enums.OrderType;
import cn.exrick.xboot.common.upay.utils.DateUtils;
import cn.exrick.xboot.common.utils.PageUtil;
import cn.exrick.xboot.common.utils.ResultUtil;
import cn.exrick.xboot.common.utils.SecurityUtil;
import cn.exrick.xboot.common.utils.ToolUtil;
import cn.exrick.xboot.common.vo.PageVo;
import cn.exrick.xboot.common.vo.Result;
import cn.exrick.xboot.modules.base.entity.Role;
import cn.exrick.xboot.modules.base.entity.User;
import cn.exrick.xboot.modules.base.entity.UserRole;
import cn.exrick.xboot.modules.base.service.RoleService;
import cn.exrick.xboot.modules.base.service.UserService;
import cn.exrick.xboot.modules.base.service.mybatis.IUserRoleService;
import cn.exrick.xboot.modules.bussiness.applicationSendGoodsOrder.pojo.ApplicationSendGoodsOrder;
import cn.exrick.xboot.modules.bussiness.applicationSendGoodsOrder.service.IapplicationSendGoodsOrderService;
import cn.exrick.xboot.modules.bussiness.inventoryOrder.VO.InventoryVO;
import cn.exrick.xboot.modules.bussiness.inventoryOrder.pojo.InventoryOrder;
import cn.exrick.xboot.modules.bussiness.inventoryOrder.service.IInventoryOrderService;
import cn.exrick.xboot.modules.bussiness.purchaseOrder.VO.PurchaseOrderVO;
import cn.exrick.xboot.modules.bussiness.purchaseOrder.service.IpurchaseOrderService;
import cn.exrick.xboot.modules.bussiness.sendGoodsOrder.VO.SendGoodsVO;
import cn.exrick.xboot.modules.bussiness.sendGoodsOrder.VO.SendOrderCommentVO;
import cn.exrick.xboot.modules.bussiness.sendGoodsOrder.VO.TaskSendGoodVO;
import cn.exrick.xboot.modules.bussiness.sendGoodsOrder.pojo.SendGoodsOrder;
import cn.exrick.xboot.modules.bussiness.sendGoodsOrder.service.IsendGoodsOrderService;
import cn.exrick.xboot.modules.bussiness.shiWarehouse.pojo.ShiWarehouse;
import cn.exrick.xboot.modules.bussiness.shiWarehouse.service.IShiWarehouseService;
import cn.hutool.core.date.DateUtil;
import cn.hutool.core.map.MapBuilder;
import cn.hutool.core.map.MapUtil;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.google.common.base.Splitter;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * @author xiaofei
 */
@Slf4j
@RestController
@Api(description = "发货单管理接口")
@RequestMapping("/xboot/sendGoodsOrder")
@Transactional
public class sendGoodsOrderController {

    @Autowired
    private IsendGoodsOrderService isendGoodsOrderService;
    @Autowired
    private IInventoryOrderService inventoryOrderService;
    @Autowired
    private IShiWarehouseService warehouseService;
    @Autowired
    private SecurityUtil securityUtil;
    @Autowired
    private UserService userService;
    @Autowired
    private RoleService roleService;
    @Autowired
    private IUserRoleService userRoleService;

    /**
     * 申请发货单
     */
    @Autowired
    private IapplicationSendGoodsOrderService applicationSendGoodsOrderService;

    /**
     * 产品基础信息
     */
    @Autowired
    private IpurchaseOrderService purchaseService;


    private static final Splitter SPLITTER = Splitter.on(",").trimResults().omitEmptyStrings();

    @PostMapping("/getDetail")
    @ApiOperation(value = "通过id获取")
    public Result<Object> getDetail(String id,String userId) {
        if (ToolUtil.isEmpty(id)) {
            return new ResultUtil<>().setErrorMsg("传入id为空！");
        } else {
            MapBuilder<String, Object> mapBuilder = MapUtil.builder();
            if (userId == null) {
                //获取当前登录人 ID
                User u = securityUtil.getCurrUser();
                String userIdd= u.getId();
                userId= userIdd;
            }
            List<InventoryVO> list = isendGoodsOrderService.getDetail(id,userId);
            for (InventoryVO i : list ){
                SendGoodsOrder sendGoodsOrder = isendGoodsOrderService.getById(id);
                i.setSendGoodsOrder( sendGoodsOrder);

                //通过申请发货单的id获取申请发货单的全部数据
                ApplicationSendGoodsOrder applicationSendGoodsOrder = applicationSendGoodsOrderService.getById(sendGoodsOrder.getApplicationSendGoodsId());

                List<PurchaseOrderVO> purchaseOrderVOS = new ArrayList<>();
                if(ToolUtil.isNotEmpty(applicationSendGoodsOrder)){
                    purchaseOrderVOS  = purchaseService.getPurchaseList(OrderType.applicationOrder,applicationSendGoodsOrder.getId());
                    i.setQuantityReceived(purchaseOrderVOS.get(0).getHavaSend().toString());
                }
            }
            BigDecimal totalAmout = list.stream().map(l -> l.getProductCommon().getPrice()).filter(p -> Optional.ofNullable(p).isPresent() == true)
                    .reduce(BigDecimal.ZERO,BigDecimal::add);
            mapBuilder.put("data", list)
                    .put("amount", isendGoodsOrderService.getTotalAmount(id))
                    .put("totalAmout",totalAmout);
            return new ResultUtil<>().setData(mapBuilder.map());
        }
    }

    @PostMapping("/getAll")
    @ApiOperation(value = "获取全部数据")
    public Result<Page<SendGoodsVO>> getAll(@ModelAttribute PageVo pageVo, String unitName, String userId, String code) {
        //找到当前输入的序列号的库存id
        Integer type = 2;
        String serial = null;
        String user = ToolUtil.isNotEmpty(userId) ? userId : securityUtil.getCurrUser().getId();
        if (ToolUtil.isNotEmpty(userService.get(user))) {
            UserRole userRole = userRoleService.getOne(Wrappers.<UserRole>lambdaQuery()
                    .eq(UserRole::getUserId, user));
            Role role = roleService.get(userRole.getRoleId());
            if (role.getName().equals("ROLE_ADMIN")) {
                type = 1;
                if (ToolUtil.isNotEmpty(unitName)) {
                    InventoryOrder inventoryOrder = inventoryOrderService.getOne(Wrappers.<InventoryOrder>lambdaQuery()
                            .eq(InventoryOrder::getSerialNum, unitName)
                            .eq(InventoryOrder::getSerialNumOut, "2"));
                    if (ToolUtil.isNotEmpty(inventoryOrder)) {
                        serial = inventoryOrder.getId();
                    }
                }
            }
        }
        return new ResultUtil<Page<SendGoodsVO>>().setData(isendGoodsOrderService.getSendGoodsOrderByUser(PageUtil.initMpPage(pageVo), unitName, userId, code, type, serial));
    }

    @PostMapping("/selectById")
    @ApiOperation(value = "获取全部数据")
    public Result<List<SendGoodsVO>> selectById(String id) {
        SendGoodsVO pageVo = isendGoodsOrderService.selectById(id);
        List<SendGoodsVO> list = new ArrayList<>();
        list.add(pageVo);
        return new ResultUtil<List<SendGoodsVO>>().setData(list);
    }

    @RequestMapping(value = "/delByIds/{ids}", method = RequestMethod.DELETE)
    @ApiOperation(value = "批量通过id删除")
    public Result<Object> delAllByIds(@PathVariable String[] ids) {

        for (String id : ids) {
            isendGoodsOrderService.removeById(id);
        }
        return new ResultUtil<Object>().setSuccessMsg("批量通过id删除数据成功");
    }

    /**
     * 物流发货员pc处理库存
     *
     * @param taskSendGoodVO
     * @return
     */
    @PostMapping("/taskInser")
    public Result<Object> taskInser(@ModelAttribute TaskSendGoodVO taskSendGoodVO) {
        List<String> serial = new ArrayList<>();
        if ("1".equals(taskSendGoodVO.getCode())) {
            serial = isendGoodsOrderService.checkSeri(taskSendGoodVO.getSerial(), taskSendGoodVO.getApplicationSendGoodsId());

        } else {
            serial = isendGoodsOrderService.filterInventory(taskSendGoodVO.getSerial());
        }
        if (ToolUtil.isNotEmpty(serial)) {
            return new ResultUtil<>().setErrorMsg(201, "以下序列号库存为空！", serial);
        } else {
            isendGoodsOrderService.taskInser(taskSendGoodVO);
            return new ResultUtil<>().setSuccessMsg("流程成功！");
        }
    }

    /**
     * 确认收货
     */
    @PostMapping("/confirmationOfReceipt")
    @ApiOperation(value = "确认收货")
    public Result<Object> confirmationOfReceipt(@ModelAttribute SendOrderCommentVO sendOrderCommentVO) {

        String userId = ToolUtil.isNotEmpty(sendOrderCommentVO.getUserId()) ? sendOrderCommentVO.getUserId() : securityUtil.getCurrUser().getId();
        //1.插入到 申请发货单中
        if (ToolUtil.isNotEmpty(sendOrderCommentVO)) {
            SendGoodsOrder sendGoodsOrder = isendGoodsOrderService.getById(sendOrderCommentVO.getId());
            if (ToolUtil.isNotEmpty(sendGoodsOrder)) {
                sendGoodsOrder.setComment(sendOrderCommentVO.getCommon());
                sendGoodsOrder.setUpdateBy(userId);
                sendGoodsOrder.setUpdateTime(DateUtil.parse(DateUtils.format(new Date(), DateUtils.YYYYMMDDHHMMSS)));
                sendGoodsOrder.setIsComplete(IsComplete.IS_COMPLETE);
                isendGoodsOrderService.updateById(sendGoodsOrder);
                //2.判断当前经销商是否有仓库
                ShiWarehouse warehouse = warehouseService.getOne(Wrappers.<ShiWarehouse>lambdaQuery()
                        .eq(ShiWarehouse::getDeptId, userService.get(userId).getDepartmentId()));
                if (ToolUtil.isNotEmpty(warehouse)) {
                    //3.产品入库
                    List<String> inventoryIds = SPLITTER.splitToList(sendGoodsOrder.getInventoryOrderIds());
                    List<InventoryOrder> inventoryOrders = inventoryIds.stream().map(id -> inventoryOrderService.getById(id))
                            .collect(Collectors.toList());
                    inventoryOrders.stream().forEach(Order -> {
                        Order.setSerialNumOut(2);
                        inventoryOrderService.updateById(Order);
                        //同时生成一个 是经销商库存的库存信息
                        InventoryOrder inventoryOrder = new InventoryOrder();
                        inventoryOrder.setSerialNum(Order.getSerialNum());
                        inventoryOrder.setAmount(Order.getAmount());
                        inventoryOrder.setProductOrderId(Order.getProductOrderId());
                        inventoryOrder.setCreateBy(userId);
                        inventoryOrder.setBatchNumber(Order.getBatchNumber());
                        inventoryOrder.setWarhouseId(warehouse.getId());
                        inventoryOrder.setSerialNumOut(1);
                        inventoryOrderService.save(inventoryOrder);
                    });
                } else {
                    return new ResultUtil<>().setErrorMsg(201, "仓库为空！");
                }
            }
        } else {
            return new ResultUtil<>().setErrorMsg("传入参数为空！");
        }
        return new ResultUtil<>().setSuccessMsg("确认收货成功！");
    }
}
