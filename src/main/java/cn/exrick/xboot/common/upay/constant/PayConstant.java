package cn.exrick.xboot.common.upay.constant;

import org.bouncycastle.math.ec.custom.sec.SecP192K1Curve;

/**
 * 支付常量
 * @author zhou
 */
public class PayConstant {
    /**
     * 测试环境
     */
    public final static String SERVER_URL_TEST  = "https://test.zjpay.cn/upay/gateway";
    /**
     * 生产环境
     */
    public static final String SERVER_URL_PRO = "https://qr-test2.chinaums.com/netpay-route-server/api/";

    /**
     * 消息类型
     */
    public static final String msgType = "wx.unifiedOrder";

    public static final String msgSrcId = "3194";

    /**
     * 消息来源
     */
    public static final String msgSrc =  "WWW.TEST.COM";

    /**
     * 用户子标识
     * 微信必传
     * 需要商户自行调用微信平台接口获取，具体获取方式 请根据微信接口文档。
     */
    public static  final  String subOpenId ="o_o0o4_4rOYHW1OzHAd6ZPPIPhkY";

    public static final  String subAppId = "wxb73a514f60d1edeb";
    /**
     * 交易类型
     */
    public static final  String tradeType = "MINIfcAmtnx7MwismjWNhNKdHC44mNXtnEQeJkRrhKJwyrW2ysRR";


    /**
     * 商户号
     */
    public static final String mid = "898340149000005";
    /**
     * 设备终端号
     */
    public static final String tid = "88880001";

    /**
     * 机构商户号
     */
    public static final String instMid ="MINIDEFAULT";

    /**
     * MD5 key
     */
    public static final String MD5_KEY = "fcAmtnx7MwismjWNhNKdHC44mNXtnEQeJkRrhKJwyrW2ysRR";

    /**
     * 签名方式 MD5
     */
    public final static String SIGN_TYPE_MD5 = "MD5";


}
