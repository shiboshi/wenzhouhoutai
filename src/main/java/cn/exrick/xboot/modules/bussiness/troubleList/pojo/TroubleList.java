package cn.exrick.xboot.modules.bussiness.troubleList.pojo;

import cn.exrick.xboot.base.XbootBaseEntity;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModelProperty;
import io.swagger.annotations.ApiModel;
import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

/**
 * @author tqr
 */
@Data
@Entity
@Table(name = "t_trouble_list")
@TableName("t_trouble_list")
@ApiModel(value = "问题表")
public class TroubleList extends XbootBaseEntity {

    private static final long serialVersionUID = 1L;

    /**
     * 问题类型，关联问题类型id
     */
    @Column(length = 60)
    private String troubleTypeId;

    /**
     * 问题图片
     */
    @Column(length = 64)
    private String troublePicture;

    /**
     * 问题描述
     */
    @Column(length = 255)
    private String troubleDescription;

    /**
     * 问题解决方式
     */
    @Column(length = 60)
    private String troubleSolve;
}