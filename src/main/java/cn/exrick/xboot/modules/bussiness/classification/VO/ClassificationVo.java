package cn.exrick.xboot.modules.bussiness.classification.VO;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
@ApiModel("产品分类")
public class ClassificationVo {

    @ApiModelProperty("id")
    private String id;

    @ApiModelProperty("分类名称")
    private String name;

    private Boolean isCheck;
}
