package cn.exrick.xboot.modules.bussiness.putInWarehouse.mapper;

import cn.exrick.xboot.modules.bussiness.putInWarehouse.PutInWarehouseProvider;
import cn.exrick.xboot.modules.bussiness.putInWarehouse.VO.PutInWarehouseVO;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import cn.exrick.xboot.modules.bussiness.putInWarehouse.pojo.PutInWarehouse;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.SelectProvider;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * 入库数据处理层
 * @author tqr
 */
@Repository
public interface PutInWarehouseMapper extends BaseMapper<PutInWarehouse> {

    @SelectProvider(type = PutInWarehouseProvider.class,method = "getPutInWarehouse")
    IPage<PutInWarehouseVO> getPutInWarehouse(@Param("initMpPage") Page initMpPage,@Param("productOrderName") String productOrderName,@Param("status")String status);

    @SelectProvider(type = PutInWarehouseProvider.class,method = "getById")
    PutInWarehouseVO getById(@Param("id") String id);
}