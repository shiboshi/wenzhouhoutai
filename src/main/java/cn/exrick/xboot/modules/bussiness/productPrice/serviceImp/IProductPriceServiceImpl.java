package cn.exrick.xboot.modules.bussiness.productPrice.serviceImp;

import cn.exrick.xboot.common.utils.ToolUtil;
import cn.exrick.xboot.modules.bussiness.product.mapper.productModelMapper;
import cn.exrick.xboot.modules.bussiness.product.pojo.ProductModel;
import cn.exrick.xboot.modules.bussiness.productPrice.VO.ProductPriceVO;
import cn.exrick.xboot.modules.bussiness.productPrice.mapper.ProductPriceMapper;
import cn.exrick.xboot.modules.bussiness.productPrice.pojo.ProductPrice;
import cn.exrick.xboot.modules.bussiness.productPrice.service.IProductPriceService;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

/**
 * 产品价格表接口实现
 *
 * @author tqr
 */
@Slf4j
@Service
@Transactional
public class IProductPriceServiceImpl extends ServiceImpl<ProductPriceMapper, ProductPrice> implements IProductPriceService {

    @Autowired
    private ProductPriceMapper productPriceMapper;
    @Autowired
    private productModelMapper productModelMapper1;

    @Override
    public IPage<ProductPriceVO> getProductPrice(Page initMpPage, String condition) {
        IPage<ProductPriceVO> list = productPriceMapper.getProductPrice(initMpPage, condition);
        List<ProductPriceVO> productPriceVOS = list.getRecords();
        for (ProductPriceVO p : productPriceVOS) {
            if (ToolUtil.isNotEmpty(p.getProductModelId())) {
                p.setModelNameIn(productModelMapper1.selectList(new QueryWrapper<ProductModel>().lambda().eq(ProductModel::getKindId, p.getProductModelId())).get(0).getModelNameIn());
            }
        }
        list.setRecords(productPriceVOS);
        return list;
    }

    @Override
    public boolean insertPrice(List<ProductPrice> list) {
        try {
            this.saveBatch(list);
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    @Override
    public List<ProductPrice> getByCondition(String productOrderId, String levelId) {
        List<ProductPrice> list = productPriceMapper.selectList(new QueryWrapper<ProductPrice>().lambda().eq(ProductPrice::getLevelId, levelId).eq(ProductPrice::getProductOrderId, productOrderId));
        return list;
    }
}