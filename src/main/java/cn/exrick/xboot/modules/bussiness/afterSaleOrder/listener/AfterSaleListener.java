package cn.exrick.xboot.modules.bussiness.afterSaleOrder.listener;

import cn.exrick.xboot.common.enums.CheckStatus;
import cn.exrick.xboot.config.springContext.SpringContextHolder;
import cn.exrick.xboot.modules.bussiness.afterSaleOrder.pojo.AfterSaleOrder;
import cn.exrick.xboot.modules.bussiness.afterSaleOrder.service.IafterSaleOrderService;
import org.activiti.engine.delegate.DelegateExecution;
import org.activiti.engine.delegate.ExecutionListener;

/**
 * @Author : xiaofei
 * @Date: 2019/8/22
 */
public class AfterSaleListener implements ExecutionListener {
    @Override
    public void notify(DelegateExecution delegateExecution) {

        String tableId = (String) delegateExecution.getVariable("tableId");
        IafterSaleOrderService iafterSaleOrderService = SpringContextHolder.getBean(IafterSaleOrderService.class);
        if (delegateExecution.getEventName().equals("end")){
            AfterSaleOrder afterSaleOrders = iafterSaleOrderService.getById(tableId);
            afterSaleOrders.setCheckStatus(CheckStatus.sucess);
            iafterSaleOrderService.updateById(afterSaleOrders);
        }
    }
}
