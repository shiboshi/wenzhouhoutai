package cn.exrick.xboot.common.upay.response;

import cn.exrick.xboot.common.upay.utils.CipherUtils;
import cn.exrick.xboot.common.upay.utils.StringUtils;
import cn.exrick.xboot.common.upay.utils.Utils;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import lombok.Data;
import lombok.extern.java.Log;

import java.math.BigDecimal;
import java.security.interfaces.RSAPublicKey;
import java.util.Map;
import java.util.TreeMap;
import java.util.logging.Logger;

/**
 * Created by vectoryang on 2017/7/17.
 */
@Log
public class UpayResponse {
    private String msgType;
    private String msgSrc;
    private String merName;
    private String mid;
    private String miniPayRequest;
    private String settleRefId;
    private String tid;
    private BigDecimal totalAmount;
    private String qrCode;

    public String getMsgType() {
        return msgType;
    }

    public void setMsgType(String msgType) {
        this.msgType = msgType;
    }

    public String getMsgSrc() {
        return msgSrc;
    }

    public void setMsgSrc(String msgSrc) {
        this.msgSrc = msgSrc;
    }

    public String getMerName() {
        return merName;
    }

    public void setMerName(String merName) {
        this.merName = merName;
    }

    public String getMid() {
        return mid;
    }

    public void setMid(String mid) {
        this.mid = mid;
    }

    public String getMiniPayRequest() {
        return miniPayRequest;
    }

    public void setMiniPayRequest(String miniPayRequest) {
        this.miniPayRequest = miniPayRequest;
    }

    public String getSettleRefId() {
        return settleRefId;
    }

    public void setSettleRefId(String settleRefId) {
        this.settleRefId = settleRefId;
    }

    public String getTid() {
        return tid;
    }

    public void setTid(String tid) {
        this.tid = tid;
    }

    public BigDecimal getTotalAmount() {
        return totalAmount;
    }

    public void setTotalAmount(BigDecimal totalAmount) {
        this.totalAmount = totalAmount;
    }

    public String getQrCode() {
        return qrCode;
    }

    public void setQrCode(String qrCode) {
        this.qrCode = qrCode;
    }

    public String getTargetMid() {
        return targetMid;
    }

    public void setTargetMid(String targetMid) {
        this.targetMid = targetMid;
    }

    public String getResponseTimestamp() {
        return responseTimestamp;
    }

    public void setResponseTimestamp(String responseTimestamp) {
        this.responseTimestamp = responseTimestamp;
    }

    public String getErrCode() {
        return errCode;
    }

    public void setErrCode(String errCode) {
        this.errCode = errCode;
    }

    public String getPrepayId() {
        return prepayId;
    }

    public void setPrepayId(String prepayId) {
        this.prepayId = prepayId;
    }

    public String getTargetStatus() {
        return targetStatus;
    }

    public void setTargetStatus(String targetStatus) {
        this.targetStatus = targetStatus;
    }

    public String getSeqId() {
        return seqId;
    }

    public void setSeqId(String seqId) {
        this.seqId = seqId;
    }

    public String getMerOrderId() {
        return merOrderId;
    }

    public void setMerOrderId(String merOrderId) {
        this.merOrderId = merOrderId;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getTargetSys() {
        return targetSys;
    }

    public void setTargetSys(String targetSys) {
        this.targetSys = targetSys;
    }

    public String getSign() {
        return sign;
    }

    public void setSign(String sign) {
        this.sign = sign;
    }

    public static Logger getLog() {
        return log;
    }

    private String targetMid;
    private String responseTimestamp;
    private String errCode;
    private String prepayId;
    private String targetStatus;
    private String seqId;
    private String merOrderId;
    private String status;
    private String targetSys;
    private String sign;

    public static boolean design(JSONObject jsonObject, String designkey) {
        jsonObject.remove("sign_format");
        String sign = (String) jsonObject.remove("sign");
        String signType = (String) jsonObject.remove("sign_type");

        TreeMap<String, String> params = new TreeMap<>();
        for (Map.Entry<String, Object> entry : jsonObject.entrySet()) {
            if (!StringUtils.isEmpty(entry.getValue())) {
                params.put(entry.getKey(), entry.getValue().toString());
            }
        }

        StringBuffer sbtr = new StringBuffer();
        for (Map.Entry<String, String> entry : params.entrySet()) {
            sbtr.append(entry.getKey());
            sbtr.append("=");
            sbtr.append(entry.getValue());
            sbtr.append("&");
        }
        log.info("报文:"+sbtr);

        String signStr = sbtr.substring(0, sbtr.toString().length() - 1);
        if ("RSA".equalsIgnoreCase(signType)) {
            // 加载公钥
            RSAPublicKey pubKey = CipherUtils.getRSAPubKey(Utils.decodeBase64(designkey));
            return CipherUtils.designRSA(signStr, Utils.decodeBase64(sign), pubKey);
        } else if ("MD5".equalsIgnoreCase(signType)) {
            return CipherUtils.encodeMd5(signStr + "&key=" + designkey).equalsIgnoreCase(sign);
        } else {
            return false;
        }
    }


    @Override
    public String toString() {
        return JSON.toJSONString(this);
    }
}
