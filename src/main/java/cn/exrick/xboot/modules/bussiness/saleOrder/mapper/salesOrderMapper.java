package cn.exrick.xboot.modules.bussiness.saleOrder.mapper;

import cn.exrick.xboot.modules.bussiness.saleOrder.SQL.Sql;
import cn.exrick.xboot.modules.bussiness.saleOrder.VO.SalesOrderVO;
import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import cn.exrick.xboot.modules.bussiness.saleOrder.pojo.SalesOrder;
import com.baomidou.mybatisplus.core.toolkit.Constants;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.apache.ibatis.annotations.*;
import org.springframework.stereotype.Repository;

/**
 * 销售订单数据处理层
 * @author xiaofei
 */
@Repository
public interface salesOrderMapper extends BaseMapper<SalesOrder> {


    /**
     * 获取销售列表
     * @param page
     * @param orderNum
     * @return
     */
    @SelectProvider(type = Sql.class,method = "selectList")
    Page<SalesOrderVO> getList(@Param("page") Page page,@Param("orderNum") String orderNum,@Param("userId") String userId,@Param("checkStatus")String checkStatus,@Param("warehouseType")String warehouseType);


    /**
     * 自定义SQL语句找寻最新的一个id
     * @return
     */
    String selectLast();

    @SelectProvider(type = Sql.class,method = "getById")
    SalesOrderVO getById(@Param("id") String id);
}