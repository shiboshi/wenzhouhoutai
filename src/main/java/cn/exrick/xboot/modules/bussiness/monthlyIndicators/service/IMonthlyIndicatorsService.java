package cn.exrick.xboot.modules.bussiness.monthlyIndicators.service;

import com.baomidou.mybatisplus.extension.service.IService;
import cn.exrick.xboot.modules.bussiness.monthlyIndicators.pojo.MonthlyIndicators;

import java.util.List;

/**
 * 月份指标接口
 * @author tqr
 */
public interface IMonthlyIndicatorsService extends IService<MonthlyIndicators> {

}