package cn.exrick.xboot.common.upay.request.transaction;


import cn.exrick.xboot.common.upay.enums.BizChannel;
import cn.exrick.xboot.common.upay.enums.BizType;
import cn.exrick.xboot.common.upay.request.RequestBody;
import cn.exrick.xboot.common.utils.ToolUtil;

public class QueryRequestBody extends RequestBody {
    private String type;
    private String ext_no;
    private String trade_no;
    private String phone_code;
    private String refund_trade_no;
    private String mchtno;
    private String termno;
    private String extend_params;

    public QueryRequestBody() {
    }

    @Override
    public boolean validate() {

        if (ToolUtil.isEmpty(type)) {
            throw new RuntimeException("查询类型[type]不可为空");
        }

        if ("refund".equals(type) && ToolUtil.isEmpty(refund_trade_no)) {
            throw new RuntimeException("退款交易查询的退款订单号[refund_trade_no]不可为空");
        }

        return true;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getExt_no() {
        return ext_no;
    }

    public void setExt_no(String ext_no) {
        this.ext_no = ext_no;
    }

    public String getTrade_no() {
        return trade_no;
    }

    public void setTrade_no(String trade_no) {
        this.trade_no = trade_no;
    }

    public String getPhone_code() {
        return phone_code;
    }

    public void setPhone_code(String phone_code) {
        this.phone_code = phone_code;
    }

    public String getRefund_trade_no() {
        return refund_trade_no;
    }

    public void setRefund_trade_no(String refund_trade_no) {
        this.refund_trade_no = refund_trade_no;
    }

    public String getMchtno() {
        return mchtno;
    }

    public void setMchtno(String mchtno) {
        this.mchtno = mchtno;
    }

    public String getTermno() {
        return termno;
    }

    public void setTermno(String termno) {
        this.termno = termno;
    }

    public String getExtend_params() {
        return extend_params;
    }

    public void setExtend_params(String extend_params) {
        this.extend_params = extend_params;
    }

    @Override
    public String acquireBizChannel() {
        return BizChannel.UMSZJ_CHANNEL_COMMON.getEn();
    }

    @Override
    public String acquireBizType() {
        return BizType.UMSZJ_TRADE_QUERY.getEn();
    }

}
