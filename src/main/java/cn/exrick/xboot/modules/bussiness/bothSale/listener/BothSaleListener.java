package cn.exrick.xboot.modules.bussiness.bothSale.listener;

import cn.exrick.xboot.common.enums.CheckStatus;
import cn.exrick.xboot.common.utils.ToolUtil;
import cn.exrick.xboot.config.springContext.SpringContextHolder;
import cn.exrick.xboot.modules.activiti.service.ActBusinessService;
import cn.exrick.xboot.modules.activiti.service.ActProcessService;
import cn.exrick.xboot.modules.bussiness.bothSale.pojo.BothSale;
import cn.exrick.xboot.modules.bussiness.bothSale.service.IBothSaleService;
import org.activiti.engine.delegate.DelegateExecution;
import org.activiti.engine.delegate.ExecutionListener;

/**
 * @Author : xiaofei
 * @Date: 2019/8/16
 */
public class BothSaleListener implements ExecutionListener {
    @Override
    public void notify(DelegateExecution delegateExecution) throws Exception {

        String tableId = (String) delegateExecution.getVariable("tableId");
        IBothSaleService iBothSaleService = SpringContextHolder.getBean(IBothSaleService.class);
        ActProcessService actProcessService = SpringContextHolder.getBean(ActProcessService.class);
        ActBusinessService actBusinessService = SpringContextHolder.getBean(ActBusinessService.class);
        BothSale bothSale = iBothSaleService.getById(tableId);
        if (delegateExecution.getEventName().equals("end")) {
            if (ToolUtil.isNotEmpty(bothSale)) {
                bothSale.setCheckCodeDealer(CheckStatus.sucess);
                iBothSaleService.updateById(bothSale);
            }
        }
    }
}
