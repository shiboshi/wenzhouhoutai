package cn.exrick.xboot.modules.bussiness.dealerLevel.VO;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.math.BigDecimal;

@Data
@ApiModel("经销商")
public class DealerLevelVO {

    @ApiModelProperty("id")
    private String id;

    /**
     * 经销商等级id
     */
    @ApiModelProperty("等级id")
    private String levelId;

    @ApiModelProperty("等级")
    private String level;

    @ApiModelProperty("名字")
    private String name;

    @ApiModelProperty("备注")
    private String remark;

    @ApiModelProperty("关联部门id")
    private String deptId;

    @ApiModelProperty("部门")
    private String dept;

    @ApiModelProperty("信用额度")
    private BigDecimal credit;

    @ApiModelProperty("经销商详细地址")
    private String address;

    @ApiModelProperty("账号余额")
    private BigDecimal balance;

    @ApiModelProperty("区域id")
    private String areaId;

    @ApiModelProperty("区域")
    private String area;

}
