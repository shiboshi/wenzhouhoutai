package cn.exrick.xboot.modules.bussiness.putInWarehouseOrder.pojo;

import cn.exrick.xboot.base.XbootBaseEntity;
import cn.exrick.xboot.common.vo.CommonVO;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.Table;
import java.util.List;

/**
 * @author tqr
 */
@Data
@ApiModel(value = "入库单集合")
public class PutInWarehouseOrderVO extends CommonVO {

    private static final long serialVersionUID = 1L;

    private String putInWarehouseIds;

    private String id;

    private List<String> productNames;

    private String putInNumb;
}