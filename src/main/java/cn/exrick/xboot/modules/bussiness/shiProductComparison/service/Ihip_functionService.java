package cn.exrick.xboot.modules.bussiness.shiProductComparison.service;

import com.baomidou.mybatisplus.extension.service.IService;
import cn.exrick.xboot.modules.bussiness.shiProductComparison.pojo.hip_function;

import java.util.List;

/**
 * 臀部功能接口
 * @author 石博士
 */
public interface Ihip_functionService extends IService<hip_function> {

}