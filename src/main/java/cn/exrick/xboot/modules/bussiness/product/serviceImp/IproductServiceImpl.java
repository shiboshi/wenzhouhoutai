package cn.exrick.xboot.modules.bussiness.product.serviceImp;

import ch.qos.logback.core.net.SyslogConstants;
import cn.exrick.xboot.common.constant.CommonConstant;
import cn.exrick.xboot.common.utils.SecurityUtil;
import cn.exrick.xboot.common.utils.ToolUtil;
import cn.exrick.xboot.common.vo.Result;
import cn.exrick.xboot.modules.base.dao.mapper.UserMapper;
import cn.exrick.xboot.modules.bussiness.classification.mapper.ClassificationMapper;
import cn.exrick.xboot.modules.bussiness.classification.pojo.Classification;
import cn.exrick.xboot.modules.bussiness.product.VO.ProductVO;
import cn.exrick.xboot.modules.bussiness.product.mapper.productMapper;
import cn.exrick.xboot.modules.bussiness.product.mapper.productModelMapper;
import cn.exrick.xboot.modules.bussiness.product.pojo.Product;
import cn.exrick.xboot.modules.bussiness.product.pojo.ProductModel;
import cn.exrick.xboot.modules.bussiness.product.service.IproductService;
import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.bean.copier.CopyOptions;
import cn.hutool.core.bean.copier.ValueProvider;
import cn.hutool.core.date.DateUtil;
import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.core.toolkit.support.SFunction;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.lang.reflect.Type;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * 产品大类接口实现
 *
 * @author xiaofei
 */
@Slf4j
@Service
@Transactional
public class IproductServiceImpl extends ServiceImpl<productMapper, Product> implements IproductService {
    private final static Logger LOGGER = LoggerFactory.getLogger(IproductOrderServiceImpl.class);

    @Autowired
    private SecurityUtil securityUtil;
    @Autowired
    private ClassificationMapper classificationMapper;
    @Autowired
    private productMapper productMapper;
    @Autowired
    private UserMapper userMapper;

    @Override
    public List<ProductVO> getProdcutList(Product product) {

        Map<SFunction<Product, ?>, Object> map = new HashMap<>();
        map.put(Product::getProductName, product.getProductName());
        map.put(Product::getDelFlag, CommonConstant.NOTDELETE);
        List<Product> products = this.getBaseMapper().selectList(Wrappers.<Product>lambdaQuery()
                .allEq(true, map, false));

        boolean isBean = BeanUtil.isBean(ProductVO.class);
        if (isBean) {
            List<ProductVO> productVOS = products.stream().map(p -> {

                ProductVO productVO = BeanUtil.fillBean(new ProductVO(), new ValueProvider<String>() {
                    @Override
                    public Object value(String s, Type type) {
                        switch (s) {
                            case "id":
                                return p.getId();
                            case "productName":
                                return p.getProductName();
                            case "createBy":
                                return p.getCreateBy();
                            case "createTime":
                                return p.getCreateTime();
                            case "kindId":
                                return p.getKindId();
                        }
                        return null;
                    }

                    @Override
                    public boolean containsKey(String s) {
                        return true;
                    }
                }, CopyOptions.create());
                return productVO;
            }).collect(Collectors.toList());
            for (ProductVO pv : productVOS) {
                if (ToolUtil.isNotEmpty(pv.getCreateBy())) {
                    if (ToolUtil.isNotEmpty(userMapper.selectById(pv.getCreateBy()))) {
                        pv.setUserName(userMapper.selectById(pv.getCreateBy()).getUsername());
                    }
                }
            }
            return productVOS;
        } else {
            return null;
        }
    }

    @Override
    public void insertProduct(Product product) {
        product.setKindId(CommonConstant.TOP_PID);
        this.save(product);
    }

    @Override
    public ProductVO productDetail(String productId) {
        Product product = this.getById(productId);
        ProductVO productVO = new ProductVO().setProductName(product.getProductName())
                .setCreateBy(securityUtil.getCurrUser().getUsername())
                .setCreateTime(DateUtil.parse(DateUtil.today()));
        return productVO;
    }


    @Override
    public void deleteProduct(String productId) {
        if (StringUtils.isNotBlank(productId)) {
            this.removeById(productId);
        }
    }

    @Override
    public Result<Product> addProduct(Product product) {
        Result<Product> result = new Result<>();
        try {
            //添加产品型号
            Classification classification = new Classification();
            classification.setParentId("0");
            classification.setTitle(product.getProductName());
            classification.setIsParent(true);
            classification.setParentTitle("一级分类");
            classification.setCreateBy(securityUtil.getCurrUser().getId());
            classification.setCreateTime(new Date());
            classificationMapper.insert(classification);
            //添加产品
            product.setKindId(classificationMapper.selectOne(new QueryWrapper<Classification>().orderByDesc("create_time").last(" limit 1 ")).getId());
            product.setCreateBy(securityUtil.getCurrUser().getId());
            product.setCreateTime(new Date());
            productMapper.insert(product);
            result.setCode(200);
            result.setMessage("添加成功");
            result.setSuccess(true);
            result.setResult(product);
        } catch (Exception e) {
            LOGGER.error("添加失败：{}", e);
            result.setCode(500);
            result.setMessage("添加失败");
            result.setSuccess(false);
        }

        return result;
    }

    @Override
    public Result<Product> updateProduct(Product product) {
        Result<Product> result = new Result<>();
        try {
            //修改产品型号
            Classification classification = new Classification();
            classification.setTitle(product.getProductName());
            classification.setUpdateBy(securityUtil.getCurrUser().getId());
            classification.setUpdateTime(new Date());
            classification.setId(product.getKindId());
            classificationMapper.updateById(classification);
            //修改产品
            product.setUpdateBy(securityUtil.getCurrUser().getId());
            product.setUpdateTime(new Date());
            productMapper.updateById(product);
            result.setCode(200);
            result.setMessage("修改成功");
            result.setSuccess(true);
        } catch (Exception e) {
            result.setCode(500);
            result.setMessage("修改失败");
            result.setSuccess(false);
        }
        return result;
    }
}