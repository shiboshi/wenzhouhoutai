package cn.exrick.xboot.modules.bussiness.shiProductComparison.serviceImp;

import cn.exrick.xboot.modules.bussiness.shiProductComparison.mapper.additional_functionMapper;
import cn.exrick.xboot.modules.bussiness.shiProductComparison.pojo.additional_function;
import cn.exrick.xboot.modules.bussiness.shiProductComparison.service.Iadditional_functionService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

/**
 * 附加功能表接口实现
 * @author 石博士
 */
@Slf4j
@Service
@Transactional
public class Iadditional_functionServiceImpl extends ServiceImpl<additional_functionMapper, additional_function> implements Iadditional_functionService {

    @Autowired
    private additional_functionMapper additional_functionMapper;
}