package cn.exrick.xboot.common.enums;

import com.baomidou.mybatisplus.core.enums.IEnum;

import java.io.Serializable;

/**
 * @Author : xiaofei
 * @Date: 2019/8/10
 */
public enum Sql implements IEnum {

    Last("selectLast","获取最新的id");

    public String getMethodName() {
        return methodName;
    }

    public void setMethodName(String methodName) {
        this.methodName = methodName;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    Sql(String methodName, String desc) {
        this.methodName = methodName;
        this.desc = desc;
    }

    private String methodName;
    private String desc;

    @Override
    public Serializable getValue() {
        return this.methodName;
    }
}
