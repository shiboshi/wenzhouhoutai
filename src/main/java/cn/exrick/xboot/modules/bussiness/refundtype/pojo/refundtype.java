package cn.exrick.xboot.modules.bussiness.refundtype.pojo;

import cn.exrick.xboot.base.XbootBaseEntity;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModelProperty;
import io.swagger.annotations.ApiModel;
import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.Table;

/**
 * @author 石博士
 */
@Data
@Entity
@Table(name = "refundtype")
@TableName("refundtype")
@ApiModel(value = "退款类型")
public class refundtype extends XbootBaseEntity {

    private static final long serialVersionUID = 1L;


    /**
     * 退款类型
     */
    private String refundtypename;









}