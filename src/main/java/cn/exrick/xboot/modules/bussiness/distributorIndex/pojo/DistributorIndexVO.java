package cn.exrick.xboot.modules.bussiness.distributorIndex.pojo;

import cn.exrick.xboot.base.XbootBaseEntity;
import cn.exrick.xboot.common.vo.CommonVO;
import cn.exrick.xboot.modules.bussiness.monthlyIndicators.pojo.MonthlyIndicators;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.Table;

/**
 * @author tqr
 */
@Data
@ApiModel(value = "经销商指标")
public class DistributorIndexVO extends CommonVO {

    private String id;
    private String deptId;

    //部门名称
    private String deptName;

    private String monthlyId;

    private MonthlyIndicators monthlyIndicators;
}