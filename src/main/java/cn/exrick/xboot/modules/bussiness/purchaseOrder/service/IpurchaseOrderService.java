package cn.exrick.xboot.modules.bussiness.purchaseOrder.service;

import cn.exrick.xboot.common.enums.OrderType;
import cn.exrick.xboot.modules.bussiness.purchaseOrder.VO.PurchaseOrderVO;
import com.baomidou.mybatisplus.extension.service.IService;
import cn.exrick.xboot.modules.bussiness.purchaseOrder.pojo.PurchaseOrder;

import java.util.List;

/**
 * 采购单接口
 * @author xiaofei
 */
public interface IpurchaseOrderService extends IService<PurchaseOrder> {


    //1.先获取当前订单详情中，采购的id的集合
    List<String> getPurchaseIds(OrderType orderType,String id);

    //2.遍历每个采购单id，获取订单详情信息
    List<PurchaseOrderVO> getPurchaseList(OrderType orderType,String id);

    //3.掺入到采购单中，返回插入到数据中的字符串集合
    String getPurchaseIds(List<PurchaseOrder> purchaseOrders,String userId);


}