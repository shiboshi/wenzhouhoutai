package cn.exrick.xboot.modules.bussiness.color;

/**
 * 产品分类SQL
 *
 * @author zhou
 */
public class ColorProvider {
    /**
     * 产品产品分类SQL
     *
     * @param parentId 父级ID
     * @return
     */
    public String getColorSql(String parentId) {
        StringBuffer stringBuffer = new StringBuffer();
        stringBuffer.append("select * from t_color where parent_id = ")
                .append(parentId).append(" and del_flag = 0");
        return stringBuffer.toString();
    }

    public String getColor(String id) {
        String sql = "";
        String str = "";
        str += "SELECT c.title AS material," +
                "(SELECT title FROM t_color WHERE id = c.parent_id) AS colorNumber," +
                "(SELECT title  FROM t_color WHERE id = (SELECT parent_id FROM t_color WHERE id = c.parent_id) ) AS color" +
                " from t_color c where c.id = '" + id + "' and (c.del_flag = 0 or c.del_flag is null) ";
        sql += str;
        return sql;
    }
}
