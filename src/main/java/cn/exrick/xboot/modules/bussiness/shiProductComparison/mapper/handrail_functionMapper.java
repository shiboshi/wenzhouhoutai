package cn.exrick.xboot.modules.bussiness.shiProductComparison.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import cn.exrick.xboot.modules.bussiness.shiProductComparison.pojo.handrail_function;

import java.util.List;

/**
 * 臀部功能数据处理层
 * @author 石博士
 */
public interface handrail_functionMapper extends BaseMapper<handrail_function> {

}