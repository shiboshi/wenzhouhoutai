package cn.exrick.xboot.modules.bussiness.classification.mapper;

import cn.exrick.xboot.modules.bussiness.classification.ClassificationProvider;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import cn.exrick.xboot.modules.bussiness.classification.pojo.Classification;
import org.apache.ibatis.annotations.SelectProvider;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * 产品分类表数据处理层
 * @author tqr
 */
@Repository
public interface ClassificationMapper extends BaseMapper<Classification> {
    /**
     * 通过父id获取
     * @param parentId
     * @return
     */
    @SelectProvider(type = ClassificationProvider.class, method = "getClassificationSql")
    List<Classification> findByParentIdOrderBySortOrder(String parentId);

}