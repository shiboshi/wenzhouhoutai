package cn.exrick.xboot.modules.bussiness.inventoryOrder.controller;

import cn.exrick.xboot.common.utils.PageUtil;
import cn.exrick.xboot.common.utils.ResultUtil;
import cn.exrick.xboot.common.utils.ToolUtil;
import cn.exrick.xboot.common.vo.PageVo;
import cn.exrick.xboot.common.vo.Result;
import cn.exrick.xboot.modules.bussiness.inventoryOrder.pojo.InventoryOrderResult;
import cn.exrick.xboot.modules.bussiness.inventoryOrder.service.IInventoryOrderService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.transaction.annotation.Transactional;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author tqr
 */
@Slf4j
@RestController
@Api(description = "库存表管理接口")
@RequestMapping("/xboot/inventoryOrder")
@Transactional
public class InventoryOrderController {

    @Autowired
    private IInventoryOrderService iInventoryOrderService;
    /**
     * 查询序列号
     * @param page
     * @return
     */
    @RequestMapping(value = "/getAllByPage", method = RequestMethod.POST)
    @ApiOperation(value = "分页获取")
    public Result<Map<String, Object>> getAllByPage(@ModelAttribute PageVo page, String condition){
        if(ToolUtil.isEmpty(condition)){
            condition = "";
        }
        Map<String, Object> map = new HashMap<>();
        List<InventoryOrderResult> data = iInventoryOrderService.getByPage(condition);
        map.put("total",data.size());
        data = PageUtil.listToPage(page,data);
        map.put("data", data);
        return new ResultUtil<Map<String, Object>>().setData(map);
    }

    /**
     * 查询库存
     * @param page
     * @return
     */
    @RequestMapping(value = "/getInventoryOrder", method = RequestMethod.POST)
    @ApiOperation(value = "分页获取")
    public Result<Map<String, Object>> getInventoryOrder(@ModelAttribute PageVo page, String condition){
        if(ToolUtil.isEmpty(condition)){
            condition = "";
        }
        Map<String, Object> map = new HashMap<>();
        List<InventoryOrderResult> data = iInventoryOrderService.getInventoryOrder(condition);
        map.put("total",data.size());
        data = PageUtil.listToPage(page,data);
        map.put("data", data);
        return new ResultUtil<Map<String, Object>>().setData(map);
    }

    /**
     * 查询总仓
     * @param page
     * @return
     */
    @RequestMapping(value = "/getMainWarehouse", method = RequestMethod.POST)
    @ApiOperation(value = "分页获取")
    public Result<Map<String, Object>> getAllByPage(@ModelAttribute PageVo page, String condition,String id) {
        if (ToolUtil.isEmpty(condition)) {
            condition = "";
        }
        Map<String, Object> map = new HashMap<>();
        List<InventoryOrderResult> data = iInventoryOrderService.getMainWarehouse(condition, id);
        map.put("total", data.size());
        data = PageUtil.listToPage(page, data);
        map.put("data", data);
        return new ResultUtil<Map<String, Object>>().setData(map);
    }

    @RequestMapping(value = "/getDoor", method = RequestMethod.POST)
    @ApiOperation(value = "查询门店库存")
    public Result<Map<String, Object>> getDoor(@ModelAttribute PageVo page, String condition,String userId){
        if(ToolUtil.isEmpty(condition)){
            condition = "";
        }
        Map<String, Object> map = new HashMap<>();
        List<InventoryOrderResult> data = iInventoryOrderService.getDoor(condition,userId);
        map.put("total",data.size());
        data = PageUtil.listToPage(page,data);
        map.put("data", data);
        return new ResultUtil<Map<String, Object>>().setData(map);
    }
}
