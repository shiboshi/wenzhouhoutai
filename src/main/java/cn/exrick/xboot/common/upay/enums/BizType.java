package cn.exrick.xboot.common.upay.enums;

/**
 * Created by vectoryang on 2017/7/17.
 */
public enum BizType {
    UMSZJ_TRADE_OTAINIT       ("umszj.trade.otainit"    , "OTA初始化"),
    UMSZJ_TRADE_SIGN          ("umszj.trade.signin"     , "签到"),
    UMSZJ_TRADE_SIGN_OUT      ("umszj.trade.signout"    , "签退"),
    UMSZJ_TRADE_SETTLE        ("umszj.trade.settle"     , "结算"),
    UMSZJ_TRADE_AUTH          ("umszj.trade.auth"       , "商户授权"),
    UMSZJ_TRADE_PRINT_SETTLE  ("umszj.trade.printsettle", "结算单打印"),
    UMSZJ_TRADE_PAY           ("umszj.trade.pay"        , "条码支付"),
	UMSZJ_TRADE_PRECREATE     ("umszj.trade.precreate"  , "扫码支付"),
	UMSZJ_TRADE_CLOSE         ("umszj.trade.close"      , "关闭订单"),
	UMSZJ_TRADE_REFUND        ("umszj.trade.refund"     , "交易退款"),
	UMSZJ__TRADE_JSAPI        ("umszj.trade.jsapi"      , "微信公众号支付"),
	UMSZJ_TRADE_TRANSFER      ("umszj.trade.transfer"   , "银联类交易透传"),
	UMSZJ_TRADE_QUERY         ("umszj.trade.query"      , "交易查询"),
	UMSZJ_TRADE_WITHDRAW      ("umszj.trade.withdraw"   , "银行卡提现"),
	UMSZJ_TRADE_VERIQFY       ("umszj.trade.verify"     , "验证类交易"),
	UMSZJ_UNION_QUERY         ("umszj.trade.unionquery" , "银行卡查询");

    private String en;
    private String desc;

    BizType(String en, String desc) {
        this.en = en;
        this.desc = desc;
    }

    public String getEn() {
        return en;
    }

    public String getDesc() {
        return desc;
    }

}
