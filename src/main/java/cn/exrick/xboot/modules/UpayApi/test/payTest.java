package cn.exrick.xboot.modules.UpayApi.test;

import cn.exrick.xboot.modules.UpayApi.serverMethod.RequestMethod;
import com.alibaba.fastjson.JSON;
import org.apache.commons.lang3.time.DateFormatUtils;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

public class payTest {

    public static void main(){
        String url = "https://open.yeepay.com/yop-center/rest/v1.0/paperorder/unified/firstp";
        Map<String, String> paramsMap = new HashMap<>();
        paramsMap.put("merchantno","10000469946");
        paramsMap.put("requestno","201812251730000000000001");
        paramsMap.put("identityid","13797457271");
        paramsMap.put("identitytype","PHONE");
        paramsMap.put("cardno","6227002660090180177");
        paramsMap.put("idcardno","429006199210091939");
        paramsMap.put("idcardtype","ID");
        paramsMap.put("username","黄振");
        paramsMap.put("phone","13797457271");
        paramsMap.put("amount","0.01");
        paramsMap.put("authtype","COMMON_FOUR");
        paramsMap.put("issms","true");
        paramsMap.put("requesttime", DateFormatUtils.format(new Date(), "yyyy-MM-dd HH:mm:ss"));
        paramsMap.put("terminalno","SQKKSCENE10");
        String strReqJsonStr = JSON.toJSONString(paramsMap);
        RequestMethod.sendPost(url,strReqJsonStr);
    }
}
