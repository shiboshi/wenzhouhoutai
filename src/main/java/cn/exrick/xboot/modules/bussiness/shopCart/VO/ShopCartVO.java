package cn.exrick.xboot.modules.bussiness.shopCart.VO;

import lombok.Data;

import java.math.BigDecimal;
import java.util.Date;

@Data
public class ShopCartVO {

    private String id;
    private  String productId;
    private  String unitId;

    /**
     * 产品信息
     */
    private  String productName;
    private  String productModelName;
    private  String productOrderName;

    /**
     * 产品图片
     */
    private  String productPic;

    /**
     * 购买单位名称
     */
    private  String unitName;

    /**
     * 产品价格
     */
    private BigDecimal price;

    /**
     * 添加时间
     */
    private String addtime;

    /**
     * 购买数量
     */
    private Integer amount;

    /**
     *
     */
    private boolean checked;
    private Long amout;

    private BigDecimal salsPrice;

}
