package cn.exrick.xboot.modules.activiti.service.mybatis;

import cn.exrick.xboot.modules.activiti.dao.mapper.ActbussinessMapper;
import cn.exrick.xboot.modules.activiti.entity.ActBusiness;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * @Author : xiaofei
 * @Date: 2019/8/23
 */
@Service
public class IActbusinessService extends ServiceImpl<ActbussinessMapper,ActBusiness> {
}
