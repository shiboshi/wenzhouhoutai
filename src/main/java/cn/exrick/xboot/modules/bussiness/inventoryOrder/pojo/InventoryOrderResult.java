package cn.exrick.xboot.modules.bussiness.inventoryOrder.pojo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
@ApiModel("库存")
public class InventoryOrderResult {

    @ApiModelProperty("id")
    private String id;

    @ApiModelProperty("创建时间")
    private String createTime;

    /**
     * 数量
     */
    @ApiModelProperty("数量")
    private Long amount;

    /**
     * 批次
     */
    @ApiModelProperty("批次")
    private String batchNumber;

    /**
     * 仓库id
     */
    @ApiModelProperty("仓库id")
    private String warhouseId;

    /**
     * 序列号
     */
    @ApiModelProperty("序列号")
    private String serialNum;

    /**
     * 产品编号id
     */
    @ApiModelProperty("产品编号id")
    private String productOrderId;

    /**
     * 状态值1.未出库 2.已出库
     */
    @ApiModelProperty("状态值")
    private String  serialNumOut;
    /**
     * 仓库名
     */
    @ApiModelProperty("仓库名")
    private String warhouseName;

    /**
     * 产品品号
     */
    @ApiModelProperty("产品品号")
    private String productOrderName;

    /**
     *产品型号
     */
    @ApiModelProperty("产品型号")
    private String productModelName;

    /**
     * 产品大类
     */
    @ApiModelProperty("产品大类")
    private String productName;

    @ApiModelProperty("对内型号")
    private String modelNameIn;
}