package cn.exrick.xboot.modules.bussiness.channel.pojo;

import cn.exrick.xboot.base.XbootBaseEntity;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModelProperty;
import io.swagger.annotations.ApiModel;
import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.Table;

/**
 * @author tqr
 */
@Data
@Entity
@Table(name = "t_channel")
@TableName("t_channel")
@ApiModel(value = "渠道")
public class Channel extends XbootBaseEntity {

    private static final long serialVersionUID = 1L;

    /**
     * 渠道名称
     */
    private String channelName;

    /**
     * 渠道备注
     */
    private String channelRemark;
}