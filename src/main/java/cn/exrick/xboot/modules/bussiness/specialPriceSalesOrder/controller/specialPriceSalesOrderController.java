package cn.exrick.xboot.modules.bussiness.specialPriceSalesOrder.controller;

import cn.exrick.xboot.common.utils.PageUtil;
import cn.exrick.xboot.common.utils.ResultUtil;
import cn.exrick.xboot.common.utils.ToolUtil;
import cn.exrick.xboot.common.vo.PageVo;
import cn.exrick.xboot.common.vo.Result;
import cn.exrick.xboot.modules.bussiness.product.VO.ProductCommon;
import cn.exrick.xboot.modules.bussiness.product.service.IproductOrderService;
import cn.exrick.xboot.modules.bussiness.purchaseOrder.pojo.PurchaseOrder;
import cn.exrick.xboot.modules.bussiness.purchaseOrder.service.IpurchaseOrderService;
import cn.exrick.xboot.modules.bussiness.specialPriceSalesOrder.VO.SpecialPriceSalesOrderVO;
import cn.exrick.xboot.modules.bussiness.specialPriceSalesOrder.pojo.SpecialPriceSalesOrder;
import cn.exrick.xboot.modules.bussiness.specialPriceSalesOrder.service.IspecialPriceSalesOrderService;
import com.alibaba.fastjson.JSON;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.google.common.base.Splitter;
import com.google.common.collect.Maps;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * @author xiaofei
 */
@Slf4j
@RestController
@Api(description = "特价申请销售订单管理接口")
@RequestMapping("/xboot/specialPriceSalesOrder")
@Transactional
public class specialPriceSalesOrderController {

    @Autowired
    private IspecialPriceSalesOrderService ispecialPriceSalesOrderService;
    @Autowired
    private IpurchaseOrderService purchaseOrderService;
    @Autowired
    private IproductOrderService productOrderService;

    private static final Splitter SPLITTER = Splitter.on(",").trimResults().omitEmptyStrings();

    @RequestMapping(value = "/get/{id}", method = RequestMethod.GET)
    @ApiOperation(value = "通过id获取")
    public Result<SpecialPriceSalesOrder> get(@PathVariable String id) {

        SpecialPriceSalesOrder specialPriceSalesOrder = ispecialPriceSalesOrderService.getById(id);
        return new ResultUtil<SpecialPriceSalesOrder>().setData(specialPriceSalesOrder);
    }

    @RequestMapping(value = "/getAll", method = RequestMethod.GET)
    @ApiOperation(value = "获取全部数据")
    public Result<List<SpecialPriceSalesOrder>> getAll() {

        List<SpecialPriceSalesOrder> list = ispecialPriceSalesOrderService.list();
        return new ResultUtil<List<SpecialPriceSalesOrder>>().setData(list);
    }

    @RequestMapping(value = "/getByPage", method = RequestMethod.GET)
    @ApiOperation(value = "分页获取")
    public Result<IPage<SpecialPriceSalesOrder>> getByPage(@ModelAttribute PageVo page) {

        IPage<SpecialPriceSalesOrder> data = ispecialPriceSalesOrderService.page(PageUtil.initMpPage(page));
        return new ResultUtil<IPage<SpecialPriceSalesOrder>>().setData(data);
    }

    @RequestMapping(value = "/insertOrUpdate", method = RequestMethod.POST)
    @ApiOperation(value = "编辑或更新数据")
    public Result<SpecialPriceSalesOrder> saveOrUpdate(@ModelAttribute SpecialPriceSalesOrder specialPriceSalesOrder) {

        if (ispecialPriceSalesOrderService.saveOrUpdate(specialPriceSalesOrder)) {
            return new ResultUtil<SpecialPriceSalesOrder>().setData(specialPriceSalesOrder);
        }
        return new ResultUtil<SpecialPriceSalesOrder>().setErrorMsg("操作失败");
    }

    @RequestMapping(value = "/delByIds/{ids}", method = RequestMethod.DELETE)
    @ApiOperation(value = "批量通过id删除")
    public Result<Object> delAllByIds(@PathVariable String[] ids) {
        for (String id : ids) {
            ispecialPriceSalesOrderService.removeById(id);
        }
        return new ResultUtil<Object>().setSuccessMsg("批量通过id删除数据成功");
    }

    @RequestMapping(value = "/insertSecialPriceOrder", method = RequestMethod.POST)
    @ApiOperation(value = "编辑或更新数据")
    public Result<SpecialPriceSalesOrder> insertSecialPriceOrder(String params, String proDefId,String userId,String phone) {
        List<PurchaseOrder> purchaseOrders = JSON.parseArray(params, PurchaseOrder.class);
        try {
            ispecialPriceSalesOrderService.insertSecialPriceOrder(purchaseOrders, proDefId,userId,phone);
            return new ResultUtil<SpecialPriceSalesOrder>().setSuccessMsg("添加成功");
        } catch (Exception e) {
            return new ResultUtil<SpecialPriceSalesOrder>().setErrorMsg("操作失败");
        }

    }

    @PostMapping("/getList")
    public Result<IPage<SpecialPriceSalesOrderVO>> getList(@ModelAttribute PageVo page,String userId,String checkStatus) {

        IPage<SpecialPriceSalesOrderVO> data = ispecialPriceSalesOrderService.getList(PageUtil.initMpPage(page),userId,checkStatus);
        return new ResultUtil<IPage<SpecialPriceSalesOrderVO>>().setData(data);
    }

    @PostMapping("/getDetail")
    @ApiOperation(value = "通过id获取销售单详情")
    public Result<Map<String,Object>> getDetail(String id, @ModelAttribute PageVo pageVo){

        List<String> purchaseIds = SPLITTER.splitToList(ispecialPriceSalesOrderService.getById(id).getPurchaseOrderIds());
        List<SpecialDetailVo> saleOrderDetail = purchaseIds.stream().map(saleOderId ->{
            SpecialDetailVo specialDetailVo = new SpecialDetailVo();
            PurchaseOrder purchaseOrder = purchaseOrderService.getById(saleOderId);
            if (ToolUtil.isNotEmpty(purchaseOrder)){
                ProductCommon productCommon = productOrderService.getProductCommon(purchaseOrder.getProductId());
                ToolUtil.copyProperties(productCommon,specialDetailVo);
                ToolUtil.copyProperties(purchaseOrder, specialDetailVo);
                return specialDetailVo;
            }
            return null;
        }).collect(Collectors.toList());
        for(SpecialDetailVo s : saleOrderDetail){
            SpecialPriceSalesOrder specialPriceSalesOrder = ispecialPriceSalesOrderService.getById(id);
            s.setSpecialPriceSalesOrder(specialPriceSalesOrder);
        }
        Map<String,Object> map = Maps.newHashMap();
        map.put("data",PageUtil.listToPage(pageVo,saleOrderDetail));
        map.put("size",saleOrderDetail.size());
        return new ResultUtil<Map<String,Object>>().setData(map);
    }
}
