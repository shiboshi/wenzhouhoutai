package cn.exrick.xboot.modules.bussiness.applicationAfterSaleOrder.service;

import cn.exrick.xboot.common.enums.AfterSaleType;
import cn.exrick.xboot.common.vo.PageVo;
import cn.exrick.xboot.modules.bussiness.applicationAfterSaleOrder.VO.ApplicationAfterSaleOrderVO;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;
import cn.exrick.xboot.modules.bussiness.applicationAfterSaleOrder.pojo.ApplicationAfterSaleOrder;

import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeoutException;

/**
 * 申请售后单接口
 * @author xiaofei
 */
public interface IapplicationAfterSaleOrderService extends IService<ApplicationAfterSaleOrder> {

    /**
     * 提交售后申请
     * @param afterSaleType
     */
    void commitApplication(String inventoryIds, AfterSaleType afterSaleType,String procDefId);

    //获取售后申请列表
    Page<ApplicationAfterSaleOrderVO> getList(ApplicationAfterSaleOrderVO applicationAfterSaleOrderVO, PageVo pageVo,String userId,String checkStatus,String numb);
}