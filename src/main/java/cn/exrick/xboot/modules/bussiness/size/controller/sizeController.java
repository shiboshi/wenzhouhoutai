package cn.exrick.xboot.modules.bussiness.size.controller;

import cn.exrick.xboot.common.utils.PageUtil;
import cn.exrick.xboot.common.utils.ResultUtil;
import cn.exrick.xboot.common.vo.PageVo;
import cn.exrick.xboot.common.vo.Result;
import cn.exrick.xboot.modules.bussiness.size.pojo.size;
import cn.exrick.xboot.modules.bussiness.size.service.IsizeService;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * @author tqr
 */
@Slf4j
@RestController
@Api(description = "产品尺寸管理接口")
@RequestMapping("/xboot/size")
@Transactional
public class sizeController {

    @Autowired
    private IsizeService isizeService;

    @RequestMapping(value = "/get/{id}", method = RequestMethod.GET)
    @ApiOperation(value = "通过id获取")
    public Result<size> get(@PathVariable String id){

        size size = isizeService.getById(id);
        return new ResultUtil<size>().setData(size);
    }

    @RequestMapping(value = "/getAll", method = RequestMethod.GET)
    @ApiOperation(value = "获取全部数据")
    public Result<List<size>> getAll(){

        List<size> list = isizeService.list();
        return new ResultUtil<List<size>>().setData(list);
    }

    @RequestMapping(value = "/getByPage", method = RequestMethod.GET)
    @ApiOperation(value = "分页获取")
    public Result<IPage<size>> getByPage(@ModelAttribute PageVo page){

        IPage<size> data = isizeService.page(PageUtil.initMpPage(page));
        return new ResultUtil<IPage<size>>().setData(data);
    }

    @RequestMapping(value = "/insertOrUpdate", method = RequestMethod.POST)
    @ApiOperation(value = "编辑或更新数据")
    public Result<size> saveOrUpdate(@ModelAttribute size size){

        if(isizeService.saveOrUpdate(size)){
            return new ResultUtil<size>().setData(size);
        }
        return new ResultUtil<size>().setErrorMsg("操作失败");
    }

    @RequestMapping(value = "/delByIds/{ids}", method = RequestMethod.DELETE)
    @ApiOperation(value = "批量通过id删除")
    public Result<Object> delAllByIds(@PathVariable String[] ids){

        for(String id : ids){
            isizeService.removeById(id);
        }
        return new ResultUtil<Object>().setSuccessMsg("批量通过id删除数据成功");
    }
}
