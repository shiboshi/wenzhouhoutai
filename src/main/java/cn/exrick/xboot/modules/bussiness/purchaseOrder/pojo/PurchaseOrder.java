package cn.exrick.xboot.modules.bussiness.purchaseOrder.pojo;

import cn.exrick.xboot.base.XbootBaseEntity;
import cn.exrick.xboot.common.enums.IsComplete;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.Table;
import java.math.BigDecimal;

/**
 * @author xiaofei
 */
@Data
@Entity
@Table(name = "x_purchase_order")
@TableName("x_purchase_order")
@ApiModel(value = "采购单")
public class PurchaseOrder extends XbootBaseEntity {

    private static final long serialVersionUID = 1L;

    /**
     * 购买产品id
     */
    private String productId;

    /**
     * 购买数量
     */
    private BigDecimal orderNumbers;

    /**
     * 购买数量
     */
    private Integer afterNumbers = 0;

    /**
     * 产品单价
     */
    private  BigDecimal price;

    /**
     * 购买渠道
     */
    private  String channel;

    /**
     * 购买单位
     */
    private String unitId;

    /**
     * 购买包装尺寸
     */
    private String packageSizeId;

    /**
     * 总金额
     */
    private BigDecimal orderAmount;

    /**
     * 已发数量
     */
    private BigDecimal havaSend;

    /**
     * 未发数量
     */
    private BigDecimal notSend;

    /**
     * 备注
     */
    private String remark;
}