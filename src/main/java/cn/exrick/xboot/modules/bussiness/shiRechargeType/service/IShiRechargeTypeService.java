package cn.exrick.xboot.modules.bussiness.shiRechargeType.service;

import com.baomidou.mybatisplus.extension.service.IService;
import cn.exrick.xboot.modules.bussiness.shiRechargeType.pojo.ShiRechargeType;

import java.util.List;

/**
 * 充值分类接口
 * @author tqr
 */
public interface IShiRechargeTypeService extends IService<ShiRechargeType> {

}