package cn.exrick.xboot.modules.bussiness.applicationSendGoodsOrder.serviceImp;

import cn.exrick.xboot.common.constant.ActivitiConstant;
import cn.exrick.xboot.common.enums.*;
import cn.exrick.xboot.common.factory.ConstantFactory;
import cn.exrick.xboot.common.utils.CommonUtil;
import cn.exrick.xboot.common.utils.PageUtil;
import cn.exrick.xboot.common.utils.SecurityUtil;
import cn.exrick.xboot.common.utils.ToolUtil;
import cn.exrick.xboot.common.vo.PageVo;
import cn.exrick.xboot.modules.activiti.entity.ActBusiness;
import cn.exrick.xboot.modules.activiti.service.ActBusinessService;
import cn.exrick.xboot.modules.bussiness.appliSendNum.pojo.AppliSendNum;
import cn.exrick.xboot.modules.bussiness.appliSendNum.service.IappliSendNumService;
import cn.exrick.xboot.modules.bussiness.applicationSendGoodsOrder.VO.AppliParam;
import cn.exrick.xboot.modules.bussiness.applicationSendGoodsOrder.VO.AppliSendGoodsVO;
import cn.exrick.xboot.modules.bussiness.applicationSendGoodsOrder.VO.FilterVO;
import cn.exrick.xboot.modules.bussiness.applicationSendGoodsOrder.VO.SendDistinctVO;
import cn.exrick.xboot.modules.bussiness.applicationSendGoodsOrder.mapper.ApplicationSendGoodsOrderMapper;
import cn.exrick.xboot.modules.bussiness.applicationSendGoodsOrder.pojo.ApplicationSendGoodsOrder;
import cn.exrick.xboot.modules.bussiness.applicationSendGoodsOrder.service.IapplicationSendGoodsOrderService;
import cn.exrick.xboot.modules.bussiness.indentOrder.mapper.IndentOrderMapper;
import cn.exrick.xboot.modules.bussiness.indentOrder.pojo.IndentOrder;
import cn.exrick.xboot.modules.bussiness.indentOrder.service.IindentOrderService;
import cn.exrick.xboot.modules.bussiness.product.VO.ProductCommon;
import cn.exrick.xboot.modules.bussiness.product.service.IproductOrderService;
import cn.exrick.xboot.modules.bussiness.purchaseOrder.VO.PurchaseOrderVO;
import cn.exrick.xboot.modules.bussiness.purchaseOrder.VO.UpdatePurchaseOrderVO;
import cn.exrick.xboot.modules.bussiness.purchaseOrder.mapper.purchaseOrderMapper;
import cn.exrick.xboot.modules.bussiness.purchaseOrder.pojo.PurchaseOrder;
import cn.exrick.xboot.modules.bussiness.purchaseOrder.service.IpurchaseOrderService;
import cn.exrick.xboot.modules.bussiness.sendGoodsOrder.VO.SendGoodsVO;
import com.alibaba.druid.sql.dialect.mysql.visitor.transform.OrderByResolve;
import com.alibaba.fastjson.JSON;
import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.core.toolkit.support.SFunction;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.google.common.base.Joiner;
import com.google.common.base.Predicate;
import com.google.common.base.Splitter;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import lombok.extern.slf4j.Slf4j;
import org.bouncycastle.crypto.modes.gcm.Tables1kGCMExponentiator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.actuate.context.properties.ConfigurationPropertiesReportEndpoint;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Nullable;
import javax.annotation.Resource;
import javax.persistence.Id;
import java.math.BigDecimal;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.function.Function;
import java.util.jar.JarFile;
import java.util.stream.Collectors;

/**
 * 申请发货单接口实现
 * @author xiaofei
 */
@Slf4j
@Service
@Transactional
public class IapplicationSendGoodsOrderServiceImpl extends ServiceImpl<ApplicationSendGoodsOrderMapper, ApplicationSendGoodsOrder> implements IapplicationSendGoodsOrderService {

    @Resource
    private ApplicationSendGoodsOrderMapper applicationSendGoodsOrderMapper;
    @Autowired
    private SecurityUtil securityUtil;
    @Autowired
    private IpurchaseOrderService purchaseOrderService;
    @Autowired
    private ActBusinessService actBusinessService;
    @Autowired
    private IindentOrderService indentOrderService;
    @Autowired
    private IappliSendNumService appliSendNumService;
    @Autowired
    private IproductOrderService productOrderService;

    private static final Joiner JOINER = Joiner.on(",").skipNulls();
    private static final Splitter SPLITTER = Splitter.on(",").trimResults().omitEmptyStrings();

    @Override
    public Page<AppliSendGoodsVO> getAll(ApplicationSendGoodsOrder applicationSendGoodsOrder, PageVo pageVo,String userId,String checkStatus) {

        String user = ToolUtil.isNotEmpty(userId)? userId : securityUtil.getCurrUser().getId();
        Map<SFunction<ApplicationSendGoodsOrder, ?>, Object> map = Maps.newHashMap();
        Wrapper wrapper = null;
        map.put(ApplicationSendGoodsOrder::getOrderNum, applicationSendGoodsOrder.getOrderNum());
        if ("1".equals(checkStatus)) {
            map.put(ApplicationSendGoodsOrder::getCheckStatus, CheckStatus.pengindg.getCode());
        } else if ("2".equals(checkStatus)) {
            map.put(ApplicationSendGoodsOrder::getCheckStatus, CheckStatus.sucess.getCode());
        } else if ("3".equals(checkStatus)) {
            map.put(ApplicationSendGoodsOrder::getCheckStatus, CheckStatus.fail.getCode());
        }
        map.put(ApplicationSendGoodsOrder::getDelFlag,0);
        map.put(ApplicationSendGoodsOrder::getCreateBy,user);
        wrapper = Wrappers.<ApplicationSendGoodsOrder>lambdaQuery().allEq(true, map, false);
        Page lists = applicationSendGoodsOrderMapper.getAllList(PageUtil.initMpPage(pageVo), wrapper);

        List<AppliSendGoodsVO> appliSendGoodsVOS = lists.getRecords();
        appliSendGoodsVOS.forEach(s -> {
            s.setUserName(ConstantFactory.me().getUserName(s.getCreateBy()));
            s.setUserDept(ConstantFactory.me().getUserDeptName(s.getCreateBy()));
            s.setUserLevel(ConstantFactory.me().getUserLevelName(s.getCreateBy()));
        });
        lists.setRecords(appliSendGoodsVOS);
        return lists;
    }

    @Override
    public List<SendGoodsVO> getSendGoodOrderList(String applicationSendGoodsId){
        return applicationSendGoodsOrderMapper.getSendGoodOrderList(applicationSendGoodsId);
    }

    @Override
    public List<PurchaseOrderVO> getDetail(String id) {
        List<PurchaseOrderVO> purchaseOrderVOS = purchaseOrderService.getPurchaseList(OrderType.applicationOrder,id);
        return purchaseOrderVOS;
    }


    @Override
    @Transactional
    public ActBusiness insertOrder(AppliParam appliParam) {
        List<UpdatePurchaseOrderVO> updatePurchaseOrderVOS = Lists.newArrayList();
        if (ToolUtil.isNotEmpty(appliParam.getParam())) {
             updatePurchaseOrderVOS = JSON.parseArray(appliParam.getParam(), UpdatePurchaseOrderVO.class);
        }
        String userId = ToolUtil.isNotEmpty(appliParam.getUserId()) ? appliParam.getUserId() : securityUtil.getCurrUser().getId();

        //1.插入申请发货单
        ApplicationSendGoodsOrder applicationSendGoodsOrder = new ApplicationSendGoodsOrder();
        ToolUtil.copyProperties(appliParam, applicationSendGoodsOrder);
        applicationSendGoodsOrder.setCreateBy(userId);
        if (ToolUtil.isNotEmpty(updatePurchaseOrderVOS)) {
            List<String> purchaseId = updatePurchaseOrderVOS.stream().map(UpdatePurchaseOrderVO::getId).collect(Collectors.toList());
            applicationSendGoodsOrder.setPurchaseOrderIds(JOINER.join(purchaseId));
        }
        applicationSendGoodsOrder.setCheckStatus(CheckStatus.pengindg);
        applicationSendGoodsOrder.setSendStatus(SendStatus.PENDING);
        applicationSendGoodsOrder.setOrderNum(CommonUtil.createOrderSerial(Order.SQ_));
        Optional.ofNullable(appliParam.getAddress()).ifPresent(address -> applicationSendGoodsOrder.setAddress(address));
        this.save(applicationSendGoodsOrder);

        //获取最新的一条记录
        ApplicationSendGoodsOrder applicationSendGoodsOrderNew = this.getOne(Wrappers.<ApplicationSendGoodsOrder>lambdaQuery()
        .orderByDesc(ApplicationSendGoodsOrder::getId)
        .last("limit 1"));

        //2.插入发货数量单
        updatePurchaseOrderVOS.forEach(Order -> {
            appliSendNumService.insert(applicationSendGoodsOrderNew.getId(), Order.getId(), Order.getDispatchNum());
        });

        //3.插入到流程审核单中
        ActBusiness actBusiness = new ActBusiness();
        actBusiness.setTableId(applicationSendGoodsOrderNew.getId());
        actBusiness.setProcDefId(appliParam.getProcDefId());
        actBusiness.setUserId(userId);
        ActBusiness actBusinessIns = actBusinessService.save(actBusiness);

        //4.更新申请发货单
        applicationSendGoodsOrderNew.setBussinessId(actBusinessIns.getId());
        this.updateById(applicationSendGoodsOrderNew);

        updateIndentOrderIsComplete(appliParam.getIndentOrderId());
        return actBusinessIns;
    }



    /**
     * 查询当前订单的采购商品是否已经发完，如果发完默认订单已经完成
     * @param id
     */
    public void  updateIndentOrderIsComplete(String id){
      if (ToolUtil.isNotEmpty(id)){
          IndentOrder indentOrder = indentOrderService.getById(id);
          if (ToolUtil.isNotEmpty(indentOrder)) {
              List<PurchaseOrder> purchaseOrders = purchaseOrderService.list(Wrappers.<PurchaseOrder>lambdaQuery()
                      .in(PurchaseOrder::getId,Lists.newArrayList(SPLITTER.split(indentOrder.getPurchaseOrderIds()))));
              List<PurchaseOrder> purchaseOrdersIsNotComplete = purchaseOrders.stream().filter(order -> order.getNotSend().intValue() != 0).collect(Collectors.toList());
              if (purchaseOrdersIsNotComplete.size() == 0L){
                  indentOrder.setIsComplete(IsComplete.IS_COMPLETE);
                  indentOrderService.updateById(indentOrder);
              }
          }
      }
    }

    @Override
    public List<ApplicationSendGoodsOrder> get() {
        List<ApplicationSendGoodsOrder> list = applicationSendGoodsOrderMapper.get();
        return list;
    }

    @Override
    public List<FilterVO> filterCanNUm(AppliParam appliParam) {

        //1.找到当前订货单的所有启用的申请发货单
        //2.统计申请发货单每个采购商品的发货数量
        String indentOrderId = appliParam.getIndentOrderId();
        List<ApplicationSendGoodsOrder> applicationSendGoodsOrders = this.list(Wrappers.<ApplicationSendGoodsOrder>lambdaQuery()
                .eq(ApplicationSendGoodsOrder::getIndentOrderId, indentOrderId));

        List<ApplicationSendGoodsOrder> applicationSendGoodsOrderList = applicationSendGoodsOrders.stream().filter(Order -> actBusinessService.get(Order.getBussinessId())
                .getStatus() == ActivitiConstant.PROCESS_STATUS_ACTIVE)
                .collect(Collectors.toList());

        List<AppliSendNum> appliSendNums = Lists.newArrayList();
        if (ToolUtil.isNotEmpty(applicationSendGoodsOrderList)) {
            applicationSendGoodsOrders.stream().forEach(Order -> {
                List<AppliSendNum> appliSendNumsSon = appliSendNumService.list(Wrappers.<AppliSendNum>lambdaQuery()
                        .eq(AppliSendNum::getApplicationSendOrderId, Order.getId()));
                appliSendNums.addAll(appliSendNumsSon);
            });
            //统计还在流程中的每种采购单的发货数量
           Map<String,List<AppliSendNum>> mapfilter = appliSendNums.stream().collect(Collectors
           .groupingBy(AppliSendNum::getPurchaseId));
          List<AppliSendNum> appliSendNumsCount = Lists.newArrayList();
          for (Map.Entry<String,List<AppliSendNum>> map:mapfilter.entrySet()){
              AppliSendNum appliSendNum = new AppliSendNum();
              appliSendNum.setPurchaseId(map.getKey());
              appliSendNum.setSendNum(map.getValue().stream().mapToLong(AppliSendNum::getSendNum)
              .sum());
              appliSendNumsCount.add(appliSendNum);
          }


            List<UpdatePurchaseOrderVO> purchaseOrdersParam = JSON.parseArray(appliParam.getParam(),UpdatePurchaseOrderVO.class);
            List<UpdatePurchaseOrderVO> purchaseOrders = purchaseOrdersParam.stream().filter(Order -> filterPurchase(Order, appliSendNumsCount) == true)
                    .collect(Collectors.toList());
            List<FilterVO> filterVOS = null;
            if (ToolUtil.isNotEmpty(purchaseOrders)){
                filterVOS = purchaseOrders.stream().map(Order -> {
                    FilterVO filterVO = new FilterVO();
                    ProductCommon productCommon = productOrderService.getProductCommon(purchaseOrderService
                            .getById(Order.getId()).getProductId());
                    if (ToolUtil.isNotEmpty(productCommon)){
                        ToolUtil.copyProperties(productCommon, filterVO);
                    }
                    filterVO.setDisPathNum(Order.getDispatchNum());
                    return filterVO;
                }).collect(Collectors.toList());
            }
            return filterVOS;

        }
        return null;
    }

    @Override
    public AppliSendGoodsVO selectById(String id) {
        ApplicationSendGoodsOrder appliSendGoods = this.getById(id);
        AppliSendGoodsVO appliSendGoodsVO = new AppliSendGoodsVO();
        ToolUtil.copyProperties(appliSendGoods, appliSendGoodsVO);
        String userId = null;
        if (ToolUtil.isNotEmpty(appliSendGoods.getCreateBy())) {
            userId = appliSendGoods.getCreateBy();
        }
        String uId = userId;
        appliSendGoodsVO.setUserName(ConstantFactory.me().getUserName(uId));
        appliSendGoodsVO.setUserDept(ConstantFactory.me().getUserDeptName(uId));
        appliSendGoodsVO.setUserLevel(ConstantFactory.me().getUserLevelName(uId));

        return appliSendGoodsVO;
    }


    public boolean filterPurchase(UpdatePurchaseOrderVO updatePurchaseOrderVO,List<AppliSendNum> appliSendNums){
        //未发数量 和流程数量+申请发货数量 比较
        //1.求出当前id的的未发数量
        PurchaseOrder purchaseOrder = purchaseOrderService.getById(updatePurchaseOrderVO.getId());
        //2.求出当前id的流程数量
        List<AppliSendNum> appliSendNum = appliSendNums.stream().filter(Number -> Number.getPurchaseId().equals(updatePurchaseOrderVO.getId())).collect(Collectors.toList());
        if (ToolUtil.isNotEmpty(appliSendNum)){
            if (purchaseOrder.getNotSend().compareTo(new BigDecimal(appliSendNum.get(0).getSendNum())
            .add(new BigDecimal(updatePurchaseOrderVO.getDispatchNum()))) == -1){
                return true;
            }
        }else {
            if (purchaseOrder.getNotSend().compareTo(new BigDecimal(updatePurchaseOrderVO.getDispatchNum())) == -1){
                return  true;
            }
        }
     return  false;
    }



}


