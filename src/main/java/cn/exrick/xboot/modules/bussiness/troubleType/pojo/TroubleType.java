package cn.exrick.xboot.modules.bussiness.troubleType.pojo;

import cn.exrick.xboot.base.XbootBaseEntity;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModelProperty;
import io.swagger.annotations.ApiModel;
import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

/**
 * @author tqr
 */
@Data
@Entity
@Table(name = "t_trouble_type")
@TableName("t_trouble_type")
@ApiModel(value = "问题类型表")
public class TroubleType extends XbootBaseEntity {

    private static final long serialVersionUID = 1L;

    /**
     * 问题类型
     */
    @Column(length = 60)
    private String troubleType;
}