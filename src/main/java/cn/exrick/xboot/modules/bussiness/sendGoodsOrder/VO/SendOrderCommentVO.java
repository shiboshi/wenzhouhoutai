package cn.exrick.xboot.modules.bussiness.sendGoodsOrder.VO;

import lombok.Data;
import org.hibernate.procedure.spi.ParameterRegistrationImplementor;

/**
 * @Author : xiaofei
 * @Date: 2019/8/26
 */
@Data
public class SendOrderCommentVO {

    private String id;
    private String common;
    private String userId;
}
