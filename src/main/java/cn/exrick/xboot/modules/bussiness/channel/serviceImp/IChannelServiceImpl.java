package cn.exrick.xboot.modules.bussiness.channel.serviceImp;

import cn.exrick.xboot.modules.bussiness.channel.VO.ChannelVO;
import cn.exrick.xboot.modules.bussiness.channel.mapper.ChannelMapper;
import cn.exrick.xboot.modules.bussiness.channel.pojo.Channel;
import cn.exrick.xboot.modules.bussiness.channel.service.IChannelService;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

/**
 * 渠道接口实现
 * @author tqr
 */
@Slf4j
@Service
@Transactional
public class IChannelServiceImpl extends ServiceImpl<ChannelMapper, Channel> implements IChannelService {

    @Autowired
    private ChannelMapper channelMapper;

    @Override
    public IPage<ChannelVO> getByPage(Page initMpPage, String channelName) {
        IPage<ChannelVO> list = channelMapper.selectPage(initMpPage,new QueryWrapper<Channel>().lambda().like(Channel::getChannelName,channelName));
        return list;
    }
}