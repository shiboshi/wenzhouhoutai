package cn.exrick.xboot.modules.bussiness.shiProductComparison.serviceImp;

import cn.exrick.xboot.modules.bussiness.shiProductComparison.mapper.shoulder_functionMapper;
import cn.exrick.xboot.modules.bussiness.shiProductComparison.pojo.shoulder_function;
import cn.exrick.xboot.modules.bussiness.shiProductComparison.service.Ishoulder_functionService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

/**
 * 肩部功能接口实现
 * @author 石博士
 */
@Slf4j
@Service
@Transactional
public class Ishoulder_functionServiceImpl extends ServiceImpl<shoulder_functionMapper, shoulder_function> implements Ishoulder_functionService {

    @Autowired
    private shoulder_functionMapper shoulder_functionMapper;
}