package cn.exrick.xboot.common.enums;

import com.baomidou.mybatisplus.core.enums.IEnum;
import lombok.Data;

import java.io.Serializable;

/**
 * @Author : xiaofei
 * @Date: 2019/8/10
 */
public enum OrderType implements IEnum {

    indentOrder(1, "订货单详情"),
    applicationOrder(2, "申请发货单详情"),
    applicationReturnOrder(5, "申请退货单详情"),
    salsOrder(3, "销售订单详情"),
    specialSaleOrder(4,"特价销售单详情");

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    OrderType(Integer type, String desc) {
        this.type = type;
        this.desc = desc;
    }

    private Integer type;
    private String desc;


    @Override
    public Serializable getValue() {
        return null;
    }
}
