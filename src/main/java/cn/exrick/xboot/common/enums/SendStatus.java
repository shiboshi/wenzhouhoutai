package cn.exrick.xboot.common.enums;

import com.baomidou.mybatisplus.annotation.EnumValue;
import com.fasterxml.jackson.annotation.JsonValue;

/**
 * @Author : xiaofei
 * @Date: 2019/8/17
 */
public enum SendStatus {
    PENDING(1,"待发货"),
    DOING(2,"发货完成");

    @EnumValue
    private final int cdoe;
    private String desc;

    public int getCdoe() {
        return cdoe;
    }

    @JsonValue
    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    SendStatus(int cdoe, String desc) {
        this.cdoe = cdoe;
        this.desc = desc;
    }
}
