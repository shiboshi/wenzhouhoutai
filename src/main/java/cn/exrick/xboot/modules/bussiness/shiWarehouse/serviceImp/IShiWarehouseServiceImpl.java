package cn.exrick.xboot.modules.bussiness.shiWarehouse.serviceImp;

import cn.exrick.xboot.common.exception.XbootException;
import cn.exrick.xboot.common.utils.CommonUtil;
import cn.exrick.xboot.common.utils.SecurityUtil;
import cn.exrick.xboot.common.utils.ToolUtil;
import cn.exrick.xboot.common.vo.Result;
import cn.exrick.xboot.modules.base.dao.mapper.DepartmentMapper;
import cn.exrick.xboot.modules.base.entity.Department;
import cn.exrick.xboot.modules.base.entity.Role;
import cn.exrick.xboot.modules.base.entity.User;
import cn.exrick.xboot.modules.bussiness.dealerLevel.mapper.DealerLevelMapper;
import cn.exrick.xboot.modules.bussiness.inventoryOrder.pojo.InventoryOrderResult;
import cn.exrick.xboot.modules.bussiness.shiWarehouse.WarehouseVO.WarehouseVO;
import cn.exrick.xboot.modules.bussiness.shiWarehouse.mapper.ShiWarehouseMapper;
import cn.exrick.xboot.modules.bussiness.shiWarehouse.pojo.ShiWarehouse;
import cn.exrick.xboot.modules.bussiness.shiWarehouse.service.IShiWarehouseService;
import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.*;
import java.util.stream.Collectors;

/**
 * 仓库表接口实现
 *
 * @author tqr
 */
@Slf4j
@Service
@Transactional
public class IShiWarehouseServiceImpl extends ServiceImpl<ShiWarehouseMapper, ShiWarehouse> implements IShiWarehouseService {

    @Autowired
    private ShiWarehouseMapper shiWarehouseMapper;
    @Autowired
    private SecurityUtil securityUtil;
    @Autowired
    private DepartmentMapper departmentMapper;
    @Autowired
    private DealerLevelMapper dealerLevelMapper;

    @Override
    public boolean insertWarehouse(ShiWarehouse shiWarehouse) {

        try {
            ShiWarehouse warehouse = shiWarehouseMapper.selectOne(new QueryWrapper<ShiWarehouse>().orderByDesc("create_time").last("limit 1"));
            if (warehouse != null) {
                int endLength = warehouse.getSerialNum().length();
                String str = warehouse.getSerialNum().substring(4, endLength);
                shiWarehouse.setSerialNum(warehouse.getSerialNum().substring(0, 4).concat((Integer.parseInt(str) + 1) + ""));
            } else {
                shiWarehouse.setSerialNum("CK000");
            }
            shiWarehouse.setCreateBy(securityUtil.getCurrUser().getId());
            shiWarehouse.setCreateTime(new Date());
            shiWarehouse.setDelFlag(0);
            shiWarehouseMapper.insert(shiWarehouse);
            return true;
        } catch (NumberFormatException e) {
            e.printStackTrace();
        }
        return false;
    }

    @Override
    public List<WarehouseVO> getWarehouse( String name) {
        if(ToolUtil.isNotEmpty(securityUtil.getCurrUser().getRoles())){
            for(Role role : securityUtil.getCurrUser().getRoles()){
                if("ROLE_ADMIN".equals(role.getName())){
                    String deptId = null;
                    //如果为管理员查看所有库存
                    List<WarehouseVO> list = shiWarehouseMapper.getWarehouse(deptId);
                    if (ToolUtil.isNotEmpty(list) && ToolUtil.isNotEmpty(name)) {
                        list = list
                                .stream()
                                .filter(s -> (s.getWName().contains(name)))
                                .collect(Collectors.toList());
                    }
                    return list;
                }
            }
        }
        String deptId = securityUtil.getCurrUser().getDepartmentId();
        if (ToolUtil.isNotEmpty(deptId)) {
            List<WarehouseVO> list = getRecursion(deptId);
            list = list.stream()
                    .collect(Collectors
                            .collectingAndThen
                                    (Collectors.toCollection(() -> new TreeSet<WarehouseVO>(Comparator.comparing(t -> t.getId()))), ArrayList::new)
                    );
            if (ToolUtil.isNotEmpty(list) && ToolUtil.isNotEmpty(name)) {
                list = list
                        .stream()
                        .filter(s -> (s.getWName().contains(name)))
                        .collect(Collectors.toList());
            }
            return list;
        }
        return null;
    }

    public List<WarehouseVO> getRecursion(String deptId) {
        //查询该部门所拥有的仓库
        List<WarehouseVO> list = shiWarehouseMapper.getWarehouse(deptId);
        //查询该部门子集部门
        List<String> ids = departmentMapper.selectList(new QueryWrapper<Department>().lambda().eq(Department::getParentId, deptId))
                .stream()
                .map(department -> department.getId())
                .collect(Collectors.toList());
        if (ids.size() > 0) {
            for (String s : ids) {
                //查询该部门所拥有的仓库
                List<WarehouseVO> list1 = shiWarehouseMapper.getWarehouse(s);
                list.addAll(list1);
                List<WarehouseVO> list2 = getRecursion(s);
                list.addAll(list2);
            }

        }
        return list;
    }

    @Override
    public Result<ShiWarehouse> editWarehouse(ShiWarehouse warehouse) {
        Result<ShiWarehouse> data = null;
        try {
            data = new Result<>();
            shiWarehouseMapper.updateById(warehouse);
            data.setSuccess(true);
            data.setMessage("修改成功");
            data.setCode(200);
        } catch (Exception e) {
            data.setSuccess(false);
            data.setMessage("修改失败");
            data.setCode(500);
        }
        return data;
    }

    @Override
    public Result<List<ShiWarehouse>> getWarehouseByDept() {
        Result<List<ShiWarehouse>> data = new Result<>();
        List<ShiWarehouse> list = shiWarehouseMapper.selectList(new QueryWrapper<>());
        if(ToolUtil.isNotEmpty(list)){
            list = list.stream().filter(l->departmentMapper.selectById(l.getDeptId())!=null).collect(Collectors.toList());
        }
        data.setMessage("查询成功");
        data.setSuccess(true);
        data.setResult(list);
        data.setCode(200);
        return data;
    }

    @Override
    public List<WarehouseVO> getTransferWarehouse() {
        if(ToolUtil.isNotEmpty(securityUtil.getCurrUser().getRoles())){
            for(Role role : securityUtil.getCurrUser().getRoles()){
                if("ROLE_ADMIN".equals(role.getName())){
                    String deptId = null;
                    //如果为管理员查看所有库存
                    List<WarehouseVO> list = shiWarehouseMapper.getWarehouse(deptId);
                    return list;
                }
            }
        }
        String deptId = securityUtil.getCurrUser().getDepartmentId();
        if (!ToolUtil.isNotEmpty(deptId)) {
            List<WarehouseVO> list = getRecursions(deptId);
            list = list.stream()
                    .collect(Collectors
                            .collectingAndThen
                                    (Collectors.toCollection(() -> new TreeSet<>(Comparator.comparing(t -> t.getId()))), ArrayList::new)
                    );

            return list;
        }
        return null;
    }

    public List<WarehouseVO> getRecursions(String deptId) {
        List<WarehouseVO> list = new ArrayList<>();
        //查询所有经销商
        List<String> deptIds = dealerLevelMapper.selectList(new QueryWrapper<>()).stream().map(d->d.getDeptId()).collect(Collectors.toList());
        if(deptIds.contains(deptId)){
            //查询该部门所拥有的仓库
            list = shiWarehouseMapper.getWarehouse(deptId);
        }
        //查询该部门子集部门
        List<String> ids = departmentMapper.selectList(new QueryWrapper<Department>().lambda().eq(Department::getParentId, deptId))
                .stream()
                .map(department -> department.getId())
                .collect(Collectors.toList());
        if (ids.size() > 0) {
            for (String s : ids) {
                //查询该部门所拥有的仓库
                List<WarehouseVO> list1 = shiWarehouseMapper.getWarehouse(s);
                list.addAll(list1);
                List<WarehouseVO> list2 = getRecursion(s);
                list.addAll(list2);
            }

        }
        return list;
    }
}