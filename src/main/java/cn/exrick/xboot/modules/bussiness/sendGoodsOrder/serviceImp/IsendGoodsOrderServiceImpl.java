package cn.exrick.xboot.modules.bussiness.sendGoodsOrder.serviceImp;

import cn.exrick.xboot.common.enums.IsComplete;
import cn.exrick.xboot.common.enums.Order;
import cn.exrick.xboot.common.factory.ConstantFactory;
import cn.exrick.xboot.common.utils.CommonUtil;
import cn.exrick.xboot.common.utils.SecurityUtil;
import cn.exrick.xboot.common.utils.ToolUtil;
import cn.exrick.xboot.common.vo.CommonVO;
import cn.exrick.xboot.modules.activiti.entity.ActBusiness;
import cn.exrick.xboot.modules.activiti.service.ActBusinessService;
import cn.exrick.xboot.modules.activiti.service.ActProcessService;
import cn.exrick.xboot.modules.base.dao.mapper.UserMapper;
import cn.exrick.xboot.modules.base.entity.User;
import cn.exrick.xboot.modules.bussiness.afterIndentOrder.mapper.afterIndentOrderMapper;
import cn.exrick.xboot.modules.bussiness.afterIndentOrder.pojo.AfterIndentOrder;
import cn.exrick.xboot.modules.bussiness.appliSendNum.pojo.AppliSendNum;
import cn.exrick.xboot.modules.bussiness.appliSendNum.service.IappliSendNumService;
import cn.exrick.xboot.modules.bussiness.applicationAfterSaleOrder.mapper.applicationAfterSaleOrderMapper;
import cn.exrick.xboot.modules.bussiness.applicationAfterSaleOrder.pojo.ApplicationAfterSaleOrder;
import cn.exrick.xboot.modules.bussiness.applicationSendGoodsOrder.pojo.ApplicationSendGoodsOrder;
import cn.exrick.xboot.modules.bussiness.applicationSendGoodsOrder.service.IapplicationSendGoodsOrderService;
import cn.exrick.xboot.modules.bussiness.inventoryOrder.VO.InventoryVO;
import cn.exrick.xboot.modules.bussiness.inventoryOrder.pojo.InventoryOrder;
import cn.exrick.xboot.modules.bussiness.inventoryOrder.service.IInventoryOrderService;
import cn.exrick.xboot.modules.bussiness.purchaseOrder.pojo.PurchaseOrder;
import cn.exrick.xboot.modules.bussiness.purchaseOrder.service.IpurchaseOrderService;
import cn.exrick.xboot.modules.bussiness.sendGoodsOrder.VO.SendGoodsVO;
import cn.exrick.xboot.modules.bussiness.sendGoodsOrder.VO.TaskSendGoodVO;
import cn.exrick.xboot.modules.bussiness.sendGoodsOrder.mapper.sendGoodsOrderMapper;
import cn.exrick.xboot.modules.bussiness.sendGoodsOrder.pojo.SendGoodsOrder;
import cn.exrick.xboot.modules.bussiness.sendGoodsOrder.service.IsendGoodsOrderService;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.google.common.base.Joiner;
import com.google.common.base.Splitter;
import lombok.extern.slf4j.Slf4j;
import org.activiti.engine.RuntimeService;
import org.activiti.engine.TaskService;
import org.activiti.engine.runtime.ProcessInstance;
import org.activiti.engine.task.Task;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.util.*;
import java.util.stream.Collectors;

/**
 * 发货单接口实现
 *
 * @author xiaofei
 */
@Slf4j
@Service
@Transactional
public class IsendGoodsOrderServiceImpl extends ServiceImpl<sendGoodsOrderMapper, SendGoodsOrder> implements IsendGoodsOrderService {
    @Autowired
    private sendGoodsOrderMapper sendGoodsOrderMapper;
    @Autowired
    private IInventoryOrderService inventoryOrderService;
    @Autowired
    private SecurityUtil securityUtil;
    @Autowired
    private UserMapper userMapper;
    @Autowired
    private TaskService taskService;
    @Autowired
    private RuntimeService runtimeService;
    @Autowired
    private ActProcessService actProcessService;
    @Autowired
    private ActBusinessService actBusinessService;
    @Autowired
    private IapplicationSendGoodsOrderService applicationSendGoodsOrderService;
    @Autowired
    private IappliSendNumService appliSendNumService;
    @Autowired
    private IpurchaseOrderService purchaseOrderService;
    @Autowired
    private afterIndentOrderMapper afterIndentOrderMapper1;
    @Autowired
    private applicationAfterSaleOrderMapper applicationAfterSaleOrderMapper1;

    private Splitter splitter = Splitter.on("//").trimResults().omitEmptyStrings();
    private Splitter sp = Splitter.on(",").trimResults().omitEmptyStrings();
    private Joiner joiner = Joiner.on(",").skipNulls();

    @Override
    public Page<SendGoodsVO> getSendGoodsOrderByUser(Page initMpPage, String orderNum, String userId, String code,Integer type,String serial) {
        if (ToolUtil.isEmpty(userId)) {
            userId = securityUtil.getCurrUser().getId();
        }
        Page<SendGoodsVO> sendGoodsOrderPage = sendGoodsOrderMapper.getSendGoodsOrder(initMpPage, orderNum, userId, code,type,serial);
        List<SendGoodsVO> sendGoodsVOS = sendGoodsOrderPage.getRecords();

        for (SendGoodsVO sendGoodsVO : sendGoodsVOS) {
            ApplicationSendGoodsOrder applicationSendGoodsOrder = applicationSendGoodsOrderService.getById(sendGoodsVO.getApplicationId());
            if (ToolUtil.isNotEmpty(applicationSendGoodsOrder)) {
                String createBy = applicationSendGoodsOrder.getCreateBy();
                sendGoodsVO.setDeptName(ConstantFactory.me().getUserDeptName(createBy));
                sendGoodsVO.setUserLevel(ConstantFactory.me().getUserLevelName(createBy));
                sendGoodsVO.setUserName(ConstantFactory.me().getUserName(createBy));
            }
        }
        sendGoodsOrderPage.setRecords(sendGoodsVOS);
        return sendGoodsOrderPage;
    }

   /* @Override
    public List<SendGoodsVO>  getGoodOrderListByApplicationId(String applicationSendGoodsId){
        List<SendGoodsVO> sendGoodsOrders = sendGoodsOrderMapper.selectList();

        *//*Page<SendGoodsVO> sendGoodsOrderPage =sendGoodsOrderMapper.getGoodOrderListByApplicationId(applicationSendGoodsId);
        new QueryWrapper<SendGoodsOrder>().lambda().eq(SendGoodsOrder::getSaleOrderId,id*//*
        return sendGoodsOrders;
    }*/

    @Override
    public SendGoodsVO selectById(String id) {
        SendGoodsVO goodsVO = sendGoodsOrderMapper.getById(id);
        goodsVO.setDeptName(ConstantFactory.me().getUserDeptName(goodsVO.getCreateBy()));
        goodsVO.setUserLevel(ConstantFactory.me().getUserLevelName(goodsVO.getCreateBy()));
        goodsVO.setUserName(ConstantFactory.me().getUserName(goodsVO.getUserName()));

        return goodsVO;
    }

    @Override
    public void taskInser(TaskSendGoodVO taskSendGoodVO) {

        if (ToolUtil.isEmpty(taskSendGoodVO.getComment())){
            taskSendGoodVO.setComment("");
        }
        //添加评论信息
        taskService.addComment(taskSendGoodVO.getTaskId(), taskSendGoodVO.getInstanceId(), taskSendGoodVO.getComment());

        //完成当前流程
        taskService.complete(taskSendGoodVO.getTaskId());

        //分配审批人
        List<Task> tasks = taskService.createTaskQuery().processInstanceId(taskSendGoodVO.getInstanceId()).list();
        tasks.forEach(task -> {
            List<User> users = actProcessService.getNode(task.getTaskDefinitionKey()).getUsers();
            users.forEach(user -> {
                taskService.addCandidateUser(task.getId(), user.getId());
            });
            //设置优先级
            if (ToolUtil.isEmpty(taskSendGoodVO.getPriority())){
                taskService.setPriority(task.getId(),0 );
            }
        });

        //生成发货单
         insertSendOrder(taskSendGoodVO);
    }

    @Override
    public BigDecimal getTotalPrice(String applicationSendGoodId) {
        ApplicationSendGoodsOrder applicationSendGoodsOrder = applicationSendGoodsOrderService.getById(applicationSendGoodId);
        if (ToolUtil.isNotEmpty(applicationSendGoodsOrder)){
            //获取当前发货单对应的 发货数量单的 采购数量
            List<AppliSendNum> appliSendNums = appliSendNumService.list(Wrappers.<AppliSendNum>lambdaQuery()
            .eq(AppliSendNum::getApplicationSendOrderId,applicationSendGoodId));

            BigDecimal totalAmout = appliSendNums.stream().map(appliSendNum ->getTotal(appliSendNum))
                    .reduce(BigDecimal.ZERO, BigDecimal::add);
            return totalAmout;
        }
        return null;
    }

    @Override
    public BigDecimal getTotalAmount(String id) {
        SendGoodsOrder sendGoodsOrder = this.getById(id);
        if (ToolUtil.isNotEmpty(sendGoodsOrder)){
            ApplicationSendGoodsOrder applicationSendGoodsOrder = applicationSendGoodsOrderService.getById(sendGoodsOrder
            .getApplicationSendGoodsId());
            if (ToolUtil.isNotEmpty(applicationSendGoodsOrder)){
                List<String> ids = sp.splitToList(applicationSendGoodsOrder.getPurchaseOrderIds());
                List<PurchaseOrder> purchaseOrders = purchaseOrderService.list(Wrappers.<PurchaseOrder>lambdaQuery()
                .in(PurchaseOrder::getId,ids));
                if (ToolUtil.isNotEmpty(purchaseOrders)){
                    BigDecimal totalAmout = purchaseOrders.stream().map(PurchaseOrder::getOrderNumbers)
                            .reduce(BigDecimal::add )
                            .get();
                    return totalAmout;
                }
            }
        }
        return null;
    }

    @Override
    public List<SendGoodsVO> getSalesSendList(String id) {
        List<SendGoodsOrder> sendGoodsOrders = sendGoodsOrderMapper.selectList(new QueryWrapper<SendGoodsOrder>().lambda().eq(SendGoodsOrder::getSaleOrderId,id));
        List<SendGoodsVO> list = new ArrayList<>();
        if(ToolUtil.isNotEmpty(sendGoodsOrders)){
            for (SendGoodsOrder s : sendGoodsOrders){
                SendGoodsVO sendGoodsVO = new SendGoodsVO();
                ToolUtil.copyProperties(s,sendGoodsVO);
                CommonVO commonVO = userMapper.getUserMessage(s.getCreateBy());
                ToolUtil.copyProperties(commonVO,sendGoodsVO);
                if (ToolUtil.isNotEmpty(sendGoodsVO)){
                    list.add(sendGoodsVO);
                }
            }
        }
        return list;
    }

    @Override
    public List<String> checkSeri(String serial,String id) {

        if (ToolUtil.isNotEmpty(serial)){
            //查询申请发货单
            ApplicationSendGoodsOrder applicationSendGoodsOrder = applicationSendGoodsOrderService.getById(id);
            if(ToolUtil.isNotEmpty(applicationSendGoodsOrder)){
                AfterIndentOrder afterIndentOrder = afterIndentOrderMapper1.selectById(applicationSendGoodsOrder.getIndentOrderId());
                ApplicationAfterSaleOrder applicationAfterSaleOrder = applicationAfterSaleOrderMapper1.selectById(afterIndentOrder.getApplicationAfterSaleOrderId());
                List<String> afterIds = Arrays.asList(applicationAfterSaleOrder.getInventoryIds().split(","));
                List<String> afterSeri = afterIds.stream().map(a->(inventoryOrderService.getBaseMapper().selectById(a).getSerialNum())).collect(Collectors.toList());
                List<String> filter = CommonUtil.getSerial(serial);
                afterSeri.removeAll(filter);
                if (ToolUtil.isNotEmpty(afterSeri)){
                    return afterSeri;
                }
            }
        }
        return null;
    }

    public BigDecimal getTotal(AppliSendNum appliSendNum){

       PurchaseOrder purchaseOrder = purchaseOrderService.getById(appliSendNum.getPurchaseId());
       if (ToolUtil.isNotEmpty(purchaseOrder)){
           return purchaseOrder.getPrice().multiply(new BigDecimal(appliSendNum.getSendNum())).setScale(2,BigDecimal.ROUND_HALF_UP);
       }else {
           return new BigDecimal(0.00);
       }

   }



    @Override
    public List<InventoryVO> getDetail(String id,String userId) {
        SendGoodsOrder sendGoodsOrder = this.getById(id);
        if (ToolUtil.isNotEmpty(sendGoodsOrder)) {
            return inventoryOrderService.getInvetoryList(sendGoodsOrder.getInventoryOrderIds(),userId);
        }
        return null;
    }


    @Override
    public boolean confirmationOfReceipt(String id, String common, String userId) {
        try {
            SendGoodsOrder sendGoodsOrder = new SendGoodsOrder();
            sendGoodsOrder.setIsComplete(IsComplete.IS_COMPLETE);
            sendGoodsOrder.setId(id);
            sendGoodsOrder.setComment(common);
            if (ToolUtil.isEmpty(userId)) {
                sendGoodsOrder.setUpdateBy(securityUtil.getCurrUser().getId());
            } else {
                sendGoodsOrder.setUpdateBy(userId);
            }
            sendGoodsOrder.setUpdateTime(new Date());
            sendGoodsOrderMapper.updateById(sendGoodsOrder);
            return true;
        } catch (Exception e) {
            return false;
        }

    }

    /**
     * 插入申请发货单
     * @param taskSendGoodVO
     * @return
     */
    public SendGoodsOrder  insertSendOrder(TaskSendGoodVO taskSendGoodVO){
        SendGoodsOrder sendGoodsOrder = new SendGoodsOrder();
        if (taskSendGoodVO.getType() == 1) {
            sendGoodsOrder.setApplicationSendGoodsId(taskSendGoodVO.getApplicationSendGoodsId());
            ApplicationSendGoodsOrder applicationSendGoodsOrder = applicationSendGoodsOrderService.getById(taskSendGoodVO.getApplicationSendGoodsId());
            if (ToolUtil.isNotEmpty(applicationSendGoodsOrder)){
                sendGoodsOrder.setCreateBy(applicationSendGoodsOrder.getCreateBy());
            }
        }else if (taskSendGoodVO.getType() == 2){
            sendGoodsOrder.setSaleOrderId(taskSendGoodVO.getSalsOrderId());
            sendGoodsOrder.setCreateBy(securityUtil.getCurrUser().getId());
        }
        //根据用户输入的序列号进行查询库存
        List<String> serialss = CommonUtil.getSerial(taskSendGoodVO.getSerial());
        List<String> inventoryId = serialss.stream().map(s -> inventoryOrderService.getOne(Wrappers.<InventoryOrder>lambdaQuery()
        .eq(InventoryOrder::getSerialNum,s)).getId()).collect(Collectors.toList());
        sendGoodsOrder.setInventoryOrderIds(joiner.join(inventoryId));
        sendGoodsOrder.setOrderNum(CommonUtil.createOrderSerial(Order.FH_));
        sendGoodsOrder.setIsComplete(IsComplete.PENDING);
        this.save(sendGoodsOrder);

        //获取刚插入的发货单
        SendGoodsOrder sendGoodsOrderNew = this.getOne(Wrappers.<SendGoodsOrder>lambdaQuery()
        .orderByDesc(SendGoodsOrder::getCreateTime)
        .last("limit 1"));
        return sendGoodsOrderNew;
    }

    /**
     * 判断库存
     * @param serial
     * @return
     */
    public List<String> filterInventory(String serial){
        if (ToolUtil.isNotEmpty(serial)){
           List<String> filter = CommonUtil.getSerial(serial).stream().filter(s -> ToolUtil.isEmpty(inventoryOrderService.getOne(Wrappers.<InventoryOrder>lambdaQuery()
                    .eq(InventoryOrder::getSerialNum,s).eq(InventoryOrder::getSerialNumOut,1)))).collect(Collectors.toList());
            if (ToolUtil.isNotEmpty(filter)){
                return filter;
            }
            }
          return null;
    }

}