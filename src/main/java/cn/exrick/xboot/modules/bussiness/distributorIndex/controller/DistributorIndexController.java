package cn.exrick.xboot.modules.bussiness.distributorIndex.controller;

import cn.exrick.xboot.common.utils.PageUtil;
import cn.exrick.xboot.common.utils.ResultUtil;
import cn.exrick.xboot.common.utils.SecurityUtil;
import cn.exrick.xboot.common.utils.ToolUtil;
import cn.exrick.xboot.common.vo.PageVo;
import cn.exrick.xboot.common.vo.Result;
import cn.exrick.xboot.modules.bussiness.distributorIndex.pojo.DistributorIndex;
import cn.exrick.xboot.modules.bussiness.distributorIndex.pojo.DistributorIndexVO;
import cn.exrick.xboot.modules.bussiness.distributorIndex.service.IDistributorIndexService;
import cn.exrick.xboot.modules.bussiness.monthlyIndicators.pojo.MonthlyIndicators;
import cn.exrick.xboot.modules.bussiness.monthlyIndicators.service.IMonthlyIndicatorsService;
import com.alibaba.fastjson.JSON;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author tqr
 */
@Slf4j
@RestController
@Api(description = "经销商指标管理接口")
@RequestMapping("/xboot/distributorIndex")
@Transactional
public class DistributorIndexController {

    @Autowired
    private IDistributorIndexService iDistributorIndexService;
    @Autowired
    private IMonthlyIndicatorsService iMonthlyIndicatorsService;
    @Autowired
    private SecurityUtil securityUtil;

    @RequestMapping(value = "/delByIds/{ids}", method = RequestMethod.DELETE)
    @ApiOperation(value = "批量通过id删除")
    public Result<Object> delAllByIds(@PathVariable String[] ids) {

        for (String id : ids) {
            iDistributorIndexService.removeById(id);
        }
        return new ResultUtil<Object>().setSuccessMsg("批量通过id删除数据成功");
    }

    @PostMapping("/add")
    public Result<Object> add(String deptId, String monthly) {
        if(ToolUtil.isEmpty(iDistributorIndexService.getBaseMapper().selectList(new QueryWrapper<DistributorIndex>().lambda().eq(DistributorIndex::getDeptId,deptId)))) {
            MonthlyIndicators monthlyIndicators = JSON.parseObject(monthly, MonthlyIndicators.class);
            if (ToolUtil.isNotEmpty(monthlyIndicators)) {
                monthlyIndicators.setCreateBy(securityUtil.getCurrUser().getId());
                iMonthlyIndicatorsService.save(monthlyIndicators);
            }
            DistributorIndex distributorIndex = new DistributorIndex();
            String monthlyId = iMonthlyIndicatorsService.getBaseMapper().selectOne(new QueryWrapper<MonthlyIndicators>().lambda().orderByDesc(MonthlyIndicators::getCreateTime).last("limit 1")).getId();
            distributorIndex.setDeptId(deptId);
            distributorIndex.setMonthlyId(monthlyId);
            distributorIndex.setCreateBy(securityUtil.getCurrUser().getId());
            iDistributorIndexService.save(distributorIndex);
            return new ResultUtil<Object>().setSuccessMsg("添加成功");
        } else {
            return new ResultUtil<Object>().setErrorMsg("重复添加");
        }
    }

    @PostMapping("/select")
    public Result<Map<String, Object>> select(PageVo pageVo, String deptName) {
        Map<String, Object> map = new HashMap<>();
        List<DistributorIndexVO> data = iDistributorIndexService.select(PageUtil.initMpPage(pageVo), deptName);
        map.put("total", data.size());
        data = PageUtil.listToPage(pageVo, data);
        map.put("result", data);
        return new ResultUtil<Map<String, Object>>().setData(map);
    }

    @PostMapping("/edit")
    public Result<Object> edit(String id,String deptId, String monthly) {
        //查询该条记录
        DistributorIndex distributorIndex = iDistributorIndexService.getById(id);
        if(ToolUtil.isNotEmpty(distributorIndex)){
            MonthlyIndicators monthlyIndicators = JSON.parseObject(monthly, MonthlyIndicators.class);
            if (ToolUtil.isNotEmpty(monthlyIndicators)) {
                monthlyIndicators.setId(distributorIndex.getMonthlyId());
                monthlyIndicators.setUpdateTime(new Date());
                monthlyIndicators.setUpdateBy(securityUtil.getCurrUser().getId());
                iMonthlyIndicatorsService.getBaseMapper().updateById(monthlyIndicators);
            }
        }
        if(ToolUtil.isNotEmpty(deptId)){
            distributorIndex.setDeptId(deptId);
            distributorIndex.setUpdateBy(securityUtil.getCurrUser().getId());
            distributorIndex.setUpdateTime(new Date());
            iDistributorIndexService.getBaseMapper().updateById(distributorIndex);
        }
        return new ResultUtil<Object>().setSuccessMsg("修改成功");
    }
}
