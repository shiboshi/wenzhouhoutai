package cn.exrick.xboot.modules.bussiness.applicationAfterSaleOrder.mapper;


import cn.exrick.xboot.modules.bussiness.applicationAfterSaleOrder.VO.ApplicationAfterSaleOrderVO;
import cn.exrick.xboot.modules.bussiness.applicationAfterSaleOrder.sqll.ApplicationSql;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import cn.exrick.xboot.modules.bussiness.applicationAfterSaleOrder.pojo.ApplicationAfterSaleOrder;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.ResultType;
import org.apache.ibatis.annotations.SelectProvider;
import org.springframework.stereotype.Repository;

/**
 * 申请售后单数据处理层
 * @author xiaofei
 */
@Repository
public interface applicationAfterSaleOrderMapper extends BaseMapper<ApplicationAfterSaleOrder> {


    /**
     * 获取申请发货单列表
     * @param page
     * @param orderNum
     * @return
     */

    @SelectProvider(type = ApplicationSql.class,method = "SelectList")
    @ResultType(ApplicationAfterSaleOrderVO.class)
    Page<ApplicationAfterSaleOrderVO> selectList(@Param("page") Page page, @Param("orderNum") String orderNum,@Param("userId") String userId,@Param("checkStatus") String checkStatus,@Param("numb") String numb);

    //自定义sql语句
    String selectLast();

}