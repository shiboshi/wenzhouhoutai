package cn.exrick.xboot.modules.bussiness.applicationReturnGoodsOrder.serviceImp;

import cn.exrick.xboot.common.enums.*;
import cn.exrick.xboot.common.factory.ConstantFactory;
import cn.exrick.xboot.common.utils.CommonUtil;
import cn.exrick.xboot.common.utils.PageUtil;
import cn.exrick.xboot.common.utils.SecurityUtil;
import cn.exrick.xboot.common.utils.ToolUtil;
import cn.exrick.xboot.common.vo.PageVo;
import cn.exrick.xboot.modules.activiti.entity.ActBusiness;
import cn.exrick.xboot.modules.activiti.service.ActBusinessService;
import cn.exrick.xboot.modules.bussiness.appliSendNum.service.IappliSendNumService;
import cn.exrick.xboot.modules.bussiness.applicationReturnGoodsOrder.VO.AppliReturnGoodsVO;
import cn.exrick.xboot.modules.bussiness.applicationReturnGoodsOrder.mapper.ApplicationReturnGoodsOrderMapper;
import cn.exrick.xboot.modules.bussiness.applicationReturnGoodsOrder.pojo.ApplicationReturnGoodsOrder;
import cn.exrick.xboot.modules.bussiness.applicationReturnGoodsOrder.service.IApplicationReturnGoodsOrderService;
import cn.exrick.xboot.modules.bussiness.applicationSendGoodsOrder.VO.AppliParam;
import cn.exrick.xboot.modules.bussiness.applicationSendGoodsOrder.VO.AppliSendGoodsVO;
import cn.exrick.xboot.modules.bussiness.applicationSendGoodsOrder.pojo.ApplicationSendGoodsOrder;
import cn.exrick.xboot.modules.bussiness.purchaseOrder.VO.PurchaseOrderVO;
import cn.exrick.xboot.modules.bussiness.purchaseOrder.VO.UpdatePurchaseOrderVO;
import cn.exrick.xboot.modules.bussiness.purchaseOrder.pojo.PurchaseOrder;
import cn.exrick.xboot.modules.bussiness.purchaseOrder.service.IpurchaseOrderService;
import com.alibaba.fastjson.JSON;
import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.core.toolkit.support.SFunction;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.google.common.base.Joiner;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * 申请退货表接口实现
 *
 * @author tqr
 */
@Slf4j
@Service
@Transactional
public class IApplicationReturnGoodsOrderServiceImpl extends ServiceImpl<ApplicationReturnGoodsOrderMapper, ApplicationReturnGoodsOrder> implements IApplicationReturnGoodsOrderService {

    @Autowired
    private ApplicationReturnGoodsOrderMapper applicationReturnGoodsOrderMapper;
    @Autowired
    private SecurityUtil securityUtil;
    @Autowired
    private IpurchaseOrderService purchaseOrderService;
    @Autowired
    private ActBusinessService actBusinessService;
    @Autowired
    private IappliSendNumService appliSendNumService;

    private static final Joiner JOINER = Joiner.on(",").skipNulls();

    @Override
    public List<PurchaseOrderVO> getDetail(String id) {
        List<PurchaseOrderVO> purchaseOrderVOS = purchaseOrderService.getPurchaseList(OrderType.applicationOrder, id);
        return purchaseOrderVOS;
    }

    @Override
    public Page<AppliReturnGoodsVO> getAll(ApplicationReturnGoodsOrder applicationReturnGoodsOrder, PageVo pageVo) {
        Map<SFunction<ApplicationReturnGoodsOrder, ?>, Object> map = Maps.newHashMap();
        Wrapper wrapper = null;
        map.put(ApplicationReturnGoodsOrder::getOrderNum, applicationReturnGoodsOrder.getOrderNum());
        map.put(ApplicationReturnGoodsOrder::getDelFlag, 0);
        map.put(ApplicationReturnGoodsOrder::getCreateBy,securityUtil.getCurrUser().getId());
        wrapper = Wrappers.<ApplicationReturnGoodsOrder>lambdaQuery().allEq(true, map, false);
        Page lists = applicationReturnGoodsOrderMapper.getAllList(PageUtil.initMpPage(pageVo), wrapper);

        List<AppliReturnGoodsVO> appliReturnGoodsVOS = lists.getRecords();
        String userId = securityUtil.getCurrUser().getId();
        appliReturnGoodsVOS.forEach(s -> {
            s.setUserName(ConstantFactory.me().getUserName(s.getCreateBy()));
            s.setLevelName(ConstantFactory.me().getUserLevelName(s.getCreateBy()));
            s.setDeptName(ConstantFactory.me().getUserDeptName(s.getCreateBy()));
        });
        lists.setRecords(appliReturnGoodsVOS);
        return lists;
    }

    @Override
    public AppliReturnGoodsVO selectById(String id) {

        ApplicationReturnGoodsOrder pageVo = applicationReturnGoodsOrderMapper.selectById(id);
        AppliReturnGoodsVO appliReturnGoodsVO = new AppliReturnGoodsVO();
        ToolUtil.copyProperties(pageVo, appliReturnGoodsVO);
        String userId = pageVo.getCreateBy();
        appliReturnGoodsVO.setUserName(ConstantFactory.me().getUserName(userId));
        appliReturnGoodsVO.setLevelName(ConstantFactory.me().getUserLevelName(userId));
        appliReturnGoodsVO.setDeptName(ConstantFactory.me().getUserDeptName(userId));
        return appliReturnGoodsVO;
    }

    @Override
    public void insertOrder(AppliParam appliParam) {
        List<UpdatePurchaseOrderVO> updatePurchaseOrderVOS = JSON.parseArray(appliParam.getParam(), UpdatePurchaseOrderVO.class);
        String userId = ToolUtil.isNotEmpty(appliParam.getUserId()) ? appliParam.getUserId() : securityUtil.getCurrUser().getId();

        //1.插入申请发货单
        ApplicationReturnGoodsOrder applicationReturnGoodsOrder = new ApplicationReturnGoodsOrder();
        ToolUtil.copyProperties(appliParam, applicationReturnGoodsOrder);
        applicationReturnGoodsOrder.setCreateBy(userId);
        List<String> purchaseId = updatePurchaseOrderVOS.stream().map(UpdatePurchaseOrderVO::getId).collect(Collectors.toList());
        applicationReturnGoodsOrder.setPurchaseOrderIds(JOINER.join(purchaseId));
        applicationReturnGoodsOrder.setCheckStatus(CheckStatus.pengindg);
        applicationReturnGoodsOrder.setOrderNum(CommonUtil.createOrderSerial(Order.SQ_));
        this.save(applicationReturnGoodsOrder);

        //获取最新的一条记录
        ApplicationReturnGoodsOrder applicationReturnGoodsOrderNew = this.getOne(Wrappers.<ApplicationReturnGoodsOrder>lambdaQuery()
                .orderByDesc(ApplicationReturnGoodsOrder::getId)
                .last("limit 1"));

        //2.插入发货数量单
        updatePurchaseOrderVOS.forEach(Order -> {
            appliSendNumService.insert(applicationReturnGoodsOrderNew.getId(), Order.getId(), Order.getDispatchNum());
        });

        //3.插入到流程审核单中
        ActBusiness actBusiness = new ActBusiness();
        actBusiness.setTableId(applicationReturnGoodsOrderNew.getId());
        actBusiness.setProcDefId(appliParam.getProcDefId());
        actBusiness.setUserId(userId);
        ActBusiness actBusinessIns = actBusinessService.save(actBusiness);

        //4.更新申请发货单
        applicationReturnGoodsOrderNew.setBussinessId(actBusinessIns.getId());
        this.updateById(applicationReturnGoodsOrderNew);
    }

}