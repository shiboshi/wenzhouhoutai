package cn.exrick.xboot.modules.UpayApi.test.test;

import cn.exrick.xboot.common.upay.exception.UpayException;
import cn.exrick.xboot.common.upay.response.UpayResponse;
import cn.exrick.xboot.common.upay.utils.CipherUtils;
import cn.exrick.xboot.common.upay.utils.Utils;
import cn.exrick.xboot.common.utils.ToolUtil;
import cn.exrick.xboot.modules.UpayApi.WxPay.PayVO;
import cn.exrick.xboot.modules.UpayApi.WxPay.WxInterface;
import cn.exrick.xboot.modules.UpayApi.WxPay.WxPayService;
import cn.hutool.http.HttpUtil;
import com.alibaba.fastjson.JSON;
import lombok.extern.java.Log;
import net.sf.json.JSONObject;
import org.elasticsearch.common.MacAddressProvider;
import org.springframework.stereotype.Service;

import java.security.interfaces.RSAPublicKey;
import java.util.Map;
import java.util.TreeMap;

import static cn.exrick.xboot.modules.UpayApi.WxPay.Util.jsonToMap;
import static cn.exrick.xboot.modules.UpayApi.WxPay.Util.makeSign;


/**
 * @Author : xiaofei
 * @Date: 2019/9/5
 */
@Service
@Log
public class WxpayService {


    public String pay(PayVO payVO){

        JSONObject jsonObject = WxPayService.WXPay(payVO);
        Map<String, String> map = jsonToMap(jsonObject);
        jsonObject.put("sign", makeSign(WxInterface.md5, map));
            String jsonStr = jsonObject.toString();
            log.info("请求报文："+jsonStr);
            String result = HttpUtil.post(WxInterface.miniStr, jsonStr);
            return result;
    }


    public static boolean design(JSONObject jsonObject,Map<String,String> map, String designkey) {
        String sign = (String) jsonObject.get("sign");

        StringBuffer sbtr = new StringBuffer();
        for (Map.Entry<String, String> entry : map.entrySet()) {
            sbtr.append(entry.getKey());
            sbtr.append("=");
            sbtr.append(entry.getValue());
            sbtr.append("&");
        }

        String signStr = sbtr.substring(0, sbtr.toString().length() - 1);
        log.info("带验签字符串"+signStr);
        return sign.equalsIgnoreCase(CipherUtils.encodeMd5(signStr + "&key=" + designkey));
    }



}
