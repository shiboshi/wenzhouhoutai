package cn.exrick.xboot.modules.bussiness.address.pojo;

import cn.exrick.xboot.common.constant.CommonConstant;
import cn.exrick.xboot.modules.base.entity.Permission;
import com.baomidou.mybatisplus.annotation.TableField;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.persistence.Transient;
import java.math.BigDecimal;
import java.util.List;

@Data
public class AddressVO {

    @ApiModelProperty("id")
    private String id;

    private String createTime;
    /**
     * 分类名称
     */
    @ApiModelProperty("分类名称")
    private String title;

    /**
     * 父id
     */
    @ApiModelProperty("父id")
    private String parentId;

    /**
     * 父级分类名称
     */
    @ApiModelProperty("parentTitle")
    private String parentTitle;

    @ApiModelProperty(value = "是否为父节点(含子节点) 默认false")
    private Boolean isParent = false;

    @ApiModelProperty(value = "是否启用 0启用 -1禁用")
    private Integer status = CommonConstant.STATUS_NORMAL;

    @ApiModelProperty(value = "排序值")
    private BigDecimal sortOrder;

    @ApiModelProperty(value = "是否勾选 前端所需")
    private Boolean checked = false;

    @ApiModelProperty(value = "子菜单/权限")
    private List<AddressVO> children;
}
