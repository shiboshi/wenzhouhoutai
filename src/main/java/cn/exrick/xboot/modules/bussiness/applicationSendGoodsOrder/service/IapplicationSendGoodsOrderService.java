package cn.exrick.xboot.modules.bussiness.applicationSendGoodsOrder.service;

import cn.exrick.xboot.common.enums.OrderType;
import cn.exrick.xboot.common.vo.PageVo;
import cn.exrick.xboot.modules.activiti.entity.ActBusiness;
import cn.exrick.xboot.modules.bussiness.applicationSendGoodsOrder.VO.AppliParam;
import cn.exrick.xboot.modules.bussiness.applicationSendGoodsOrder.VO.AppliSendGoodsVO;
import cn.exrick.xboot.modules.bussiness.applicationSendGoodsOrder.VO.FilterVO;
import cn.exrick.xboot.modules.bussiness.indentOrder.pojo.IndentOrder;
import cn.exrick.xboot.modules.bussiness.purchaseOrder.VO.PurchaseOrderVO;
import cn.exrick.xboot.modules.bussiness.purchaseOrder.VO.UpdatePurchaseOrderVO;
import cn.exrick.xboot.modules.bussiness.purchaseOrder.pojo.PurchaseOrder;
import cn.exrick.xboot.modules.bussiness.sendGoodsOrder.VO.SendGoodsVO;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;
import cn.exrick.xboot.modules.bussiness.applicationSendGoodsOrder.pojo.ApplicationSendGoodsOrder;
import org.springframework.context.ApplicationContext;

import java.util.List;

/**
 * 申请发货单接口
 * @author xiaofei
 */
public interface IapplicationSendGoodsOrderService extends IService<ApplicationSendGoodsOrder> {

    /**
     * 获取所有申请发货单
     * @return
     */
    Page<AppliSendGoodsVO> getAll(ApplicationSendGoodsOrder applicationSendGoodsOrder, PageVo pageVo,String userId,String checkStatus);

    /**
     * 获取所有发货订单
     * */
    List<SendGoodsVO> getSendGoodOrderList(String applicationSendGoodsId);

    /**
     * 获取当前订单的订购详情
     * @param id
     * @return
     */
    List<PurchaseOrderVO> getDetail(String id);

    /**insert
     * 插入申请发货单
     * @param appliParam
     * @return
     */
    ActBusiness insertOrder(AppliParam appliParam);


    List<ApplicationSendGoodsOrder> get();

    /**
     * TODO
     * 判断当前申请发货的数量是否满足
     * @return
     */
    List<FilterVO> filterCanNUm(AppliParam appliParam);

    AppliSendGoodsVO selectById(String id);


}