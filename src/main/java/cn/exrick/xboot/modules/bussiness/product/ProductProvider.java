package cn.exrick.xboot.modules.bussiness.product;

import cn.exrick.xboot.common.utils.ToolUtil;
import org.apache.commons.lang3.StringUtils;

public class ProductProvider {

    public String getAllProduct(String productModelName) {
        String sql = "";
        String str = "";
        str += "SELECT p.molecule,p.unit_weight,p.volume,p.gross_weight,p.conversion_ratio,p.report_certificate reportCertificate, p.kind_id,p.pic_path picPath, p.create_time,p.id AS id,c. title AS material,c.id as materialId," +
                "(SELECT title FROM t_color WHERE id = c.parent_id and del_flag = 0) AS colorNumber,(SELECT id FROM t_color WHERE id = c.parent_id and del_flag = 0) AS colorNumberId," +
                "(SELECT id  FROM t_color WHERE id = (SELECT parent_id FROM t_color WHERE id = c.parent_id and del_flag = 0) and del_flag = 0 ) AS colorId,(SELECT title  FROM t_color WHERE id = (SELECT parent_id FROM t_color WHERE id = c.parent_id  and del_flag = 0) and del_flag = 0 ) AS color," +
                "u.unit_name AS unitName,u.id AS unit,n.unit_name AS unitMaxName,n.id AS unitMax,i.unit_name AS unitMinName,i.id AS unitMin,p.price AS price," +
                "p.time_to_market AS timeToMarket,p.min_order_amount AS minOrderAmount,p.min_selling_price AS minSellingPrice," +
                "p.uniform_sales_price AS uniformSalesPrice,p.conversion_ratio AS conversion_ratio,p.bothfee AS bothfee," +
                "p.remark AS remark,p.two AS two,p.four AS four,p.fourh AS fourHQ,l.title AS productOrderName," +
                "(SELECT title FROM t_classification WHERE id = l.parent_id and del_flag = 0) AS productModelName,(SELECT id FROM t_classification WHERE id = l.parent_id and del_flag = 0) AS productModelId," +
                "(SELECT title FROM t_classification WHERE id = (SELECT parent_id FROM t_classification WHERE id = l.parent_id and del_flag = 0) and del_flag = 0) AS productName" +
                " FROM x_product_order p LEFT JOIN t_color c ON p.color_id = c.id " +
                "LEFT JOIN t_unit u ON p.unit = u.id LEFT JOIN t_unit n ON p.unit_max = n.id LEFT JOIN t_unit i ON p.unit_min = i.id " +
                "LEFT JOIN t_classification l ON p.kind_id = l.id where (p.del_flag = 0 or p.del_flag is null) ";
        if(StringUtils.isNotBlank(productModelName)){
            str += " and p.product_order_name like '%"+productModelName+"%'";
        }
        sql += str;
        return sql;
    }

    public String getAllProductByDist(String productModelName,String[] idS,String userId){
        String sql = "SELECT o.uniform_sales_price AS uniformSalesPrice,o.molecule,o.unit_weight,o.volume,o.gross_weight, o.create_time,(SELECT title FROM t_classification WHERE id = s.parent_id) AS productModelName," +
                "(SELECT title FROM t_classification WHERE id = (SELECT parent_id FROM t_classification WHERE id = s.parent_id)) AS productName," +
                "s.title AS productOrderName,o.id AS id,(SELECT title FROM t_color WHERE id = (SELECT parent_id FROM t_color WHERE id = c.parent_id and del_flag = 0) and del_flag = 0) AS color," +
                "(SELECT title FROM t_color WHERE id = c.parent_id) AS colorNumber,c. title AS material," +
                "(SELECT  product_price FROM t_product_price WHERE level_id = (SELECT  level_id FROM t_dealer_level WHERE dept_id = (SELECT department_id FROM t_user WHERE id = '"+userId+"' and del_flag = 0) and del_flag = 0) AND product_order_id = o.kind_id and del_flag = 0) AS price," +
                "o.conversion_ratio AS conversionRatio,(SELECT unit_name FROM t_unit WHERE id = o.unit_min and del_flag = 0) AS unitName," +
                "o.time_to_market AS timeToMarket,o.unit AS unitId FROM x_product_order o JOIN t_classification s ON o.kind_id = s.id " +
                "LEFT JOIN t_color c ON o.color_id = c.id  where (o.del_flag = 0 or o.del_flag is null) ";
        String str = "";
        if (ToolUtil.isNotEmpty(idS)){
            str += " and o.id in ( ";
            for (int i = 0;i<idS.length;i++){
                if (i+1==idS.length){
                    str += "'"+idS[i]+"') ";
                }
                str += "'"+idS[i]+"',";
            }
        }
        if(StringUtils.isNotBlank(productModelName)){
            str += " and o.product_order_name like '%"+productModelName+"%'";
        }
        sql += str;
        return sql;
    }

    public String getAllProductByStore(String productModelName,String[] idS,String userId){
        String sql = "SELECT o.create_time,(SELECT title FROM t_classification WHERE id = s.parent_id and del_flag = 0) AS productModelName," +
                "(SELECT title FROM t_classification WHERE id = (SELECT parent_id FROM t_classification WHERE id = s.parent_id and del_flag = 0) and del_flag = 0) AS productName," +
                "s.title AS productOrderName,o.id AS id,(SELECT title FROM t_color WHERE id = (SELECT parent_id FROM t_color WHERE id = c.parent_id and del_flag = 0) and del_flag = 0) AS color," +
                "(SELECT title FROM t_color WHERE id = c.parent_id and del_flag = 0) AS colorNumber,c. title AS material," +
                "o.orice AS price," +
                "o.conversion_ratio AS conversionRatio,(SELECT unit_name FROM t_unit WHERE id = o.unit and del_flag = 0) AS unitName," +
                "o.time_to_market AS timeToMarket,o.unit AS unitId FROM x_product_order o JOIN t_classification s ON o.kind_id = s.id " +
                "LEFT JOIN t_color c ON o.color_id = c.id where (o.del_flag = 0 or o.del_flag is null) ";
        String str = "";
        if (ToolUtil.isNotEmpty(idS)){
            str += " and o.kind_id in ( ";
            for (int i = 0;i<idS.length;i++){
                if (i+1==idS.length){
                    str += "'"+idS[i]+"') ";
                }
                str += "'"+idS[i]+"',";
            }
        }
        if(StringUtils.isNotBlank(productModelName)){
            str += " and o.product_order_name like '%"+productModelName+"%'";
        }
        sql += str;
        return sql;
    }

    public String getAllProductDetail(String id) {
        String sql = "";
        String str = "";
        str += "SELECT p.create_time,p.kind_id,p.id AS id,p.unit as unitId,c. title AS material," +
                "(SELECT title FROM t_color WHERE id = c.parent_id and del_flag = 0) AS colorNumber," +
                "(SELECT title  FROM t_color WHERE id = (SELECT parent_id FROM t_color WHERE id = c.parent_id and del_flag = 0) and del_flag = 0) AS color," +
                "u.unit_name AS unit,p.price AS price,p.pic_path,p.molecule," +
                "p.time_to_market AS timeToMarket,p.min_order_amount AS minOrderAmount,p.min_selling_price AS minSellingPrice," +
                "p.uniform_sales_price AS uniformSalesPrice,p.conversion_ratio AS conversion_ratio,p.product_order_name AS priductOrderName,p.bothfee AS bothfee," +
                "p.remark AS remark,p.two AS two,p.four AS four,p.fourh AS fourHQ,l.title AS productOrderName," +
                "(SELECT title FROM t_classification WHERE id = l.parent_id and del_flag = 0) AS productModelName,(SELECT id FROM t_classification WHERE id = l.parent_id and del_flag = 0) AS productModelId," +
                "(SELECT title FROM t_classification WHERE id = (SELECT parent_id FROM t_classification WHERE id = l.parent_id and del_flag = 0) and del_flag = 0) AS productName" +
                " FROM x_product_order p LEFT JOIN t_color c ON p.color_id = c.id " +
                "LEFT JOIN t_unit u ON p.unit = u.id " +
                "LEFT JOIN t_classification l ON p.kind_id = l.id where p.id = '"+id+"'";
        sql += str;
        return sql;
    }

    public String getProductUnit(String id) {
        String sql = "";
        String str = "";
        str += "select b.id,b.unit_name as unitName from x_product_order a " +
                "right join t_unit b on (a.unit_min=b.id or a.unit_max=b.id) " +
                "where a.id = '"+id+"'";
        sql += str;
        return sql;
    }
}
