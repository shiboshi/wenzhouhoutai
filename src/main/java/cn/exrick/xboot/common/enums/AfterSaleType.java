package cn.exrick.xboot.common.enums;

import com.baomidou.mybatisplus.annotation.EnumValue;
import com.fasterxml.jackson.annotation.JsonValue;

/**
 * @Author : xiaofei
 * @Date: 2019/8/11
 */
public enum AfterSaleType{

    maintain(1,"维修"),
    SaleReturn(2,"退货");

    AfterSaleType(int code, String desc) {
        this.code = code;
        this.desc = desc;
    }

    public int getCode() {
        return code;
    }

    @JsonValue
    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    @EnumValue
    private final  int code ;

    private String desc;


}
