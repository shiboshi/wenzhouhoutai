package cn.exrick.xboot.modules.bussiness.inventoryOrder.service;

import cn.exrick.xboot.modules.bussiness.inventoryOrder.VO.InventoryVO;
import cn.exrick.xboot.modules.bussiness.inventoryOrder.pojo.InventoryOrderResult;
import com.baomidou.mybatisplus.extension.service.IService;
import cn.exrick.xboot.modules.bussiness.inventoryOrder.pojo.InventoryOrder;

import java.util.List;

/**
 * 库存表接口
 * @author tqr
 */
public interface IInventoryOrderService extends IService<InventoryOrder> {

    List<InventoryOrderResult> getByPage(String condition);

    /**
     * 获取需要查询的数据
     * @param ids
     * @return
     */
    List<InventoryVO> getInvetoryList(String ids,String userId);


    List<InventoryOrderResult> getMainWarehouse(String condition, String id);

    List<InventoryOrderResult> getInventoryOrder(String condition);

    List<InventoryOrderResult> getDoor(String condition, String userId);
}