package cn.exrick.xboot.modules.bussiness.appliSendNum.controller;

import cn.exrick.xboot.common.utils.PageUtil;
import cn.exrick.xboot.common.utils.ResultUtil;
import cn.exrick.xboot.common.vo.PageVo;
import cn.exrick.xboot.common.vo.Result;
import cn.exrick.xboot.modules.bussiness.appliSendNum.pojo.AppliSendNum;
import cn.exrick.xboot.modules.bussiness.appliSendNum.service.IappliSendNumService;
import com.baomidou.mybatisplus.core.metadata.IPage;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * @author fei
 */
@Slf4j
@RestController
@Api(description = "申请发货管理接口")
@RequestMapping("/xboot/appliSendNum")
@Transactional
public class appliSendNumController {

    @Autowired
    private IappliSendNumService iappliSendNumService;

    @RequestMapping(value = "/get/{id}", method = RequestMethod.GET)
    @ApiOperation(value = "通过id获取")
    public Result<AppliSendNum> get(@PathVariable String id){

        AppliSendNum appliSendNum = iappliSendNumService.getById(id);
        return new ResultUtil<AppliSendNum>().setData(appliSendNum);
    }

    @RequestMapping(value = "/getAll", method = RequestMethod.GET)
    @ApiOperation(value = "获取全部数据")
    public Result<List<AppliSendNum>> getAll(){

        List<AppliSendNum> list = iappliSendNumService.list();
        return new ResultUtil<List<AppliSendNum>>().setData(list);
    }

    @RequestMapping(value = "/getByPage", method = RequestMethod.GET)
    @ApiOperation(value = "分页获取")
    public Result<IPage<AppliSendNum>> getByPage(@ModelAttribute PageVo page){

        IPage<AppliSendNum> data = iappliSendNumService.page(PageUtil.initMpPage(page));
        return new ResultUtil<IPage<AppliSendNum>>().setData(data);
    }

    @RequestMapping(value = "/insertOrUpdate", method = RequestMethod.POST)
    @ApiOperation(value = "编辑或更新数据")
    public Result<AppliSendNum> saveOrUpdate(@ModelAttribute AppliSendNum appliSendNum){

        if(iappliSendNumService.saveOrUpdate(appliSendNum)){
            return new ResultUtil<AppliSendNum>().setData(appliSendNum);
        }
        return new ResultUtil<AppliSendNum>().setErrorMsg("操作失败");
    }

    @RequestMapping(value = "/delByIds/{ids}", method = RequestMethod.DELETE)
    @ApiOperation(value = "批量通过id删除")
    public Result<Object> delAllByIds(@PathVariable String[] ids){

        for(String id : ids){
            iappliSendNumService.removeById(id);
        }
        return new ResultUtil<Object>().setSuccessMsg("批量通过id删除数据成功");
    }
}
