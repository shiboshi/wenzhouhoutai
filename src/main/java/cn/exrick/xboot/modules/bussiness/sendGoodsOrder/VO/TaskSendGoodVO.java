package cn.exrick.xboot.modules.bussiness.sendGoodsOrder.VO;

import lombok.Data;

/**
 * @Author : xiaofei
 * @Date: 2019/8/30
 */
@Data
public class TaskSendGoodVO {


    /**
     * 发货类型 2.经销商 物流发货
     */
    private Integer type;

    /**
     * 任务id
     */
   private String taskId;

    /**
     * 流程实例id
     */
   private String instanceId;

    /**
     * 申请发货id
     */
   private String applicationSendGoodsId;

    /**
     * 销售订单id
     */
   private String salsOrderId;
    /**
     * 序列号
     */
   private String serial;

    /**
     * 评论信息
     */
   private String comment;

    /**
     * 优先级
     */
   private Integer priority;

   private String code;

}
