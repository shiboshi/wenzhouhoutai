package cn.exrick.xboot.modules.bussiness.srecharge.mapper;

import cn.exrick.xboot.modules.bussiness.srecharge.pojo.SrechargeOV;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import cn.exrick.xboot.modules.bussiness.srecharge.pojo.Srecharge;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

/**
 * 代充值数据处理层
 * @author tqr
 */
@Repository
public interface SrechargeMapper extends BaseMapper<Srecharge> {


    /**
     * 查询所有集合
     */
    IPage<SrechargeOV> QuerySrecharge(Page page, @Param("id") String id, @Param("username") String username);


    /**
     * 根据id查询详情
     */
    IPage<SrechargeOV> getByPageId(Page page, @Param("id") String id);




}