package cn.exrick.xboot.modules.bussiness.unit.serviceImp;

import cn.exrick.xboot.modules.bussiness.unit.VO.UnitVO;
import cn.exrick.xboot.modules.bussiness.unit.mapper.UnitMapper;
import cn.exrick.xboot.modules.bussiness.unit.pojo.Unit;
import cn.exrick.xboot.modules.bussiness.unit.service.IUnitService;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

/**
 * 产品单位接口实现
 * @author tqr
 */
@Slf4j
@Service
@Transactional
public class IUnitServiceImpl extends ServiceImpl<UnitMapper, Unit> implements IUnitService {

    @Autowired
    private UnitMapper unitMapper;

    @Override
    public IPage<UnitVO> getByName(Page initMpPage,String unitName) {
        IPage<UnitVO> list = unitMapper.selectPage(initMpPage,new QueryWrapper<Unit>().like("unit_name",unitName));
        return list;
    }
}