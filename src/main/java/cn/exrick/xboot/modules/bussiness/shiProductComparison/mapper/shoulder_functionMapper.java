package cn.exrick.xboot.modules.bussiness.shiProductComparison.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import cn.exrick.xboot.modules.bussiness.shiProductComparison.pojo.shoulder_function;

import java.util.List;

/**
 * 肩部功能数据处理层
 * @author 石博士
 */
public interface shoulder_functionMapper extends BaseMapper<shoulder_function> {

}