package cn.exrick.xboot.modules.bussiness.shiRechargeType.controller;

import cn.exrick.xboot.common.utils.PageUtil;
import cn.exrick.xboot.common.utils.ResultUtil;
import cn.exrick.xboot.common.vo.PageVo;
import cn.exrick.xboot.common.vo.Result;
import cn.exrick.xboot.modules.bussiness.shiRechargeType.pojo.ShiRechargeType;
import cn.exrick.xboot.modules.bussiness.shiRechargeType.service.IShiRechargeTypeService;
import com.baomidou.mybatisplus.core.metadata.IPage;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * @author tqr
 */
@Slf4j
@RestController
@Api(description = "充值分类管理接口")
@RequestMapping("/xboot/shiRechargeType")
@Transactional
public class ShiRechargeTypeController {

    @Autowired
    private IShiRechargeTypeService iShiRechargeTypeService;

    @RequestMapping(value = "/get/{id}", method = RequestMethod.GET)
    @ApiOperation(value = "通过id获取")
    public Result<ShiRechargeType> get(@PathVariable String id){

        ShiRechargeType shiRechargeType = iShiRechargeTypeService.getById(id);
        return new ResultUtil<ShiRechargeType>().setData(shiRechargeType);
    }

    @RequestMapping(value = "/getAll", method = RequestMethod.POST)
    @ApiOperation(value = "查询数据")
    public Result<List<ShiRechargeType>> getAll(){

        List<ShiRechargeType> list = iShiRechargeTypeService.list();
        return new ResultUtil<List<ShiRechargeType>>().setData(list);
    }

    @RequestMapping(value = "/getByPage", method = RequestMethod.POST)
    @ApiOperation(value = "查询充值分类的数据")
    public Result<IPage<ShiRechargeType>> getByPage(@ModelAttribute PageVo page){

        IPage<ShiRechargeType> data = iShiRechargeTypeService.page(PageUtil.initMpPage(page));
        return new ResultUtil<IPage<ShiRechargeType>>().setData(data);
    }

    @RequestMapping(value = "/insertOrUpdate", method = RequestMethod.POST)
    @ApiOperation(value = "编辑或更新数据")
    public Result<ShiRechargeType> saveOrUpdate(@ModelAttribute ShiRechargeType shiRechargeType){

        if(iShiRechargeTypeService.saveOrUpdate(shiRechargeType)){
            return new ResultUtil<ShiRechargeType>().setData(shiRechargeType);
        }
        return new ResultUtil<ShiRechargeType>().setErrorMsg("操作失败");
    }




    @RequestMapping(value = "/delAllByIds", method = RequestMethod.DELETE)
    @ApiOperation(value = "通过id删除")
    public Result<Object> delAllByIds(String id){
            iShiRechargeTypeService.removeById(id);
        return new ResultUtil<Object>().setSuccessMsg("批量通过id删除数据成功");
    }






}
