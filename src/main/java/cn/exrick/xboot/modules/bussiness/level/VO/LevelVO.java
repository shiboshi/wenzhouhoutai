package cn.exrick.xboot.modules.bussiness.level.VO;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@ApiModel("经销商等级")
@Data
public class LevelVO {

    @ApiModelProperty("id")
    private String id;

    @ApiModelProperty("等级名称")
    private String levelName;
}
