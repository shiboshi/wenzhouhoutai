package cn.exrick.xboot.modules.bussiness.address.mapper;

import cn.exrick.xboot.modules.bussiness.address.AddressProvider;
import cn.exrick.xboot.modules.bussiness.address.pojo.Address;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.SelectProvider;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * 地址数据处理层
 * @author tqr
 */
@Repository
public interface AddressMapper extends BaseMapper<Address> {

    @SelectProvider(type = AddressProvider.class,method = "getAddressSql")
    List<Address> findByParentIdOrderBySortOrder(String parentId);
}