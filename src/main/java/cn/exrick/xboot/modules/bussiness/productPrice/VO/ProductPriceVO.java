package cn.exrick.xboot.modules.bussiness.productPrice.VO;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.math.BigDecimal;

@ApiModel("产品价格")
@Data
public class ProductPriceVO {

    @ApiModelProperty("id")
    private String id;

    @ApiModelProperty("产品编码Id")
    private String productOrderId;

    @ApiModelProperty("产品大类")
    private String productName;

    @ApiModelProperty("产品型号")
    private String productModelName;

    @ApiModelProperty("产品型号")
    private String productModelId;

    @ApiModelProperty("产品对内型号")
    private String modelNameIn;

    @ApiModelProperty("产品品号")
    private String productOrderName;

    @ApiModelProperty("经销商等级id")
    private Integer levelId;

    @ApiModelProperty("经销商等级")
    private String level;

    @ApiModelProperty("产品价格")
    private BigDecimal productPrice;

}
