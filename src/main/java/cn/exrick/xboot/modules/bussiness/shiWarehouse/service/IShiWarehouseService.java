package cn.exrick.xboot.modules.bussiness.shiWarehouse.service;

import cn.exrick.xboot.common.vo.Result;
import cn.exrick.xboot.modules.bussiness.shiWarehouse.WarehouseVO.WarehouseVO;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;
import cn.exrick.xboot.modules.bussiness.shiWarehouse.pojo.ShiWarehouse;

import java.util.List;

/**
 * 仓库表接口
 * @author tqr
 */
public interface IShiWarehouseService extends IService<ShiWarehouse> {

    boolean insertWarehouse(ShiWarehouse shiWarehouse);

    List<WarehouseVO> getWarehouse(String name);

    Result<ShiWarehouse> editWarehouse(ShiWarehouse warehouse);

    Result<List<ShiWarehouse>> getWarehouseByDept();

    List<WarehouseVO> getTransferWarehouse();
}