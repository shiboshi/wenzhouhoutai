package cn.exrick.xboot.modules.base.service.mybatis;

import cn.exrick.xboot.modules.base.dao.RoleDepartmentDao;
import cn.exrick.xboot.modules.base.dao.mapper.RoleDepartmentMapper;
import cn.exrick.xboot.modules.base.entity.Permission;
import cn.exrick.xboot.modules.base.entity.RoleDepartment;
import com.baomidou.mybatisplus.extension.service.IService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.cache.annotation.CacheConfig;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author Exrickx
 */
@Service
public class IRoleDepartmentService extends ServiceImpl<RoleDepartmentMapper,RoleDepartment> {

}
