package cn.exrick.xboot.modules.base.service;

import cn.exrick.xboot.modules.base.entity.User;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * @Author : xiaofei
 * @Date: 2019/8/13
 */
public interface IUserService extends IService<User> {

}
