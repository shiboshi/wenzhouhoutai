package cn.exrick.xboot.modules.bussiness.bothSale.mapper;
import cn.exrick.xboot.modules.bussiness.bothSale.BothSaleProvider;
import cn.exrick.xboot.modules.bussiness.inventoryOrder.VO.BothSaleVO;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import cn.exrick.xboot.modules.bussiness.bothSale.pojo.BothSale;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.SelectProvider;
import org.springframework.stereotype.Repository;
/**
 * 两地销售数据处理层
 *
 * @author tqr
 */
@Repository
public interface BothSaleMapper extends BaseMapper<BothSale> {

    @SelectProvider(type = BothSaleProvider.class, method = "getBothSaleCommit")
    IPage<BothSaleVO> getBothSaleCommit(@Param("initMpPage") Page initMpPage,@Param("userName") String userName,@Param("id") String id);

    @SelectProvider(type = BothSaleProvider.class, method = "getBothSale")
    IPage<BothSaleVO> getBothSale(@Param("initMpPage")Page initMpPage,@Param("userName") String userName,@Param("id") String id);

    @SelectProvider(type = BothSaleProvider.class, method = "getBothSaleCommitByUser")
    IPage<BothSaleVO> getBothSaleCommitByUser(@Param("initMpPage") Page initMpPage,@Param("userId") String userId,@Param("id") String id,@Param("code") String code);

    @SelectProvider(type = BothSaleProvider.class, method = "getBothSaleByUser")
    IPage<BothSaleVO> getBothSaleByUser(@Param("initMpPage") Page initMpPage,@Param("userId") String userId,@Param("id") String id,@Param("code") String code);
}