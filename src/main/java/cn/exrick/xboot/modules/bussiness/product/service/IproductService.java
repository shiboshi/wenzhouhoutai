package cn.exrick.xboot.modules.bussiness.product.service;

import cn.exrick.xboot.common.vo.Result;
import cn.exrick.xboot.modules.bussiness.product.VO.ProductVO;
import com.baomidou.mybatisplus.extension.service.IService;
import cn.exrick.xboot.modules.bussiness.product.pojo.Product;

import java.util.List;

/**
 * 产品大类接口
 * @author xiaofei
 */
public interface IproductService extends IService<Product> {

    //1.获取产品列表
    //2.添加产品
    //3.查看产品详情
    //4.编辑产品
    //5.删除产品

    /**
     * 获取产品列表
     * @return
     */
    List<ProductVO> getProdcutList(Product product);

    /**
     * 添加产品大类
     * @param product
     */
    void insertProduct(Product product);


    /**
     * 查看产品详情
     * @param productId
     * @return
     */
    ProductVO productDetail(String productId);

    

    /**
     * 删除产品
     * @param productId
     */
    void  deleteProduct(String productId);

    Result<Product> addProduct(Product product);

    Result<Product> updateProduct(Product product);
}