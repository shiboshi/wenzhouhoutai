package cn.exrick.xboot.common.upay.enums;

/**
 * 支付渠道 Created by vectoryang on 2017/7/19.
 */
public enum BizChannel {

	UMSZJ_CHANNEL_MIX       ("umszj.channel.mix"     , "综合支付"),
	UMSZJ_CHANNEL_ALIPAY    ("umszj.channel.alipay"  , "支付宝"),
	UMSZJ_CHANNEL_WEIXIN    ("umszj.channel.wxpay"   , "微信"),
	UMSZJ_CHANNEL_EPAY      ("umszj.channel.epay"    , "翼支付"),
	UMSZJ_CHANNEL_YPAY      ("umszj.channel.ypay"    , "壹钱包"),
	UMSZJ_CHANNEL_COMMON    ("umszj.channel.common"  , "通用交易"),
	UMSZJ_CHANNEL_UNIONPAY  ("umszj.channel.unionpay", "银行卡"),
	UMSZJ_CHANNLE_MIN       ("umszj.channel.mix"     , "综合支付"),
	UMSZJ_CHANNEL_MANAGE    ("umszj.channel.manage"  , "管理类交易");

	private String en;
	private String desc;

	BizChannel(String en, String desc) {
		this.en = en;
		this.desc = desc;
	}

	public String getEn() {
		return en;
	}

	public String getDesc() {
		return desc;
	}

}
