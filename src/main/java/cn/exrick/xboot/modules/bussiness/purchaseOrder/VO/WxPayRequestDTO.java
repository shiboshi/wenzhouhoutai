package cn.exrick.xboot.modules.bussiness.purchaseOrder.VO;

import lombok.Data;

import java.math.BigDecimal;

/**
 * @Author : xiaofei
 * @Date: 2019/9/17
 */
@Data
public class WxPayRequestDTO {

    private String productId;
    private String unitId;
    private String orderNumbers;
    private BigDecimal price;
}
