package cn.exrick.xboot.modules.bussiness.saleOrder.SQL;

import cn.exrick.xboot.common.utils.SecurityUtil;
import cn.exrick.xboot.common.utils.ToolUtil;
import cn.exrick.xboot.config.springContext.SpringContextHolder;
import com.alibaba.druid.sql.visitor.functions.Locate;
import org.apache.ibatis.jdbc.SQL;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.DependsOn;
import org.springframework.transaction.annotation.Transactional;

import java.util.Map;

/**
 * @Author : xiaofei
 * @Date: 2019/8/15
 */
public class Sql {


    /**
     * 获取销售单列表
     *
     * @param map
     * @return
     */
    public String selectList(Map<String, Object> map) {

        return new SQL() {{
            SELECT("id as id,create_by, create_time as createTime,order_num as orderNum,buyer_name as buyerName,buyer_addr as buyerAddr,buyer_phone as buyerPhone, " +
                    "(case(warehouse_type) WHEN 1 THEN '门店仓库' WHEN 2 THEN '经销商仓库' END) as warehouseType," +
                    "(case(check_status) WHEN 1 THEN '待审核' WHEN 2 THEN '审核成功' when 3 then '审核失败' END) as checkStatus" +
                    ",(case(send_status) when 1 then '待发货' when 2 then '已发货' end) as sendStatus ");
            FROM("x_sales_order");
            if (ToolUtil.isNotEmpty(map.get("userId"))){
                WHERE("create_by=".concat(map.get("userId").toString()));
            }
            if (ToolUtil.isNotEmpty(map.get("orderNum"))) {
                String orderNum = (String) map.get("orderNum");
                WHERE("locate(" + orderNum + ",order_num)>0");
            }
            if (ToolUtil.isNotEmpty(map.get("checkStatus"))) {
                WHERE(" check_status = " + map.get("checkStatus") + "");
            }
            if (ToolUtil.isNotEmpty(map.get("warehouseType"))) {
                WHERE(" warehouse_type = " + map.get("warehouseType") + "");
            }
            WHERE("del_flag = 0");
            ORDER_BY("create_time desc");
        }}.toString();
    }

    public String getById(String id) {

        return new SQL() {{
            SELECT("id as id,purchase_order_ids,create_by,create_time as createTime,order_num as orderNum,buyer_name as buyerName,buyer_addr as buyerAddr,buyer_addr_id as buyerAddrId,buyer_detail_addr as buyerDetailAddr,buyer_phone as buyerPhone, " +
                    "warehouse_type as warehouseType," +
                    "(case(check_status) WHEN 1 THEN '待审核' WHEN 2 THEN '审核成功' when 3 then '审核失败' END) as checkStatus" +
                    ",(case(send_status) when 1 then '待发货' when 2 then '已发货' end) as sendStatus ");
            FROM("x_sales_order");
            WHERE(" id='"+id+"' ");
            WHERE(" del_flag = 0");
            ORDER_BY("create_time desc");
        }}.toString();
    }
}
