package cn.exrick.xboot.modules.bussiness.afterPurchase.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import cn.exrick.xboot.modules.bussiness.afterPurchase.pojo.AfterPurchase;
import org.springframework.stereotype.Repository;

/**
 * 售后单清单数据处理层
 * @author xiaofei
 */
@Repository
public interface afterPurchaseMapper extends BaseMapper<AfterPurchase> {

}