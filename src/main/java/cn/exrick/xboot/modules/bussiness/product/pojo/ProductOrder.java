package cn.exrick.xboot.modules.bussiness.product.pojo;

import cn.exrick.xboot.base.XbootBaseEntity;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import java.math.BigDecimal;
import java.util.Date;

/**
 * @author xiaofei
 */
@Data
@Entity
@Table(name = "x_product_order")
@TableName("x_product_order")
@ApiModel(value = "产品品号")
public class ProductOrder extends XbootBaseEntity {

    private static final long serialVersionUID = 1L;

    /**
     * 产品品号
     */
    private String productOrderName;

    /**
     * 产品分类id
     */
    private String kindId;

    /**
     * 颜色表id
     */
    private String colorId;

    /**
     * 产品单位
     */
    private String unit;

    /**
     * 产品价格
     */
    private String price;

    /**
     * 最小单位
     */
    private String unitMin;

    /**
     * 最大单位
     */
    private String unitMax;

    /**
     * 上市时间
     */
    @JsonFormat(timezone = "GMT+8", pattern = "yyyy-MM-dd")
    private Date timeToMarket;

    /**
     * 报告
     */
    private String reportCertificate;

    /**
     * 最低起订量
     */
    private BigDecimal minOrderAmount;

    /**
     * 最低销售价
     */
    private BigDecimal minSellingPrice;

    /**
     * 全国统一价格
     */
    private BigDecimal uniformSalesPrice;

    /**
     * 换算比例分子
     */
    private String conversionRatio;

    /**
     * 换算比例分母
     */
    private  String molecule;

    /**
     * 两地费
     */
    private BigDecimal bothfee;

    /**
     * 产品图片
     */
    @Column(length=512)
    private String picPath;

    /**
     * 备注
     */
    private String remark;

    private  String two;

    private  String four;

    private  String fourh;

    /**
     * 净重
     */
    private BigDecimal unitWeight;

    /**
     * 毛重
     */
    private BigDecimal grossWeight;

    /**
     * 体积
     */
    private BigDecimal volume;







}