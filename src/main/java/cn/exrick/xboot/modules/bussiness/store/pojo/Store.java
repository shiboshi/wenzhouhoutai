package cn.exrick.xboot.modules.bussiness.store.pojo;

import cn.exrick.xboot.base.XbootBaseEntity;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.Table;

/**
 * @author tqr
 */
@Data
@Entity
@Table(name = "t_store")
@TableName("t_store")
@ApiModel(value = "门店表")
public class Store extends XbootBaseEntity {

    private static final long serialVersionUID = 1L;

    /**
     * 门店名称
     */
    @ApiModelProperty("门店名称")
    private String storeName;

    /**
     * 门店经度
     */
    @ApiModelProperty("门店经度")
    private String storeLongitude;


    /**
     * 门店纬度
     */
    @ApiModelProperty("门店纬度")
    private String storelatitude;


    /**
     * 详细地址
     */
    @ApiModelProperty("详细地址")
    private String addressDetail;

    /**
     * 押金
     */
    @ApiModelProperty("押金")
    private String cash;

    /**
     * 账号数
     */
    @ApiModelProperty("账号数")
    private Integer acountNum;

    /**
     * 多图片，隔开
     */
    @ApiModelProperty("图片")
    private String picPath;

    /**
     * 附件
     */
    @ApiModelProperty("附件")
    private String accessoryPath;

    /**
     * 申请人
     */
    @ApiModelProperty("申请人")
    private String userId;

    /**
     *设计图纸
     */
    @ApiModelProperty("设计图纸")
    private  String paperPath;


    /**
     * 审核状态
     */
    @ApiModelProperty("审核状态")
    private String stortStatus;


    /**
     * 流程id
     */
    @ApiModelProperty("流程id")
    private String bussinessId;


    @ApiModelProperty("完成状态")
    private String completionStatus;












}