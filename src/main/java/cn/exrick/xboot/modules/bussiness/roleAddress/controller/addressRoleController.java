package cn.exrick.xboot.modules.bussiness.roleAddress.controller;

import cn.exrick.xboot.common.utils.PageUtil;
import cn.exrick.xboot.common.utils.ResultUtil;
import cn.exrick.xboot.common.vo.PageVo;
import cn.exrick.xboot.common.vo.Result;
import cn.exrick.xboot.modules.base.entity.Role;
import cn.exrick.xboot.modules.bussiness.address.pojo.AddressVO;
import cn.exrick.xboot.modules.bussiness.roleAddress.pojo.AddressRole;
import cn.exrick.xboot.modules.bussiness.roleAddress.service.IaddressRoleService;
import com.alibaba.fastjson.JSON;
import com.baomidou.mybatisplus.core.metadata.IPage;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.transaction.annotation.Transactional;

import java.util.Arrays;
import java.util.List;

/**
 * @author fei
 */
@Slf4j
@RestController
@Api(description = "角色区域表管理接口")
@RequestMapping("/xboot/addressRole")
@Transactional
public class addressRoleController {

    @Autowired
    private IaddressRoleService iaddressRoleService;

    @RequestMapping(value = "/get/{id}", method = RequestMethod.GET)
    @ApiOperation(value = "通过id获取")
    public Result<AddressRole> get(@PathVariable String id){

        AddressRole addressRole = iaddressRoleService.getById(id);
        return new ResultUtil<AddressRole>().setData(addressRole);
    }

    @RequestMapping(value = "/getAll", method = RequestMethod.GET)
    @ApiOperation(value = "获取全部数据")
    public Result<List<AddressRole>> getAll(){

        List<AddressRole> list = iaddressRoleService.list();
        return new ResultUtil<List<AddressRole>>().setData(list);
    }

    @RequestMapping(value = "/getByPage", method = RequestMethod.GET)
    @ApiOperation(value = "分页获取")
    public Result<IPage<AddressRole>> getByPage(@ModelAttribute PageVo page){

        IPage<AddressRole> data = iaddressRoleService.page(PageUtil.initMpPage(page));
        return new ResultUtil<IPage<AddressRole>>().setData(data);
    }

    @RequestMapping(value = "/insertOrUpdate", method = RequestMethod.POST)
    @ApiOperation(value = "编辑或更新数据")
    public Result<AddressRole> saveOrUpdate(@ModelAttribute AddressRole addressRole){

        if(iaddressRoleService.saveOrUpdate(addressRole)){
            return new ResultUtil<AddressRole>().setData(addressRole);
        }
        return new ResultUtil<AddressRole>().setErrorMsg("操作失败");
    }

    @RequestMapping(value = "/delByIds/{ids}", method = RequestMethod.DELETE)
    @ApiOperation(value = "批量通过id删除")
    public Result<Object> delAllByIds(@PathVariable String[] ids){

        for(String id : ids){
            iaddressRoleService.removeById(id);
        }
        return new ResultUtil<Object>().setSuccessMsg("批量通过id删除数据成功");
    }

    /**
     * 添加
     * @param permIds
     * @param roleId
     * @return
     */
    @RequestMapping(value = "/insertAddrRole", method = RequestMethod.POST)
    @ApiOperation(value = "编辑或更新数据")
    public Result<AddressRole> insertAddrRole(String permIds,String roleId){
        List<String> addressIds = Arrays.asList(permIds.split(","));
        if(iaddressRoleService.insertAddrRole(addressIds,roleId)){
            return new ResultUtil<AddressRole>().setSuccessMsg("添加成功");
        }
        return new ResultUtil<AddressRole>().setErrorMsg("操作失败");
    }

    /**
     * 获取角色地址列表
     * @return
     */
    @RequestMapping(value = "/getAddrRole", method = RequestMethod.POST)
    @ApiOperation(value = "编辑或更新数据")
    public Result<List<AddressVO>> getAddrRole(String roleId){

        List<AddressVO> data = iaddressRoleService.getList(roleId);
        return new ResultUtil<List<AddressVO>>().setData(data);
    }

    /**
     * 删除
     * @param param
     * @param roleId
     * @return
     */
    @RequestMapping(value = "/deleteAddressRole", method = RequestMethod.POST)
    public Result<Object> updateAddressRole(String param,String roleId){
        List<String> addressIds = JSON.parseArray(param,String.class);
        if(iaddressRoleService.updateAddressRole(addressIds,roleId)){
            return new ResultUtil<>().setSuccessMsg("删除成功");
        }
        return new ResultUtil<>().setErrorMsg("删除失败");
    }

    /**
     * 获取负责区域角色列表
     */
    @RequestMapping(value = "/getRole", method = RequestMethod.POST)
    public Result<List<Role>> getRole(){
        List<Role> list = iaddressRoleService.getRole();
        return new ResultUtil<List<Role>>().setData(list);
    }

}
