package cn.exrick.xboot.common.enums;

/**
 * 编号自动生成前缀
 * @Author : xiaofei
 * @Date: 2019/6/22
 */
public enum Order {
    /**
     * 批号
     */
    PH_,
    /**
     * 序列号
     */
    XLH_,

    /**
     * 销售单编号
     */
    XS_,
    /**
     * 默认生成用户名
     */
    SQ_,
    /**
     * 订单前缀
     */
    NM_,

    /**
     * 发货单编号
     */
    FH_,

    /**
     * 售后单编号
     */
    SH_,
    /**
     * 特价申请编号
     */
    TJ_,
    /**
     * 入库单编号
     */
    RK_,

    /**
     * 调拨单编号
     */
    DB_,

    /**
     * 售后申请单编号
     */
    QH_,

    /**
     * 两地销售单编号
     */
    LD_;
}
