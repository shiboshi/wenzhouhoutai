package cn.exrick.xboot.modules.bussiness.color.VO;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@ApiModel("/色号/材质")
@Data
public class ColorVO {

    @ApiModelProperty("id")
    private String id;

    @ApiModelProperty("分类名称")
    private String name;

    @ApiModelProperty("父级id")
    private String pid;

    @ApiModelProperty("颜色名称拼接")
    private String allName;

    @ApiModelProperty("产品颜色")
    private String color;

    @ApiModelProperty("产品色号")
    private String colorNumber;

    @ApiModelProperty("产品材质")
    private String material;
}
