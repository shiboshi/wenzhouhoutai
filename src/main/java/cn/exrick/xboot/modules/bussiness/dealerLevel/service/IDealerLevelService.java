package cn.exrick.xboot.modules.bussiness.dealerLevel.service;

import cn.exrick.xboot.modules.bussiness.dealerLevel.VO.DealerLevelVO;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;
import cn.exrick.xboot.modules.bussiness.dealerLevel.pojo.DealerLevel;

import java.util.List;

/**
 * 经销商等级接口
 * @author tqr
 */
public interface IDealerLevelService extends IService<DealerLevel> {

    IPage<DealerLevelVO> getDealer(Page initMpPage, String name);
}