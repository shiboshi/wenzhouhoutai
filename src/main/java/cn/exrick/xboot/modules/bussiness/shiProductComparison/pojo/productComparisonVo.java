package cn.exrick.xboot.modules.bussiness.shiProductComparison.pojo;

import cn.exrick.xboot.common.constant.CommonConstant;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * 产品对比返回值实体类
 */

@Data
public class productComparisonVo{

    private static final long serialVersionUID = 1L;


    private String id;

    private String createBy;

    private String createTime;

    private String updateBy;

    private String updateTime;

    private Integer delFlag = CommonConstant.STATUS_NORMAL;






    @ApiModelProperty("产品名称")
    private String name;

    @ApiModelProperty("头部功能")
    private String heads;

    @ApiModelProperty("肩部功能")
    private String shoulder;

    @ApiModelProperty("背部功能")
    private String backs;

    @ApiModelProperty("臀部功能")
    private String buttocks;

    @ApiModelProperty("扶手功能")
    private String handrail;

    @ApiModelProperty("小腿功能")
    private String calfs;

    @ApiModelProperty("附加功能")
    private String appends;

    @ApiModelProperty("基本参数")
    private String basicParameter;

    @ApiModelProperty("产品规格，关联规格表")
    private String product_specification;

    @ApiModelProperty("产品备注")
    private String remarks;

    @ApiModelProperty("关联产品表")
    private String product_order_id;

    @ApiModelProperty("产品名称")
    private String productModelName;


    /**
     * 头部功能
     */
    @ApiModelProperty("头靠调节,0没有，1有，2选配")
    private String headAdjustment;

    @ApiModelProperty("颈部热疗，0没有，1有，2选配")
    private String neckHT;

    @ApiModelProperty("颈部凸点按摩，0没有，1有，2选配")
    private String neckBumpMassage;

    @ApiModelProperty("颈部气压按摩，0没有，1有，2选配")
    private String heckPressureMassage;

    @ApiModelProperty("中药香薰，0没有，1有，2选配")
    private String medicinalAromatherapy;


    /**
     * 肩部功能
     */

    @ApiModelProperty("肩部气压，0没有，1有，2选配")
    private String shoulderPressure;

    @ApiModelProperty("翻转式手臂器，0没有，1有，2选配")
    private String flipArm;

    @ApiModelProperty("负氧离子，0没有，1有，2选配")
    private String negativeIon;

    @ApiModelProperty("肩部调节，0没有，1有，2选配")
    private String shoulderAdjustment;

    @ApiModelProperty("数字音响，0没有，1有，2选配")
    private String digitalSound;

    @ApiModelProperty("肩位置检测，0没有，1有，2选配")
    private String shoulderPosition;


    /**
     * 背部功能
     */
    @ApiModelProperty("机械手，手工输入")
    private String manipulator;

    @ApiModelProperty("扭腰功能，0没有，1有，2选配")
    private String twistTheWaist;

    @ApiModelProperty("背部振动，0没有，1有，2选配")
    private String backVibration;

    @ApiModelProperty("背部气压，手工输入")
    private String backAirPressure;

    @ApiModelProperty("顶腰功能，0没有，1有，2选配")
    private String parietal;

    @ApiModelProperty("热疗功能，0没有，1有，2选配")
    private String hyperthermic;

    @ApiModelProperty("靠背升降，0没有，1有，2选配")
    private String backLift;

    @ApiModelProperty("导轨（L/S），0表示L，1表示S")
    private String guideway;

    @ApiModelProperty("腰部牵引，0没有，1有，2选配")
    private String lumbarTraction;

    @ApiModelProperty("背部磁疗，手工输入")
    private String magnetotherapy;


    /**
     * 臀部功能
     */
    @ApiModelProperty("臀部气压，手工输入")
    private String pressure;

    @ApiModelProperty("臀部振动，0没有，1有，2选配")
    private String vibration;

    @ApiModelProperty("机械悬浮式摆臀，0没有，1有，2选配")
    private String swingTheButtocks;

    @ApiModelProperty("臀部敲击，0没有，1有，2选配")
    private String knocks;


    /**
     * 扶手功能
     */
    @ApiModelProperty("手臂气压，手工输入")
    private String armPressure;

    @ApiModelProperty("扶手拆装，0没有，1有，2选配")
    private String disassemblyAssembly;

    @ApiModelProperty("手臂牵引，0没有，1有，2选配")
    private String armTraction;

    @ApiModelProperty("扶手快捷按键，0没有，1有，2选配")
    private String quickButton;

    @ApiModelProperty("手臂加热，0没有，1有，2选配")
    private String armHeating;

    @ApiModelProperty("LED灯光，0没有，1有，2选配")
    private String ledLighting;

    @ApiModelProperty("USB接口，0没有，1有，2选配")
    private String usbInterface;


    /**
     * 小腿功能
     */
    @ApiModelProperty("腿部/脚部气囊，手工输入")
    private String footAirbag;

    @ApiModelProperty("腿部拉伸，0没有，1有，2选配")
    private String stretch;

    @ApiModelProperty("小腿升降，0没有，1有，2选配")
    private String riseFall;

    @ApiModelProperty("腿部凸点按摩，0没有，1有，2选配")
    private String bumpMassage;

    @ApiModelProperty("足部翻转，0没有，1有，2选配")
    private String footTurn;

    @ApiModelProperty("磁疗功能，手工输入")
    private String magnetic;

    @ApiModelProperty("腿部揉搓，0没有，1有，2选配")
    private String knead;

    @ApiModelProperty("腿部伸缩，0没有，1有，2选配")
    private String telescopic;

    @ApiModelProperty("膝盖/足部加热，0没有，1有，2选配")
    private String footHeating;

    @ApiModelProperty("足部凸点按摩，0没有，1有，2选配")
    private String footBumpMassages;

    @ApiModelProperty("足部滚轮刮痧，0没有，1有，2选配")
    private String rollerScraping;

    @ApiModelProperty("腿部/足部振动，0没有，1有，2选配")
    private String footVibration;


    /**
     * 附加功能
     */
    @ApiModelProperty("零空间，0没有，1有，2选配")
    private String zeroSpace;

    @ApiModelProperty("零重力，0没有，1有，2选配")
    private String zeroGravity;

    @ApiModelProperty("蓝牙APP，0没有，1有，2选配")
    private String bluetoothApp;

    @ApiModelProperty("累计工作时间，0没有，1有，2选配")
    private String workingTime;

    @ApiModelProperty("投币功能，0没有，1有，2选配")
    private String coins;

    @ApiModelProperty("微信支付，0没有，1有，2选配")
    private String weChatPay;

    @ApiModelProperty("自动模式，手工输入")
    private String automaticMode;

    @ApiModelProperty("记忆模式，手工输入")
    private String memoryPattern;

    @ApiModelProperty("安全保护，0没有，1有，2选配")
    private String safeguard;

    @ApiModelProperty("时间设定，0没有，1有，2选配")
    private String timeSetting;

    @ApiModelProperty("玉石热疗器，0没有，1有，2选配")
    private String jadeStone;

    @ApiModelProperty("整机联动，0没有，1有，2选配")
    private String machineLinkage;


    /**
     * 基本参数
     */
    @ApiModelProperty("额定电压，手写")
    private String voltage;

    @ApiModelProperty("额定频率，手写")
    private String frequency;

    @ApiModelProperty("最小气压，手写")
    private String minimumPressure;

    @ApiModelProperty("待机功率，手写")
    private String standbyPower;

    @ApiModelProperty("负离子浓度，手写")
    private String concentration;

    @ApiModelProperty("额定功率，手写")
    private String ratedPower;

    @ApiModelProperty("噪音，手写")
    private String noise;

    @ApiModelProperty("最大气压，手写")
    private String maximumPressure;

    @ApiModelProperty("磁场强度，手写")
    private String strength;

    @ApiModelProperty("气袋总数，手写")
    private String total;

    @ApiModelProperty("电磁阀总数，手写")
    private String totalElectricity;

    @ApiModelProperty("气泵总数，手写")
    private String totalGas;




    /**
     * 产品名称
     */
    @ApiModelProperty("产品名称")
    private String productOrderName;








}