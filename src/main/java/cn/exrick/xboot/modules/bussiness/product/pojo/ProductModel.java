package cn.exrick.xboot.modules.bussiness.product.pojo;

import cn.exrick.xboot.base.XbootBaseEntity;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.Table;

/**
 * @author xiaofei
 */
@Data
@Entity
@Table(name = "x_product_model")
@TableName("x_product_model")
@ApiModel(value = "产品型号")
public class ProductModel extends XbootBaseEntity {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty("产品型号名")
    private String  productModelName;

    @ApiModelProperty("分类id")
    private String kindId;

    @ApiModelProperty("对内型号")
    private String modelNameIn;

    @ApiModelProperty("对外型号")
    private String modelNameOut;

}