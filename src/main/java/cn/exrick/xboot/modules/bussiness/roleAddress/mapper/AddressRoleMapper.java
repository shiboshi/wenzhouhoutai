package cn.exrick.xboot.modules.bussiness.roleAddress.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import cn.exrick.xboot.modules.bussiness.roleAddress.pojo.AddressRole;
import org.springframework.stereotype.Repository;

/**
 * 角色区域表数据处理层
 * @author fei
 */
@Repository
public interface AddressRoleMapper extends BaseMapper<AddressRole> {

}