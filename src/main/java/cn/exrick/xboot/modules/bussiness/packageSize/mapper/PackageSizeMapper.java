package cn.exrick.xboot.modules.bussiness.packageSize.mapper;

import cn.exrick.xboot.modules.bussiness.packageSize.PackageSizeProvider;
import cn.exrick.xboot.modules.bussiness.packageSize.VO.PackageSizeVO;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import cn.exrick.xboot.modules.bussiness.packageSize.pojo.PackageSize;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.SelectProvider;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * 包装尺寸数据处理层
 * @author tqr
 */
@Repository
public interface PackageSizeMapper extends BaseMapper<PackageSize> {

    @SelectProvider(type = PackageSizeProvider.class,method = "getPackageSize")
    IPage<PackageSizeVO> getPackageSize(@Param("initMpPage") Page initMpPage,@Param("condition") String condition);

    @SelectProvider(type = PackageSizeProvider.class,method = "getByProductId")
    IPage<PackageSizeVO> getByProductId(@Param("initMpPage")Page initMpPage,@Param("id") String id);
}