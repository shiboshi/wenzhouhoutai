package cn.exrick.xboot.modules.bussiness.indentOrder.VO;

import lombok.Data;

import java.math.BigDecimal;

/**订货单参数
 * @Author : xiaofei
 * @Date: 2019/7/18
 */
@Data
public class IndentOrderSubmitDto {

    private  Long productId;
    private  Integer unitId;
    private  Long  orderNumbers;
    private BigDecimal price;
}
