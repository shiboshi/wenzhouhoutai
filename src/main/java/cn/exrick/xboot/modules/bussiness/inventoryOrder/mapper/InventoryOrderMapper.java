package cn.exrick.xboot.modules.bussiness.inventoryOrder.mapper;

import cn.exrick.xboot.modules.bussiness.inventoryOrder.InventoryProvider;
import cn.exrick.xboot.modules.bussiness.inventoryOrder.pojo.InventoryOrderResult;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import cn.exrick.xboot.modules.bussiness.inventoryOrder.pojo.InventoryOrder;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.SelectProvider;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * 库存表数据处理层
 *
 * @author tqr
 */
@Repository
public interface InventoryOrderMapper extends BaseMapper<InventoryOrder> {

    @SelectProvider(type = InventoryProvider.class, method = "getByPage")
    List<InventoryOrderResult> getByPage(@Param("deptId") String deptId, @Param("condition") String condition);

    @SelectProvider(type = InventoryProvider.class, method = "getMainWarehouse")
    List<InventoryOrderResult> getMainWarehouse(@Param("id") String id, @Param("condition") String condition);

    @SelectProvider(type = InventoryProvider.class, method = "getInventoryOrder")
    List<InventoryOrderResult> getInventoryOrder(@Param("deptId") String deptId, @Param("condition") String condition);
}