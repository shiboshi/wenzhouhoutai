package cn.exrick.xboot.modules.bussiness.applicationAfterSaleOrder.serviceImp;

import cn.exrick.xboot.common.enums.AfterSaleType;
import cn.exrick.xboot.common.enums.CheckStatus;
import cn.exrick.xboot.common.enums.Order;
import cn.exrick.xboot.common.factory.ConstantFactory;
import cn.exrick.xboot.common.utils.CommonUtil;
import cn.exrick.xboot.common.utils.PageUtil;
import cn.exrick.xboot.common.utils.SecurityUtil;
import cn.exrick.xboot.common.utils.ToolUtil;
import cn.exrick.xboot.common.vo.CommonVO;
import cn.exrick.xboot.common.vo.PageVo;
import cn.exrick.xboot.modules.activiti.entity.ActBusiness;
import cn.exrick.xboot.modules.activiti.service.ActBusinessService;
import cn.exrick.xboot.modules.base.dao.mapper.UserMapper;
import cn.exrick.xboot.modules.bussiness.applicationAfterSaleOrder.VO.ApplicationAfterSaleOrderVO;
import cn.exrick.xboot.modules.bussiness.applicationAfterSaleOrder.mapper.applicationAfterSaleOrderMapper;
import cn.exrick.xboot.modules.bussiness.applicationAfterSaleOrder.pojo.ApplicationAfterSaleOrder;
import cn.exrick.xboot.modules.bussiness.applicationAfterSaleOrder.service.IapplicationAfterSaleOrderService;
import cn.exrick.xboot.modules.bussiness.applicationSendGoodsOrder.pojo.ApplicationSendGoodsOrder;
import cn.exrick.xboot.modules.bussiness.indentOrder.service.IindentOrderService;
import cn.exrick.xboot.modules.bussiness.saleOrder.VO.SalesOrderVO;
import cn.hutool.core.thread.ThreadUtil;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.core.toolkit.support.SFunction;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.google.gson.Gson;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.lang.Nullable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Map;
import java.util.concurrent.*;
import java.util.stream.Collectors;

/**
 * 申请售后单接口实现
 *
 * @author xiaofei
 */
@Slf4j
@Service
@Transactional
public class IapplicationAfterSaleOrderServiceImpl extends ServiceImpl<applicationAfterSaleOrderMapper, ApplicationAfterSaleOrder> implements IapplicationAfterSaleOrderService {

    @Autowired
    private applicationAfterSaleOrderMapper applicationAfterSaleOrderMapper;
    @Autowired
    private ActBusinessService actBusinessService;
    @Autowired
    private SecurityUtil securityUtil;
    @Autowired
    private IindentOrderService indentOrderService;
    @Autowired
    private UserMapper userMapper;
    //有界阻塞队列
    private static final ArrayBlockingQueue<ApplicationAfterSaleOrder> QUE = new ArrayBlockingQueue<ApplicationAfterSaleOrder>(10);

    private static final List<ApplicationAfterSaleOrderVO> LIST = Lists.newArrayList();


    @Override
    public void commitApplication(String inventoryIds, AfterSaleType afterSaleType,String procDefId) {
        if (ToolUtil.isNotEmpty(inventoryIds) && ToolUtil.isNotEmpty(afterSaleType)) {
            ApplicationAfterSaleOrder applicationAfterSaleOrder = new ApplicationAfterSaleOrder();
            applicationAfterSaleOrder.setAfterSaleType(afterSaleType);
            applicationAfterSaleOrder.setInventoryIds(inventoryIds);
            applicationAfterSaleOrder.setCreateBy(securityUtil.getCurrUser().getId());
            applicationAfterSaleOrder.setCheckStatus(CheckStatus.pengindg);
            applicationAfterSaleOrder.setAppliAfterNumb(CommonUtil.createOrderSerial(Order.QH_));
            this.save(applicationAfterSaleOrder);
            //插入业务流程表
            ActBusiness actBusiness = new ActBusiness();
            ApplicationAfterSaleOrder applicationAfterSaleOrderLast = this
                    .getById(applicationAfterSaleOrderMapper.selectOne(new QueryWrapper<ApplicationAfterSaleOrder>().orderByDesc("create_time").last(" limit 1 ")).getId());
            actBusiness.setTableId(applicationAfterSaleOrderLast.getId())
                    .setUserId(securityUtil.getCurrUser().getId());
            actBusiness.setProcDefId(procDefId);
            ActBusiness actBusinessInset = actBusinessService.save(actBusiness);

            //更新业务表
            applicationAfterSaleOrderLast.setBusinessId(actBusinessInset.getId());
            this.updateById(applicationAfterSaleOrderLast);
        }
    }

    @Override
    public Page<ApplicationAfterSaleOrderVO> getList(ApplicationAfterSaleOrderVO applicationAfterSaleOrderVO, PageVo pageVo,String userId,String checkStatus,String numb) {

        userId = ToolUtil.isNotEmpty(userId)?userId:securityUtil.getCurrUser().getId();
        Page<ApplicationAfterSaleOrderVO> applicationAfterSaleOrderVOPage = applicationAfterSaleOrderMapper.selectList(PageUtil.initMpPage(pageVo), applicationAfterSaleOrderVO.getOrderNum(),userId,checkStatus,numb);
        List<ApplicationAfterSaleOrderVO> applicationAfterSaleOrderVOS = applicationAfterSaleOrderVOPage.getRecords();
        for (ApplicationAfterSaleOrderVO applicationAfterSaleOrderVOs : applicationAfterSaleOrderVOS) {
            CommonVO commonVO = userMapper.getUserMessage(applicationAfterSaleOrderVOs.getCreateBy());
            applicationAfterSaleOrderVOs.setUserDept(commonVO.getUserDept());
            applicationAfterSaleOrderVOs.setUserLevel(commonVO.getUserLevel());
            applicationAfterSaleOrderVOs.setUserName(commonVO.getUserName());
        }
//        CommonVO commonVO = userMapper.getUserMessage(securityUtil.getCurrUser().getId());
//        applicationAfterSaleOrderVOPage.setRecords(changeMessage(commonVO, applicationAfterSaleOrderVOS));
        return applicationAfterSaleOrderVOPage;
    }


    /**
     * 转移公共类
     *
     * @param commonVO
     * @param ApplicationAfterSaleOrderVO
     * @return
     */
    public List<ApplicationAfterSaleOrderVO> changeMessage(CommonVO commonVO, List<ApplicationAfterSaleOrderVO> ApplicationAfterSaleOrderVO) {
        if (!ToolUtil.isOneEmpty(commonVO, ApplicationAfterSaleOrderVO)) {
            List<ApplicationAfterSaleOrderVO> ApplicationAfterSaleOrderVOSReturn = ApplicationAfterSaleOrderVO.stream().map(order -> {
                ToolUtil.copyProperties(commonVO, order);
                return order;
            }).collect(Collectors.toList());
            return ApplicationAfterSaleOrderVOSReturn;
        }
        return null;
    }


}