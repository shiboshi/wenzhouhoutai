package cn.exrick.xboot.common.upay.request.manage;


import cn.exrick.xboot.common.upay.enums.BizChannel;
import cn.exrick.xboot.common.upay.enums.BizType;
import cn.exrick.xboot.common.upay.request.RequestBody;


public class SignOutRequestBody extends RequestBody {
    private String union_request;

    public SignOutRequestBody() {
    }

    @Override
    public boolean validate() {
        return true;
    }

    public String getUnion_request() {
        return union_request;
    }

    public void setUnion_request(String union_request) {
        this.union_request = union_request;
    }

    @Override
    public String acquireBizChannel() {
        return BizChannel.UMSZJ_CHANNEL_MANAGE.getEn();
    }

    @Override
    public String acquireBizType() {
        return BizType.UMSZJ_TRADE_SIGN_OUT.getEn();
    }

}
