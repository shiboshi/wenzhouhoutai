package cn.exrick.xboot.modules.UpayApi.test.qrcode;

import org.apache.commons.codec.binary.Base64;
import org.apache.commons.codec.digest.DigestUtils;

import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;
import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.UUID;

public class PoslinkTransactionDemo {
    public static byte[] hmacSHA256(byte[] data, byte[] key) throws NoSuchAlgorithmException, InvalidKeyException {
        String algorithm = "HmacSHA256";
        Mac mac = Mac.getInstance(algorithm);
        mac.init(new SecretKeySpec(key, algorithm));
        return mac.doFinal(data);
    }
    public static String getOpenBodySig(String appId, String appKey, String timestamp, String nonce, String body)throws Exception{
        String str1_C = appId+timestamp+nonce+ DigestUtils.sha256Hex(new ByteArrayInputStream(body.getBytes()));
        byte[] localSignature = hmacSHA256(str1_C.getBytes(), appKey.getBytes());

        return ("OPEN-BODY-SIG AppId="+"\""+appId+"\""+", Timestamp="+"\""+timestamp+"\""+", Nonce="+"\""+nonce+"\""+", Signature="+"\""+(Base64.encodeBase64String(localSignature))+"\"");
    }
    /*public static String send(String appId, String appKey, String url, String entity) throws Exception{
        String timestamp = new SimpleDateFormat("yyyyMMddHHmmss").format(new Date());
        String nonce = UUID.randomUUID().toString().replace("-","");
        String authorization = getOpenBodySig(appId, appKey, timestamp, nonce, entity);
        System.out.println(authorization);
        CloseableHttpClient httpClient = HttpClients.createDefault();
        HttpPost httpPost = new HttpPost(url);
        httpPost.addHeader("Authorization", authorization);
        StringEntity se = new StringEntity(entity,"UTF-8");
        se.setContentType("application/json");
        httpPost.setEntity(se);
        System.out.println("sd " + new SimpleDateFormat("yyyy-MM-dd HH:mm:ss:SSS").format(new Date()));
        CloseableHttpResponse response = httpClient.execute(httpPost);
        System.out.println("rv " + new SimpleDateFormat("yyyy-MM-dd HH:mm:ss:SSS").format(new Date()));
        HttpEntity entity1 = response.getEntity();
        String resStr = null;
        if(entity1 != null){
            resStr = EntityUtils.toString(entity1, "UTF-8");
        }
        httpClient.close();
        response.close();
        return resStr;
    }*/
    public static String sendGet(String appId, String appKey, String url, String entity) throws Exception {
        String result = "";
        BufferedReader in = null;
        String timestamp = new SimpleDateFormat("yyyyMMddHHmmss").format(new Date());
        String nonce = UUID.randomUUID().toString().replace("-","");
        String authorization = getOpenBodySig(appId, appKey, timestamp, nonce, entity);
        System.out.println(authorization);
        try {
            String urlNameString = url + "?" + entity;
            URL realUrl = new URL(urlNameString);
            // 打开和URL之间的连接
            URLConnection connection = realUrl.openConnection();
            // 设置通用的请求属性
            connection.setRequestProperty("accept", "*/*");
            connection.setRequestProperty("Authorization",authorization);
            connection.setRequestProperty("connection", "Keep-Alive");
            connection.setRequestProperty("user-agent",
                    "Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.1;SV1)");
            // 建立实际的连接
            connection.connect();
            // 获取所有响应头字段
            Map<String, List<String>> map = connection.getHeaderFields();
            // 遍历所有的响应头字段
            for (String key : map.keySet()) {
                System.out.println(key + "--->" + map.get(key));
            }
            // 定义 BufferedReader输入流来读取URL的响应
            in = new BufferedReader(new InputStreamReader(
                    connection.getInputStream()));
            String line;
            while ((line = in.readLine()) != null) {
                result += line;
            }
        } catch (Exception e) {
            System.out.println("发送GET请求出现异常！" + e);
            e.printStackTrace();
        }
        // 使用finally块来关闭输入流
        finally {
            try {
                if (in != null) {
                    in.close();
                }
            } catch (Exception e2) {
                e2.printStackTrace();
            }
        }
        return result;
    }

    public static void main(String[] args) throws Exception{
        String appId = "f0ec96ad2c3848b5b810e7aadf369e2f";
        String appKey = "775481e2556e4564985f5439a5e6a277";
        String body = "{" +
                "\"merchantCode\":\"123456789900081\",\"terminalCode\":\"00810001\"" +
                ",\"transactionCurrencyCode\":\"156\",\"merchantRemark\":\"测试\",\"payMode\":\"CODE_SCAN\"" +
                ",\"merchantOrderId\":\""+ UUID.randomUUID().toString().replace("-", "") +"\"" +
                ",\"transactionAmount\":\"500\"" +
                ",\"payCode\":\"134576899346554131\"" +
                ", \"goods\":[{\"goodsId\":\"12345678\",\"goodsName\":\"A\",\"quantity\":\"1\",\"price\":\"102\"}]" +
                "}";
        System.out.println(body);
        String url = "http://58.247.0.18:29015/v2/poslink/transaction/pay";
        System.out.println(sendGet(appId, appKey, url, body));
    }
}
