package cn.exrick.xboot.config.mybatisplus;

import cn.exrick.xboot.common.MybatisMethod.SelectLast;
import com.baomidou.mybatisplus.core.injector.AbstractMethod;
import com.baomidou.mybatisplus.core.injector.DefaultSqlInjector;
import org.springframework.context.annotation.Configuration;

import java.util.List;

/**
 * MPsql注入器
 * @Author : xiaofei
 * @Date: 2019/8/10
 */
@Configuration
public class MyLogicSqlInjector extends DefaultSqlInjector {
    @Override
    public List<AbstractMethod> getMethodList() {
        List<AbstractMethod> abstractMethods = super.getMethodList();
        abstractMethods.add(new SelectLast());
        return abstractMethods;
    }
}