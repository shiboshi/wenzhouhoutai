package cn.exrick.xboot.modules.bussiness.shiVO;

import cn.exrick.xboot.common.constant.CommonConstant;
import lombok.Data;

/**
 * 门店申请表返回值
 */
@Data
public class StoreVO{

    private static final long serialVersionUID = 1L;


    private String id;

    private String createBy;

    private String createTime;

    private String updateBy;

    private String updateTime;

    private Integer delFlag = CommonConstant.STATUS_NORMAL;




    /**
     * 门店名称
     */
    private String storeName;

    /**
     * 门店经度
     */
    private String storeLongitude;


    /**
     * 门店纬度
     */
    private String storelatitude;


    /**
     * 详细地址
     */
    private String addressDetail;

    /**
     * 押金
     */
    private String cash;

    /**
     * 账号数
     */
    private Integer acountNum;

    /**
     * 多图片，隔开
     */
    private String picPath;

    /**
     * 附件
     */
    private String accessoryPath;

    /**
     * 申请人
     */
    private String userId;

    /**
     *设计图纸
     */
    private  String paperPath;


    /**
     * 审核状态
     */
    private String stortStatus;


    /**
     * 申请人返回值
     */
    private String username;

    /**
     * 申请状态返回值
     */
    private String shiStortStatus;


    /**
     * 申请人部门
     */
    private String titles;




    /**
     * 完成状态
     */
    private String completionStatus;








}