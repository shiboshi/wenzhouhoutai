package cn.exrick.xboot.modules.bussiness.address.serviceImp;

import cn.exrick.xboot.common.utils.PageUtil;
import cn.exrick.xboot.common.vo.PageVo;
import cn.exrick.xboot.modules.bussiness.address.mapper.AddressMapper;
import cn.exrick.xboot.modules.bussiness.address.pojo.Address;
import cn.exrick.xboot.modules.bussiness.address.service.IAddressService;
import cn.exrick.xboot.modules.bussiness.classification.pojo.Classification;
import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * 地址接口实现
 *
 * @author tqr
 */
@Slf4j
@Service
@Transactional
public class IAddressServiceImpl extends ServiceImpl<AddressMapper, Address> implements IAddressService {

    @Autowired
    private AddressMapper addressMapper;

    @Override
    public IPage<Address> getByCondition(String condition, Page page) {
        QueryWrapper<Address> queryWrapper = new QueryWrapper<>();
        IPage<Address> addressList = addressMapper.selectPage(page,queryWrapper.lambda()
                .like(Address::getParentTitle, condition));
        return addressList;
    }

    @Override
    public IPage<Address> getByPage(Page page) {
        IPage<Address> addressList = addressMapper.selectPage(page,new QueryWrapper<>());
        return addressList;
    }

    @Override
    public List<Address> findByParentIdOrderBySortOrder(String parentId) {
        return addressMapper.findByParentIdOrderBySortOrder(parentId);
    }
}