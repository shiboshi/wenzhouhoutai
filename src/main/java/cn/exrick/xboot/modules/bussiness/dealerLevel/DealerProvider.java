package cn.exrick.xboot.modules.bussiness.dealerLevel;

import org.apache.commons.lang3.StringUtils;

public class DealerProvider {

    public String getDealer(String name) {
        String sql = "";
        String str = "";
        str += " select a.area_id areaId,a.create_time,a.name,a.id,c.id deptId,c.title as dept,b.id levelId,b.level_name as level,a.address,a.remark,a.balance,a.credit," +
                "CONCAT((SELECT title from t_address where id =(SELECT parent_id from t_address where id =(select parent_id from t_address where id = a.area_id)))," +
                "(select title from t_address where id = (SELECT parent_id from t_address where id = a.area_id)) ,(SELECT title from t_address where id = a.area_id)) as area" +
                " from t_dealer_level a LEFT JOIN t_level b on a.level_id = b.id LEFT JOIN t_department c on a.dept_id = c.id where (a.del_flag = 0 or a.del_flag is null) ";
        if(StringUtils.isNotBlank(name)){
            str += " and c.title like '%"+name+"%'";
        }
        sql += str;
        return sql;
    }
}
