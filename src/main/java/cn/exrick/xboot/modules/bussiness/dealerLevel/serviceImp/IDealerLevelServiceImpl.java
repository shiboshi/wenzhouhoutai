package cn.exrick.xboot.modules.bussiness.dealerLevel.serviceImp;

import cn.exrick.xboot.modules.bussiness.dealerLevel.VO.DealerLevelVO;
import cn.exrick.xboot.modules.bussiness.dealerLevel.mapper.DealerLevelMapper;
import cn.exrick.xboot.modules.bussiness.dealerLevel.pojo.DealerLevel;
import cn.exrick.xboot.modules.bussiness.dealerLevel.service.IDealerLevelService;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

/**
 * 经销商等级接口实现
 *
 * @author tqr
 */
@Slf4j
@Service
@Transactional
public class IDealerLevelServiceImpl extends ServiceImpl<DealerLevelMapper, DealerLevel> implements IDealerLevelService {

    @Autowired
    private DealerLevelMapper dealerLevelMapper;

    @Override
    public IPage<DealerLevelVO> getDealer(Page initMpPage, String name) {
        IPage<DealerLevelVO> list = new Page<>();
        list = dealerLevelMapper.getDealer(initMpPage, name);
        return list;
    }
}