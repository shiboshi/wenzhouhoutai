package cn.exrick.xboot.modules.bussiness.monthlyIndicators.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import cn.exrick.xboot.modules.bussiness.monthlyIndicators.pojo.MonthlyIndicators;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * 月份指标数据处理层
 * @author tqr
 */
@Repository
public interface MonthlyIndicatorsMapper extends BaseMapper<MonthlyIndicators> {

}