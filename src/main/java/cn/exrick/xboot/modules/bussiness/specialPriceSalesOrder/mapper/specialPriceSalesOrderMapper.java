package cn.exrick.xboot.modules.bussiness.specialPriceSalesOrder.mapper;

import cn.exrick.xboot.modules.bussiness.specialPriceSalesOrder.SpecialPriceSalesOrderProvider;
import cn.exrick.xboot.modules.bussiness.specialPriceSalesOrder.VO.SpecialPriceSalesOrderVO;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import cn.exrick.xboot.modules.bussiness.specialPriceSalesOrder.pojo.SpecialPriceSalesOrder;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.SelectProvider;
import org.springframework.stereotype.Repository;

/**
 * 特价申请销售订单数据处理层
 * @author xiaofei
 */
@Repository
public interface specialPriceSalesOrderMapper extends BaseMapper<SpecialPriceSalesOrder> {

     String selectLast();

     @SelectProvider(type = SpecialPriceSalesOrderProvider.class,method = "getList")
    IPage<SpecialPriceSalesOrderVO> getList(@Param("initMpPage") Page initMpPage,@Param("userId")String userId,@Param("checkStatus")String checkStatus);
}