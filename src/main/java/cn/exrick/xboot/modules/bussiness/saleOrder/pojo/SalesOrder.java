package cn.exrick.xboot.modules.bussiness.saleOrder.pojo;

import cn.exrick.xboot.base.XbootBaseEntity;
import cn.exrick.xboot.common.enums.CheckStatus;
import cn.exrick.xboot.common.enums.IsComplete;
import cn.exrick.xboot.common.enums.SendStatus;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

/**
 * @author xiaofei
 */
@Data
@Entity
@Table(name = "x_sales_order")
@TableName("x_sales_order")
@ApiModel(value = "销售订单")
public class SalesOrder extends XbootBaseEntity {

    private static final long serialVersionUID = 1L;

    /**
     * 关联业务流程id
     */
    private String bussinessId;

    /**
     *订单编号
     */
    private String orderNum;

    /**
     * 购买者姓名
     */
    private String buyerName;

    /**
     * 购买者手机号
     */
    private String buyerPhone;

    /**
     * 购买者收货地址
     */
    private String buyerAddr;

    /**
     * 购买者收货地址id
     */
    @Column(length = 500)
    private String buyerAddrId;

    /**
     * 购买者收货详细地址
     */
    @Column(length = 500)
    private String buyerDetailAddr;

    /**
     * 采购单ids
     */
    private String purchaseOrderIds;

    /**
     * 选择门店库存的ids
     */
    private String inventoryOrderIds;
    /**
     * 销售类型1.门店仓库2.经销商仓库
     */
    private Integer warehouseType;

    /**
     * 审核状态
     */
    private CheckStatus checkStatus;
    /**
     * 发货状态
     */
    private SendStatus sendStatus;

}