package cn.exrick.xboot.modules.bussiness.product.VO;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

import java.math.BigDecimal;

/**
 * @Author : xiaofei
 * @Date: 2019/9/17
 */
@Data
@Accessors(chain =  true)
public class WxProductList {

    /**
     * 品号id
     */
    private String id;
    /**
     * 产品品号名
     */
    private String productName;
    /**
     * 产品品号id
     */
    private String productId;

    /**
     * 产品图片
     */
    private String picPath;

    /**
     * 产品价格
     */
    private BigDecimal price;

    @ApiModelProperty("库存数量")
    private Long amount;






    @ApiModelProperty("对内型号")
    private String modelNameIn;


    @ApiModelProperty("对外型号")
    private String modelNameOut;


    @ApiModelProperty("产品颜色")
    private String color;


    @ApiModelProperty("产品色号")
    private String colorNumber;










}
