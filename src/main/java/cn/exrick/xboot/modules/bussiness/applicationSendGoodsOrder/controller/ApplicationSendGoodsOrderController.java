package cn.exrick.xboot.modules.bussiness.applicationSendGoodsOrder.controller;

import cn.exrick.xboot.common.enums.OrderType;
import cn.exrick.xboot.common.utils.PageUtil;
import cn.exrick.xboot.common.utils.ResultUtil;
import cn.exrick.xboot.common.utils.ToolUtil;
import cn.exrick.xboot.common.vo.PageVo;
import cn.exrick.xboot.common.vo.Result;
import cn.exrick.xboot.modules.bussiness.appliSendNum.pojo.AppliSendNum;
import cn.exrick.xboot.modules.bussiness.appliSendNum.service.IappliSendNumService;
import cn.exrick.xboot.modules.bussiness.applicationSendGoodsOrder.VO.AppliParam;
import cn.exrick.xboot.modules.bussiness.applicationSendGoodsOrder.VO.AppliSendGoodsVO;
import cn.exrick.xboot.modules.bussiness.applicationSendGoodsOrder.VO.FilterVO;
import cn.exrick.xboot.modules.bussiness.applicationSendGoodsOrder.VO.SendGoodInfoVO;
import cn.exrick.xboot.modules.bussiness.applicationSendGoodsOrder.mapper.ApplicationSendGoodsOrderMapper;
import cn.exrick.xboot.modules.bussiness.applicationSendGoodsOrder.pojo.ApplicationSendGoodsOrder;
import cn.exrick.xboot.modules.bussiness.applicationSendGoodsOrder.service.IapplicationSendGoodsOrderService;
import cn.exrick.xboot.modules.bussiness.product.pojo.ProductOrder;
import cn.exrick.xboot.modules.bussiness.product.service.IproductOrderService;
import cn.exrick.xboot.modules.bussiness.purchaseOrder.VO.PurchaseOrderVO;
import cn.exrick.xboot.modules.bussiness.purchaseOrder.pojo.PurchaseOrder;
import cn.exrick.xboot.modules.bussiness.purchaseOrder.service.IpurchaseOrderService;
import cn.exrick.xboot.modules.bussiness.sendGoodsOrder.VO.SendGoodsVO;
import cn.exrick.xboot.modules.bussiness.sendGoodsOrder.pojo.SendGoodsOrder;
import cn.exrick.xboot.modules.bussiness.sendGoodsOrder.service.IsendGoodsOrderService;
import cn.exrick.xboot.modules.bussiness.unit.pojo.Unit;
import cn.exrick.xboot.modules.bussiness.unit.service.IUnitService;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.google.common.collect.Maps;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * @author xiaofei
 */
@Slf4j
@RestController
@Api(description = "申请发货单管理接口")
@RequestMapping("/xboot/applicationSendGoodsOrder")
@Transactional
public class ApplicationSendGoodsOrderController {

    @Autowired
    private IapplicationSendGoodsOrderService applicationSendGoodsOrderService;
    @Autowired
    private ApplicationSendGoodsOrderMapper applicationSendGoodsOrderMapper;
    @Autowired
    private IappliSendNumService appliSendNumService;
    @Autowired
    private IpurchaseOrderService purchaseService;
    @Autowired
    private IsendGoodsOrderService sendGoodsOrderService;
    @Autowired
    private IsendGoodsOrderService isendGoodsOrderService;
    /**
     * 单位表
     */
    @Autowired
    private IUnitService iUnitService;

    /**
     * 产品表
     */
    @Autowired
    private IproductOrderService iproductOrderService;

    /**
     * 采购单
     */
    @Autowired
    private IpurchaseOrderService ipurchaseOrderService;


    @PostMapping("/getDetail")
    @ApiOperation(value = "通过id获取申请发货单详情")
    public Result<Map<String, Object>> getDetail(String id, @ModelAttribute PageVo pageVo) {

        Map<String,Object> map = Maps.newHashMap();
        //获取当前申请退货单中的退货详情
        ApplicationSendGoodsOrder applicationSendGoodsOrder = applicationSendGoodsOrderService.getById(id);
        List<PurchaseOrderVO> purchaseOrderVOS = new ArrayList<>();
        if(ToolUtil.isNotEmpty(applicationSendGoodsOrder)){
            purchaseOrderVOS  = purchaseService.getPurchaseList(OrderType.applicationOrder,applicationSendGoodsOrder.getId());
        }

        //发货单信息
        if(ToolUtil.isNotEmpty(applicationSendGoodsOrder)) {
            String applicationSendGoodsId= applicationSendGoodsOrder.getId();
            /*List<SendGoodsVO> sendGoodsVOPage= isendGoodsOrderService.getGoodOrderListByApplicationId(applicationSendGoodsId);
            map.put("sendGoodsVOPage",sendGoodsVOPage);*/
            List<SendGoodsVO> sendGoodsVOPage= applicationSendGoodsOrderService.getSendGoodOrderList(applicationSendGoodsId);
            map.put("sendGoodsVOPage",sendGoodsVOPage);
        }

        map.put("size", purchaseOrderVOS.size());
        List<AppliSendNum> appliSendNums = appliSendNumService.list(Wrappers.<AppliSendNum>lambdaQuery()
                .eq(AppliSendNum::getApplicationSendOrderId,id));
        for(PurchaseOrderVO purchaseOrderVO : purchaseOrderVOS ){
            for (AppliSendNum appliSendNum :appliSendNums){
                if(appliSendNum.getPurchaseId().equals(purchaseOrderVO.getId())){
                    purchaseOrderVO.setDispatchNum(appliSendNum.getSendNum());
                }
            }

            PurchaseOrder purchaseOrder = ipurchaseOrderService.getById(applicationSendGoodsOrder.getPurchaseOrderIds());
            if (null!=purchaseOrder){
                //根据purchase_order_ids 查询出 主键 product_id
                ProductOrder productOrder = iproductOrderService.getById(purchaseOrder.getProductId());
                //最后查询最大单位
                Unit unitMaxs = iUnitService.getById(productOrder.getUnitMax());

                //查询最小单位
                Unit unitMins = iUnitService.getById(productOrder.getUnitMin());

                //存入最大单位
                purchaseOrderVO.setUnitMaxs(unitMaxs.getUnitName());

                //存入最小单位
                purchaseOrderVO.setUnitMins(unitMins.getUnitName());
            }
        }
        purchaseOrderVOS = PageUtil.listToPage(pageVo,purchaseOrderVOS);
        map.put("data", purchaseOrderVOS);
        map.put("applicatSend",applicationSendGoodsOrder);
        map.put("address",applicationSendGoodsOrder.getAddress());
        return new ResultUtil<Map<String,Object>>().setData(map,"查询成功！");
    }

    @PostMapping("/getList")
    @ApiOperation(value = "获取申请发货列表")
    public Result<Page<AppliSendGoodsVO>> getList(PageVo pageVo, ApplicationSendGoodsOrder applicationSendGoodsOrder,String userId,String code) {
        Page<AppliSendGoodsVO> page = applicationSendGoodsOrderService.getAll(applicationSendGoodsOrder, pageVo,userId,code);
        return new ResultUtil<Page<AppliSendGoodsVO>>().setData(page);
    }

    @PostMapping("/selectById")
    @ApiOperation(value = "获取申请发货列表")
    public Result<List<AppliSendGoodsVO>> selectById(String id) {
        AppliSendGoodsVO page = applicationSendGoodsOrderService.selectById(id);
        List<AppliSendGoodsVO> list = new ArrayList<>();
        list.add(page);
        return new ResultUtil<List<AppliSendGoodsVO>>().setData(list);
    }

    @PostMapping("/insertOrder")
    @ApiOperation(value = "插入申请发货单")
    public Result<?> insertOrder(AppliParam appliParam) {
        //判断当前申请发货的数量是否满足
        List<FilterVO> purchaseOrderFilter = applicationSendGoodsOrderService.filterCanNUm(appliParam);
        if (ToolUtil.isEmpty(purchaseOrderFilter)){
            applicationSendGoodsOrderService.insertOrder(appliParam);
            return new ResultUtil<>().setSuccessMsg("插入订单成功！");
        }else {
            return new ResultUtil<>().setErrorMsg(201, "以下商品不能提交！！！", purchaseOrderFilter);
        }
    }

    @PostMapping("/delAllByIds")
    @ApiOperation(value = "通过id删除")
    public Result<Object> delAllByIds(String id) {
        applicationSendGoodsOrderService.removeById(id);
        return new ResultUtil<Object>().setSuccessMsg("批量通过id删除数据成功");
    }

    /**
     * 添加发货信息
     */
    @PostMapping("/addSendInfo")
    @ApiOperation(value = "通过id删除")
    public Result<Object> addSendInfo(SendGoodInfoVO appliParam) {
        //获取当前申请发货单的 发货单
        SendGoodsOrder sendGoodsOrder = sendGoodsOrderService.getOne(Wrappers.<SendGoodsOrder>lambdaQuery()
        .eq(SendGoodsOrder::getApplicationSendGoodsId,appliParam.getId()));
        sendGoodsOrder.setArrivalTime(appliParam.getArrivalTime());
        sendGoodsOrder.setTelephone(appliParam.getTelephone());
        sendGoodsOrder.setProductPic(appliParam.getProductPic());
        sendGoodsOrderService.updateById(sendGoodsOrder);
        return new ResultUtil<>().setSuccessMsg("更改发货单成功！");
    }
}
