package cn.exrick.xboot.modules.wx.VO;

import lombok.Data;

import java.util.Date;

/**
 * @Author : xiaofei
 * @Date: 2019/9/18
 */
@Data
public class DebugDetail {

    private String productName;
    private String productModelName;
    private String productOrderName;

    /**
     * 产品颜色
     */
    private String color;

    /**
     * 产品色号
     */
    private String colorNumber;

    /**
     * 产品材质
     */
    private String material;
    /**
     * 序列号
     */
    private String serials;

    /**
     * 维修前图片
     */
    private String beforePic;

    /**
     * 保修时间
     */
    private Date warrantyPeriod;

}
