package cn.exrick.xboot.modules.bussiness.afterIndentOrder.controller;

import cn.exrick.xboot.common.enums.OrderType;
import cn.exrick.xboot.common.factory.ConstantFactory;
import cn.exrick.xboot.common.utils.PageUtil;
import cn.exrick.xboot.common.utils.ResultUtil;
import cn.exrick.xboot.common.utils.ToolUtil;
import cn.exrick.xboot.common.vo.PageVo;
import cn.exrick.xboot.common.vo.Result;
import cn.exrick.xboot.modules.bussiness.afterIndentOrder.VO.AfterIndentVO;
import cn.exrick.xboot.modules.bussiness.afterIndentOrder.pojo.AfterIndentOrder;
import cn.exrick.xboot.modules.bussiness.afterIndentOrder.service.IafterIndentOrderService;
import cn.exrick.xboot.modules.bussiness.applicationAfterSaleOrder.pojo.ApplicationAfterSaleOrder;
import cn.exrick.xboot.modules.bussiness.applicationAfterSaleOrder.service.IapplicationAfterSaleOrderService;
import cn.exrick.xboot.modules.bussiness.applicationSendGoodsOrder.pojo.ApplicationSendGoodsOrder;
import cn.exrick.xboot.modules.bussiness.applicationSendGoodsOrder.service.IapplicationSendGoodsOrderService;
import cn.exrick.xboot.modules.bussiness.inventoryOrder.pojo.InventoryOrderResult;
import cn.exrick.xboot.modules.bussiness.product.VO.ProductCommon;
import com.google.common.collect.Maps;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.lang.Nullable;
import org.springframework.web.bind.annotation.*;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author xiaofei
 */
@Slf4j
@RestController
@Api(description = "订货单维修单管理接口")
@RequestMapping("/xboot/afterIndentOrder")
@Transactional
public class afterIndentOrderController {

    @Autowired
    private IafterIndentOrderService iafterIndentOrderService;
    @Autowired
    private IapplicationSendGoodsOrderService applicationSendGoodsOrderService;
    @Autowired
    private IapplicationAfterSaleOrderService iapplicationAfterSaleOrderService;

    @PostMapping("/insertAfterOrder")
    @ApiOperation(value = "获取列表")
    public Result<Object> insertAfterOrder(String applicationAfterSaleOrderId, String inventoryOrderIds,String procDefId) {
        iafterIndentOrderService.insertOrder(applicationAfterSaleOrderId, inventoryOrderIds,procDefId);
        return new ResultUtil<Object>().setSuccessMsg("添加成功");
    }

    @PostMapping("/getList")
    @ApiOperation(value = "获取列表")
    public Result<Map<String, Object>> getList(PageVo pageVo,String checkStatus,String numb) {
        Map<String, Object> page = iafterIndentOrderService.getList(pageVo,checkStatus,numb);
        return new ResultUtil<Map<String, Object>>().setData(page);
    }

    @PostMapping("/getDetail")
    @ApiOperation(value = "获取详情")
    public Result<Map<String, Object>> getDetail(PageVo pageVo, String id) {
        Map<String, Object> page = new HashMap<>();
        List<ProductCommon> result = iafterIndentOrderService.getDetail(PageUtil.initMpPage(pageVo), id);
        page.put("total",result.size());
        page.put("result",PageUtil.listToPage(pageVo,result));
        //查询售后类型
        ApplicationAfterSaleOrder applicationAfterSaleOrder = iapplicationAfterSaleOrderService.getById(iafterIndentOrderService.getById(id).getApplicationAfterSaleOrderId());
        page.put("type",applicationAfterSaleOrder.getAfterSaleType().getCode());
        return new ResultUtil<Map<String, Object>>().setData(page);
    }

    @PostMapping("/selectById")
    @ApiOperation(value = "获取全部数据")
    @Nullable
    public Result<Map<String,Object>>selectById(String id){
        AfterIndentOrder page = iafterIndentOrderService.getBaseMapper().selectById(id);
        AfterIndentVO afterIndentVO = new AfterIndentVO();
        ToolUtil.copyProperties(page,afterIndentVO);
        afterIndentVO.setUserName(ConstantFactory.me().getUserName(page.getCreateBy()));
        afterIndentVO.setUserLevel(ConstantFactory.me().getUserLevelName(page.getCreateBy()));
        afterIndentVO.setUserDept(ConstantFactory.me().getUserDeptName(page.getCreateBy()));
        Map<String,Object> map = Maps.newHashMap();
        map.put("result",afterIndentVO);
        return new ResultUtil<Map<String,Object>>().setData(map);
    }

    @PostMapping("/insertReturnMoney")
    @ApiOperation(value = "添加退款金额")
    public Result<Object> insertReturnMoney(String id,String money) {
        if(ToolUtil.isNotEmpty(money)){
            BigDecimal reMoney = new BigDecimal(money);
           AfterIndentOrder afterIndentOrder = iafterIndentOrderService.getById(id);
           if (ToolUtil.isNotEmpty(afterIndentOrder)){
               afterIndentOrder.setReturnMoney(reMoney);
               iafterIndentOrderService.getBaseMapper().updateById(afterIndentOrder);
               return new ResultUtil<>().setSuccessMsg("修改成功");
           }
            return new ResultUtil<>().setErrorMsg("查询失败");
        }
        return new ResultUtil<>().setErrorMsg("参数不全");
    }

    @PostMapping("/insertAfterIndentSend")
    @ApiOperation(value = "售后人员提交")
    public Result<Object> insertAfterIndentSend(String id,String procDefId) {
        iafterIndentOrderService.insertAfterIndentSend(id,procDefId);
        return new ResultUtil<Object>().setSuccessMsg("申请成功");
    }

    @PostMapping("/getAfterIndentSend")
    @ApiOperation(value = "查询详情")
    public Result<Map<String,Object>> getAfterIndentSend(String id, @ModelAttribute PageVo pageVo) {
        Map<String,Object> map = Maps.newHashMap();
        //获取当前申请退货单中的退货详情
        ApplicationSendGoodsOrder applicationSendGoodsOrder = applicationSendGoodsOrderService.getById(id);
        List<ProductCommon> result = new ArrayList<>();
        if(ToolUtil.isNotEmpty(applicationSendGoodsOrder)){
            result = iafterIndentOrderService.getDetail(PageUtil.initMpPage(pageVo),applicationSendGoodsOrder.getIndentOrderId());
        }
        map.put("size", result.size());
        result = PageUtil.listToPage(pageVo,result);
        map.put("data", result);
        return new ResultUtil<Map<String,Object>>().setData(map,"查询成功！");
    }
}
