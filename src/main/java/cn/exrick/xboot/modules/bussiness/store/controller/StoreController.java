package cn.exrick.xboot.modules.bussiness.store.controller;

import cn.exrick.xboot.common.utils.PageUtil;
import cn.exrick.xboot.common.utils.ResultUtil;
import cn.exrick.xboot.common.utils.SecurityUtil;
import cn.exrick.xboot.common.utils.ToolUtil;
import cn.exrick.xboot.common.vo.PageVo;
import cn.exrick.xboot.common.vo.Result;
import cn.exrick.xboot.modules.activiti.entity.ActBusiness;
import cn.exrick.xboot.modules.activiti.service.ActBusinessService;
import cn.exrick.xboot.modules.base.entity.User;
import cn.exrick.xboot.modules.bussiness.shiVO.StoreVO;
import cn.exrick.xboot.modules.bussiness.store.pojo.Store;
import cn.exrick.xboot.modules.bussiness.store.service.IStoreService;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpSession;
import java.util.List;

/**
 * @author tqr
 */
@Slf4j
@RestController
@Api(description = "门店表管理接口")
@RequestMapping("/xboot/store")
@Transactional
public class StoreController {


    @Autowired
    private SecurityUtil securityUtil;

    @Autowired
    private IStoreService iStoreService;

    @Autowired
    private ActBusinessService actBusinessService;



    @RequestMapping(value = "/get/{id}", method = RequestMethod.POST)
    @ApiOperation(value = "通过id获取")
    public Result<Store> get(@PathVariable String id){

        Store store = iStoreService.getById(id);
        return new ResultUtil<Store>().setData(store);
    }

    @RequestMapping(value = "/getAll", method = RequestMethod.POST)
    @ApiOperation(value = "获取全部数据")
    public Result<List<Store>> getAll(){

        List<Store> list = iStoreService.list();
        return new ResultUtil<List<Store>>().setData(list);
    }

    @RequestMapping(value = "/getByPage", method = RequestMethod.POST)
    @ApiOperation(value = "分页获取")
    public Result<IPage<StoreVO>> getByPage(@ModelAttribute PageVo page,HttpSession session, @RequestParam(value="procDefId") String procDefId){
        session.setAttribute("procDefId",procDefId);
        return new ResultUtil<IPage<StoreVO>>().setData(iStoreService.QueryStore(PageUtil.initMpPage(page),securityUtil.getCurrUser().getUsername()));
    }

    @RequestMapping(value = "/insertOrUpdate", method = RequestMethod.POST)
    @ApiOperation(value = "编辑或更新数据")
    public Result<Store> saveOrUpdate(Store store, HttpSession session){

        //获取当前登录人 ID
        User u = securityUtil.getCurrUser();
        store.setUserId(u.getId());
        //增加审核状态
        store.setStortStatus("0");
        store.setCompletionStatus("0");
        if(iStoreService.saveOrUpdate(store)){

            ActBusiness actBusiness = new ActBusiness();
            actBusiness.setProcDefId(session.getAttribute("procDefId").toString());
            actBusiness.setUserId(u.getId());
            store = iStoreService.getOne(Wrappers.<Store>lambdaQuery()
                    .orderByDesc(Store::getId).last("limit 1"));
            actBusiness.setTableId(store.getId());
            ActBusiness actBusinessUse = actBusinessService.save(actBusiness);
            store.setBussinessId(actBusinessUse.getId());
            iStoreService.updateById(store);


            return new ResultUtil<Store>().setData(store);
        }
        return new ResultUtil<Store>().setErrorMsg("操作失败");
    }

    @RequestMapping(value = "/delByIds", method = RequestMethod.DELETE)
    @ApiOperation(value = "批量通过id删除")
    public Result<Object> delAllByIds(String id){
        iStoreService.removeById(id);
        return new ResultUtil<Object>().setSuccessMsg("批量通过id删除数据成功");
    }




    @RequestMapping(value = "/storeDrawing", method = RequestMethod.POST)
    @ApiOperation(value = "设计部出门店图纸")
    public Result<Store> storeDrawing(Store store){
        //更新设计图纸
        if(ToolUtil.isNotEmpty(store)){
            Store s = iStoreService.getOne(Wrappers.<Store>lambdaQuery()
                    .orderByDesc(Store::getId).last("limit 1"));
            iStoreService.updateById(store);
            return new ResultUtil<Store>().setData(store);
        }
        return new ResultUtil<Store>().setErrorMsg("操作失败");
    }












}
