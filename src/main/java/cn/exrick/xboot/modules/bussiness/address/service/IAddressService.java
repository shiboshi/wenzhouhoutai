package cn.exrick.xboot.modules.bussiness.address.service;


import cn.exrick.xboot.modules.bussiness.address.pojo.Address;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;


/**
 * 地址接口
 * @author tqr
 */
public interface IAddressService extends IService<Address> {

    IPage<Address> getByCondition(String condition, Page page);

    IPage<Address> getByPage(Page page);

    List<Address> findByParentIdOrderBySortOrder(String parentId);
}