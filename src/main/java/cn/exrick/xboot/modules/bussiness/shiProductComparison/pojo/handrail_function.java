package cn.exrick.xboot.modules.bussiness.shiProductComparison.pojo;

import cn.exrick.xboot.base.XbootBaseEntity;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.Table;

/**
 * @author 石博士
 */
@Data
@Entity
@Table(name = "s_handrail_function")
@TableName("s_handrail_function")
@ApiModel(value = "扶手功能")
public class handrail_function extends XbootBaseEntity {

    private static final long serialVersionUID = 1L;


    @ApiModelProperty("手臂气压，手工输入")
    private String arm_pressure;

    @ApiModelProperty("扶手拆装，0没有，1有，2选配")
    private String disassembly_assembly;

    @ApiModelProperty("手臂牵引，0没有，1有，2选配")
    private String arm_traction;

    @ApiModelProperty("扶手快捷按键，0没有，1有，2选配")
    private String quick_button;

    @ApiModelProperty("手臂加热，0没有，1有，2选配")
    private String arm_heating;

    @ApiModelProperty("LED灯光，0没有，1有，2选配")
    private String led_lighting;

    @ApiModelProperty("USB接口，0没有，1有，2选配")
    private String usb_interface;








}