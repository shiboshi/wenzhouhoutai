package cn.exrick.xboot.modules.bussiness.shiProductComparison.controller;

import cn.exrick.xboot.common.utils.PageUtil;
import cn.exrick.xboot.common.utils.ResultUtil;
import cn.exrick.xboot.common.utils.SecurityUtil;
import cn.exrick.xboot.common.vo.PageVo;
import cn.exrick.xboot.common.vo.Result;
import cn.exrick.xboot.modules.base.entity.User;
import cn.exrick.xboot.modules.bussiness.product.pojo.ProductModel;
import cn.exrick.xboot.modules.bussiness.product.pojo.ProductOrder;
import cn.exrick.xboot.modules.bussiness.product.service.IproductModelService;
import cn.exrick.xboot.modules.bussiness.product.service.IproductOrderService;
import cn.exrick.xboot.modules.bussiness.shiProductComparison.pojo.*;
import cn.exrick.xboot.modules.bussiness.shiProductComparison.service.*;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpSession;
import java.util.List;

/**
 * @author 石博士
 */
@Slf4j
@RestController
@Api(description = "产品对比管理接口")
@RequestMapping("/xboot/s_ProductComparison")
@Transactional
public class s_ProductComparisonController {

    private HttpSession session;

    @Autowired
    private Is_ProductComparisonService is_ProductComparisonService;

    @Autowired
    private Ihead_functionService ihead_functionService;

    @Autowired
    private Ishoulder_functionService ishoulder_functionService;

    @Autowired
    private Iback_functionService iback_functionService;

    @Autowired
    private Ihip_functionService ihip_functionService;

    @Autowired
    private Ihandrail_functionService ihandrail_functionService;

    @Autowired
    private Icalf_functionService icalf_functionService;

    @Autowired
    private Iadditional_functionService iadditional_functionService;

    @Autowired
    private Ibasic_parameterService ibasic_parameterService;

    @Autowired
    private IproductOrderService iproductOrderService;

    @Autowired
    private IproductModelService iproductModelService;

    @Autowired
    private SecurityUtil securityUtil;


    @RequestMapping(value = "/get/{id}", method = RequestMethod.POST)
    @ApiOperation(value = "通过id获取")
    public Result<s_ProductComparison> get(@PathVariable String id){

        s_ProductComparison S_ProductComparison = is_ProductComparisonService.getById(id);
        return new ResultUtil<s_ProductComparison>().setData(S_ProductComparison);
    }

    @RequestMapping(value = "/getAll", method = RequestMethod.POST)
    @ApiOperation(value = "获取全部数据")
    public Result<List<s_ProductComparison>> getAll(){

        List<s_ProductComparison> list = is_ProductComparisonService.list();
        return new ResultUtil<List<s_ProductComparison>>().setData(list);
    }

    @RequestMapping(value = "/getByPage", method = RequestMethod.POST)
    @ApiOperation(value = "分页获取")
    public Result<IPage<productComparisonVo>> getByPage(@ModelAttribute PageVo page){
        return new ResultUtil<IPage<productComparisonVo>>().setData(is_ProductComparisonService.QueryProductComparisonVo(PageUtil.initMpPage(page)));
    }

    @RequestMapping(value = "/insertOrUpdate", method = RequestMethod.POST)
    @ApiOperation(value = "编辑或更新数据")
    public Result<s_ProductComparison> saveOrUpdate(@ModelAttribute s_ProductComparison S_ProductComparison, HttpSession session, ProductOrder productOrder){

        //保存头部功能id
        String headsId = session.getAttribute("headsId").toString();
        S_ProductComparison.setHeads(headsId);
        //保存肩部功能id
        String ShoulderId = session.getAttribute("ShoulderId").toString();
        S_ProductComparison.setShoulder(ShoulderId);
        //保存背部功能id
        String BacksId = session.getAttribute("BacksId").toString();
        S_ProductComparison.setBacks(BacksId);
        //保存臀部功能id
        String ButtocksId = session.getAttribute("ButtocksId").toString();
        S_ProductComparison.setButtocks(ButtocksId);
        //保存扶手功能id
        String HandrailId = session.getAttribute("HandrailId").toString();
        S_ProductComparison.setHandrail(HandrailId);
        //保存小腿功能id
        String CalfsId = session.getAttribute("CalfsId").toString();
        S_ProductComparison.setCalfs(CalfsId);
        //保存附加功能id
        String AppendsId = session.getAttribute("AppendsId").toString();
        S_ProductComparison.setAppends(AppendsId);
        //保存基本功能id
        String ParameterId = session.getAttribute("ParameterId").toString();
        S_ProductComparison.setBasicParameter(ParameterId);
        //保存产品品号表id
        S_ProductComparison.setProduct_order_id(productOrder.getProductOrderName());
        //保存备注
        S_ProductComparison.setRemarks(S_ProductComparison.getRemarks());
        S_ProductComparison.setDelFlag(0);
        S_ProductComparison.setProductModelName(S_ProductComparison.getProductModelName());
        if(is_ProductComparisonService.saveOrUpdate(S_ProductComparison)){
            return new ResultUtil<s_ProductComparison>().setData(S_ProductComparison);

        }
        return new ResultUtil<s_ProductComparison>().setErrorMsg("操作失败");
    }


    @RequestMapping(value = "/delByIds/{ids}", method = RequestMethod.DELETE)
    @ApiOperation(value = "批量通过id删除")
    public Result<Object> delAllByIds(@PathVariable String[] ids){

        for(String id : ids){
            is_ProductComparisonService.removeById(id);
        }
        return new ResultUtil<Object>().setSuccessMsg("批量通过id删除数据成功");
    }



    /**
     * 产品对比接口
     */
    @RequestMapping(value = "/QueryProductComparisonVoID", method = RequestMethod.POST)
    @ApiOperation(value = "产品对比接口")
    public Result<IPage<productComparisonVo>> QueryProductComparisonVoID(String id,@ModelAttribute PageVo page){
        IPage<productComparisonVo> list = is_ProductComparisonService.QueryProductComparisonVoIDList(PageUtil.initMpPage(page),id);
        return new ResultUtil<IPage<productComparisonVo>>().setData(list);
    }




    @RequestMapping(value = "/productComparisonAddHeads", method = RequestMethod.POST)
    @ApiOperation(value = "产品对比添加头部功能")
    public Result<head_function> productComparisonAddHeads(@ModelAttribute head_function Head_function, HttpSession session){
        Head_function.setDelFlag(0);
        if(ihead_functionService.saveOrUpdate(Head_function)){
            head_function headFunction = ihead_functionService
                    .getOne(Wrappers.<head_function>lambdaQuery()
                            .orderByDesc(head_function::getId)
                            .last("limit 1"));
            String id = headFunction.getId();
            //头部功能
            session.setAttribute("headsId",id);
            return new ResultUtil<head_function>().setData(Head_function);
        }
        return new ResultUtil<head_function>().setErrorMsg("操作失败");
    }



    @RequestMapping(value = "/productComparisonAddShoulder", method = RequestMethod.POST)
    @ApiOperation(value = "产品对比添加肩部功能")
    public Result<shoulder_function> productComparisonAddShoulder(@ModelAttribute shoulder_function Soulder_function, HttpSession session){
        Soulder_function.setDelFlag(0);
        if(ishoulder_functionService.saveOrUpdate(Soulder_function)){
            shoulder_function shoulderFunction = ishoulder_functionService
                    .getOne(Wrappers.<shoulder_function>lambdaQuery()
                            .orderByDesc(shoulder_function::getId)
                            .last("limit 1"));
            String id = shoulderFunction.getId();
            //肩部功能Id
            session.setAttribute("ShoulderId",id);
            return new ResultUtil<shoulder_function>().setData(Soulder_function);
        }
        return new ResultUtil<shoulder_function>().setErrorMsg("操作失败");
    }



    @RequestMapping(value = "/productComparisonAddBacks", method = RequestMethod.POST)
    @ApiOperation(value = "产品对比添加背部功能")
    public Result<back_function> productComparisonAddBacks(@ModelAttribute back_function Back_function, HttpSession session){
        Back_function.setDelFlag(0);
        if(iback_functionService.saveOrUpdate(Back_function)){
            back_function backFunction = iback_functionService
                    .getOne(Wrappers.<back_function>lambdaQuery()
                            .orderByDesc(back_function::getId)
                            .last("limit 1"));
            String id = backFunction.getId();
            //背部功能Id
            session.setAttribute("BacksId",id);
            return new ResultUtil<back_function>().setData(Back_function);
        }
        return new ResultUtil<back_function>().setErrorMsg("操作失败");
    }



    @RequestMapping(value = "/productComparisonAddButtocks", method = RequestMethod.POST)
    @ApiOperation(value = "产品对比添加臀部功能")
    public Result<hip_function> productComparisonAddButtocks(@ModelAttribute hip_function Hip_function, HttpSession session){
        Hip_function.setDelFlag(0);
        if(ihip_functionService.saveOrUpdate(Hip_function)){
            hip_function hipFunction = ihip_functionService
                    .getOne(Wrappers.<hip_function>lambdaQuery()
                            .orderByDesc(hip_function::getId)
                            .last("limit 1"));
            String id = hipFunction.getId();
            //背部功能Id
            session.setAttribute("ButtocksId",id);
            return new ResultUtil<hip_function>().setData(Hip_function);
        }
        return new ResultUtil<hip_function>().setErrorMsg("操作失败");
    }



    @RequestMapping(value = "/productComparisonAddHandrail", method = RequestMethod.POST)
    @ApiOperation(value = "产品对比添加扶手功能")
    public Result<handrail_function> productComparisonAddHandrail(@ModelAttribute handrail_function Handrail_function, HttpSession session){
        Handrail_function.setDelFlag(0);
        if(ihandrail_functionService.saveOrUpdate(Handrail_function)){
            handrail_function handrailFunction = ihandrail_functionService
                    .getOne(Wrappers.<handrail_function>lambdaQuery()
                            .orderByDesc(handrail_function::getId)
                            .last("limit 1"));
            String id = handrailFunction.getId();
            //背部功能Id
            session.setAttribute("HandrailId",id);
            return new ResultUtil<handrail_function>().setData(Handrail_function);
        }
        return new ResultUtil<handrail_function>().setErrorMsg("操作失败");
    }



    @RequestMapping(value = "/productComparisonAddCalfs", method = RequestMethod.POST)
    @ApiOperation(value = "产品对比添加小腿功能")
    public Result<calf_function> productComparisonAddCalfs(@ModelAttribute calf_function Calf_function, HttpSession session){
        Calf_function.setDelFlag(0);
        if(icalf_functionService.saveOrUpdate(Calf_function)){
            calf_function calfFunction = icalf_functionService
                    .getOne(Wrappers.<calf_function>lambdaQuery()
                            .orderByDesc(calf_function::getId)
                            .last("limit 1"));
            String id = calfFunction.getId();
            //背部功能Id
            session.setAttribute("CalfsId",id);
            return new ResultUtil<calf_function>().setData(Calf_function);
        }
        return new ResultUtil<calf_function>().setErrorMsg("操作失败");
    }



    @RequestMapping(value = "/productComparisonAddAppends", method = RequestMethod.POST)
    @ApiOperation(value = "产品对比添加附加功能")
    public Result<additional_function> productComparisonAddAppends(@ModelAttribute additional_function Additional_function, HttpSession session){
        Additional_function.setDelFlag(0);
        if(iadditional_functionService.saveOrUpdate(Additional_function)){
            additional_function additionalFunction = iadditional_functionService
                    .getOne(Wrappers.<additional_function>lambdaQuery()
                            .orderByDesc(additional_function::getId)
                            .last("limit 1"));
            String id = additionalFunction.getId();
            //背部功能Id
            session.setAttribute("AppendsId",id);
            return new ResultUtil<additional_function>().setData(Additional_function);
        }
        return new ResultUtil<additional_function>().setErrorMsg("操作失败");
    }



    @RequestMapping(value = "/productComparisonAddBasicParameter", method = RequestMethod.POST)
    @ApiOperation(value = "产品对比添加基本功能")
    public Result<basic_parameter> productComparisonAddBasicParameter(@ModelAttribute basic_parameter Basic_parameter, HttpSession session){
        Basic_parameter.setDelFlag(0);
        if(ibasic_parameterService.saveOrUpdate(Basic_parameter)){
            basic_parameter basicParameter = ibasic_parameterService
                    .getOne(Wrappers.<basic_parameter>lambdaQuery()
                            .orderByDesc(basic_parameter::getId)
                            .last("limit 1"));
            String id = basicParameter.getId();
            //背部功能Id
            session.setAttribute("ParameterId",id);
            return new ResultUtil<basic_parameter>().setData(Basic_parameter);
        }
        return new ResultUtil<basic_parameter>().setErrorMsg("操作失败");
    }




    @RequestMapping(value = "/getAllProductNumber", method = RequestMethod.POST)
    @ApiOperation(value = "查询产品品号")
    public Result<List<ProductOrder>> getAllProductNumber() {
        List<ProductOrder> list = iproductOrderService.list();
        return new ResultUtil<List<ProductOrder>>().setData(list);
    }



    @RequestMapping(value = "/getAllProductModel", method = RequestMethod.POST)
    @ApiOperation(value = "查询产品名称")
    public Result<List<ProductModel>> getAllProductModel() {

        List<ProductModel> list = iproductModelService.list();
        return new ResultUtil<List<ProductModel>>().setData(list);
    }



    @RequestMapping(value = "/deleteProductComparison", method = RequestMethod.DELETE)
    @ApiOperation(value = "删除产品对比数据")
    public Result<Object> deleteProductComparison(String id){
        //首先通过查询产品对比主表的所有数据
        s_ProductComparison productComparison = is_ProductComparisonService.getById(id);

        //删除头部功能
        ihead_functionService.removeById(productComparison.getHeads());

        //删除肩部功能
        ishoulder_functionService.removeById(productComparison.getShoulder());

        //删除背部功能
        iback_functionService.removeById(productComparison.getBacks());

        //删除臀部功能
        ihip_functionService.removeById(productComparison.getButtocks());

        //删除扶手功能
        ihandrail_functionService.removeById(productComparison.getHandrail());

        //删除小腿功能
        icalf_functionService.removeById(productComparison.getCalfs());

        //删除附加功能
        iadditional_functionService.removeById(productComparison.getAppends());

        //删除基本功能
        ibasic_parameterService.removeById(productComparison.getBasicParameter());

        //删除主表
        is_ProductComparisonService.removeById(productComparison.getId());

        return new ResultUtil<Object>().setSuccessMsg("删除数据成功");
    }



    @RequestMapping(value = "/getTheRegistrant", method = RequestMethod.POST)
    @ApiOperation(value = "获取登录人信息")
    public Result<User> getTheRegistrant(){
        return new ResultUtil<User>().setData(securityUtil.getCurrUser());
    }













}
