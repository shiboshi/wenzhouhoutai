package cn.exrick.xboot.modules.bussiness.shiProductComparison.controller;

import cn.exrick.xboot.common.utils.PageUtil;
import cn.exrick.xboot.common.utils.ResultUtil;
import cn.exrick.xboot.common.vo.PageVo;
import cn.exrick.xboot.common.vo.Result;
import cn.exrick.xboot.modules.bussiness.shiProductComparison.pojo.calf_function;
import cn.exrick.xboot.modules.bussiness.shiProductComparison.service.Icalf_functionService;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * @author 石博士
 */
@Slf4j
@RestController
@Api(description = "小腿功能表管理接口")
@RequestMapping("/xboot/calf_function")
@Transactional
public class calf_functionController {

    @Autowired
    private Icalf_functionService icalf_functionService;

    @RequestMapping(value = "/get/{id}", method = RequestMethod.GET)
    @ApiOperation(value = "通过id获取")
    public Result<calf_function> get(@PathVariable String id){

        calf_function calf_function = icalf_functionService.getById(id);
        return new ResultUtil<calf_function>().setData(calf_function);
    }

    @RequestMapping(value = "/getAll", method = RequestMethod.GET)
    @ApiOperation(value = "获取全部数据")
    public Result<List<calf_function>> getAll(){

        List<calf_function> list = icalf_functionService.list();
        return new ResultUtil<List<calf_function>>().setData(list);
    }

    @RequestMapping(value = "/getByPage", method = RequestMethod.GET)
    @ApiOperation(value = "分页获取")
    public Result<IPage<calf_function>> getByPage(@ModelAttribute PageVo page){

        IPage<calf_function> data = icalf_functionService.page(PageUtil.initMpPage(page));
        return new ResultUtil<IPage<calf_function>>().setData(data);
    }

    @RequestMapping(value = "/insertOrUpdate", method = RequestMethod.POST)
    @ApiOperation(value = "编辑或更新数据")
    public Result<calf_function> saveOrUpdate(@ModelAttribute calf_function calf_function){

        if(icalf_functionService.saveOrUpdate(calf_function)){
            return new ResultUtil<calf_function>().setData(calf_function);
        }
        return new ResultUtil<calf_function>().setErrorMsg("操作失败");
    }

    @RequestMapping(value = "/delByIds/{ids}", method = RequestMethod.DELETE)
    @ApiOperation(value = "批量通过id删除")
    public Result<Object> delAllByIds(@PathVariable String[] ids){

        for(String id : ids){
            icalf_functionService.removeById(id);
        }
        return new ResultUtil<Object>().setSuccessMsg("批量通过id删除数据成功");
    }
}
