package cn.exrick.xboot.modules.bussiness.product.mapper;

import cn.exrick.xboot.modules.bussiness.product.ProductProvider;
import cn.exrick.xboot.modules.bussiness.product.VO.ProductOrderVo;
import cn.exrick.xboot.modules.bussiness.unit.VO.UnitVO;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import cn.exrick.xboot.modules.bussiness.product.pojo.ProductOrder;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.SelectProvider;
import org.springframework.stereotype.Repository;


import java.util.List;

/**
 * 产品型号数据处理层
 * @author xiaofei
 */
@Repository
public interface ProductOrderMapper extends BaseMapper<ProductOrder> {

    @SelectProvider(type = ProductProvider.class,method = "getAllProduct")
    IPage<ProductOrderVo> getAllProduct(@Param("page") Page page,@Param("productModelName") String productModelName);

    @SelectProvider(type = ProductProvider.class,method = "getAllProductByStore")
    IPage<ProductOrderVo> getAllProductByStore(@Param("initMpPage")Page initMpPage,@Param("productModelName") String productModelName,@Param("idS") String[] idS,@Param("userId") String userId);

    @SelectProvider(type = ProductProvider.class,method = "getAllProductByDist")
    IPage<ProductOrderVo> getAllProductByDist(@Param("initMpPage")Page initMpPage, @Param("productModelName") String productModelName,@Param("idS") String[] idS,@Param("userId") String userId);

    @SelectProvider(type = ProductProvider.class,method = "getAllProductDetail")
    ProductOrderVo getAllProductDetail(@Param("id") String id);

    @SelectProvider(type = ProductProvider.class,method = "getProductUnit")
    IPage<UnitVO> getProductUnit(@Param("initMpPage")Page initMpPage,@Param("id") String id);
}