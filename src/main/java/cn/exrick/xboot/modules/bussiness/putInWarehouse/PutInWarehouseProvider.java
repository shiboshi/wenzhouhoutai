package cn.exrick.xboot.modules.bussiness.putInWarehouse;

import cn.exrick.xboot.common.utils.ToolUtil;
import org.apache.commons.lang3.StringUtils;

public class PutInWarehouseProvider {

    public String getPutInWarehouse(String productOrderName,String status) {
        String sql = "";
        String str = "";
        str += "SELECT " +
                " a.id, " +
                " a.create_time, " +
                " a. STATUS, " +
                " a.amount, " +
                " a.batch_number,r.title AS material,(" +
                " SELECT " +
                " title " +
                " FROM " +
                " t_color " +
                " WHERE " +
                " id = r.parent_id " +
                " AND del_flag = 0 " +
                " ) AS colorNumber, " +
                " ( " +
                " SELECT " +
                " title " +
                " FROM " +
                " t_color " +
                " WHERE " +
                " id = ( " +
                " SELECT " +
                " parent_id " +
                " FROM " +
                " t_color " +
                " WHERE " +
                " id = r.parent_id " +
                " AND del_flag = 0 " +
                " ) " +
                " AND del_flag = 0 " +
                " ) AS color, " +
                " l.title AS productOrderName, " +
                " ( " +
                " SELECT " +
                " title " +
                " FROM " +
                " t_classification " +
                " WHERE " +
                " id = l.parent_id " +
                " ) AS productModelName, " +
                " ( " +
                " SELECT " +
                " title " +
                " FROM " +
                " t_classification " +
                " WHERE " +
                " id = ( " +
                " SELECT " +
                " parent_id " +
                " FROM " +
                " t_classification " +
                " WHERE " +
                "  id = l.parent_id " +
                " ) " +
                " ) AS productName, " +
                " b.w_name AS warehouseName, " +
                " c.username AS userName, " +
                " d.title AS deptName " +
                "FROM " +
                " t_put_in_warehouse a " +
                "LEFT JOIN x_product_order p ON p.id = a.product_order_id and p.del_flag = 0 " +
                "LEFT JOIN t_color r ON p.color_id = r.id and r.del_flag = 0 " +
                "LEFT JOIN t_classification l on p.kind_id = l.id and l.del_flag = 0 " +
                "LEFT JOIN t_shi_warehouse b ON a.warehouse_id = b.id and b.del_flag = 0 " +
                "LEFT JOIN t_user c ON a.create_by = c.id and c.del_flag = 0 " +
                "LEFT JOIN t_department d ON c.department_id = d.id where a.del_flag = 0 ";
        if(StringUtils.isNotBlank(productOrderName)){
            str += " and l.title like '%"+productOrderName+"%'";
        }
        if(ToolUtil.isNotEmpty(status)){
            str += " and a. status = '"+status+"'";
        }
        sql += str;
        return sql;
    }

    public String getById(String id) {
        String sql = "";
        String str = "";
        str += "SELECT " +
                " a.id, " +
                " a.create_time,p.unit_weight,p.gross_weight,p.volume,u.unit_name as unitName, " +
                " a. STATUS, " +
                " a.amount, " +
                " a.batch_number,r.title AS material,(" +
                " SELECT " +
                " title " +
                " FROM " +
                " t_color " +
                " WHERE " +
                " id = r.parent_id " +
                " AND del_flag = 0 " +
                " ) AS colorNumber, " +
                " ( " +
                " SELECT " +
                " title " +
                " FROM " +
                " t_color " +
                " WHERE " +
                " id = ( " +
                " SELECT " +
                " parent_id " +
                " FROM " +
                " t_color " +
                " WHERE " +
                " id = r.parent_id " +
                " AND del_flag = 0 " +
                " ) " +
                " AND del_flag = 0 " +
                " ) AS color, " +
                " l.title AS productOrderName, " +
                " ( " +
                " SELECT " +
                " title " +
                " FROM " +
                " t_classification " +
                " WHERE " +
                " id = l.parent_id " +
                " ) AS productModelName, " +
                " ( " +
                " SELECT " +
                " title " +
                " FROM " +
                " t_classification " +
                " WHERE " +
                " id = ( " +
                " SELECT " +
                " parent_id " +
                " FROM " +
                " t_classification " +
                " WHERE " +
                "  id = l.parent_id " +
                " ) " +
                " ) AS productName, " +
                " b.w_name AS warehouseName, " +
                " c.username AS userName, " +
                " d.title AS deptName " +
                "FROM " +
                " t_put_in_warehouse a " +
                "LEFT JOIN x_product_order p ON p.id = a.product_order_id and p.del_flag = 0 " +
                "LEFT JOIN t_unit u on p.unit=u.id and u.del_flag = 0 " +
                "LEFT JOIN t_color r ON p.color_id = r.id and r.del_flag = 0 " +
                "LEFT JOIN t_classification l on p.kind_id = l.id and l.del_flag = 0 " +
                "LEFT JOIN t_shi_warehouse b ON a.warehouse_id = b.id and b.del_flag = 0 " +
                "LEFT JOIN t_user c ON a.create_by = c.id and c.del_flag = 0 " +
                "LEFT JOIN t_department d ON c.department_id = d.id where a.del_flag = 0 ";
        if(ToolUtil.isNotEmpty(id)){
            str += " and a.id = '"+id+"'";
        }
        sql += str;
        return sql;
    }
}
