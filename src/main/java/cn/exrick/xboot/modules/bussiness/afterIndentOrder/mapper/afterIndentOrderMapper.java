package cn.exrick.xboot.modules.bussiness.afterIndentOrder.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import cn.exrick.xboot.modules.bussiness.afterIndentOrder.pojo.AfterIndentOrder;
import org.springframework.stereotype.Repository;

/**
 * 订货单维修单数据处理层
 * @author xiaofei
 */
@Repository
public interface afterIndentOrderMapper extends BaseMapper<AfterIndentOrder> {

    String selectLast();

}