package cn.exrick.xboot.modules.bussiness.shiProductComparison.pojo;

import cn.exrick.xboot.base.XbootBaseEntity;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.Table;

/**
 * @author 石博士
 */
@Data
@Entity
@Table(name = "s_shoulder_function")
@TableName("s_shoulder_function")
@ApiModel(value = "肩部功能")
public class shoulder_function extends XbootBaseEntity {

    private static final long serialVersionUID = 1L;



    @ApiModelProperty("肩部气压，0没有，1有，2选配")
    private String shoulder_pressure;

    @ApiModelProperty("翻转式手臂器，0没有，1有，2选配")
    private String flip_arm;

    @ApiModelProperty("负氧离子，0没有，1有，2选配")
    private String negative_ion;

    @ApiModelProperty("肩部调节，0没有，1有，2选配")
    private String shoulder_adjustment;

    @ApiModelProperty("数字音响，0没有，1有，2选配")
    private String digital_sound;

    @ApiModelProperty("肩位置检测，0没有，1有，2选配")
    private String Shoulder_position;














}