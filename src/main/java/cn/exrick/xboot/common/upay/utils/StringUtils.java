package cn.exrick.xboot.common.upay.utils;

/**
 * TODO
 * 
 * @author jcouyang@chinaums.com
 * @date 2017年5月3日 上午10:38:01
 */
public final class StringUtils {
    
    public static boolean isEmpty(Object obj) {
        return null == obj || "".equals(obj);
    }
    
    /**
     * 检查指定的字符串列表是否不为空。
     */
    public static boolean areNotEmpty(String... values) {
        boolean result = true;
        if (values == null || values.length == 0) {
            result = false;
        } else {
            for (String value : values) {
                result &= !isEmpty(value);
            }
        }
        return result;
    }

    /**
     * 十六进制 转换 byte[]
     * 
     * @param hexStr
     * @return
     */
    public static byte[] hexString2Bytes(String hexStr) {
        if (hexStr == null)
            return null;
        if (hexStr.length() % 2 != 0) {
            hexStr += "0";
        }
        byte[] data = new byte[hexStr.length() / 2];
        for (int i = 0; i < hexStr.length() / 2; i++) {
            char hc = hexStr.charAt(2 * i);
            char lc = hexStr.charAt(2 * i + 1);
            byte hb = hexChar2Byte(hc);
            byte lb = hexChar2Byte(lc);
            if (hb < 0 || lb < 0) {
                return null;
            }
            int n = hb << 4;
            data[i] = (byte) (n + lb);
        }
        return data;
    }

    public static byte hexChar2Byte(char c) {
        if (c >= '0' && c <= '9')
            return (byte) (c - '0');
        if (c >= 'a' && c <= 'f')
            return (byte) (c - 'a' + 10);
        if (c >= 'A' && c <= 'F')
            return (byte) (c - 'A' + 10);
        return -1;
    }

    /**
     * byte[] 转 16进制字符串
     * 
     * @param arr
     * @return
     */
    public static String bytes2HexString(byte[] arr) {
        return bytes2HexString(arr, true);
    }
    
    /**
     * byte[] 转 16进制字符串
     * 
     * @param arr
     * @param upcase 是否大写
     * @return
     */
    public static String bytes2HexString(byte[] arr, boolean upcase) {
        StringBuilder sbd = new StringBuilder();
        for (byte b : arr) {
            String tmp = Integer.toHexString(0xFF & b);
            if (tmp.length() < 2)
                tmp = "0" + tmp;
            sbd.append(tmp);
        }
        
        if (upcase) {
            return sbd.toString().toUpperCase();            
        } 
        else {
            return sbd.toString();
        }
    }

    public static String separator() {
        return System.getProperty("file.separator");
    }

}
