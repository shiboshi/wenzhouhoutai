package cn.exrick.xboot.modules.bussiness.transferOrder.service;

import cn.exrick.xboot.modules.bussiness.inventoryOrder.VO.InventoryVO;
import cn.exrick.xboot.modules.bussiness.transferOrder.VO.TransferVO;
import com.baomidou.mybatisplus.extension.service.IService;
import cn.exrick.xboot.modules.bussiness.transferOrder.pojo.TransferOrder;

import java.util.List;
import java.util.Map;

/**
 * 调拨单接口
 * @author fei
 */
public interface ItransferOrderService extends IService<TransferOrder> {


    /**
     * 创建调拨单
     * @param transferOrder
     */
    void  insertTransferOrder(TransferOrder transferOrder);


    /**
     * 获取列表
     * @param transferOrder
     * @return
     */
    List<TransferVO> getList(TransferOrder transferOrder);


    /**
     * 获取详情
     * @param id
     * @return
     */
    List<InventoryVO> getDetail(String id,String userId);


    /**
     * 获取当前经销商仓库下面的子仓库
     * @return
     */
    List<Map<String,Object>> getWarhouseList();


    void arrival(String id);
}