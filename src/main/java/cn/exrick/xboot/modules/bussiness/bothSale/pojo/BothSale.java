package cn.exrick.xboot.modules.bussiness.bothSale.pojo;

import cn.exrick.xboot.base.XbootBaseEntity;
import cn.exrick.xboot.common.enums.CheckStatus;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModelProperty;
import io.swagger.annotations.ApiModel;
import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.Table;
import java.util.Date;

/**
 * @author tqr
 */
@Data
@Entity
@Table(name = "t_both_sale")
@TableName("t_both_sale")
@ApiModel(value = "两地销售")
public class BothSale extends XbootBaseEntity {

    private static final long serialVersionUID = 1L;


    /**
     * 申请状态
     */
    private CheckStatus checkCode;

    /**
     * 指定的经销商部门id
     */
    private String dealerDeptId;

    /**
     * 区域id
     */
    private String addressId;

    /**
     * 详细地址
     */
    private String address;

    /**
     * 经销商审核状态
     */
    private CheckStatus checkCodeDealer;

    /**
     * 产品id
     */
    private String productOrderIds;

    /**
     * 产品序列号
     */
    private String serialNumber;

    /**
     * 流程id
     */
    private String businessId;

    private String orderNumb;
}