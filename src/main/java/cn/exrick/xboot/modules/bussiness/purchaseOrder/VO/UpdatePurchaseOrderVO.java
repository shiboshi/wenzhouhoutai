package cn.exrick.xboot.modules.bussiness.purchaseOrder.VO;

import lombok.Data;

/**
 * 用户申请发货VO
 * @Author : xiaofei
 * @Date: 2019/8/10
 */
@Data
public class UpdatePurchaseOrderVO {

    /**
     * id
     */
    private String id;
    /**
     * 申请发货/退货数量
     */
    private Long dispatchNum;
}
