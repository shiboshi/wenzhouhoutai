package cn.exrick.xboot.common.vo;

import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import java.util.Date;

/**
 * 公共VO封装
 * @Author : xiaofei
 * @Date: 2019/8/10
 */
@Data
public class CommonVO {

    @ApiModelProperty("id")
    private String id;

    @ApiModelProperty("经销商姓名")
    private String userName;

    @ApiModelProperty("经销商部门")
    private String userDept;

    @ApiModelProperty("经销商等级")
    private String userLevel;

    @JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd HH:mm:ss")
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @ApiModelProperty("创建时间")
    private Date createTime;
}
