package cn.exrick.xboot.modules.bussiness.packageSize.controller;

import cn.exrick.xboot.common.utils.PageUtil;
import cn.exrick.xboot.common.utils.ResultUtil;
import cn.exrick.xboot.common.vo.PageVo;
import cn.exrick.xboot.common.vo.Result;
import cn.exrick.xboot.modules.bussiness.packageSize.VO.PackageSizeVO;
import cn.exrick.xboot.modules.bussiness.packageSize.pojo.PackageSize;
import cn.exrick.xboot.modules.bussiness.packageSize.service.IPackageSizeService;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * @author tqr
 */
@Slf4j
@RestController
@Api(description = "包装尺寸管理接口")
@RequestMapping("/xboot/packageSize")
@Transactional
public class PackageSizeController {

    @Autowired
    private IPackageSizeService iPackageSizeService;

    @RequestMapping(value = "/get/{id}", method = RequestMethod.GET)
    @ApiOperation(value = "通过id获取")
    public Result<PackageSize> get(@PathVariable String id){

        PackageSize packageSize = iPackageSizeService.getById(id);
        return new ResultUtil<PackageSize>().setData(packageSize);
    }

    @RequestMapping(value = "/getAll", method = RequestMethod.GET)
    @ApiOperation(value = "获取全部数据")
    public Result<List<PackageSize>> getAll(){

        List<PackageSize> list = iPackageSizeService.list();
        return new ResultUtil<List<PackageSize>>().setData(list);
    }

    @RequestMapping(value = "/getByPage", method = RequestMethod.GET)
    @ApiOperation(value = "分页获取")
    public Result<IPage<PackageSize>> getByPage(@ModelAttribute PageVo page){

        IPage<PackageSize> data = iPackageSizeService.page(PageUtil.initMpPage(page));
        return new ResultUtil<IPage<PackageSize>>().setData(data);
    }

    @RequestMapping(value = "/insertOrUpdate", method = RequestMethod.POST)
    @ApiOperation(value = "编辑或更新数据")
    public Result<PackageSize> saveOrUpdate(@ModelAttribute PackageSize packageSize){

        if(iPackageSizeService.saveOrUpdate(packageSize)){
            return new ResultUtil<PackageSize>().setData(packageSize);
        }
        return new ResultUtil<PackageSize>().setErrorMsg("操作失败");
    }

    @RequestMapping(value = "/delByIds/{ids}", method = RequestMethod.DELETE)
    @ApiOperation(value = "批量通过id删除")
    public Result<Object> delAllByIds(@PathVariable String[] ids){

        for(String id : ids){
            iPackageSizeService.removeById(id);
        }
        return new ResultUtil<Object>().setSuccessMsg("批量通过id删除数据成功");
    }

    @RequestMapping(value = "/getPackageSize", method = RequestMethod.POST)
    @ApiOperation(value = "分页获取")
    public Result<IPage<PackageSizeVO>> getPackageSize(PageVo page, Integer productModelName){
        String condition;
        if (productModelName == null) {
            condition = "";
        } else {
            condition = productModelName.toString();
        }
        IPage<PackageSizeVO> data = iPackageSizeService.getPackageSize(PageUtil.initMpPage(page), condition);
        return new ResultUtil<IPage<PackageSizeVO>>().setData(data);
    }

    @RequestMapping(value = "/getByProductId", method = RequestMethod.POST)
    @ApiOperation(value = "分页获取")
    public Result<IPage<PackageSizeVO>> getByProductId(@ModelAttribute PageVo page, String id){

        IPage<PackageSizeVO> data = iPackageSizeService.getByProductId(PageUtil.initMpPage(page),id);
        return new ResultUtil<IPage<PackageSizeVO>>().setData(data);
    }



    @RequestMapping(value = "/getPackageSizeList", method = RequestMethod.POST)
    @ApiOperation(value = "通过产品品号的id获取多个包装的尺寸")
    public Result<List<PackageSize>> getPackageSizeList(String id){

        QueryWrapper<PackageSize> packageSize = new QueryWrapper<>();
        packageSize.eq("product_order_id",id);
        List<PackageSize> list = iPackageSizeService.list(packageSize);
        return new ResultUtil<List<PackageSize>>().setData(list);
    }







}
