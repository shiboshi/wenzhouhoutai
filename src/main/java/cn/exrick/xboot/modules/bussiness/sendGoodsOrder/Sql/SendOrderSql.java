package cn.exrick.xboot.modules.bussiness.sendGoodsOrder.Sql;

import cn.exrick.xboot.common.utils.ToolUtil;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.jdbc.SQL;
import org.hibernate.annotations.OrderBy;

import java.util.Map;

/**
 * @Author : xiaofei
 * @Date: 2019/8/21
 */
public class SendOrderSql {

    /*
    获取发货单列表
     */
    public String selectList(String orderNum, String userId, String code,Integer type,String serial) {
        return new SQL() {{
            SELECT("s.id as id,s.order_num as orderNum,s.application_send_goods_id as applicationId,s.create_by,s.create_time,d.title as deptName," +
                    "s.comment as comment,(case s.is_complete WHEN 0 THEN '未完成' WHEN 1 THEN  '已完成' END) as isComplete,s.create_time as  createTime" +
                    ",s.product_pic as productPic,s.telephone as telephone,s.arrival_time as arrivalTime");
            FROM("x_send_goods_order s");
            LEFT_OUTER_JOIN("t_user u  on s.create_by = u.id");
            LEFT_OUTER_JOIN("t_department d on u.department_id =d.id");

            if (type == 1) {
                if (ToolUtil.isNotEmpty(serial)){
                    WHERE("locate(" + serial + ",inventory_order_ids) >0");
                }
            }
            if (ToolUtil.isNotEmpty(code)) {
                WHERE(" s.is_complete = '" + code + "' ");
            }
            if (type == 2){
                if (ToolUtil.isNotEmpty(userId)) {
                    WHERE("s.create_by = " + userId + " ");
                }
            }
            WHERE("s.del_flag = 0");
            ORDER_BY("s.create_time  desc");
        }}.toString();
    }


   /* public String selectGoodOrderListByApplicationId(String applicationSendGoodsId) {
        return new SQL() {{
            SELECT("s.id as id,s.order_num as orderNum,s.application_send_goods_id as applicationId,s.create_by,s.create_time," +
                    "s.comment as comment,s.create_time as  createTime" +
                    ",s.product_pic as productPic,s.telephone as telephone,s.arrival_time as arrivalTime");
            FROM("x_send_goods_order s");
            WHERE("s.application_send_goods_id = '" + applicationSendGoodsId + "'");
            ORDER_BY("s.create_time  desc");
        }}.toString();
    }*/


    public String getById(String id) {
        return new SQL() {{
            SELECT("s.id as id,s.order_num as orderNum,s.create_by,s.create_time,d.title as deptName, " +
                    "s.logistics as logistics,s.comment as comment,(case s.is_complete WHEN 0 THEN '未完成' WHEN 1 THEN  '已完成' END) as isComplete,s.create_time as  createTime");
            FROM("x_send_goods_order s");
            LEFT_OUTER_JOIN("t_user u  on s.create_by = u.id");
            LEFT_OUTER_JOIN("t_department d on u.department_id =d.id");
            WHERE("s.del_flag = 0");
            WHERE("s.id = '" + id + "'");

            ORDER_BY("s.create_time  desc");
        }}.toString();
    }
}
