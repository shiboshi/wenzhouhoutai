package cn.exrick.xboot.modules.bussiness.distributorIndex.pojo;

import cn.exrick.xboot.base.XbootBaseEntity;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModelProperty;
import io.swagger.annotations.ApiModel;
import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.Table;

/**
 * @author tqr
 */
@Data
@Entity
@Table(name = "t_distributor_index")
@TableName("t_distributor_index")
@ApiModel(value = "经销商指标")
public class DistributorIndex extends XbootBaseEntity {

    private static final long serialVersionUID = 1L;

    private String deptId;

    private String monthlyId;

}