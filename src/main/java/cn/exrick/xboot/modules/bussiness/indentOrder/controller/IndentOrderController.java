package cn.exrick.xboot.modules.bussiness.indentOrder.controller;

import cn.exrick.xboot.common.utils.PageUtil;
import cn.exrick.xboot.common.utils.ResultUtil;
import cn.exrick.xboot.common.utils.SecurityUtil;
import cn.exrick.xboot.common.utils.ToolUtil;
import cn.exrick.xboot.common.vo.PageVo;
import cn.exrick.xboot.common.vo.Result;
import cn.exrick.xboot.modules.base.service.UserService;
import cn.exrick.xboot.modules.bussiness.channel.pojo.Channel;
import cn.exrick.xboot.modules.bussiness.channel.service.IChannelService;
import cn.exrick.xboot.modules.bussiness.dealerLevel.pojo.DealerLevel;
import cn.exrick.xboot.modules.bussiness.dealerLevel.service.IDealerLevelService;
import cn.exrick.xboot.modules.bussiness.indentOrder.VO.IndentOrderDetailVO;
import cn.exrick.xboot.modules.bussiness.indentOrder.VO.IndentOrderVO;
import cn.exrick.xboot.modules.bussiness.indentOrder.VO.InsertVO;
import cn.exrick.xboot.modules.bussiness.indentOrder.pojo.IndentOrder;
import cn.exrick.xboot.modules.bussiness.indentOrder.service.IindentOrderService;
import cn.exrick.xboot.modules.bussiness.product.pojo.ProductOrder;
import cn.exrick.xboot.modules.bussiness.product.service.IproductOrderService;
import cn.exrick.xboot.modules.bussiness.purchaseOrder.VO.PurchaseOrderVO;
import cn.exrick.xboot.modules.bussiness.purchaseOrder.pojo.PurchaseOrder;
import cn.exrick.xboot.modules.bussiness.purchaseOrder.service.IpurchaseOrderService;
import cn.exrick.xboot.modules.bussiness.unit.pojo.Unit;
import cn.exrick.xboot.modules.bussiness.unit.service.IUnitService;
import com.alibaba.fastjson.JSON;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.google.common.collect.Maps;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.lang.Nullable;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * @author xiaofei
 */
@Slf4j
@RestController
@Api(description = "订货单管理接口")
@RequestMapping("/xboot/indentOrder")
@Transactional
public class IndentOrderController {

    @Autowired
    private IindentOrderService iindentOrderService;
    @Autowired
    private SecurityUtil securityUtil;
    @Autowired
    private IDealerLevelService dealerLevelService;
    @Autowired
    private UserService userService;
    @Autowired
    private IChannelService iChannelService;

    /**
     * 单位表
     */
    @Autowired
    private IUnitService iUnitService;

    /**
     * 采购单
     */
    @Autowired
    private IpurchaseOrderService ipurchaseOrderService;

    /**
     * 产品表
     */
    @Autowired
    private IproductOrderService iproductOrderService;


    /**
     * 获取全部订单数据
     * @param indentOrder
     * @param pageVo
     * @return
     */
    @PostMapping("/getList")
    @ApiOperation(value = "获取全部数据")
    @Nullable
    public Result<Map<String,Object>>getList(String userId,IndentOrder indentOrder, PageVo pageVo,Integer code){

        Page<IndentOrderVO> page = iindentOrderService.getAll(indentOrder,pageVo,code,userId);

        Map<String,Object> map = Maps.newHashMap();
        //获取经销商的收货地址
        String deptId = null;
        if(ToolUtil.isEmpty(userId)){
            deptId = securityUtil.getCurrUser().getDepartmentId();
        } else {
            deptId = userService.get(userId).getDepartmentId();
        }
        map.put("page",page);
        DealerLevel dealerLevel = dealerLevelService.getOne(Wrappers.<DealerLevel>lambdaQuery()
                .eq(DealerLevel::getDeptId,deptId)
                .last("limit 1"));
        if (ToolUtil.isNotEmpty(dealerLevel)){
            map.put("address",dealerLevel.getAddress());
        }
        return new ResultUtil<Map<String,Object>>().setData(map);
    }

    @PostMapping("/selectById")
    @ApiOperation(value = "获取全部数据")
    @Nullable
    public Result<Map<String,Object>>selectById(String userId,String id){

        IndentOrderVO page = iindentOrderService.selectById(id);
        List<IndentOrderVO> list = new ArrayList<>();
        list.add(page);
        Map<String,Object> map = Maps.newHashMap();
        //获取经销商的收货地址
        String deptId = null;
        if(ToolUtil.isEmpty(userId)){
            deptId = securityUtil.getCurrUser().getDepartmentId();
        } else {
            deptId = userService.get(userId).getDepartmentId();
        }
        map.put("page",list);
        DealerLevel dealerLevel = dealerLevelService.getOne(Wrappers.<DealerLevel>lambdaQuery()
                .eq(DealerLevel::getDeptId,deptId));
        if (ToolUtil.isNotEmpty(dealerLevel)){
            map.put("address",dealerLevel.getAddress());
        }
        //查詢渠道
        List<Channel> channelList = iChannelService.list(new QueryWrapper<Channel>());
        map.put("chanel",channelList);
        return new ResultUtil<Map<String,Object>>().setData(map);
    }

    /**
     * 获取订单购买详情
     * @param id
     * @returngetDetail
     */
    @PostMapping("/getDetail")
    @ApiOperation(value = "通过id获取产品详情")
    public Result<List<IndentOrderDetailVO>> getDetail(@ModelAttribute PageVo pageVo, String id){
        List<PurchaseOrderVO> indentOrderDetailVOS = iindentOrderService.getIndentOrderDetail(id);
       for (PurchaseOrderVO p : indentOrderDetailVOS){
           IndentOrder indentOrder = iindentOrderService.getById(id);
           p.setIndentOrder(indentOrder);

           /*Unit unit = iUnitService.getById(p.getUnitName());*/
           /**
            * 最小单位
            */
          /* p.setUnitMins(unit.getUnitName());*/
           if(p.getUnitName()!=null){
               p.setUnitMins(p.getUnitName());
           }

           /**
            * 最大单位
            */
           //根据主键id 查询出 purchase_order_ids
           PurchaseOrder purchaseOrder = ipurchaseOrderService.getById(indentOrder.getPurchaseOrderIds());
            if (null!=purchaseOrder){

               System.out.println("----------getProductId---------"+purchaseOrder.getProductId());

               //根据purchase_order_ids 查询出 主键 product_id
               ProductOrder productOrder = iproductOrderService.getById(purchaseOrder.getProductId());

                //最后查询最大单位
                Unit unitMaxs = iUnitService.getById(productOrder.getUnitMax());

                //最低订货数量
                p.setMinOrderAmount(productOrder.getMinOrderAmount());

                //存入上市时间
                p.setTimeToMarket(productOrder.getTimeToMarket());

                //存入最大单位
                p.setUnitMaxs(unitMaxs.getUnitName());
            }
       }

        return new ResultUtil<List<IndentOrderDetailVO>>().setData(PageUtil.listToPage(pageVo,indentOrderDetailVOS));
    }

    /**
     * 插入订货单
     * @param insertVO
     * @return
     */
    @ApiOperation(value = "插入订货单")
    @PostMapping("/insertIndentOrder")
    public Result<Object> insertIndentOrder(@ModelAttribute InsertVO insertVO,String userId){

//        String user = ToolUtil.isEmpty(userId)?securityUtil.getCurrUser().getId():userId;
        List<PurchaseOrder> purchaseOrders = JSON.parseArray(insertVO.getParams(),PurchaseOrder.class);
//        BigDecimal totalAmout = new BigDecimal(purchaseOrders.stream().mapToDouble(p -> p.getPrice().multiply(p.getOrderNumbers()).setScale(2,BigDecimal.ROUND_HALF_UP).doubleValue()).sum());
//
//        DealerLevel dealerLevel = dealerLevelService.getOne(Wrappers.<DealerLevel>lambdaQuery()
//        .eq(DealerLevel::getDeptId,userService.get(user).getDepartmentId()));
//
//        if (insertVO.getPayType().equals(PayType.BLANCE.getValue())){
//            if (dealerLevel.getBalance().compareTo(totalAmout) == -1){
//                return new ResultUtil<>().setErrorMsg(201,"账户余额不足，请走代充值渠道！！！");
//            }
//        }else if (insertVO.getPayType().equals(PayType.CRIDIT.getValue())){
//            if (dealerLevel.getCredit().compareTo(totalAmout) == -1){
//                return new ResultUtil<>().setErrorMsg(202,"信用额度不足，请走代充值渠道!!! ");
//            }
//        }
        iindentOrderService.insertIndentOrder(purchaseOrders,insertVO.getProcDefId(),userId,insertVO.getPayType(),insertVO.getIndentOrderType());
        return new ResultUtil<>().setSuccessMsg("插入订单成功！");
    }


    @ApiOperation("获取审核流程中的表单数据")
    @PostMapping("/getCheckItem")
    public Result<IndentOrderVO> getCheckItem(String indentOrderId){
        if (ToolUtil.isEmpty(indentOrderId)){
            return new ResultUtil<IndentOrderVO>().setErrorMsg("插入id为空！");
        }else {
            IndentOrderVO indentOrderVO = iindentOrderService.formItem(indentOrderId);
            return new ResultUtil<IndentOrderVO>().setData(indentOrderVO,"查询成功!");
        }
    }
    /**
     * 删除订货单
     * @param id
     * @return
     */
    @PostMapping("/delAllByIds")
    @ApiOperation(value = "批量通过id删除")
    public Result<Object> delAllByIds(String id){
        iindentOrderService.removeById(id);
        return new ResultUtil<Object>().setSuccessMsg("批量通过id删除数据成功");
    }

    /**
     * 修改订货单
     * @param insertVO
     * @return
     */
    @ApiOperation(value = "修改订货单")
    @PostMapping("/editIndentOrder")
    public Result<Object> editIndentOrder(@ModelAttribute InsertVO insertVO,String userId,String id){
        List<PurchaseOrder> purchaseOrders = JSON.parseArray(insertVO.getParams(),PurchaseOrder.class);
        Integer payType = null;
        if(ToolUtil.isNotEmpty(insertVO.getPayType())){
            payType = insertVO.getPayType();
        }
        iindentOrderService.editIndentOrder(purchaseOrders,insertVO.getProcDefId(),userId,id,payType);
        return new ResultUtil<>().setSuccessMsg("插入订单成功！");
    }
}
