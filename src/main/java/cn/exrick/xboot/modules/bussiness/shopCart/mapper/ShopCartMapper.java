package cn.exrick.xboot.modules.bussiness.shopCart.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import cn.exrick.xboot.modules.bussiness.shopCart.pojo.ShopCart;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

/**
 * 购物车数据处理层
 * @author tqr
 */
@Mapper
public interface ShopCartMapper extends BaseMapper<ShopCart> {

}