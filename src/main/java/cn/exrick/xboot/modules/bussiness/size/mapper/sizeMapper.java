package cn.exrick.xboot.modules.bussiness.size.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import cn.exrick.xboot.modules.bussiness.size.pojo.size;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * 产品尺寸数据处理层
 * @author tqr
 */
@Mapper
public interface sizeMapper extends BaseMapper<size> {

}