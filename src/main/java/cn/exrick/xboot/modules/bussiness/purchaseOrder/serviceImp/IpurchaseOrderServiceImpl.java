package cn.exrick.xboot.modules.bussiness.purchaseOrder.serviceImp;

import cn.exrick.xboot.common.constant.CommonConstant;
import cn.exrick.xboot.common.enums.IsComplete;
import cn.exrick.xboot.common.enums.OrderType;
import cn.exrick.xboot.common.utils.CommonUtil;
import cn.exrick.xboot.common.utils.PageUtil;
import cn.exrick.xboot.common.utils.SecurityUtil;
import cn.exrick.xboot.common.utils.ToolUtil;
import cn.exrick.xboot.common.vo.PageVo;
import cn.exrick.xboot.modules.bussiness.applicationReturnGoodsOrder.pojo.ApplicationReturnGoodsOrder;
import cn.exrick.xboot.modules.bussiness.applicationReturnGoodsOrder.service.IApplicationReturnGoodsOrderService;
import cn.exrick.xboot.modules.bussiness.applicationSendGoodsOrder.pojo.ApplicationSendGoodsOrder;
import cn.exrick.xboot.modules.bussiness.applicationSendGoodsOrder.service.IapplicationSendGoodsOrderService;
import cn.exrick.xboot.modules.bussiness.channel.service.IChannelService;
import cn.exrick.xboot.modules.bussiness.classification.pojo.Classification;
import cn.exrick.xboot.modules.bussiness.classification.service.IClassificationService;
import cn.exrick.xboot.modules.bussiness.indentOrder.pojo.IndentOrder;
import cn.exrick.xboot.modules.bussiness.indentOrder.service.IindentOrderService;
import cn.exrick.xboot.modules.bussiness.packageSize.VO.PackageSizeVO;
import cn.exrick.xboot.modules.bussiness.packageSize.mapper.PackageSizeMapper;
import cn.exrick.xboot.modules.bussiness.packageSize.service.IPackageSizeService;
import cn.exrick.xboot.modules.bussiness.product.VO.ProductCommon;
import cn.exrick.xboot.modules.bussiness.product.VO.ProductOrderVo;
import cn.exrick.xboot.modules.bussiness.product.mapper.ProductOrderMapper;
import cn.exrick.xboot.modules.bussiness.product.mapper.productModelMapper;
import cn.exrick.xboot.modules.bussiness.product.pojo.ProductModel;
import cn.exrick.xboot.modules.bussiness.product.service.IproductModelService;
import cn.exrick.xboot.modules.bussiness.product.service.IproductOrderService;
import cn.exrick.xboot.modules.bussiness.purchaseOrder.VO.PurchaseOrderVO;
import cn.exrick.xboot.modules.bussiness.purchaseOrder.mapper.purchaseOrderMapper;
import cn.exrick.xboot.modules.bussiness.purchaseOrder.pojo.PurchaseOrder;
import cn.exrick.xboot.modules.bussiness.purchaseOrder.service.IpurchaseOrderService;
import cn.exrick.xboot.modules.bussiness.saleOrder.pojo.SalesOrder;
import cn.exrick.xboot.modules.bussiness.saleOrder.service.IsalesOrderService;
import cn.exrick.xboot.modules.bussiness.specialPriceSalesOrder.mapper.specialPriceSalesOrderMapper;
import cn.exrick.xboot.modules.bussiness.specialPriceSalesOrder.pojo.SpecialPriceSalesOrder;
import cn.exrick.xboot.modules.bussiness.specialPriceSalesOrder.service.IspecialPriceSalesOrderService;
import cn.exrick.xboot.modules.bussiness.unit.service.IUnitService;
import cn.hutool.core.bean.BeanUtil;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.google.common.base.Joiner;
import com.google.common.base.Splitter;
import com.google.common.collect.Lists;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.lang.Nullable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.util.List;
import java.util.stream.Collectors;

/**
 * 采购单接口实现
 *
 * @author xiaofei
 */
@Slf4j
@Service
@Transactional
public class IpurchaseOrderServiceImpl extends ServiceImpl<purchaseOrderMapper, PurchaseOrder> implements IpurchaseOrderService {
    @Autowired
    private IindentOrderService indentOrderService;
    @Autowired
    private IapplicationSendGoodsOrderService applicationSendGoodsOrderService;
    @Autowired
    private IUnitService unitService;
    @Autowired
    private IChannelService channelService;
    @Autowired
    private IPackageSizeService packageSizeService;
    @Autowired
    private IproductOrderService productOrderService;
    @Autowired
    private IsalesOrderService salesOrderService;
    @Autowired
    private SecurityUtil securityUtil;
    @Autowired
    private PackageSizeMapper packageSizeMapper;
    @Autowired
    private IspecialPriceSalesOrderService ispecialPriceSalesOrderService;
    @Autowired
    private IClassificationService classificationService;
    @Autowired
    private IApplicationReturnGoodsOrderService iApplicationReturnGoodsOrderService;
    @Autowired
    private IproductModelService productModelService;

    /**
     * 拆字符串
     */
    private static final Splitter SPLITTER = Splitter.on(",").trimResults().omitEmptyStrings();
    private static final Joiner JOINER = Joiner.on(",").skipNulls();

    @Override
    public List<String> getPurchaseIds(OrderType orderType, String id) {

        if (!ToolUtil.isAllEmpty(orderType, id)) {
            switch (orderType.getType()) {
                case 1:
                    IndentOrder indentOrder = indentOrderService.getById(id);
                    if (ToolUtil.isNotEmpty(indentOrder)) {
                        List<String> ids = Lists.newArrayList(SPLITTER.split(indentOrder.getPurchaseOrderIds()));
                        return ids;
                    }
                    break;
                case 2:
                    ApplicationSendGoodsOrder applicationSendGoodsOrder = applicationSendGoodsOrderService.getById(id);
                    if (ToolUtil.isNotEmpty(applicationSendGoodsOrder)) {
                        List<String> ids = Lists.newArrayList(SPLITTER.split(applicationSendGoodsOrder.getPurchaseOrderIds()));
                        return ids;
                    }
                    break;
                case 3:
                    SalesOrder salesOrder = salesOrderService.getById(id);
                    if (ToolUtil.isNotEmpty(salesOrder)) {
                        List<String> ids = Lists.newArrayList(SPLITTER.split(salesOrder.getPurchaseOrderIds()));
                        return ids;
                    }
                case 4:
                    SpecialPriceSalesOrder specialPriceSalesOrder = ispecialPriceSalesOrderService.getById(id);
                    if (ToolUtil.isNotEmpty(specialPriceSalesOrder)) {
                        List<String> ids = Lists.newArrayList(SPLITTER.split(specialPriceSalesOrder.getPurchaseOrderIds()));
                        return ids;
                    }
                case 5:
                    ApplicationReturnGoodsOrder applicationReturnGoodsOrder = iApplicationReturnGoodsOrderService.getById(id);
                    if (ToolUtil.isNotEmpty(applicationReturnGoodsOrder)) {
                        List<String> ids = Lists.newArrayList(SPLITTER.split(applicationReturnGoodsOrder.getPurchaseOrderIds()));
                        return ids;
                    }
                default:
                    return null;
            }
        }
        return null;
    }

    @Override
    public List<PurchaseOrderVO> getPurchaseList(OrderType orderType, String id) {
        List<String> ids = getPurchaseIds(orderType, id);
        if (ToolUtil.isNotEmpty(ids)) {
            List<PurchaseOrder> purchaseOrders = this.list(Wrappers.<PurchaseOrder>lambdaQuery()
                    .in(true, PurchaseOrder::getId, ids));
            List<PurchaseOrderVO> purchaseOrderVOS = purchaseOrders.stream()
                    .filter(purchase -> ToolUtil.isNotEmpty(purchase))
                    .map(purchase -> {
                        PurchaseOrderVO purchaseOrderVO = new PurchaseOrderVO();
                        ProductCommon productCommon = productOrderService.getProductCommon(purchase.getProductId());
                        ToolUtil.copyProperties(purchase, purchaseOrderVO);
                        ToolUtil.copyProperties(productCommon, purchaseOrderVO);
                        purchaseOrderVO.setId(purchase.getId());
                        if(!OrderType.salsOrder.equals(orderType)){
                            purchaseOrderVO.setUnitName(purchase.getUnitId());
                        }
                        purchaseOrderVO.setIsComplete(IsComplete.PENDING);
                        purchaseOrderVO.setRemark(purchaseOrderVO.getRemark());
                        //设置渠道
                        if (ToolUtil.isNotEmpty(channelService.getById(purchase.getChannel()))) {
                            purchaseOrderVO.setChannelName(channelService.getById(purchase.getChannel())
                                    .getChannelName());
                        }
                        //获取产品对内型号
                        Classification classification = classificationService.getById(productCommon.getKindId());
                        if (ToolUtil.isNotEmpty(classification)){
                            ProductModel productModel = productModelService.getOne(Wrappers.<ProductModel>lambdaQuery()
                            .eq(ProductModel::getKindId,classification.getParentId()) );
                            if (ToolUtil.isNotEmpty(productModel)){
                                purchaseOrderVO.setModelNameIn(productModel.getModelNameIn());
                            }
                        }
                        //包装尺寸
                        PageVo pv = new PageVo();
                        Page initMpPage = PageUtil.initMpPage(pv);
                        IPage<PackageSizeVO> psList = packageSizeMapper.getByProductId(initMpPage, purchase.getProductId());
                        List<PackageSizeVO> psVoList = psList.getRecords();
                        StringBuilder info = new StringBuilder();
                        for (PackageSizeVO ps : psVoList) {
                            if (ToolUtil.isNotEmpty(ps)) {
                                    info.append(ps.getLength()).append("*").append(ps.getWide()).append("*").append(ps.getTall()).append("\n");
                            }
                        }
                        purchaseOrderVO.setPackageSize(info.toString());

                        return purchaseOrderVO;
                    }).collect(Collectors.toList());
            return purchaseOrderVOS;
        }
        return null;
    }

    @Override
    @Transactional(propagation = Propagation.SUPPORTS)
    @Nullable
    public String getPurchaseIds(List<PurchaseOrder> purchaseOrders,String userId) {

        String userid = ToolUtil.isNotEmpty(userId)?userId:securityUtil.getCurrUser().getId();
        //拼接字符串
        List<String> ids = Lists.newArrayList();
        if (purchaseOrders != null && purchaseOrders.size() > 0) {
            purchaseOrders.forEach(purchaseOrder -> {
                if (ToolUtil.isNotEmpty(purchaseOrder.getPrice())) {
                    purchaseOrder.setOrderAmount(purchaseOrder.getOrderNumbers().multiply(purchaseOrder.getPrice())
                            .setScale(2, BigDecimal.ROUND_HALF_UP));
                }
                //满减活动，如果用户当前商品的订购数大于 10件，就会赠送一件
                /*if (purchaseOrder.getOrderNumbers().compareTo(new BigDecimal(10)) == 1) {
                    purchaseOrder.setOrderNumbers(purchaseOrder.getOrderNumbers().add(new BigDecimal(1)));
                    purchaseOrder.setRemark(CommonConstant.HUODONG);
                }*/
                purchaseOrder.setHavaSend(new BigDecimal(0L));
                purchaseOrder.setNotSend(purchaseOrder.getOrderNumbers());
                purchaseOrder.setCreateBy(userid);
                this.save(purchaseOrder);
                //找到刚插入的数据
                PurchaseOrder purchaseOrderNew = this.getOne(Wrappers.<PurchaseOrder>lambdaQuery()
                        .orderByDesc(PurchaseOrder::getId)
                        .last("limit 1"));

                if (!CommonUtil.isEmpty(purchaseOrderNew)) {
                    ids.add(purchaseOrderNew.getId());
                }
            });
            String idStr = JOINER.join(ids);
            return idStr;
        } else {
            return "";
        }
    }
}