package cn.exrick.xboot.modules.bussiness.shopCart.controller;

import cn.exrick.xboot.common.utils.PageUtil;
import cn.exrick.xboot.common.utils.ResultUtil;
import cn.exrick.xboot.common.vo.PageVo;
import cn.exrick.xboot.common.vo.Result;
import cn.exrick.xboot.modules.bussiness.shopCart.pojo.ShopCart;
import cn.exrick.xboot.modules.bussiness.shopCart.service.IShopCartService;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * @author tqr
 */
@Slf4j
@RestController
@Api(description = "购物车管理接口")
@RequestMapping("/xboot/shopCart")
@Transactional
public class ShopCartController {

    @Autowired
    private IShopCartService iShopCartService;

    @RequestMapping(value = "/get/{id}", method = RequestMethod.GET)
    @ApiOperation(value = "通过id获取")
    public Result<ShopCart> get(@PathVariable String id){

        ShopCart shopCart = iShopCartService.getById(id);
        return new ResultUtil<ShopCart>().setData(shopCart);
    }

    @RequestMapping(value = "/getAll", method = RequestMethod.GET)
    @ApiOperation(value = "获取全部数据")
    public Result<List<ShopCart>> getAll(){

        List<ShopCart> list = iShopCartService.list();
        return new ResultUtil<List<ShopCart>>().setData(list);
    }

    @RequestMapping(value = "/getByPage", method = RequestMethod.GET)
    @ApiOperation(value = "分页获取")
    public Result<IPage<ShopCart>> getByPage(@ModelAttribute PageVo page){

        IPage<ShopCart> data = iShopCartService.page(PageUtil.initMpPage(page));
        return new ResultUtil<IPage<ShopCart>>().setData(data);
    }

    @RequestMapping(value = "/insertOrUpdate", method = RequestMethod.POST)
    @ApiOperation(value = "编辑或更新数据")
    public Result<ShopCart> saveOrUpdate(@ModelAttribute ShopCart shopCart){

        if(iShopCartService.saveOrUpdate(shopCart)){
            return new ResultUtil<ShopCart>().setData(shopCart);
        }
        return new ResultUtil<ShopCart>().setErrorMsg("操作失败");
    }

    @RequestMapping(value = "/delByIds/{ids}", method = RequestMethod.DELETE)
    @ApiOperation(value = "批量通过id删除")
    public Result<Object> delAllByIds(@PathVariable String[] ids){

        for(String id : ids){
            iShopCartService.removeById(id);
        }
        return new ResultUtil<Object>().setSuccessMsg("批量通过id删除数据成功");
    }
}
