package cn.exrick.xboot.modules.bussiness.store.listener;

import cn.exrick.xboot.common.utils.ToolUtil;
import cn.exrick.xboot.config.springContext.SpringContextHolder;
import cn.exrick.xboot.modules.bussiness.store.pojo.Store;
import cn.exrick.xboot.modules.bussiness.store.service.IStoreService;
import org.activiti.engine.delegate.DelegateExecution;
import org.activiti.engine.delegate.ExecutionListener;

public class storeListener implements ExecutionListener {


    @Override
    public void notify(DelegateExecution delegateExecution) throws Exception {

        String tableId = (String) delegateExecution.getVariable("tableId");
        IStoreService iStoreService = SpringContextHolder.getBean(IStoreService.class);
        Store store = iStoreService.getById(tableId);
        if(delegateExecution.getEventName().equals("end")){
            if(ToolUtil.isNotEmpty(store)){
                store.setCompletionStatus("1");
                store.setStortStatus("1");
                iStoreService.updateById(store);
            }
        }


    }
}
