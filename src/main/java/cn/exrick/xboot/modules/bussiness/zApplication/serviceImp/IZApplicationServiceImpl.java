package cn.exrick.xboot.modules.bussiness.zApplication.serviceImp;

import cn.exrick.xboot.modules.bussiness.shiVO.ZApplicationOV;
import cn.exrick.xboot.modules.bussiness.zApplication.mapper.ZApplicationMapper;
import cn.exrick.xboot.modules.bussiness.zApplication.pojo.ZApplication;
import cn.exrick.xboot.modules.bussiness.zApplication.service.IZApplicationService;
import cn.exrick.xboot.modules.shiUtil.JedisDelete;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import lombok.extern.slf4j.Slf4j;
import org.apache.ibatis.annotations.Param;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * 装修申请接口实现
 * @author tqr
 */
@Slf4j
@Service
@Transactional
public class IZApplicationServiceImpl extends ServiceImpl<ZApplicationMapper, ZApplication> implements IZApplicationService {

    @Autowired
    private ZApplicationMapper zApplicationMapper;






    /**
     * 查询所有集合
     */
    public IPage<ZApplicationOV> QueryZapplication(Page page, @Param("username") String username){
        JedisDelete d = new JedisDelete();
        d.Delete();
        return zApplicationMapper.QueryZapplication(page,username);
    }






}