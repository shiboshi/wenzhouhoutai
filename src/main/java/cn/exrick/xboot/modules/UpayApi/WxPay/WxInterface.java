package cn.exrick.xboot.modules.UpayApi.WxPay;

/**
 * @Author : xiaofei
 * @Date: 2019/9/5
 */
public interface WxInterface {

    /**
     * 小程序商户号
     */
    String mid = "898991250460127";

    /**
     * 终端号
     */
    String tid = "00000001";

    /**
     * 消息来源
     */
    String msgSrc = "WWW.ZJHZHJK.COM";

    /**
     * 来源编号
     */
    String msgSrcId = "6058";

    /**
     * 通讯密钥
     */
    String md5 = "sWDb8KhbpsxbX2ACnPzwANsHKfEpQakB3YDtxKPM3RrAbm5Y";

    /**
     * 机构商户号
     */
    String instMid = "MINIDEFAULT";

    /**
     * 交易类型
     */
    String tradeType = "MINI";

    /**
     * 消息类型
     */
    String msgType = "wx.unifiedOrder";

    /**
     * 小程序appid
     */
    String subAppId = "wx379f995c201708a1";
    /**
     * 小程序生產環境
     */
    String miniStr = "https://qr.chinaums.com/netpay-route-server/api/";
    /**
     * 获取小程序openId url
     */
    String code2Session = "https://api.weixin.qq.com/sns/jscode2session";

}
