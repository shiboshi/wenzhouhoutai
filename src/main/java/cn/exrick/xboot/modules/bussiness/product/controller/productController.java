package cn.exrick.xboot.modules.bussiness.product.controller;

import cn.exrick.xboot.common.utils.CommonUtil;
import cn.exrick.xboot.common.utils.PageUtil;
import cn.exrick.xboot.common.utils.ResultUtil;
import cn.exrick.xboot.common.vo.PageVo;
import cn.exrick.xboot.common.vo.Result;
import cn.exrick.xboot.modules.bussiness.classification.pojo.Classification;
import cn.exrick.xboot.modules.bussiness.classification.service.IClassificationService;
import cn.exrick.xboot.modules.bussiness.product.VO.ProductVO;
import cn.exrick.xboot.modules.bussiness.product.pojo.Product;
import cn.exrick.xboot.modules.bussiness.product.pojo.ProductModel;
import cn.exrick.xboot.modules.bussiness.product.pojo.ProductOrder;
import cn.exrick.xboot.modules.bussiness.product.service.IproductModelService;
import cn.exrick.xboot.modules.bussiness.product.service.IproductOrderService;
import cn.exrick.xboot.modules.bussiness.product.service.IproductService;
import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * @author xiaofei
 */
@Slf4j
@RestController
@RequestMapping("/xboot/product")
@Transactional
public class productController {

    @Autowired
    private IproductService iproductService;
    @Autowired
    private IClassificationService iClassificationService;
    @Autowired
    private IproductModelService iproductModelService;
    @Autowired
    private IproductOrderService iproductOrderService;


    /**
     * 查询产品列表
     *
     * @param pageVo
     * @param product
     * @return
     */
    @PostMapping("/getProductList")
    public Result<Object> getProductList(PageVo pageVo, Product product) {
        List<ProductVO> productVOS = iproductService.getProdcutList(product);
        return new ResultUtil<>().setData(PageUtil.listToPage(pageVo, productVOS), "查询产品列表成功");
    }

    /**
     * 获取产品详情
     *
     * @param productId
     * @return
     */
    @PostMapping("/getProductDetail")
    public Result<Object> getProductDetail(String productId) {
        return new ResultUtil<>().setData(iproductService.productDetail(productId), "查询产品详情成功");
    }

    /**
     * 插入产品
     *
     * @param product
     * @return
     */
    @PostMapping("/insertProduct")
    public Result<Object> insertProduct(Product product) {

        if (!CommonUtil.isEmpty(product)) {
            iproductService.insertProduct(product);
            return new ResultUtil<>().setSuccessMsg("添加成功！");
        } else {
            return new ResultUtil<>().setErrorMsg("空值！");
        }
    }


    /**
     * 删除产品大类
     *
     * @param
     * @return
     */
    @PostMapping("/deleteProduct")
    public Result<Object> deleteProduct(String idS) {
        String[] ids = idS.split(",");
        for (String id : ids) {
            if(iClassificationService.getBaseMapper().selectList(new QueryWrapper<Classification>().lambda().eq(Classification::getParentId,iproductService.getById(id).getKindId())).size()>0){
                return new ResultUtil<Object>().setErrorMsg("不允许删除，该分类下有数据");
            }
            deleteRecursion(id, ids);
        }
        return new ResultUtil<Object>().setSuccessMsg("删除数据成功");
    }

    public void deleteRecursion(String id, String[] ids) {

        // 查询分类表
        Product product = iproductService.getById(id);
        Classification classification = null;
        if (product != null && StrUtil.isNotBlank(product.getKindId())) {
            classification = iClassificationService.getById(product.getKindId());
            //删除product
            iproductService.removeById(id);
        }
        // 查询子节点
        if (classification != null) {
            //删除classification
            iClassificationService.removeById(classification.getId());
            //二级节点集合
            List<Classification> childrenDeps = iClassificationService.findByParentIdOrderBySortOrder(classification.getId());
            if (childrenDeps.size() > 0) {
                for (Classification d : childrenDeps) {
                    if (!CommonUtil.judgeIds(d.getId(), ids)) {
                        ProductModel productModel = iproductModelService.getOne(new QueryWrapper<ProductModel>().lambda().eq(ProductModel::getKindId, d.getId()).last(" limit 1 "));
                        Classification cf = null;
                        if (productModel != null && StrUtil.isNotBlank(productModel.getKindId())) {
                            cf = iClassificationService.getById(productModel.getKindId());
                            //删除productModel
                            iproductModelService.remove(new QueryWrapper<ProductModel>().lambda().eq(ProductModel::getKindId, d.getId()));
                        }
                        if (cf != null) {
                            ////删除classification
                            iClassificationService.removeById(cf.getId());
                            //三级级节点集合
                            List<Classification> thredDeps = iClassificationService.findByParentIdOrderBySortOrder(cf.getId());
                            if (thredDeps.size() > 0) {
                                for (Classification clfi : thredDeps) {
                                    if (!CommonUtil.judgeIds(clfi.getId(), ids)) {
                                        ProductOrder productOrder = iproductOrderService.getOne(new QueryWrapper<ProductOrder>().lambda().eq(ProductOrder::getKindId, clfi.getId()).last(" limit 1 "));
                                        Classification classification3 = null;
                                        if (productOrder != null && StrUtil.isNotBlank(productOrder.getKindId())) {
                                            classification3 = iClassificationService.getById(productOrder.getKindId());
                                            //删除productModel
                                            iproductOrderService.remove(new QueryWrapper<ProductOrder>().lambda().eq(ProductOrder::getKindId, clfi.getId()));
                                        }
                                        if (classification3 != null) {
                                            ////删除classification
                                            iClassificationService.removeById(classification3.getId());
                                        }
                                    }
                                }
                            }
                        }

                    }
                }
            }
        }

    }

    /**
     * 添加产品大类
     */
    @RequestMapping(value = "/addProduct", method = RequestMethod.POST)
    @ApiOperation(value = "添加产品")
    public Result<Product> addProduct(@ModelAttribute Product product) {

        Result<Product> result = iproductService.addProduct(product);
        return result;
    }

    /**
     * 修改产品
     */
    @RequestMapping(value = "/updateProduct", method = RequestMethod.POST)
    @ApiOperation(value = "修改产品")
    public Result<Product> updateProduct(Product product) {

        Result<Product> result = iproductService.updateProduct(product);
        return result;
    }
}
