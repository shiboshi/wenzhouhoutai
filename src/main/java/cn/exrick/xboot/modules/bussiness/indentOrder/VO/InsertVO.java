package cn.exrick.xboot.modules.bussiness.indentOrder.VO;

import lombok.Data;

/**
 * @Author : xiaofei
 * @Date: 2019/8/16
 */
@Data
public class InsertVO {

    private String params;
    private String procDefId;
    private Integer payType;
    private String indentOrderType;

}
