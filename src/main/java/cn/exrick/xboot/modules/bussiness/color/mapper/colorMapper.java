package cn.exrick.xboot.modules.bussiness.color.mapper;

import cn.exrick.xboot.modules.bussiness.color.ColorProvider;
import cn.exrick.xboot.modules.bussiness.color.VO.ColorVO;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import cn.exrick.xboot.modules.bussiness.color.pojo.Color;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.SelectProvider;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * 产品颜色数据处理层
 * @author tqr
 */
@Repository
public interface colorMapper extends BaseMapper<Color> {

    @SelectProvider(type = ColorProvider.class, method = "getColorSql")
    List<Color> findByParentIdOrderBySortOrder(String parentId);

    @SelectProvider(type = ColorProvider.class, method = "getColor")
    ColorVO getColor(@Param("id") String id);
}