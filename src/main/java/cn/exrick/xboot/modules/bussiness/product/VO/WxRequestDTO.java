package cn.exrick.xboot.modules.bussiness.product.VO;

import lombok.Data;

/**小程序查询产品品号传入参数不
 * @Author : xiaofei
 * @Date: 2019/9/17
 */
@Data
public class WxRequestDTO {
    private String userId;
    private String classificationId;
    private Integer type;
}
