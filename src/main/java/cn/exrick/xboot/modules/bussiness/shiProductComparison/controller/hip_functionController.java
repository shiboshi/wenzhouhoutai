package cn.exrick.xboot.modules.bussiness.shiProductComparison.controller;

import cn.exrick.xboot.common.utils.PageUtil;
import cn.exrick.xboot.common.utils.ResultUtil;
import cn.exrick.xboot.common.vo.PageVo;
import cn.exrick.xboot.common.vo.Result;
import cn.exrick.xboot.modules.bussiness.shiProductComparison.pojo.hip_function;
import cn.exrick.xboot.modules.bussiness.shiProductComparison.service.Ihip_functionService;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * @author 石博士
 */
@Slf4j
@RestController
@Api(description = "臀部功能管理接口")
@RequestMapping("/xboot/hip_function")
@Transactional
public class hip_functionController {

    @Autowired
    private Ihip_functionService ihip_functionService;

    @RequestMapping(value = "/get/{id}", method = RequestMethod.GET)
    @ApiOperation(value = "通过id获取")
    public Result<hip_function> get(@PathVariable String id){

        hip_function hip_function = ihip_functionService.getById(id);
        return new ResultUtil<hip_function>().setData(hip_function);
    }

    @RequestMapping(value = "/getAll", method = RequestMethod.GET)
    @ApiOperation(value = "获取全部数据")
    public Result<List<hip_function>> getAll(){

        List<hip_function> list = ihip_functionService.list();
        return new ResultUtil<List<hip_function>>().setData(list);
    }

    @RequestMapping(value = "/getByPage", method = RequestMethod.GET)
    @ApiOperation(value = "分页获取")
    public Result<IPage<hip_function>> getByPage(@ModelAttribute PageVo page){

        IPage<hip_function> data = ihip_functionService.page(PageUtil.initMpPage(page));
        return new ResultUtil<IPage<hip_function>>().setData(data);
    }

    @RequestMapping(value = "/insertOrUpdate", method = RequestMethod.POST)
    @ApiOperation(value = "编辑或更新数据")
    public Result<hip_function> saveOrUpdate(@ModelAttribute hip_function hip_function){

        if(ihip_functionService.saveOrUpdate(hip_function)){
            return new ResultUtil<hip_function>().setData(hip_function);
        }
        return new ResultUtil<hip_function>().setErrorMsg("操作失败");
    }

    @RequestMapping(value = "/delByIds/{ids}", method = RequestMethod.DELETE)
    @ApiOperation(value = "批量通过id删除")
    public Result<Object> delAllByIds(@PathVariable String[] ids){

        for(String id : ids){
            ihip_functionService.removeById(id);
        }
        return new ResultUtil<Object>().setSuccessMsg("批量通过id删除数据成功");
    }
}
