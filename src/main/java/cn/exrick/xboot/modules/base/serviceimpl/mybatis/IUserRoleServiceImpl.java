package cn.exrick.xboot.modules.base.serviceimpl.mybatis;

import cn.exrick.xboot.common.utils.ToolUtil;
import cn.exrick.xboot.modules.base.dao.mapper.UserRoleMapper;
import cn.exrick.xboot.modules.base.entity.Role;
import cn.exrick.xboot.modules.base.entity.RoleDepartment;
import cn.exrick.xboot.modules.base.entity.UserRole;
import cn.exrick.xboot.modules.base.service.RoleDepartmentService;
import cn.exrick.xboot.modules.base.service.RoleService;
import cn.exrick.xboot.modules.base.service.UserRoleService;
import cn.exrick.xboot.modules.base.service.mybatis.IRoleDepartmentService;
import cn.exrick.xboot.modules.base.service.mybatis.IUserRoleService;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;
import java.util.stream.Collectors;

/**
 * @author Exrickx
 */
@Service
public class IUserRoleServiceImpl extends ServiceImpl<UserRoleMapper, UserRole> implements IUserRoleService {

    @Resource
    private UserRoleMapper userRoleMapper;

    @Autowired
    private IUserRoleService userRoleService;
    @Autowired
    private RoleService roleService;
    @Autowired
    private IRoleDepartmentService roleDepartmentService;

    @Override
    public List<Role> findByUserId(String userId) {

        return userRoleMapper.findByUserId(userId);
    }

    @Override
    public List<String> findDepIdsByUserId(String userId) {

        //通过用户id获取 部门id
        //1获取当前用户，的所有角色
        List<UserRole> userRoles = userRoleService.list(Wrappers.<UserRole>lambdaQuery()
        .eq(UserRole::getUserId,userId));
        if (ToolUtil.isNotEmpty(userRoles)){
            List<String> roleIds = userRoles.stream().
                    filter(role -> ToolUtil.isNotEmpty(roleService.get(role.getRoleId())))
                    .map(role -> {
               return roleService.get(role.getRoleId()).getId();
            }).collect(Collectors.toList());
            //2.根据每个角色查询所有部门id取出
            if (ToolUtil.isNotEmpty(roleIds)) {
                List<String> deptIds = roleIds.stream()
                        .filter(dept -> ToolUtil.isNotEmpty(roleDepartmentService.getOne(Wrappers.<RoleDepartment>lambdaQuery()
                        .eq(RoleDepartment::getRoleId,dept))))
                        .map(dept -> {
                    return roleDepartmentService.getOne(Wrappers.<RoleDepartment>lambdaQuery()
                    .eq(RoleDepartment::getRoleId,dept)).getDepartmentId();
                }).collect(Collectors.toList());
                return deptIds;
            }
        }
        return null;
    }
}
