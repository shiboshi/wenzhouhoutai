package cn.exrick.xboot.modules.UpayApi.WxPay;

import lombok.Data;

/**
 * @Author : xiaofei
 * @Date: 2019/9/6
 */
@Data
public class OpenIdResponse {


    /**
     * 用户唯一标识
     */
    private String openid;
    /**
     * 会话密钥
     */
    private String session_key;
    /**
     * 用户在开放平台的唯一标识符
     */
    private String unionid;
    /**
     * 错误码
     */
    private String errcode;
    /**
     * 错误信息
     */
    private String errmsg;

}
