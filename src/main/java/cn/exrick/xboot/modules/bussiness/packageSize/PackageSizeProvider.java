package cn.exrick.xboot.modules.bussiness.packageSize;

import cn.exrick.xboot.common.utils.ToolUtil;
import org.apache.commons.lang3.StringUtils;

public class PackageSizeProvider {
    public String getPackageSize(String condition) {
        String sql = "";
        String str = "";
        str += " select p.create_time,p.id AS id, p.length AS length, p.wide AS wide, p.tall AS tall, " +
                "p.rough_weight AS roughWeight, p.net_weight AS netWeight, p.volume AS volume," +
                "s.title as productOrderName,(select  title from  t_classification where id = s.parent_id) as productModelName,(select id from  t_classification where id = s.parent_id) as productModelId," +
                "(select  title from  t_classification where  id = (select  parent_id from  t_classification where  id = s.parent_id)) as productName " +
                "from  t_package_size p left join t_classification s on p.product_order_id = s.id where 1=1 and (p.del_flag = 0 or p.del_flag is null) ";
        if(StringUtils.isNotBlank(condition)){
            str += " and ( s.title like '%"+condition+"%' or (select title from  t_classification where id = (select  parent_id from t_classification where id = s.parent_id)) like '%"+condition+"%' or (select title from t_classification where id = s.parent_id) like '%"+condition+"%')";
        }
        sql += str;
        return sql;
    }

    public String getByProductId(String id) {
        String sql = "";
        String str = "";
        str += " select p.id AS id, p.length AS length, p.wide AS wide, p.tall AS tall, " +
                "p.rough_weight AS roughWeight, p.net_weight AS netWeight, p.volume AS volume " +
                "from  t_package_size p left join x_product_order o on p.product_order_id = o.kind_id where 1=1 and (p.del_flag = 0 or p.del_flag is null) ";
        if(ToolUtil.isNotEmpty(id)){
            str += " and ( o.id = '"+id+"' )";
        }
        sql += str;
        return sql;
    }
}
