package cn.exrick.xboot.modules.bussiness.sendGoodsOrder.service;

import cn.exrick.xboot.modules.bussiness.inventoryOrder.VO.InventoryVO;
import cn.exrick.xboot.modules.bussiness.inventoryOrder.pojo.InventoryOrder;
import cn.exrick.xboot.modules.bussiness.purchaseOrder.VO.PurchaseOrderVO;
import cn.exrick.xboot.modules.bussiness.sendGoodsOrder.VO.SendGoodsVO;
import cn.exrick.xboot.modules.bussiness.sendGoodsOrder.VO.TaskSendGoodVO;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;
import cn.exrick.xboot.modules.bussiness.sendGoodsOrder.pojo.SendGoodsOrder;

import javax.xml.crypto.KeySelector;
import java.math.BigDecimal;
import java.util.List;

/**
 * 发货单接口
 * @author xiaofei
 */
public interface IsendGoodsOrderService extends IService<SendGoodsOrder> {

    Page<SendGoodsVO> getSendGoodsOrderByUser(Page initMpPage, String orderNum,String userId,String code,Integer type,String serial);

    /*List<SendGoodsVO>  getGoodOrderListByApplicationId(String applicationSendGoodsId);*/

    /**
     * 获取发货单详情
     * @param id
     * @return
     */
    List<InventoryVO> getDetail(String id,String userId);

    boolean confirmationOfReceipt(String id, String common,String userId);

    SendGoodsVO selectById(String id);

    /**
     * 判断库存
     * @param serial
     * @return
     */
    List<String> filterInventory(String serial);

    /**
     * 文员发货按钮(pc)
     */
    void  taskInser(TaskSendGoodVO taskSendGoodVO);

    /**
     * 获取当前发货单的总金额
     * @param applicationSendGoodId
     * @return
     */
    BigDecimal getTotalPrice(String applicationSendGoodId);

    /**
     * 获取当前发货单的发货数量
     * @param id
     * @return
     */
    BigDecimal getTotalAmount(String id);


    List<SendGoodsVO> getSalesSendList(String id);

    List<String> checkSeri(String serial,String id);
}