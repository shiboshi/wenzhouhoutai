package cn.exrick.xboot.modules.bussiness.zApplication.controller;

import cn.exrick.xboot.common.utils.PageUtil;
import cn.exrick.xboot.common.utils.ResultUtil;
import cn.exrick.xboot.common.utils.SecurityUtil;
import cn.exrick.xboot.common.vo.PageVo;
import cn.exrick.xboot.common.vo.Result;
import cn.exrick.xboot.modules.activiti.entity.ActBusiness;
import cn.exrick.xboot.modules.activiti.service.ActBusinessService;
import cn.exrick.xboot.modules.base.entity.User;
import cn.exrick.xboot.modules.bussiness.shiVO.ZApplicationOV;
import cn.exrick.xboot.modules.bussiness.zApplication.pojo.ZApplication;
import cn.exrick.xboot.modules.bussiness.zApplication.service.IZApplicationService;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpSession;
import java.util.List;

/**
 * @author tqr
 */
@Slf4j
@RestController
@Api(description = "装修申请管理接口")
@RequestMapping("/xboot/zApplication")
@Transactional
public class ZApplicationController {


    @Autowired
    private IZApplicationService iZApplicationService;

    @Autowired
    private SecurityUtil securityUtil;

    @Autowired
    private ActBusinessService actBusinessService;




    @RequestMapping(value = "/get/{id}", method = RequestMethod.POST)
    @ApiOperation(value = "通过id获取")
    public Result<ZApplication> get(@PathVariable String id){

        ZApplication zApplication = iZApplicationService.getById(id);
        return new ResultUtil<ZApplication>().setData(zApplication);
    }

    @RequestMapping(value = "/getAll", method = RequestMethod.POST)
    @ApiOperation(value = "获取全部数据")
    public Result<List<ZApplication>> getAll(){

        List<ZApplication> list = iZApplicationService.list();
        return new ResultUtil<List<ZApplication>>().setData(list);
    }

    @RequestMapping(value = "/getByPage", method = RequestMethod.POST)
    @ApiOperation(value = "分页获取")
    public Result<IPage<ZApplicationOV>> getByPage(@ModelAttribute PageVo page,HttpSession session, @RequestParam(value="procDefId") String procDefId){
        session.setAttribute("procDefId",procDefId);
        return new ResultUtil<IPage<ZApplicationOV>>().setData(iZApplicationService.QueryZapplication(PageUtil.initMpPage(page),securityUtil.getCurrUser().getUsername()));
    }

    @RequestMapping(value = "/insertOrUpdate", method = RequestMethod.POST)
    @ApiOperation(value = "编辑或更新数据")
    public Result<ZApplication> saveOrUpdate(@ModelAttribute ZApplication zApplication, HttpSession session){

        //获取当前登录人 ID
        User u = securityUtil.getCurrUser();
        zApplication.setUserId(u.getId());
        zApplication.setDecorationStatus("0");
        zApplication.setCompletionStatus("0");
        if(iZApplicationService.saveOrUpdate(zApplication)){

            ActBusiness actBusiness = new ActBusiness();
            actBusiness.setProcDefId(session.getAttribute("procDefId").toString());
            actBusiness.setUserId(u.getId());
            zApplication = iZApplicationService.getOne(Wrappers.<ZApplication>lambdaQuery()
                    .orderByDesc(ZApplication::getId).last("limit 1"));
            actBusiness.setTableId(zApplication.getId());
            ActBusiness actBusinessUse = actBusinessService.save(actBusiness);
            zApplication.setBussinessId(actBusinessUse.getId());
            iZApplicationService.updateById(zApplication);

            return new ResultUtil<ZApplication>().setData(zApplication);
        }
        return new ResultUtil<ZApplication>().setErrorMsg("操作失败");
    }

    @RequestMapping(value = "/delByIds", method = RequestMethod.DELETE)
    @ApiOperation(value = "批量通过id删除")
    public Result<Object> delAllByIds(String id){
            iZApplicationService.removeById(id);
        return new ResultUtil<Object>().setSuccessMsg("批量通过id删除数据成功");
    }







}
