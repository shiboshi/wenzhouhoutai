package cn.exrick.xboot.modules.bussiness.roleAddress.serviceImp;

import cn.exrick.xboot.common.constant.CommonConstant;
import cn.exrick.xboot.common.utils.SecurityUtil;
import cn.exrick.xboot.common.utils.ToolUtil;
import cn.exrick.xboot.modules.base.dao.mapper.RoleDepartmentMapper;
import cn.exrick.xboot.modules.base.dao.mapper.RoleMapper;
import cn.exrick.xboot.modules.base.entity.Permission;
import cn.exrick.xboot.modules.base.entity.Role;
import cn.exrick.xboot.modules.bussiness.address.mapper.AddressMapper;
import cn.exrick.xboot.modules.bussiness.address.pojo.Address;
import cn.exrick.xboot.modules.bussiness.address.pojo.AddressVO;
import cn.exrick.xboot.modules.bussiness.dealerLevel.mapper.DealerLevelMapper;
import cn.exrick.xboot.modules.bussiness.dealerLevel.pojo.DealerLevel;
import cn.exrick.xboot.modules.bussiness.dealerLevel.service.IDealerLevelService;
import cn.exrick.xboot.modules.bussiness.roleAddress.VO.AddressRoleVO;
import cn.exrick.xboot.modules.bussiness.roleAddress.mapper.AddressRoleMapper;
import cn.exrick.xboot.modules.bussiness.roleAddress.pojo.AddressRole;
import cn.exrick.xboot.modules.bussiness.roleAddress.service.IaddressRoleService;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * 角色区域表接口实现
 *
 * @author fei
 */
@Slf4j
@Service
@Transactional
public class IaddressRoleServiceImpl extends ServiceImpl<AddressRoleMapper, AddressRole> implements IaddressRoleService {

    @Autowired
    private AddressRoleMapper addressRoleMapper;
    @Autowired
    private SecurityUtil securityUtil;
    @Autowired
    private AddressMapper addressMapper;
    @Autowired
    private DealerLevelMapper dealerLevelMapper;
    @Autowired
    private RoleMapper roleMapper;

    @Override
    public boolean insertAddrRole(List<String> addressIds, String roleId) {
        try {
            //删除之前的区域
            addressRoleMapper.delete(new QueryWrapper<AddressRole>().lambda().eq(AddressRole::getRoleId,roleId));
            addressIds.stream().forEach(id -> {
                AddressRole addressRole = new AddressRole();
                addressRole.setAddressId(id).setRoleId(roleId);
                addressRoleMapper.insert(addressRole);
            });
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    @Override
    public List<AddressVO> getList(String roleId) {
        List<AddressRole> list = null;
        List<AddressVO> addressList = new ArrayList<>();
        if (ToolUtil.isNotEmpty(roleId)) {
            list = addressRoleMapper.selectList(new QueryWrapper<AddressRole>().lambda().eq(AddressRole::getRoleId, roleId));

            // 0级
            List<Address> list0 = addressMapper.selectList(new QueryWrapper<Address>().lambda().eq(Address::getParentId, CommonConstant.LEVEL_ZERO));
            for (Address p0 : list0) {
                AddressVO a0 = new AddressVO();
                ToolUtil.copyProperties(p0, a0);
                // 一级
                List<AddressVO> listA0 = new ArrayList<>();
                List<Address> list1 = addressMapper.selectList(new QueryWrapper<Address>().lambda().eq(Address::getParentId, p0.getId()));
                // 二级
                for (Address p1 : list1) {
                    AddressVO a1 = new AddressVO();
                    ToolUtil.copyProperties(p1, a1);
                    List<AddressVO> listA1 = new ArrayList<>();
                    List<Address> children1 = addressMapper.selectList(new QueryWrapper<Address>().lambda().eq(Address::getParentId, p1.getId()));
                    for (Address p2 : children1) {
                        AddressVO a2 = new AddressVO();
                        ToolUtil.copyProperties(p2, a2);
                        listA1.add(a2);
                    }
                    for (AddressVO a2 : listA1) {
                        for (AddressRole ar : list) {
                            if (a2.getId().equals(ar.getAddressId())) {
                                a2.setChecked(true);
                            }
                        }
                    }
                    a1.setChildren(listA1);
                    listA0.add(a1);
                }
                for (AddressVO a1 : listA0) {
                    for (AddressRole ar : list) {
                        if (a1.getId().equals(ar.getAddressId())) {
                            a1.setChecked(true);
                        }
                    }
                }
                a0.setChildren(listA0);
                addressList.add(a0);
            }
            for (AddressVO a0 : addressList) {
                for (AddressRole ar : list) {
                    if (a0.getId().equals(ar.getAddressId())) {
                        a0.setChecked(true);
                    }
                }
            }
        }
        return addressList;
    }

    @Override
    public boolean updateAddressRole(List<String> addressIds, String roleId) {
        try {

            List<AddressRole> addressRoles = this.getBaseMapper()
                    .selectList(Wrappers.<AddressRole>lambdaQuery()
                            .eq(AddressRole::getRoleId, roleId)
                            .in(AddressRole::getAddressId, addressIds));
            List<String> ids = addressRoles.stream().map(addr -> addr.getId())
                    .collect(Collectors.toList());
            addressRoleMapper.deleteBatchIds(ids);
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    @Override
    public AddressRoleVO getDetail(String id) {
        return null;
    }

    @Override
    public List<Role> getRole() {
        String userDept = securityUtil.getCurrUser().getDepartmentId();
        //查询经销商部门区域
        String areaId = dealerLevelMapper.selectList(new QueryWrapper<DealerLevel>().lambda().eq(DealerLevel::getDeptId,userDept)).get(0).getAreaId();
        //查询负责该区域的角色
        List<AddressRole> addressRoleList= addressRoleMapper.selectList(new QueryWrapper<AddressRole>().lambda().eq(AddressRole::getAddressId,areaId));
        List<Role> roleList = new ArrayList<>();
        for(AddressRole ar :addressRoleList){
            if(ToolUtil.isNotEmpty(ar.getRoleId())){
                Role role = roleMapper.selectById(ar.getRoleId());
                roleList.add(role);
            }
        }
        return roleList;
    }
}