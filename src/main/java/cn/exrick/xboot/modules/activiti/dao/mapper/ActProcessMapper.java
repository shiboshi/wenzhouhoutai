package cn.exrick.xboot.modules.activiti.dao.mapper;

import cn.exrick.xboot.modules.activiti.entity.ActProcess;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * @Author : xiaofei
 * @Date: 2019/8/23
 */
public interface ActProcessMapper extends BaseMapper<ActProcess> {
}
