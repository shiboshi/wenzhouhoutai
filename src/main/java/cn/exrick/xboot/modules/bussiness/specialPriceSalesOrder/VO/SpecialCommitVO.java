package cn.exrick.xboot.modules.bussiness.specialPriceSalesOrder.VO;

import lombok.Data;

import java.math.BigDecimal;

/**
 * @Author : xiaofei
 * @Date: 2019/8/28
 */
@Data
public class SpecialCommitVO {

    private String id;
    private BigDecimal price;

}
