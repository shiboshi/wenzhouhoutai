package cn.exrick.xboot.modules.bussiness.transferOrder.controller;

import cn.exrick.xboot.common.enums.Order;
import cn.exrick.xboot.common.utils.*;
import cn.exrick.xboot.common.vo.PageVo;
import cn.exrick.xboot.common.vo.Result;
import cn.exrick.xboot.modules.base.dao.mapper.DepartmentMapper;
import cn.exrick.xboot.modules.base.service.DepartmentService;
import cn.exrick.xboot.modules.bussiness.inventoryOrder.VO.InventoryVO;
import cn.exrick.xboot.modules.bussiness.inventoryOrder.pojo.InventoryOrder;
import cn.exrick.xboot.modules.bussiness.inventoryOrder.service.IInventoryOrderService;
import cn.exrick.xboot.modules.bussiness.shiWarehouse.service.IShiWarehouseService;
import cn.exrick.xboot.modules.bussiness.transferOrder.pojo.TransferOrder;
import cn.exrick.xboot.modules.bussiness.transferOrder.service.ItransferOrderService;
import com.google.common.collect.Maps;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * @author fei
 */
@Slf4j
@RestController
@Api(description = "调拨单管理接口")
@RequestMapping("/xboot/transferOrder")
@Transactional
public class transferOrderController {

    @Autowired
    private ItransferOrderService itransferOrderService;
    @Autowired
    private SecurityUtil securityUtil;
    @Autowired
    private IShiWarehouseService iShiWarehouseService;
    @Autowired
    private DepartmentMapper departmentMapper;
    @Autowired
    private IInventoryOrderService iInventoryOrderService;

    /**TODO
     * 插入订单接口
     * @param transferOrder
     * @return
     */
    @PostMapping("/insertOrder")
    public Result<Object> insertOrder(TransferOrder transferOrder){
        //判断是否有创建调拨单的权限
        String deptId1 = securityUtil.getCurrUser().getDepartmentId();
        String deptId2 = iShiWarehouseService.getBaseMapper().selectById(transferOrder.getWarhouseId()).getDeptId();
        if(departmentMapper.selectById(deptId2).getParentId().equals(deptId1)||deptId1.equals(deptId2)||departmentMapper.selectById(deptId1).getParentId().equals(departmentMapper.selectById(deptId2).getParentId())) {
           String[] ids = transferOrder.getInventoryIds().split(",");
            //查询原库存信息
            if(ids.length>0){
                InventoryOrder inventoryOrder = iInventoryOrderService.getBaseMapper().selectById(ids[0]);
                if(ToolUtil.isNotEmpty(inventoryOrder)){
                    transferOrder.setOldwarhouseId(inventoryOrder.getWarhouseId());
                } else{
                    return new ResultUtil<>().setErrorMsg("数据出错");
                }
            }
            transferOrder.setCreateBy(securityUtil.getCurrUser().getId());
            transferOrder.setCreateTime(new Date());
            transferOrder.setDelFlag(0);
            transferOrder.setStatus(0);
            transferOrder.setTransferOrderNum(CommonUtil.createOrderSerial(Order.DB_));
            if (ToolUtil.isEmpty(transferOrder)) {
                return new ResultUtil<>().setErrorMsg("传入参数为空！");
            }
            itransferOrderService.insertTransferOrder(transferOrder);
            return new ResultUtil<>().setSuccessMsg("插入成功！");
        } else {
            return new ResultUtil<>().setErrorMsg("无调拨到该仓库的权限！");
        }
    }

    /**
     * 确认到货
     * @return
     */
    @PostMapping("/arrival")
    public Result<Map<String,Object>> arrival(String id){

        try {
            itransferOrderService.arrival(id);
            return new ResultUtil<Map<String,Object>>().setSuccessMsg("操作成功！");
        } catch (Exception e) {
            return new ResultUtil<Map<String,Object>>().setErrorMsg("操作失败！");
        }
    }

    /**
     * 获取列表接口
     * @param transferOrder
     * @return
     */
    @PostMapping("/getList")
    public Result<Map<String,Object>> getList(@ModelAttribute PageVo pageVo,TransferOrder transferOrder){

        Map<String,Object> map = Maps.newHashMap();
        map.put("data", PageUtil.listToPage(pageVo,itransferOrderService.getList(transferOrder)));
        map.put("size", itransferOrderService.getList(transferOrder).size());
        return new ResultUtil<Map<String,Object>>().setData(map, "查询列表成功！");

    }

    /**
     * 获取产品详情
     * @param id
     * @return
     */
    @PostMapping("/getDetail")
    public Result<List<InventoryVO>> getDetail(String id,String userId){
        if (ToolUtil.isEmpty(id)){
            return new ResultUtil<List<InventoryVO>>().setErrorMsg("传入参数为空！");
        }
        return new ResultUtil<List<InventoryVO>>().setData(itransferOrderService.getDetail(id,userId), "查询成功！");
    }


    /**
     * 获取当前经销商的门店仓库
     * @return
     */
    @PostMapping("/getWarhouseName")
    public Result<List<Map<String,Object>>> getWarhouseName(){
        return new ResultUtil<List<Map<String,Object>>>().setData(itransferOrderService.getWarhouseList());
    }


    @RequestMapping(value = "/delByIds/{ids}", method = RequestMethod.DELETE)
    @ApiOperation(value = "批量通过id删除")
    public Result<Object> delAllByIds(@PathVariable String[] ids){

        for(String id : ids){
            itransferOrderService.removeById(id);
        }
        return new ResultUtil<Object>().setSuccessMsg("批量通过id删除数据成功");
    }
}
