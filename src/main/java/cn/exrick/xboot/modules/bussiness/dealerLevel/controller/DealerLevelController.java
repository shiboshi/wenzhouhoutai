package cn.exrick.xboot.modules.bussiness.dealerLevel.controller;

import cn.exrick.xboot.common.utils.PageUtil;
import cn.exrick.xboot.common.utils.ResultUtil;
import cn.exrick.xboot.common.utils.SecurityUtil;
import cn.exrick.xboot.common.utils.ToolUtil;
import cn.exrick.xboot.common.vo.PageVo;
import cn.exrick.xboot.common.vo.Result;
import cn.exrick.xboot.modules.bussiness.dealerLevel.VO.DealerLevelVO;
import cn.exrick.xboot.modules.bussiness.dealerLevel.pojo.DealerLevel;
import cn.exrick.xboot.modules.bussiness.dealerLevel.service.IDealerLevelService;
import cn.hutool.json.JSONArray;
import cn.hutool.json.JSONObject;
import cn.hutool.json.JSONUtil;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.transaction.annotation.Transactional;

import javax.servlet.http.HttpServletRequest;
import java.util.Date;
import java.util.List;

/**
 * @author tqr
 */
@Slf4j
@RestController
@Api(description = "经销商等级管理接口")
@RequestMapping("/xboot/dealerLevel")
@Transactional
public class DealerLevelController {

    @Autowired
    private IDealerLevelService iDealerLevelService;
    @Autowired
    private SecurityUtil securityUtil;

    @RequestMapping(value = "/get/{id}", method = RequestMethod.GET)
    @ApiOperation(value = "通过id获取")
    public Result<DealerLevel> get(@PathVariable String id){

        DealerLevel dealerLevel = iDealerLevelService.getById(id);
        return new ResultUtil<DealerLevel>().setData(dealerLevel);
    }

    @RequestMapping(value = "/getAll", method = RequestMethod.GET)
    @ApiOperation(value = "获取全部数据")
    public Result<List<DealerLevel>> getAll(){

        List<DealerLevel> list = iDealerLevelService.list();
        return new ResultUtil<List<DealerLevel>>().setData(list);
    }

    @RequestMapping(value = "/getByPage", method = RequestMethod.GET)
    @ApiOperation(value = "分页获取")
    public Result<IPage<DealerLevel>> getByPage(@ModelAttribute PageVo page){

        IPage<DealerLevel> data = iDealerLevelService.page(PageUtil.initMpPage(page));
        return new ResultUtil<IPage<DealerLevel>>().setData(data);
    }

    @RequestMapping(value = "/insertOrUpdate", method = RequestMethod.POST)
    @ApiOperation(value = "编辑或更新数据")
    public Result<DealerLevel> saveOrUpdate(@ModelAttribute DealerLevel dealerLevel){
        iDealerLevelService.save(dealerLevel);
        return new ResultUtil<DealerLevel>().setSuccessMsg("操作成功！");
    }

    @RequestMapping(value = "/delByIds/{ids}", method = RequestMethod.DELETE)
    @ApiOperation(value = "批量通过id删除")
    public Result<Object> delAllByIds(@PathVariable String[] ids){

        for(String id : ids){
            iDealerLevelService.removeById(id);
        }
        return new ResultUtil<Object>().setSuccessMsg("批量通过id删除数据成功");
    }

    /**
     * 查询经销商
     */
    @RequestMapping(value = "/getDealer", method = RequestMethod.POST)
    @ApiOperation(value = "分页获取")
    public Result<IPage<DealerLevelVO>> getDealer(@ModelAttribute PageVo page,String name){

        IPage<DealerLevelVO> data = iDealerLevelService.getDealer(PageUtil.initMpPage(page),name);
        return new ResultUtil<IPage<DealerLevelVO>>().setData(data);
    }

}
