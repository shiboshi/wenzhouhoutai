package cn.exrick.xboot.modules.bussiness.shiProductComparison.pojo;

import cn.exrick.xboot.base.XbootBaseEntity;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.Table;

/**
 * @author 石博士
 */
@Data
@Entity
@Table(name = "s_additional_function")
@TableName("s_additional_function")
@ApiModel(value = "附加功能表")
public class additional_function extends XbootBaseEntity {

    private static final long serialVersionUID = 1L;



    @ApiModelProperty("零空间，0没有，1有，2选配")
    private String zero_space;

    @ApiModelProperty("零重力，0没有，1有，2选配")
    private String zero_gravity;

    @ApiModelProperty("蓝牙APP，0没有，1有，2选配")
    private String bluetooth_app;

    @ApiModelProperty("累计工作时间，0没有，1有，2选配")
    private String working_time;

    @ApiModelProperty("投币功能，0没有，1有，2选配")
    private String coins;

    @ApiModelProperty("微信支付，0没有，1有，2选配")
    private String we_chat_pay;

    @ApiModelProperty("自动模式，手工输入")
    private String automatic_mode;

    @ApiModelProperty("记忆模式，手工输入")
    private String memory_pattern;

    @ApiModelProperty("安全保护，0没有，1有，2选配")
    private String safeguard;

    @ApiModelProperty("时间设定，0没有，1有，2选配")
    private String time_setting;

    @ApiModelProperty("玉石热疗器，0没有，1有，2选配")
    private String jade_stone;

    @ApiModelProperty("整机联动，0没有，1有，2选配")
    private String machine_linkage;









}