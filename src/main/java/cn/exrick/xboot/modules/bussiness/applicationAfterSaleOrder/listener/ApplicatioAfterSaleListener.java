package cn.exrick.xboot.modules.bussiness.applicationAfterSaleOrder.listener;

        import cn.exrick.xboot.common.enums.CheckStatus;
        import cn.exrick.xboot.common.enums.SendStatus;
        import cn.exrick.xboot.common.utils.ToolUtil;
        import cn.exrick.xboot.config.springContext.SpringContextHolder;
        import cn.exrick.xboot.modules.bussiness.appliSendNum.pojo.AppliSendNum;
        import cn.exrick.xboot.modules.bussiness.appliSendNum.service.IappliSendNumService;
        import cn.exrick.xboot.modules.bussiness.applicationAfterSaleOrder.pojo.ApplicationAfterSaleOrder;
        import cn.exrick.xboot.modules.bussiness.applicationAfterSaleOrder.service.IapplicationAfterSaleOrderService;
        import cn.exrick.xboot.modules.bussiness.applicationSendGoodsOrder.pojo.ApplicationSendGoodsOrder;
        import cn.exrick.xboot.modules.bussiness.applicationSendGoodsOrder.service.IapplicationSendGoodsOrderService;
        import cn.exrick.xboot.modules.bussiness.purchaseOrder.pojo.PurchaseOrder;
        import cn.exrick.xboot.modules.bussiness.purchaseOrder.service.IpurchaseOrderService;
        import com.baomidou.mybatisplus.core.toolkit.Wrappers;
        import org.activiti.engine.delegate.DelegateExecution;
        import org.activiti.engine.delegate.ExecutionListener;

        import java.math.BigDecimal;
        import java.util.List;

/**
 * @Author : xiaofei
 * @Date: 2019/8/22
 */
public class ApplicatioAfterSaleListener implements ExecutionListener {
    @Override
    public void notify(DelegateExecution delegateExecution) throws Exception {

        String tableId = (String) delegateExecution.getVariable("tableId");
        IapplicationAfterSaleOrderService iapplicationAfterSaleOrderService = SpringContextHolder.getBean(IapplicationAfterSaleOrderService.class);
        IappliSendNumService appliSendNumService = SpringContextHolder.getBean(IappliSendNumService.class);
        if (delegateExecution.getEventName().equals("end")) {
            ApplicationAfterSaleOrder applicationAfterSaleOrder = iapplicationAfterSaleOrderService.getById(tableId);
            applicationAfterSaleOrder.setCheckStatus(CheckStatus.sucess);
            iapplicationAfterSaleOrderService.updateById(applicationAfterSaleOrder);
        }
    }
}

