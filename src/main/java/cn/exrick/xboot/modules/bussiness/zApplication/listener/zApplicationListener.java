package cn.exrick.xboot.modules.bussiness.zApplication.listener;

import cn.exrick.xboot.common.utils.ToolUtil;
import cn.exrick.xboot.config.springContext.SpringContextHolder;
import cn.exrick.xboot.modules.bussiness.zApplication.pojo.ZApplication;
import cn.exrick.xboot.modules.bussiness.zApplication.service.IZApplicationService;
import org.activiti.engine.delegate.DelegateExecution;
import org.activiti.engine.delegate.ExecutionListener;

public class zApplicationListener implements ExecutionListener {


    @Override
    public void notify(DelegateExecution delegateExecution) throws Exception {

        String tableId = (String) delegateExecution.getVariable("tableId");
        IZApplicationService iZApplicationService = SpringContextHolder.getBean(IZApplicationService.class);
        ZApplication zApplication = iZApplicationService.getById(tableId);
        if(delegateExecution.getEventName().equals("end")){
            if(ToolUtil.isNotEmpty(zApplication)){
                zApplication.setCompletionStatus("1");
                zApplication.setDecorationStatus("1");
                iZApplicationService.updateById(zApplication);
            }
        }



    }
}
