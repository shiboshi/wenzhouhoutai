package cn.exrick.xboot.config.global;

import cn.exrick.xboot.common.constant.ActivitiConstant;
import cn.exrick.xboot.common.utils.ToolUtil;
import cn.exrick.xboot.modules.activiti.entity.ActProcess;
import cn.exrick.xboot.modules.activiti.service.ActProcessService;
import cn.exrick.xboot.modules.activiti.service.mybatis.IActProcessService;
import cn.exrick.xboot.modules.activiti.vo.ActProcessVo;
import cn.exrick.xboot.modules.activiti.vo.ProcessInsVo;
import com.alibaba.fastjson.JSON;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import lombok.extern.slf4j.Slf4j;
import org.hibernate.validator.constraints.EAN;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.core.annotation.Order;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.stream.Collectors;

/**
 * @Author : xiaofei
 * @Date: 2019/8/23
 */
@Component
@Order(1)
@Slf4j
public class GlobalConfig implements ApplicationRunner {

    @Autowired
    private StringRedisTemplate redisTemplate;
    @Autowired
    private IActProcessService actProcessService;

    @Override
    public void run(ApplicationArguments args) throws Exception {

        log.info("/*------------------开启全部配置查询！---------------------------*/");
        //查找出最新的模型数据
        List<ActProcess> actProcesses = actProcessService.list(Wrappers.<ActProcess>lambdaQuery()
        .eq(ActProcess::getLatest,true));
        if (ToolUtil.isNotEmpty(actProcesses)){
            List<ActProcessVo> processInsVos = actProcesses.stream().map(act -> {
                ActProcessVo actProcessVo = new ActProcessVo();
                actProcessVo.setProceDefId(act.getId());
                if (act.getId().contains(ActivitiConstant.indentOrder)){
                    actProcessVo.setDesc(ActivitiConstant.indentOrder);
                }else if (act.getId().contains(ActivitiConstant.applicationReturnOrder)){
                    actProcessVo.setDesc(ActivitiConstant.applicationReturnOrder);
                }else if (act.getId().contains(ActivitiConstant.aplicationSendOrder)){
                    actProcessVo.setDesc(ActivitiConstant.aplicationSendOrder);
                }else if (act.getId().contains(ActivitiConstant.afterSaleOrder)){
                    actProcessVo.setDesc(ActivitiConstant.afterSaleOrder);
                }else if (act.getId().contains(ActivitiConstant.specialOrder)){
                    actProcessVo.setDesc(ActivitiConstant.specialOrder);
                }else if (act.getId().contains(ActivitiConstant.salesOrder)){
                    actProcessVo.setDesc(ActivitiConstant.salesOrder);
                }
                return actProcessVo;
            }).collect(Collectors.toList());

            //删除之前的redis
          String process = redisTemplate.opsForValue().get(ActivitiConstant.actProcess);
          if (ToolUtil.isNotEmpty(process)){
              redisTemplate.delete(ActivitiConstant.actProcess);
          }
           //重新插入redis
            String processPut = JSON.toJSONString(processInsVos);
            redisTemplate.opsForValue().set(ActivitiConstant.actProcess,processPut);
        }
    }
}
