package cn.exrick.xboot.common.upay.response;


import cn.exrick.xboot.common.upay.enums.BizType;
import cn.exrick.xboot.common.upay.exception.UpayException;
import cn.exrick.xboot.common.upay.utils.CipherUtils;
import cn.exrick.xboot.common.upay.utils.DateUtils;
import cn.exrick.xboot.common.upay.utils.StringUtils;
import cn.exrick.xboot.common.upay.utils.Utils;
import lombok.Data;
import lombok.experimental.Accessors;
import lombok.extern.java.Log;

import java.util.Map;
import java.util.TreeMap;

/**
 * 公共部分请求数据格式
 * Created by jcouyang@chinaums.com on 2017/7/17.
 */
@Log
public class UpayRequest extends Validator{
    protected String msgType;
    protected String msgSrc;
    protected String msgSrcId;
    protected String subOpenId;
    protected String subAppId;
    protected String tradeType;

    public String getSubAppId() {
        return subAppId;
    }

    public void setSubAppId(String subAppId) {
        this.subAppId = subAppId;
    }

    protected String mid;
    protected String tid;
    protected String requestTimestamp = DateUtils.formatNow(DateUtils.YYYY_MM_DD_HH_MM_SS);
    protected String instMid;
    protected String merOrderId;
    protected String totalAmount;
    protected String sign;
    // 默认走RSA
    protected String sign_type = "MD5";
    protected String sign_format = "pkcs8";


    @Override
    public boolean validate() {

        if (StringUtils.isEmpty(merOrderId)) {
            throw new RuntimeException("商户订单号[merOrderId]不可为空");
        }

        if (StringUtils.isEmpty(mid)) {
            throw new RuntimeException("商户号[mid]不可为空");
        }

        if (StringUtils.isEmpty(tid)) {
            throw new RuntimeException("设备终端号[tid]不可为空");
        }
        if (StringUtils.isEmpty(instMid)){
            throw  new RuntimeException("机构商户号[instMid]不可为空");
        }

        if (StringUtils.isEmpty(sign_type)) {
            throw new RuntimeException("签名算法[sign_type]不可为空");
        }

        return true;
    }



    public String getMsgType() {
        return msgType;
    }

    public void setMsgType(String msgType) {
        this.msgType = msgType;
    }

    public String getMsgSrc() {
        return msgSrc;
    }

    public void setMsgSrc(String msgSrc) {
        this.msgSrc = msgSrc;
    }

    public String getMsgSrcId() {
        return msgSrcId;
    }

    public void setMsgSrcId(String msgSrcId) {
        this.msgSrcId = msgSrcId;
    }

    public String getSubOpenId() {
        return subOpenId;
    }

    public void setSubOpenId(String subOpenId) {
        this.subOpenId = subOpenId;
    }

    public String getTradeType() {
        return tradeType;
    }

    public void setTradeType(String tradeType) {
        this.tradeType = tradeType;
    }

    public String getMid() {
        return mid;
    }

    public void setMid(String mid) {
        this.mid = mid;
    }

    public String getTid() {
        return tid;
    }

    public void setTid(String tid) {
        this.tid = tid;
    }

    public String getRequestTimestamp() {
        return requestTimestamp;
    }

    public void setRequestTimestamp(String requestTimestamp) {
        this.requestTimestamp = requestTimestamp;
    }

    public String getInstMid() {
        return instMid;
    }

    public void setInstMid(String instMid) {
        this.instMid = instMid;
    }

    public String getMerOrderId() {
        return merOrderId;
    }

    public void setMerOrderId(String merOrderId) {
        this.merOrderId = merOrderId;
    }

    public String getTotalAmount() {
        return totalAmount;
    }

    public void setTotalAmount(String totalAmount) {
        this.totalAmount = totalAmount;
    }

    public String getSign() {
        return sign;
    }

    public void setSign(String sign) {
        this.sign = sign;
    }

    public String getSign_type() {
        return sign_type;
    }

    public void setSign_type(String sign_type) {
        this.sign_type = sign_type;
    }

    public String getSign_format() {
        return sign_format;
    }

    public void setSign_format(String sign_format) {
        this.sign_format = sign_format;
    }

    /**
     * 获取发送的数据，需要在所有的参数设置完成后再调用该方法
     * @return
     */
    public Map<String, String> getSendData(String key) throws UpayException {
        try {
            // 对象转为Map对象
            TreeMap<String, String> map = (TreeMap<String, String>) Utils.convertBean(this, true);

            // OTA不需要签名
                map.remove("sign");
                String signType   = map.remove("sign_type");
                String signFormat = map.remove("sign_format");

                StringBuffer sbtr = new StringBuffer();
                for (Map.Entry<String, String> entry : map.entrySet()) {
                    sbtr.append(entry.getKey());
                    sbtr.append("=");
                    sbtr.append(entry.getValue());
                    sbtr.append("&");
                }
                String signStr = sbtr.substring(0, sbtr.length() - 1);

                String sign;
                if ("md5".equalsIgnoreCase(signType)) {
                    sign = CipherUtils.md5(signStr);
                    log.info("sign ="+signStr);
                    map.put("sign", sign);
                } else {
                    sign = CipherUtils.sign(signStr, key, "UTF-8");
                    map.put("sign", sign);
                    map.put("sign_format", signFormat);
                    map.put("sign_type", signType);
                }

                this.sign        = sign;

            if(validate()) {
                return map;
            }
            else {
                throw new UpayException("参数校验不通过");
            }

        } catch (Exception e) {
            e.printStackTrace();
            throw new UpayException(e);
        }

    }
}
