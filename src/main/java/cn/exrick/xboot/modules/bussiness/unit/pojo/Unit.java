package cn.exrick.xboot.modules.bussiness.unit.pojo;

import cn.exrick.xboot.base.XbootBaseEntity;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModelProperty;
import io.swagger.annotations.ApiModel;
import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

/**
 * @author tqr
 */
@Data
@Entity
@Table(name = "t_unit")
@TableName("t_unit")
@ApiModel(value = "产品单位")
public class Unit extends XbootBaseEntity {

    private static final long serialVersionUID = 1L;

    /**
     * 单位
     */
    @Column(length = 60)
    private String unitName;

}