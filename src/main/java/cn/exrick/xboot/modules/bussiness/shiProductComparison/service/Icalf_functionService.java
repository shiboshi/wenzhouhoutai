package cn.exrick.xboot.modules.bussiness.shiProductComparison.service;

import com.baomidou.mybatisplus.extension.service.IService;
import cn.exrick.xboot.modules.bussiness.shiProductComparison.pojo.calf_function;

import java.util.List;

/**
 * 小腿功能表接口
 * @author 石博士
 */
public interface Icalf_functionService extends IService<calf_function> {

}