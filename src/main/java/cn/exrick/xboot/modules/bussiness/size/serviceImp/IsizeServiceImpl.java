package cn.exrick.xboot.modules.bussiness.size.serviceImp;

import cn.exrick.xboot.modules.bussiness.size.mapper.sizeMapper;
import cn.exrick.xboot.modules.bussiness.size.pojo.size;
import cn.exrick.xboot.modules.bussiness.size.service.IsizeService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

/**
 * 产品尺寸接口实现
 * @author tqr
 */
@Slf4j
@Service
@Transactional
public class IsizeServiceImpl extends ServiceImpl<sizeMapper, size> implements IsizeService {

    @Autowired
    private sizeMapper sizeMapper;
}