package cn.exrick.xboot.modules.bussiness.applicationSendGoodsOrder.VO;

import cn.exrick.xboot.common.enums.CheckStatus;
import cn.exrick.xboot.common.enums.SendStatus;
import cn.exrick.xboot.common.vo.CommonVO;
import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiModelProperty;
import io.swagger.annotations.ApiOperation;
import lombok.Data;
import org.elasticsearch.action.ValidateActions;
import org.springframework.format.annotation.DateTimeFormat;

import java.util.Date;

/**
 * @Author : xiaofei
 * @Date: 2019/8/9
 */
@Data
public class AppliSendGoodsVO extends CommonVO {

    @ApiModelProperty(value = "订单编号")
    private String orderNum;

    @ApiModelProperty("审核状态")
    private CheckStatus checkStatus;

    @ApiModelProperty("发货状态")
    private SendStatus sendStatus;

    private String taskId;

    private String createBy;
}
