package cn.exrick.xboot.modules.activiti.vo;

import lombok.Data;

/**
 * @Author : xiaofei
 * @Date: 2019/8/18
 */
@Data
public class DeployVO {
    private String id;
    private Integer areaStatus;
    private Integer permissionStauts;
    private String roleId;

}
