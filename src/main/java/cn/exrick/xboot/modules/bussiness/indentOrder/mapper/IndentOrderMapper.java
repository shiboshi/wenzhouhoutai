package cn.exrick.xboot.modules.bussiness.indentOrder.mapper;

import cn.exrick.xboot.modules.bussiness.indentOrder.VO.IndentOrderVO;
import cn.exrick.xboot.modules.bussiness.indentOrder.sql.IndentOrderSql;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import cn.exrick.xboot.modules.bussiness.indentOrder.pojo.IndentOrder;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.apache.ibatis.annotations.*;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Repository;

/**
 * 订货单数据处理层
 * @author xiaofei
 */
@Repository
public interface IndentOrderMapper extends BaseMapper<IndentOrder> {

    /**
     * 获取订货单列表
     * @param page
     * @param userId
     * @param orderNum
     * @return
     */
    @SelectProvider(type = IndentOrderSql.class,method = "selectList")
    @ResultType(IndentOrderVO.class)
    Page<IndentOrderVO> selectIndentOrderList(@Param("page") Page page,@Param("code") Integer code,@Param("userId") String userId,@Param("orderNum") String orderNum);

    @SelectProvider(type = IndentOrderSql.class,method = "updateIsComplete")
    void updateIsComplete(@Param("indentOrderId")String indentOrderId);

    @SelectProvider(type = IndentOrderSql.class,method = "getById")
    IndentOrderVO getById(@Param("id") String id);

    @Delete("delete from x_indent_order where id = #{id}")
    void deleteIndent(@Param("id")String id);
}