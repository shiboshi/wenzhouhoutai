package cn.exrick.xboot.modules.bussiness.classification.controller;

import cn.exrick.xboot.common.constant.CommonConstant;
import cn.exrick.xboot.common.utils.PageUtil;
import cn.exrick.xboot.common.utils.ResultUtil;
import cn.exrick.xboot.common.utils.SecurityUtil;
import cn.exrick.xboot.common.vo.PageVo;
import cn.exrick.xboot.common.vo.Result;
import cn.exrick.xboot.modules.bussiness.classification.VO.ClassificationVo;
import cn.exrick.xboot.modules.bussiness.classification.pojo.Classification;
import cn.exrick.xboot.modules.bussiness.classification.service.IClassificationService;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import java.util.Date;
import java.util.List;

/**
 * @author tqr
 */
@Slf4j
@RestController
@Api(description = "产品分类表管理接口")
@RequestMapping("/xboot/classification")
@Transactional
public class ClassificationController {
    @Autowired
    private SecurityUtil securityUtil;

    @Autowired
    private IClassificationService iClassificationService;

    @RequestMapping(value = "/get/{id}", method = RequestMethod.GET)
    @ApiOperation(value = "通过id获取")
    public Result<Classification> get(@PathVariable String id){

        Classification classification = iClassificationService.getById(id);
        return new ResultUtil<Classification>().setData(classification);
    }

    @RequestMapping(value = "/getAll", method = RequestMethod.GET)
    @ApiOperation(value = "获取全部数据")
    public Result<List<Classification>> getAll(){

        List<Classification> list = iClassificationService.list();
        return new ResultUtil<List<Classification>>().setData(list);
    }

    @RequestMapping(value = "/getByPage", method = RequestMethod.GET)
    @ApiOperation(value = "分页获取")
    public Result<IPage<ClassificationVo>> getByPage(@ModelAttribute PageVo page, String name){
        IPage<ClassificationVo> data = new Page<>();
        if (StringUtils.isNotBlank(name)){
            data =iClassificationService.getByName(PageUtil.initMpPage(page),name);
        } else {
            data =iClassificationService.page(PageUtil.initMpPage(page));
        }
        return new ResultUtil<IPage<ClassificationVo>>().setData(data);
    }

    @RequestMapping(value = "/insertOrUpdate", method = RequestMethod.POST)
    @ApiOperation(value = "编辑或更新数据")
    public Result<Classification> saveOrUpdate(@ModelAttribute Classification classification){
        String username = securityUtil.getCurrUser().getUsername();
        classification.setCreateBy(username);
        classification.setUpdateBy(username);
        classification.setCreateTime(new Date());
        classification.setUpdateTime(new Date());
        if (!iClassificationService.saveOrUpdate(classification)) {
            return new ResultUtil<Classification>().setErrorMsg("添加失败");
        }
        if(!CommonConstant.PARENT_ID.equals(classification.getParentId())){
            Classification parent = iClassificationService.getById(classification.getParentId());
            if(parent.getIsParent()==null||!parent.getIsParent()){
                parent.setIsParent(true);
                iClassificationService.saveOrUpdate(parent);
            }
        }
        return new ResultUtil<Classification>().setSuccessMsg("添加成功");
    }

    @RequestMapping(value = "/delByIds/{ids}", method = RequestMethod.DELETE)
    @ApiOperation(value = "批量通过id删除")
    public Result<Object> delAllByIds(@PathVariable String[] ids){

        for(String id : ids){
            iClassificationService.removeById(id);
        }
        return new ResultUtil<Object>().setSuccessMsg("批量通过id删除数据成功");
    }

    @GetMapping("/getByParentId/{parentId}")
    @ApiOperation(value = "通过parentId获取")
    public Result<List<Classification>> getByParentId(@PathVariable String parentId) {
        List<Classification> list = iClassificationService.findByParentIdOrderBySortOrder(parentId);
        list.forEach(c -> {
            if(!CommonConstant.PARENT_ID.equals(c.getParentId())){
                Classification parent = iClassificationService.getById(c.getParentId());
                c.setParentTitle(parent.getTitle());
            }else{
                c.setParentTitle("一级分类");
            }
        });
        return new ResultUtil<List<Classification>>().setData(list);
    }

}
