package cn.exrick.xboot.modules.bussiness.troubleList.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import cn.exrick.xboot.modules.bussiness.troubleList.pojo.TroubleList;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

/**
 * 问题表数据处理层
 * @author tqr
 */
@Mapper
public interface TroubleListMapper extends BaseMapper<TroubleList> {

}