package cn.exrick.xboot.common.enums;

import com.baomidou.mybatisplus.annotation.EnumValue;
import com.fasterxml.jackson.annotation.JsonValue;

/**
 * @Author : xiaofei
 * @Date: 2019/8/17
 */
public enum ReturnStatus {
    PENDING(1,"待退货"),
    DOING(2,"退货完成");

    @EnumValue
    private final int cdoe;
    private String desc;

    public int getCdoe() {
        return cdoe;
    }

    @JsonValue
    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    ReturnStatus(int cdoe, String desc) {
        this.cdoe = cdoe;
        this.desc = desc;
    }
}
