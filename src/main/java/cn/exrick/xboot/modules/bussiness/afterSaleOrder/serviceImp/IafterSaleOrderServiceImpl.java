package cn.exrick.xboot.modules.bussiness.afterSaleOrder.serviceImp;

import cn.exrick.xboot.modules.bussiness.afterSaleOrder.mapper.afterSaleOrderMapper;
import cn.exrick.xboot.modules.bussiness.afterSaleOrder.pojo.AfterSaleOrder;
import cn.exrick.xboot.modules.bussiness.afterSaleOrder.service.IafterSaleOrderService;
import cn.exrick.xboot.modules.bussiness.saleOrder.VO.SalesOrderVO;
import cn.exrick.xboot.modules.shiUtil.JedisDelete;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * 销售单维修单接口实现
 * @author fei
 */
@Slf4j
@Service
@Transactional
public class IafterSaleOrderServiceImpl extends ServiceImpl<afterSaleOrderMapper, AfterSaleOrder> implements IafterSaleOrderService {

    @Autowired
    private afterSaleOrderMapper afterSaleOrderMapper;



    /**
     * 根据当前的登录人和审核状态为2，
     * 审核成功查询销售订单的数据
     */
    @Override
    public List<SalesOrderVO> QueryAfterSaleOrder(String buyerName) {
        JedisDelete d = new JedisDelete();
        d.Delete();
        return afterSaleOrderMapper.QueryAfterSaleOrder(buyerName);
    }








}