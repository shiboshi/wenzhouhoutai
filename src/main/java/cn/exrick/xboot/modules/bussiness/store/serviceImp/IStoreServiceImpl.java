package cn.exrick.xboot.modules.bussiness.store.serviceImp;

import cn.exrick.xboot.modules.bussiness.shiVO.StoreVO;
import cn.exrick.xboot.modules.bussiness.store.mapper.StoreMapper;
import cn.exrick.xboot.modules.bussiness.store.pojo.Store;
import cn.exrick.xboot.modules.bussiness.store.service.IStoreService;
import cn.exrick.xboot.modules.shiUtil.JedisDelete;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import lombok.extern.slf4j.Slf4j;
import org.apache.ibatis.annotations.Param;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * 门店表接口实现
 * @author tqr
 */
@Slf4j
@Service
@Transactional
public class IStoreServiceImpl extends ServiceImpl<StoreMapper, Store> implements IStoreService {

    @Autowired
    private StoreMapper storeMapper;




    /**
     * 查询所有集合
     */
    public IPage<StoreVO> QueryStore(Page page, @Param("username") String username){
        JedisDelete d = new JedisDelete();
        d.Delete();
        return storeMapper.QueryStore(page,username);
    }








}