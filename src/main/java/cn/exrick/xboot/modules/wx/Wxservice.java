package cn.exrick.xboot.modules.wx;

import cn.exrick.xboot.common.constant.ActivitiConstant;
import cn.exrick.xboot.common.utils.SecurityUtil;
import cn.exrick.xboot.common.utils.SnowFlakeUtil;
import cn.exrick.xboot.common.utils.ToolUtil;
import cn.exrick.xboot.common.vo.PageVo;
import cn.exrick.xboot.config.springContext.SpringContextHolder;
import cn.exrick.xboot.modules.UpayApi.WxPay.PayVO;
import cn.exrick.xboot.modules.UpayApi.WxPay.ResponseVo;
import cn.exrick.xboot.modules.UpayApi.test.test.WxpayService;
import cn.exrick.xboot.modules.activiti.entity.ActBusiness;
import cn.exrick.xboot.modules.activiti.entity.ActNode;
import cn.exrick.xboot.modules.activiti.service.ActBusinessService;
import cn.exrick.xboot.modules.activiti.service.ActNodeService;
import cn.exrick.xboot.modules.activiti.service.ActProcessService;
import cn.exrick.xboot.modules.activiti.service.mybatis.IHistoryIdentityService;
import cn.exrick.xboot.modules.activiti.utils.MessageUtil;
import cn.exrick.xboot.modules.activiti.vo.ActProcessVo;
import cn.exrick.xboot.modules.activiti.vo.ProcessNodeVo;
import cn.exrick.xboot.modules.base.entity.Department;
import cn.exrick.xboot.modules.base.entity.User;
import cn.exrick.xboot.modules.base.service.IDepartmentService;
import cn.exrick.xboot.modules.base.service.IUserService;
import cn.exrick.xboot.modules.bussiness.afterSaleOrder.VO.AfterSaleVO;
import cn.exrick.xboot.modules.bussiness.afterSaleOrder.pojo.AfterSaleOrder;
import cn.exrick.xboot.modules.bussiness.bothSale.pojo.BothSale;
import cn.exrick.xboot.modules.bussiness.bothSale.service.IBothSaleService;
import cn.exrick.xboot.modules.bussiness.afterSaleOrder.service.IafterSaleOrderService;
import cn.exrick.xboot.modules.bussiness.classification.pojo.Classification;
import cn.exrick.xboot.modules.bussiness.classification.service.IClassificationService;
import cn.exrick.xboot.modules.bussiness.inventoryOrder.pojo.InventoryOrder;
import cn.exrick.xboot.modules.bussiness.inventoryOrder.service.IInventoryOrderService;
import cn.exrick.xboot.modules.bussiness.product.VO.ProductCommon;
import cn.exrick.xboot.modules.bussiness.product.pojo.ProductOrder;
import cn.exrick.xboot.modules.bussiness.product.service.IproductOrderService;
import cn.exrick.xboot.modules.bussiness.productPrice.pojo.ProductPrice;
import cn.exrick.xboot.modules.bussiness.saleOrder.VO.SalesOrderVO;
import cn.exrick.xboot.modules.bussiness.saleOrder.pojo.SalesOrder;
import cn.exrick.xboot.modules.bussiness.saleOrder.service.IsalesOrderService;
import cn.exrick.xboot.modules.wx.VO.DebugDetail;
import cn.exrick.xboot.modules.wx.VO.EditNodeIdUserVO;
import cn.exrick.xboot.modules.wx.VO.WxUserVO;
import cn.hutool.core.util.StrUtil;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import org.activiti.engine.IdentityService;
import org.activiti.engine.RuntimeService;
import org.activiti.engine.TaskService;
import org.activiti.engine.runtime.ProcessInstance;
import org.activiti.engine.task.Task;
import org.activiti.engine.task.TaskQuery;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.DependsOn;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

/**
 * @Author : xiaofei
 * @Date: 2019/8/29
 */
@Component
@DependsOn("springContextHolder")
@Transactional
public class Wxservice {

    @Autowired
    private StringRedisTemplate redisTemplate;
    @Autowired
    private IdentityService identityService;
    @Autowired
    private RuntimeService runtimeService;
    @Autowired
    private TaskService taskService;
    @Autowired
    private MessageUtil messageUtil;
    @Autowired
    private ActBusinessService actBusinessService;
    @Autowired
    private ActProcessService actProcessService;
    @Autowired
    private IInventoryOrderService inventoryOrderService;
    @Autowired
    private IproductOrderService iproductOrderService;
    @Autowired
    private WxpayService wxpayService;
    @Autowired
    private IClassificationService classificationService;
    @Autowired
    private ActNodeService actNodeService;
    @Autowired
    private IHistoryIdentityService historyIdentityService;
    @Autowired
    private SecurityUtil securityUtil;
    @Autowired
    private IBothSaleService iBothSaleService;
    @Autowired
    private IDepartmentService departmentService;
    @Autowired
    private IUserService userService;
    @Autowired
    private IafterSaleOrderService iafterSaleOrderService;

    public static Wxservice me() {
        return SpringContextHolder.getBean("wxservice");
    }

    /**
     * 过滤出需要的流程数据
     *
     * @param actProcessVos
     * @return
     */
    public ActProcessVo getFilter(List<ActProcessVo> actProcessVos, String type) {
        List<ActProcessVo> actProcessVoList = actProcessVos.stream().filter(act -> type.equals(act.getDesc())).collect(Collectors.toList());
        return actProcessVoList.get(0);
    }

    /**
     * 获取redis的流程定义信息
     *
     * @return
     */
    public String getProcDefId(String type) {
        //获取redis的值
        String param = redisTemplate.opsForValue().get(ActivitiConstant.actProcess);
        List<ActProcessVo> actProcessVos = JSON.parseArray(param, ActProcessVo.class);
        if (ToolUtil.isNotEmpty(actProcessVos)) {
            return getFilter(actProcessVos, type).getProceDefId();
        }
        return "";
    }

    /**
     * 获取流程id
     *
     * @param actBusiness
     * @return
     */
    public String startProcess(ActBusiness actBusiness, String userId) {

        identityService.setAuthenticatedUserId(userId);
        actBusiness.getParams().put("tableId", actBusiness.getTableId());
        ProcessInstance processInstance = runtimeService.startProcessInstanceById(actBusiness.getProcDefId(), actBusiness.getId(), actBusiness.getParams());
        List<Task> tasks = taskService.createTaskQuery().processInstanceId(processInstance.getId()).list();
        tasks.forEach(task -> {
            Arrays.stream(actBusiness.getAssignees()).forEach(assi -> {
                taskService.addCandidateUser(task.getId(), assi);
                // 异步发消息
                messageUtil.sendActMessage(assi, ActivitiConstant.MESSAGE_TODO_CONTENT, actBusiness.getSendMessage(),
                        actBusiness.getSendSms(), actBusiness.getSendEmail());
            });
            //设置优先级
            taskService.setPriority(task.getId(), actBusiness.getPriority());
        });
        return processInstance.getId();
    }

    /**
     * 启动流程
     *
     * @param act
     * @param userId
     */
    public void start(ActBusiness act, String userId) {
        String proceInstanceId = startProcess(act, userId);
        act.setProcInstId(proceInstanceId);
        act.setStatus(ActivitiConstant.STATUS_DEALING);
        act.setResult(ActivitiConstant.RESULT_DEALING);
        act.setApplyTime(new Date());
        actBusinessService.update(act);
    }

    public ActBusiness checkUser(ActBusiness actBusiness, String type) {
        ProcessNodeVo processNodeVo = actProcessService.getFirstNode(getProcDefId(type));
        List<User> users = processNodeVo.getUsers();
        List<String> assigneesId = users.stream().map(user -> user.getId()).collect(Collectors.toList());
        String[] ids = new String[assigneesId.size()];
        actBusiness.setAssignees(assigneesId.toArray(ids));
        actBusiness.setPriority(0);
        return actBusiness;
    }

    /**
     * 获取当前登录审批用户的待办任务集合
     *
     * @param pageVo
     * @param userId
     * @return
     */
    public List<Task> getTaskListByUser(PageVo pageVo, String userId) {

        TaskQuery taskQuery = taskService.createTaskQuery().taskCandidateOrAssigned(userId);
        taskQuery.orderByTaskCreateTime().desc();

        int first = (pageVo.getPageNumber() - 1) * pageVo.getPageSize();
        List<Task> tasks = taskQuery.listPage(first, pageVo.getPageSize());
        return tasks;
    }

    /**
     * 通过任务获取表id
     *
     * @param task
     * @return
     */
    public String getTableId(Task task) {

        ProcessInstance processInstance = runtimeService.createProcessInstanceQuery()
                .processInstanceId(task.getProcessInstanceId())
                .singleResult();
        ActBusiness actBusiness = actBusinessService.get(processInstance.getBusinessKey());
        if (ToolUtil.isNotEmpty(actBusiness)) {
            return actBusiness.getTableId();
        }
        return "";
    }

    /**
     * 获取消费者售后详情
     *
     * @return
     */
    public DebugDetail getAfterSaleVoDetail(String id){
        AfterSaleOrder order = iafterSaleOrderService.getById(id);
        if (ToolUtil.isNotEmpty(order)) {
            DebugDetail debugDetail = new DebugDetail();
            InventoryOrder inventoryOrder = inventoryOrderService.getOne(Wrappers.<InventoryOrder>lambdaQuery()
                    .eq(InventoryOrder::getSerialNum, order.getSerials())
                    .eq(InventoryOrder::getSerialNumOut, 2));
            if (ToolUtil.isNotEmpty(inventoryOrder)) {
                ProductCommon productCommon = iproductOrderService.getProductCommon(inventoryOrder.getProductOrderId());
                ToolUtil.copyProperties(productCommon, debugDetail);
                ToolUtil.copyProperties(order, debugDetail);
            }
            return debugDetail;
        }
        return null;
    }

    /**
     * 支付接口
     *
     * @param payVO
     * @return
     */
    public ResponseVo pay(PayVO payVO) {
        JSONObject jsonObject = JSONObject.parseObject(wxpayService.pay(payVO));
        JSONObject object = (JSONObject) jsonObject.get("miniPayRequest");
        ResponseVo responseVo = new ResponseVo();
        responseVo.setAppId((String) object.get("appId"))
                .setNonceStr((String) object.get("nonceStr"))
                .setPaySign((String) object.get("paySign"))
                .setPrepay_id(StrUtil.sub((String) object.get("package"), 10, ((String) object.get("package")).length() - 1))
                .setSignType((String) object.get("signType"))
                .setPackagex((String) object.get("package"))
                .setTimeStamp((String) object.get("timeStamp"));
        return responseVo;
    }

    /**
     * 筛选选择的产品分类
     *
     * @param productPrice
     * @param classificationId
     * @return
     */
    public boolean filterProduct(ProductPrice productPrice, String classificationId) {

        Classification classification = classificationService.getById(productPrice.getProductOrderId());
        if (ToolUtil.isNotEmpty(classification)) {
            if (classification.getParentId().equals(classificationId)) {
                return true;
            }
        }
        return false;
    }

    /**
     * 重新分配审批节点
     *
     * @param editNodeIdUserVO
     */
    public void editNodeUser(EditNodeIdUserVO editNodeIdUserVO) {
        if (!ToolUtil.isOneEmpty(editNodeIdUserVO.getProcInstId())) {

            ProcessNodeVo processNodeVo = actProcessService.getNextNode(editNodeIdUserVO.getProcInstId());
            if (ToolUtil.isNotEmpty(processNodeVo)) {
                actNodeService.deleteByNodeId(processNodeVo.getId());
                //重新分配用户信息
                ActNode actNode = new ActNode();
                actNode.setNodeId(processNodeVo.getId());
                if ("1".equals(editNodeIdUserVO.getCode())) {
                    actNode.setRelateId(editNodeIdUserVO.getDealerDeptId());
                    actNode.setType(ActivitiConstant.NODE_USER);
                    //查询
                    BothSale bothSale = iBothSaleService.getById(editNodeIdUserVO.getTableId());
                    if (ToolUtil.isNotEmpty(bothSale)) {
                        bothSale.setDealerDeptId(editNodeIdUserVO.getDealerDeptId());
                        iBothSaleService.updateById(bothSale);
                    }
                }
                if (ToolUtil.isEmpty(editNodeIdUserVO.getCode())){
                    actNode.setRelateId(editNodeIdUserVO.getDebugId());
                    actNode.setType(ActivitiConstant.NODE_USER);
                }
                actNodeService.save(actNode);
            }
            //启动流程
            if (ToolUtil.isEmpty(editNodeIdUserVO.getComment())) {
                editNodeIdUserVO.setComment("");
            }
            //activity消息提醒
            taskService.addComment(editNodeIdUserVO.getTaskId(), editNodeIdUserVO.getProcInstId(), editNodeIdUserVO.getComment());
            Task taskCheck = taskService.createTaskQuery().taskId(editNodeIdUserVO.getTaskId()).singleResult();
            taskService.complete(taskCheck.getId());

            //分配审批节点
            List<Task> tasks = taskService.createTaskQuery().processInstanceId(editNodeIdUserVO.getProcInstId()).list();
            if (ToolUtil.isNotEmpty(tasks)) {

                tasks.stream().forEach(t -> {
                    List<User> users = actProcessService.getNode(t.getTaskDefinitionKey()).getUsers();
                    for (User user : users) {
                        //根据设置的角色获取审批人
                        taskService.addCandidateUser(t.getId(), user.getId());
                        // 异步发消息
                        messageUtil.sendActMessage(user.getId(), ActivitiConstant.MESSAGE_TODO_CONTENT, false, false, false);
                    }
                });
            }

            // 记录实际审批人员
            historyIdentityService.insert(String.valueOf(SnowFlakeUtil.getFlowIdInstance().nextId()),
                    ActivitiConstant.EXECUTOR_TYPE, securityUtil.getCurrUser().getId(), editNodeIdUserVO.getTaskId(), editNodeIdUserVO.getProcInstId());

        }
    }

    /**
     * 获取当前用户部门的下级部门的人员
     * @return
     */
    public List<WxUserVO> getAllUser(){
       //1.获取当前登录者的下级部门
        //2.获取所有部门下面的人员

        List<Department> departments = departmentService.list(Wrappers.<Department>lambdaQuery()
        .eq(Department::getParentId,userService.getById(securityUtil.getCurrUser().getId()).getDepartmentId()));

        List<WxUserVO> wxUserVOS = departments.stream().parallel().map(d -> {

            WxUserVO wxUserVO = new WxUserVO();
            List<User> users = userService.list(Wrappers.<User>lambdaQuery()
            .eq(User::getDepartmentId,d.getId()));
            if (ToolUtil.isNotEmpty(users)){
                for (User user : users) {
                    wxUserVO.setUserId(user.getId())
                            .setUserName(user.getUsername());
                    return wxUserVO;
                }
            }
            return  null;
        }).collect(Collectors.toList());

        return wxUserVOS;
    }
}
