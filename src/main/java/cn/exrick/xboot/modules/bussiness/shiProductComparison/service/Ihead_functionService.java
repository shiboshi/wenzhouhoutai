package cn.exrick.xboot.modules.bussiness.shiProductComparison.service;

import com.baomidou.mybatisplus.extension.service.IService;
import cn.exrick.xboot.modules.bussiness.shiProductComparison.pojo.head_function;

import java.util.List;

/**
 * 头部功能接口
 * @author 石博士
 */
public interface Ihead_functionService extends IService<head_function> {

}