package cn.exrick.xboot.modules.bussiness.shiRechargeType.serviceImp;

import cn.exrick.xboot.modules.bussiness.shiRechargeType.mapper.ShiRechargeTypeMapper;
import cn.exrick.xboot.modules.bussiness.shiRechargeType.pojo.ShiRechargeType;
import cn.exrick.xboot.modules.bussiness.shiRechargeType.service.IShiRechargeTypeService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

/**
 * 充值分类接口实现
 * @author tqr
 */
@Slf4j
@Service
@Transactional
public class IShiRechargeTypeServiceImpl extends ServiceImpl<ShiRechargeTypeMapper, ShiRechargeType> implements IShiRechargeTypeService {

    @Autowired
    private ShiRechargeTypeMapper shiRechargeTypeMapper;
}