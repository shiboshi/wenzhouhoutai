package cn.exrick.xboot.modules.wx.VO;

import lombok.Data;

/**重新分配审批用户信息DTO
 * @Author : xiaofei
 * @Date: 2019/9/18
 */
@Data
public class EditNodeIdUserVO {


    /**
     * 任务id
     */
    private String taskId;
    /**
     * 流程实例id
     */
    private  String procInstId;

    /**
     * 分配的用户id
     */
    private  String debugId;

    /**
     * 评论信息
     */
    private  String comment;

    /**
     * 类型：1两地销售选择部门，
     */
    private  String code;

    /**
     * 订单id
     */
    private String tableId;

    /**
     * 经销商部门id
     */
    private String dealerDeptId;
}
