package cn.exrick.xboot.modules.bussiness.color.pojo;

import cn.exrick.xboot.base.XbootBaseEntity;
import cn.exrick.xboot.common.constant.CommonConstant;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModelProperty;
import io.swagger.annotations.ApiModel;
import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import java.math.BigDecimal;

/**
 * @author tqr
 */
@Data
@Entity
@Table(name = "t_color")
@TableName("t_color")
@ApiModel(value = "产品颜色")
public class Color extends XbootBaseEntity {

    private static final long serialVersionUID = 1L;



    /**
     * 父级
     */

    @ApiModelProperty("分类名称")
    private String title;


    @ApiModelProperty("父id")
    private String parentId;

    @ApiModelProperty("parentTitle")
    private String parentTitle;

    @ApiModelProperty(value = "是否为父节点(含子节点) 默认false")
    private Boolean isParent = false;

    @ApiModelProperty(value = "是否启用 0启用 -1禁用")
    private Integer status = CommonConstant.STATUS_NORMAL;

    @ApiModelProperty(value = "排序值")
    private BigDecimal sortOrder;

}