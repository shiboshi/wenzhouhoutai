package cn.exrick.xboot.modules.bussiness.monthlyIndicators.pojo;

import cn.exrick.xboot.base.XbootBaseEntity;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModelProperty;
import io.swagger.annotations.ApiModel;
import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.Table;

/**
 * @author tqr
 */
@Data
@Entity
@Table(name = "t_monthly_indicators")
@TableName("t_monthly_indicators")
@ApiModel(value = "月份指标")
public class MonthlyIndicators extends XbootBaseEntity {

    private static final long serialVersionUID = 1L;

    private String january;
    private String february;
    private String march;
    private String april;
    private String may;
    private String june;
    private String july;
    private String august;
    private String september;
    private String october;
    private String november;
    private String december;

}