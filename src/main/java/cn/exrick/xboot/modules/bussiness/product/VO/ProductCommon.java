package cn.exrick.xboot.modules.bussiness.product.VO;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Builder;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

/**
 * 产品公共类
 * @Author : xiaofei
 * @Date: 2019/8/9
 */
@Data
@Builder
public class ProductCommon{

    private String id;
    private String productName;
    private String productModelName;
    private String productOrderName;

    private String color;
    private String colorNumber;
    private String material;
    private String conversionRatio;
    private  String molecule;

    /**
     * 默认单位
     */
    private String unitName;

    private BigDecimal price;
    /**
     * 单台体积
     */
    private String volume;

    /**
     * 单台毛重
     */
    private String unitWeight;


    private String packageSize;

    /**
     * 上市时间
     */
    @JsonFormat(timezone = "GMT+8", pattern = "yyyy-MM-dd")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private Date timeToMarket;


    /**
     * 最低起订量
     */
    private BigDecimal minOrderAmount;

    /**
     * 最低销售价
     */
    private BigDecimal minSellingPrice;

    /**
     * 全国统一价格
     */
    private BigDecimal uniformSalesPrice;

    /**
     * 两地费
     */
    private BigDecimal bothfee;

    /**
     * 产品和报告
     */
    private List<String> picPath;
    private List<String> reportCertificate;
    /**
     * 产品分类id
     */
    private String kindId;


    private String remark;
    private String amount;
    private String modelNameIn;
    private Integer payType;
    /**
     * 最小单位
     */
    private String unitMinName;

    /**
     * 最大单位
     */
    private String unitMaxName;

    private BigDecimal productNum;

    /**
     * 收货数量
     */
    private String quantityReceived;


}
