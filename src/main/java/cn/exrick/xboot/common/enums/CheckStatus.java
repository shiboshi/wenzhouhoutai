package cn.exrick.xboot.common.enums;

import com.baomidou.mybatisplus.annotation.EnumValue;
import com.fasterxml.jackson.annotation.JsonValue;

/**
 * @Author : xiaofei
 * @Date: 2019/8/16
 */
public enum CheckStatus {

    pengindg(1,"待审核"),
    sucess(2,"审核成功"),
    fail(3,"审核失败");


    @EnumValue
    private final  int code;

    public int getCode() {
        return code;
    }

    @JsonValue
    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    private String desc;

    CheckStatus(int code, String desc) {
        this.code = code;
        this.desc = desc;
    }
}
