package cn.exrick.xboot.modules.bussiness.bothSale.controller;

import cn.exrick.xboot.common.utils.PageUtil;
import cn.exrick.xboot.common.utils.ResultUtil;
import cn.exrick.xboot.common.vo.PageVo;
import cn.exrick.xboot.common.vo.Result;
import cn.exrick.xboot.modules.base.entity.User;
import cn.exrick.xboot.modules.bussiness.bothSale.pojo.BothSale;
import cn.exrick.xboot.modules.bussiness.bothSale.service.IBothSaleService;
import cn.exrick.xboot.modules.bussiness.inventoryOrder.VO.BothSaleVO;
import cn.exrick.xboot.modules.bussiness.product.VO.ProductCommon;
import com.baomidou.mybatisplus.core.metadata.IPage;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.transaction.annotation.Transactional;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author tqr
 */
@Slf4j
@RestController
@Api(description = "两地销售管理接口")
@RequestMapping("/xboot/bothSale")
@Transactional
public class BothSaleController {

    @Autowired
    private IBothSaleService iBothSaleService;


    @RequestMapping(value = "/delByIds/{ids}", method = RequestMethod.DELETE)
    @ApiOperation(value = "批量通过id删除")
    public Result<Object> delAllByIds(@PathVariable String[] ids) {

        for (String id : ids) {
            iBothSaleService.removeById(id);
        }
        return new ResultUtil<Object>().setSuccessMsg("批量通过id删除数据成功");
    }

    /**
     * 添加两地销售申请
     */
    @PostMapping("/insertBothSaleCommit")
    public Result<BothSale> insertBothSaleCommit(BothSaleVO bothSaleVO, String procDefId
    ) {
        Result<BothSale> result = iBothSaleService.insertBothSaleCommit(bothSaleVO, procDefId);
        return result;
    }

    /**
     * 查询两地销售申请
     */
    @PostMapping("/getBothSaleCommit")
    public Result<IPage<BothSaleVO>> getBothSaleCommit(@ModelAttribute PageVo page, String userName, String id
    ) {
        IPage<BothSaleVO> data = iBothSaleService.getBothSaleCommit(PageUtil.initMpPage(page), userName, id);
        return new ResultUtil<IPage<BothSaleVO>>().setData(data);
    }

    /**
     * 用户查询两地销售申请
     */
    @PostMapping("/getBothSaleCommitByUser")
    public Result<IPage<BothSaleVO>> getBothSaleCommitByUser(@ModelAttribute PageVo page, String id, String code
    ) {
        IPage<BothSaleVO> data = iBothSaleService.getBothSaleCommitByUser(PageUtil.initMpPage(page), id, code);
        return new ResultUtil<IPage<BothSaleVO>>().setData(data);
    }

    /**
     * 生成两地销售单
     */
    @PostMapping("/insertBothSale")
    public Result<BothSale> insertBothSale(String id, String procDefId,String productIds
    ) {
        Result<BothSale> result = iBothSaleService.insertBothSale(id, procDefId,productIds);
        return result;
    }

    /**
     * 查询两地销售单
     */
    @PostMapping("/getBothSale")
    public Result<IPage<BothSaleVO>> getBothSale(@ModelAttribute PageVo page, String userName, String id
    ) {
        IPage<BothSaleVO> data = iBothSaleService.getBothSale(PageUtil.initMpPage(page), userName, id);
        return new ResultUtil<IPage<BothSaleVO>>().setData(data);
    }

    /**
     * 用户查询两地销售
     */
    @PostMapping("/getBothSaleByUser")
    public Result<IPage<BothSaleVO>> getBothSaleByUser(@ModelAttribute PageVo page, String id, String code
    ) {
        IPage<BothSaleVO> data = iBothSaleService.getBothSaleByUser(PageUtil.initMpPage(page), id, code);
        return new ResultUtil<IPage<BothSaleVO>>().setData(data);
    }

    /**
     * 用户查询两地销售详情
     */
    @PostMapping("/getBothSaleDetail")
    public Result<Map<String, Object>> getBothSaleDetail(@ModelAttribute PageVo page, String id
    ) {
        Map<String, Object> data = new HashMap<>();
        List<ProductCommon> list = iBothSaleService.getBothSaleDetail(PageUtil.initMpPage(page), id);
        data.put("total", list.size());
        list = PageUtil.listToPage(page, list);
        data.put("result", list);
        return new ResultUtil<Map<String, Object>>().setData(data);
    }

    /**
     * 修改
     */
    @PostMapping("/updateById")
    public Result<BothSale> updateById(@ModelAttribute BothSaleVO bothSaleVO
    ) {
        Result<BothSale> result = iBothSaleService.updateById(bothSaleVO);
        return result;
    }

    /**
     * 修改
     */
    @PostMapping("/choose")
    public Result<Object> choose(String id, String dealerDeptId, String procInstId
    ) {
        iBothSaleService.choose(id, dealerDeptId, procInstId);
        return new ResultUtil<>().setSuccessMsg("选择成功");
    }

    @PostMapping("/getDealer")
    public Result<Object> getDealer(String id, PageVo page
    ) {
        List<User> list = iBothSaleService.getDealer(PageUtil.initMpPage(page),id);
        return new ResultUtil<>().setData(list);
    }
}
