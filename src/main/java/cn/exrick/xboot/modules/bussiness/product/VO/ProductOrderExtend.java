package cn.exrick.xboot.modules.bussiness.product.VO;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.math.BigDecimal;

/**
 * @author tqr
 */
@Data
@ApiModel(value = "产品品号")
public class ProductOrderExtend {

    @ApiModelProperty("id")
    private String id;

    @ApiModelProperty("产品分类id")
    private String kindId;

    @ApiModelProperty("产品大类")
    private String productName;

    @ApiModelProperty("产品型号ID")
    private String productModelId;

    @ApiModelProperty("产品型号")
    private String productModelName;

    @ApiModelProperty("产品品号")
    private String productOrderName;

    @ApiModelProperty("产品颜色")
    private String color;
    @ApiModelProperty("产品颜色ID")
    private String colorId;

    @ApiModelProperty("产品色号")
    private String colorNumber;

    private String colorNumberId;

    @ApiModelProperty("产品材质")
    private String material;

    private String materialId;

    @ApiModelProperty("上市时间")
    private String timeToMarket;

    @ApiModelProperty("最低起订量")
    private String minOrderAmount;

    @ApiModelProperty("最低销售价")
    private String minSellingPrice;

    @ApiModelProperty("全国统一价格")
    private String uniformSalesPrice;

    @ApiModelProperty("换算比例")
    private String conversionRatio;

    /**
     * 换算比例分母
     */
    @ApiModelProperty("换算比例分母")
    private  String molecule;

    @ApiModelProperty("两地费")
    private String bothfee;

    @ApiModelProperty("产品单位")
    private String unit;

    @ApiModelProperty("产品单位")
    private String unitName;

    @ApiModelProperty("产品价格")
    private String price;

    @ApiModelProperty("最小单位")
    private String unitMin;

    @ApiModelProperty("最小单位")
    private String unitMinName;

    @ApiModelProperty("最大单位")
    private String unitMax;

    @ApiModelProperty("最大单位")
    private String unitMaxName;

    @ApiModelProperty("报告")
    private String reportCertificate;

    @ApiModelProperty("产品图片")
    private String picPath;

    @ApiModelProperty("备注")
    private String remark;

    @ApiModelProperty("20尺码")
    private  String two;

    @ApiModelProperty("40尺码")
    private  String four;

    @ApiModelProperty("40尺码HQ")
    private  String fourHQ;

    @ApiModelProperty("尺寸")
    private String[] packingSize;
    @ApiModelProperty("颜色/色号/材料")
    private String allName;

    private String totalPrice;
    @ApiModelProperty("净重")
    private BigDecimal unitWeight;

    @ApiModelProperty("毛重")
    private BigDecimal grossWeight;

    @ApiModelProperty("体积")
    private BigDecimal volume;
}
