package cn.exrick.xboot.modules.bussiness.inventoryOrder.serviceImp;

import cn.exrick.xboot.common.enums.IsComplete;
import cn.exrick.xboot.common.utils.SecurityUtil;
import cn.exrick.xboot.common.utils.ToolUtil;
import cn.exrick.xboot.modules.base.dao.mapper.DepartmentMapper;
import cn.exrick.xboot.modules.base.dao.mapper.UserMapper;
import cn.exrick.xboot.modules.base.entity.Department;
import cn.exrick.xboot.modules.base.entity.Role;
import cn.exrick.xboot.modules.bussiness.afterIndentOrder.mapper.afterIndentOrderMapper;
import cn.exrick.xboot.modules.bussiness.afterIndentOrder.pojo.AfterIndentOrder;
import cn.exrick.xboot.modules.bussiness.applicationAfterSaleOrder.mapper.applicationAfterSaleOrderMapper;
import cn.exrick.xboot.modules.bussiness.applicationAfterSaleOrder.pojo.ApplicationAfterSaleOrder;
import cn.exrick.xboot.modules.bussiness.dealerLevel.mapper.DealerLevelMapper;
import cn.exrick.xboot.modules.bussiness.dealerLevel.pojo.DealerLevel;
import cn.exrick.xboot.modules.bussiness.inventoryOrder.VO.InventoryVO;
import cn.exrick.xboot.modules.bussiness.inventoryOrder.mapper.InventoryOrderMapper;
import cn.exrick.xboot.modules.bussiness.inventoryOrder.pojo.InventoryOrder;
import cn.exrick.xboot.modules.bussiness.inventoryOrder.pojo.InventoryOrderResult;
import cn.exrick.xboot.modules.bussiness.inventoryOrder.service.IInventoryOrderService;
import cn.exrick.xboot.modules.bussiness.product.VO.ProductCommon;
import cn.exrick.xboot.modules.bussiness.product.service.IproductOrderService;
import cn.exrick.xboot.modules.bussiness.productPrice.mapper.ProductPriceMapper;
import cn.exrick.xboot.modules.bussiness.productPrice.pojo.ProductPrice;
import cn.exrick.xboot.modules.bussiness.shiWarehouse.pojo.ShiWarehouse;
import cn.exrick.xboot.modules.bussiness.shiWarehouse.service.IShiWarehouseService;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.google.common.base.Splitter;
import com.google.common.collect.Lists;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.util.*;
import java.util.stream.Collectors;

/**
 * 库存表接口实现
 *
 * @author tqr
 */
@Slf4j
@Service
@Transactional
public class IInventoryOrderServiceImpl extends ServiceImpl<InventoryOrderMapper, InventoryOrder> implements IInventoryOrderService {

    @Autowired
    private InventoryOrderMapper inventoryOrderMapper;
    @Autowired
    private SecurityUtil securityUtil;
    @Autowired
    private DepartmentMapper departmentMapper;
    @Autowired
    private IproductOrderService productOrderService;
    @Autowired
    private IShiWarehouseService shiWarehouseService;
    @Autowired
    private UserMapper userMapper;
    @Autowired
    private DealerLevelMapper dealerLevelMapper;
    @Autowired
    private ProductPriceMapper productPriceMapper;
    @Autowired
    private applicationAfterSaleOrderMapper applicationAfterSaleOrderMapper1;
    @Autowired
    private afterIndentOrderMapper afterIndentOrderMapper1;

    private Splitter splitter = Splitter.on(",").trimResults().omitEmptyStrings();

    @Override
    public List<InventoryOrderResult> getByPage(String condition) {
        if (ToolUtil.isNotEmpty(securityUtil.getCurrUser().getRoles())) {
            for (Role role : securityUtil.getCurrUser().getRoles()) {
                if ("ROLE_ADMIN".equals(role.getName())) {
                    String deptId = null;
                    //如果为管理员查看所有库存
                    List<InventoryOrderResult> list1 = inventoryOrderMapper.getByPage(deptId, condition);
                    List<InventoryOrderResult> list = new ArrayList<>();
                    //过滤售后中的库存
                    List<AfterIndentOrder> aftList = afterIndentOrderMapper1.selectList(new QueryWrapper<AfterIndentOrder>().lambda().eq(AfterIndentOrder::getCreateBy, securityUtil.getCurrUser().getId()).eq(AfterIndentOrder::getIsComplete, IsComplete.PENDING));
                    if (ToolUtil.isNotEmpty(aftList)) {
                        //查询售后中的库存
                        List<String> afterIds = new ArrayList<>();
                        for (AfterIndentOrder a : aftList) {
                            ApplicationAfterSaleOrder applicationAfterSaleOrder = applicationAfterSaleOrderMapper1.selectById(a.getApplicationAfterSaleOrderId());
                            List<String> aIds = new ArrayList<>(Arrays.asList(applicationAfterSaleOrder.getInventoryIds().split(",")));
                            afterIds.addAll(aIds);
                        }
                        if (ToolUtil.isNotEmpty(afterIds)) {
                            for (InventoryOrderResult o : list1) {
                                for (String i : afterIds) {
                                    if (!o.getId().equals(i)) {
                                        list.add(o);
                                    }
                                }
                            }
                        }
                    } else {
                        list = list1;
                    }
                    list = list.stream().collect(Collectors.collectingAndThen(Collectors.toCollection(
                            () -> new TreeSet<>(Comparator.comparing(o -> o.getId()))), ArrayList::new));
                    return list;
                }
            }
        }
        String deptId = securityUtil.getCurrUser().getDepartmentId();
        if (ToolUtil.isNotEmpty(deptId)) {
            List<InventoryOrderResult> list1 = getRecursion(deptId, condition);
            list1 = list1.stream()
                    .collect(Collectors
                            .collectingAndThen
                                    (Collectors.toCollection(() -> new TreeSet<InventoryOrderResult>(Comparator.comparing(t -> t.getId()))), ArrayList::new)
                    );
            List<InventoryOrderResult> list = new ArrayList<>();
            //过滤售后中的库存
            List<AfterIndentOrder> aftList = afterIndentOrderMapper1.selectList(new QueryWrapper<AfterIndentOrder>().lambda().eq(AfterIndentOrder::getCreateBy, securityUtil.getCurrUser().getId()).eq(AfterIndentOrder::getIsComplete, IsComplete.PENDING.getValue()));
            if (ToolUtil.isNotEmpty(aftList)) {
                //查询售后中的库存
                List<String> afterIds = new ArrayList<>();
                for (AfterIndentOrder a : aftList) {
                    ApplicationAfterSaleOrder applicationAfterSaleOrder = applicationAfterSaleOrderMapper1.selectById(a.getApplicationAfterSaleOrderId());
                    List<String> aIds = new ArrayList<>(Arrays.asList(applicationAfterSaleOrder.getInventoryIds().split(",")));
                    afterIds.addAll(aIds);
                }
                if (ToolUtil.isNotEmpty(afterIds)) {
                    for (InventoryOrderResult o : list1) {
                        for (String i : afterIds) {
                            if (!o.getId().equals(i)) {
                                list.add(o);
                            }
                        }
                    }
                }
            } else {
                list = list1;
            }
            list = list.stream().collect(Collectors.collectingAndThen(Collectors.toCollection(
                    () -> new TreeSet<>(Comparator.comparing(o -> o.getId()))), ArrayList::new));
            return list;
        }
        return null;
    }

    public List<InventoryOrderResult> getRecursion(String deptId, String condition) {
        //查询该部门所拥有的仓库
        List<InventoryOrderResult> list = inventoryOrderMapper.getByPage(deptId, condition);
        //查询该部门子集部门
        List<String> ids = departmentMapper.selectList(new QueryWrapper<Department>().lambda().eq(Department::getParentId, deptId))
                .stream()
                .map(department -> department.getId())
                .collect(Collectors.toList());
        if (ids.size() > 0) {
            for (String s : ids) {
                //查询该部门所拥有的仓库
                List<InventoryOrderResult> list1 = inventoryOrderMapper.getByPage(s, condition);
                list.addAll(list1);
                List<InventoryOrderResult> list2 = getRecursion(s, condition);
                list.addAll(list2);
            }
        }
        return list;
    }

    @Override
    public List<InventoryVO> getInvetoryList(String ids,String userId) {
        if (ToolUtil.isNotEmpty(ids)) {
            List<String> idsUse = Lists.newArrayList(splitter.split(ids));
            List<InventoryVO> inventoryVOS = idsUse.stream().map(id -> {
                InventoryVO inventoryVO = new InventoryVO();
                InventoryOrder inventoryOrder = this.getById(id);
                if (ToolUtil.isNotEmpty(inventoryOrder)) {
                    inventoryVO.setBatchNumber(inventoryOrder.getBatchNumber());
                    inventoryVO.setAmount(inventoryOrder.getAmount());
                    inventoryVO.setSerialNum(inventoryOrder.getSerialNum());
                    ProductCommon productOrder = productOrderService.getProductCommon(inventoryOrder.getProductOrderId());
                    productOrder.setPrice(getProductPrice(userId,inventoryOrder.getProductOrderId()));
                    Optional.ofNullable(productOrder).ifPresent(productCommon -> inventoryVO.setProductCommon(productCommon));
                    //获取仓库名称
                    ShiWarehouse shiWarehouse = shiWarehouseService.getById(inventoryOrder.getWarhouseId());
                    if (ToolUtil.isNotEmpty(shiWarehouse)) {
                        inventoryVO.setWarhouseName(shiWarehouse.getWName());
                    }
                }
                return inventoryVO;
            }).collect(Collectors.toList());
            return inventoryVOS;
        }
        return null;
    }

    /**
     * 获取价格
     * @param userId
     * @param productId
     * @return
     */
    public BigDecimal getProductPrice(String userId,String productId){

        String levelId = dealerLevelMapper.selectOne(Wrappers.<DealerLevel>lambdaQuery()
        .eq(DealerLevel::getDeptId,userMapper.selectById(userId).getDepartmentId())).getLevelId();
        ProductPrice productPrice = productPriceMapper.selectOne(Wrappers.<ProductPrice>lambdaQuery()
                .eq(ProductPrice::getProductOrderId,productId)
                .eq(ProductPrice::getLevelId,levelId));
        return  Optional.ofNullable(productPrice).map(price -> price.getProductPrice()).orElse(null);
    }

    /**
     * 查询经销商在总仓的产品
     *
     * @param condition
     * @param id
     * @return
     */
    @Override
    public List<InventoryOrderResult> getMainWarehouse(String condition, String id) {
        List<InventoryOrderResult> list = new ArrayList<>();
        List<InventoryOrderResult> result = new ArrayList<>();
        //查询总仓产品
        if (ToolUtil.isNotEmpty(id)) {
            //查询总仓库存
            list = inventoryOrderMapper.getMainWarehouse(id, condition);
        }
        if (ToolUtil.isNotEmpty(securityUtil.getCurrUser().getRoles())) {
            for (Role role : securityUtil.getCurrUser().getRoles()) {
                if ("ROLE_ADMIN".equals(role.getName())) {
                    //如果为管理员查看所有库存
                    return list;
                }
            }
        }
        String deptId = securityUtil.getCurrUser().getDepartmentId();
        if (ToolUtil.isNotEmpty(deptId)) {
            //查询登录经销商的产品
            List<String> levelId = dealerLevelMapper.selectList(new QueryWrapper<DealerLevel>().lambda().eq(DealerLevel::getDeptId, deptId)).stream().map(s -> s.getLevelId()).collect(Collectors.toList());
            if (ToolUtil.isNotEmpty(levelId)) {
                List<String> productId = productPriceMapper.selectList(new QueryWrapper<ProductPrice>().lambda().eq(ProductPrice::getLevelId, levelId.get(0))).stream().map(s -> s.getProductOrderId()).collect(Collectors.toList());
                if (ToolUtil.isNotEmpty(productId)) {
                    for (InventoryOrderResult inventoryOrderResult : list) {
                        for (String pId : productId) {
                            if (pId.equals(inventoryOrderResult.getProductOrderId())) {
                                result.add(inventoryOrderResult);
                            }
                        }
                    }
                }
            }
            return result;
        }
        return null;
    }

    @Override
    public List<InventoryOrderResult> getInventoryOrder(String condition) {
        if (ToolUtil.isNotEmpty(securityUtil.getCurrUser().getRoles())) {
            for (Role role : securityUtil.getCurrUser().getRoles()) {
                if ("ROLE_ADMIN".equals(role.getName())) {
                    String deptId = null;
                    //如果为管理员查看所有库存
                    List<InventoryOrderResult> list1 = inventoryOrderMapper.getByPage(deptId, condition);
                    list1 = list1.stream().filter(t -> "未出库".equals(t.getSerialNumOut())).collect(Collectors.toList());
                    List<InventoryOrderResult> list = list1.stream().collect(Collectors.collectingAndThen(Collectors.toCollection(
                            () -> new TreeSet<>(Comparator.comparing(o -> o.getWarhouseId() + "#" + o.getProductOrderId()))), ArrayList::new));
                    for (InventoryOrderResult i : list) {
                        Integer count = 0;
                        for (InventoryOrderResult j : list1) {
                            if (i.getWarhouseId().equals(j.getWarhouseId()) && i.getProductOrderId().equals(j.getProductOrderId())) {
                                count += 1;
                            }
                        }
                        i.setAmount(count.longValue());
                    }
                    return list;
                }
            }
        }
        String deptId = securityUtil.getCurrUser().getDepartmentId();
        if (ToolUtil.isNotEmpty(deptId)) {
            List<InventoryOrderResult> list1 = getRecursions(deptId, condition);
            list1 = list1.stream().filter(t -> "未出库".equals(t.getSerialNumOut())).collect(Collectors.toList());
            list1 = list1.stream()
                    .collect(Collectors
                            .collectingAndThen
                                    (Collectors.toCollection(
                                            () -> new TreeSet<>(Comparator.comparing(t -> t.getId()))), ArrayList::new)
                    );
            List<InventoryOrderResult> list = list1.stream().
                    collect(Collectors
                            .collectingAndThen
                                    (Collectors.toCollection(
                                            () -> new TreeSet<>(Comparator.comparing(o -> o.getWarhouseId() + "#" + o.getProductOrderId()))), ArrayList::new));
            for (InventoryOrderResult i : list) {
                Integer count = 0;
                for (InventoryOrderResult j : list1) {
                    if (i.getWarhouseId().equals(j.getWarhouseId()) && i.getProductOrderId().equals(j.getProductOrderId())) {
                        count += 1;
                    }
                }
                i.setAmount(count.longValue());
            }
            return list;
        }
        return null;
    }

    @Override
    public List<InventoryOrderResult> getDoor(String condition, String userId) {
        String deptId = userMapper.selectById(userId).getDepartmentId();
        if (ToolUtil.isNotEmpty(deptId)) {
            List<InventoryOrderResult> list1 = getRecursion(deptId, condition);
            list1 = list1.stream()
                    .collect(Collectors
                            .collectingAndThen
                                    (Collectors.toCollection(() -> new TreeSet<InventoryOrderResult>(Comparator.comparing(t -> t.getId()))), ArrayList::new)
                    );
            List<InventoryOrderResult> list = new ArrayList<>();
            //过滤售后中的库存
            List<AfterIndentOrder> aftList = afterIndentOrderMapper1.selectList(new QueryWrapper<AfterIndentOrder>().lambda().eq(AfterIndentOrder::getCreateBy, userId).eq(AfterIndentOrder::getIsComplete, IsComplete.PENDING.getValue()));
            if (ToolUtil.isNotEmpty(aftList)) {
                //查询售后中的库存
                List<String> afterIds = new ArrayList<>();
                for (AfterIndentOrder a : aftList) {
                    ApplicationAfterSaleOrder applicationAfterSaleOrder = applicationAfterSaleOrderMapper1.selectById(a.getApplicationAfterSaleOrderId());
                    List<String> aIds = new ArrayList<>(Arrays.asList(applicationAfterSaleOrder.getInventoryIds().split(",")));
                    afterIds.addAll(aIds);
                }
                if (ToolUtil.isNotEmpty(afterIds)) {
                    for (InventoryOrderResult o : list1) {
                        for (String i : afterIds) {
                            if (!o.getId().equals(i)) {
                                list.add(o);
                            }
                        }
                    }
                }
            } else {
                list = list1;
            }
            list = list.stream().collect(Collectors.collectingAndThen(Collectors.toCollection(
                    () -> new TreeSet<>(Comparator.comparing(o -> o.getId()))), ArrayList::new));
            return list;
        }
        return null;
    }

    public List<InventoryOrderResult> getRecursions(String deptId, String condition) {
        //查询该部门所拥有的仓库
        List<InventoryOrderResult> list = inventoryOrderMapper.getByPage(deptId, condition);
        //查询该部门子集部门
        List<String> ids = departmentMapper.selectList(new QueryWrapper<Department>().lambda().eq(Department::getParentId, deptId))
                .stream()
                .map(department -> department.getId())
                .collect(Collectors.toList());
        if (ids.size() > 0) {
            for (String s : ids) {
                //查询该部门所拥有的仓库
                List<InventoryOrderResult> list1 = inventoryOrderMapper.getByPage(s, condition);
                list.addAll(list1);
                List<InventoryOrderResult> list2 = getRecursions(s, condition);
                list.addAll(list2);
            }
        }
        return list;
    }
}