package cn.exrick.xboot.modules.bussiness.address.pojo;

import cn.exrick.xboot.base.XbootBaseEntity;
import cn.exrick.xboot.common.constant.CommonConstant;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.Table;
import java.math.BigDecimal;

/**
 * @author tqr
 */
@Data
@Entity
@Table(name = "t_address")
@TableName("t_address")
@ApiModel(value = "地址")
public class Address extends XbootBaseEntity {

    private static final long serialVersionUID = 1L;

    /**
     * 分类名称
     */
    @ApiModelProperty("分类名称")
    private String title;

    /**
     * 父id
     */
    @ApiModelProperty("父id")
    private String parentId;

    /**
     * 父级分类名称
     */
    @ApiModelProperty("parentTitle")
    private String parentTitle;

    @ApiModelProperty(value = "是否为父节点(含子节点) 默认false")
    private Boolean isParent = false;

    @ApiModelProperty(value = "是否启用 0启用 -1禁用")
    private Integer status = CommonConstant.STATUS_NORMAL;

    @ApiModelProperty(value = "排序值")
    private BigDecimal sortOrder;

}