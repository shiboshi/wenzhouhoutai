package cn.exrick.xboot.modules.bussiness.specialPriceSalesOrder.controller;

import cn.exrick.xboot.modules.bussiness.specialPriceSalesOrder.pojo.SpecialPriceSalesOrder;
import lombok.Data;

import java.math.BigDecimal;

/**
 * @Author : xiaofei
 * @Date: 2019/8/27
 */
@Data
public class SpecialDetailVo {

    private String id;
    /*产品基础信息*/
    private String productName;
    private String productOrderName;
    private String productModelName;
    /**
     * 最低销售价
     */
    private BigDecimal minSellingPrice;

    /**
     * 申请价格
     */
    private BigDecimal price;
    private SpecialPriceSalesOrder specialPriceSalesOrder;
}
