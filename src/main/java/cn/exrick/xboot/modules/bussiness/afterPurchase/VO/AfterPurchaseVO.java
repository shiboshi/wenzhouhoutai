package cn.exrick.xboot.modules.bussiness.afterPurchase.VO;

import cn.exrick.xboot.common.vo.CommonVO;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.math.BigDecimal;

/**
 * @Author : xiaofei
 * @Date: 2019/8/11
 */
@Data
public class AfterPurchaseVO extends CommonVO {


    @ApiModelProperty("id")
    private String id;

    @ApiModelProperty("产品名称")
    private String productName;

    @ApiModelProperty("产品品号名称")
    private String productOrderName;

    @ApiModelProperty("产品型号名称")
    private String productModelName;

    @ApiModelProperty("售后类型")
    private String saleType;

    @ApiModelProperty("退货或者维修数量")
    private BigDecimal amount;

    @ApiModelProperty("退货原因")
    private String reason;

    @ApiModelProperty("问题类型")
    private Integer troubleTypeId;


    @ApiModelProperty("维修前图片")
    private String picBefore;

    @ApiModelProperty("维修后图片")
    private String picAfter;







}
