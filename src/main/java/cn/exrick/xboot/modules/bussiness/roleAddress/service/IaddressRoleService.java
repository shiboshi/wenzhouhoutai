package cn.exrick.xboot.modules.bussiness.roleAddress.service;

import cn.exrick.xboot.modules.base.entity.Role;
import cn.exrick.xboot.modules.bussiness.address.pojo.AddressVO;
import cn.exrick.xboot.modules.bussiness.roleAddress.VO.AddressRoleVO;
import com.baomidou.mybatisplus.extension.service.IService;
import cn.exrick.xboot.modules.bussiness.roleAddress.pojo.AddressRole;

import java.util.List;

/**
 * 角色区域表接口
 * @author fei
 */
public interface IaddressRoleService extends IService<AddressRole> {


    /**
     * 插入角色地址表
     * @param addressIds
     * @param roleId
     */
    boolean insertAddrRole(List<String> addressIds,String roleId);

    /**
     * 获取角色地址列表
     * @return
     */
    List<AddressVO> getList(String roleId);

    /**
     * 更新角色地址表
     */
    boolean updateAddressRole(List<String> addressIds, String roleId);

    /**
     * 获取详情信息
     * @param id
     * @return
     */
    AddressRoleVO getDetail(String id);

    List<Role> getRole();
}