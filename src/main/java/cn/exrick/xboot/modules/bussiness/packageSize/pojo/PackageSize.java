package cn.exrick.xboot.modules.bussiness.packageSize.pojo;

import cn.exrick.xboot.base.XbootBaseEntity;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModelProperty;
import io.swagger.annotations.ApiModel;
import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.Table;
import java.math.BigDecimal;

/**
 * @author tqr
 */
@Data
@Entity
@Table(name = "t_package_size")
@TableName("t_package_size")
@ApiModel(value = "包装尺寸")
public class PackageSize extends XbootBaseEntity {

    private static final long serialVersionUID = 1L;

    /**
     * 长
     */
    private BigDecimal length;

    /**
     * 宽
     */
    private BigDecimal wide;

    /**
     * 高
     */
    private BigDecimal tall;

    /**
     * 毛重
     */
    private BigDecimal roughWeight;

    /**
     * 净重
     */
    private BigDecimal netWeight;

    /**
     * 体积
     */
    private BigDecimal volume;

    /**
     * 产品品号id
     */
    private Long productOrderId;
}