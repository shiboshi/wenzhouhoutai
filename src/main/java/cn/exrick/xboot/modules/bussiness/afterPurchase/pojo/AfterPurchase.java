package cn.exrick.xboot.modules.bussiness.afterPurchase.pojo;

import cn.exrick.xboot.base.XbootBaseEntity;
import cn.exrick.xboot.common.enums.AfterSaleType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiModelProperty;
import io.swagger.annotations.ApiModel;
import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.Table;
import java.math.BigDecimal;

/**
 * @author xiaofei
 */
@Data
@Entity
@Table(name = "after_purchase")
@TableName("after_purchase")
@ApiModel(value = "售后单清单")
public class AfterPurchase extends XbootBaseEntity {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty("品号id")
    private String productOrderId;

    @ApiModelProperty("退货类型")
    private AfterSaleType afterSaleType;

    @ApiModelProperty("退货或者维修数量")
    private BigDecimal amount;

    @ApiModelProperty("退货原因")
    private String reason;

    @ApiModelProperty("问题类型")
    private Integer troubleTypeId;

    @ApiModelProperty("维修前图片")
    private String picBefore;

    @ApiModelProperty("维修后图片")
    private String picAfter;

    /**
     * 配件选件//关联产品分类表
     */
    private Long productTypeId;

    /**
     * 维修信息
     */
    private String maintainInformation;

    /**
     * 评价
     */
    private String evaluate;


    /**
     * 状态值
     */
    private Integer status;

    /**
     * 售后单号
     */
    private String afterSaleOrder;

}