package cn.exrick.xboot.modules.bussiness.afterPurchase.controller;

import cn.exrick.xboot.common.utils.PageUtil;
import cn.exrick.xboot.common.utils.ResultUtil;
import cn.exrick.xboot.common.vo.PageVo;
import cn.exrick.xboot.common.vo.Result;
import cn.exrick.xboot.modules.bussiness.afterPurchase.pojo.AfterPurchase;
import cn.exrick.xboot.modules.bussiness.afterPurchase.service.IafterPurchaseService;
import com.alibaba.fastjson.JSON;
import com.baomidou.mybatisplus.core.metadata.IPage;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * @author xiaofei
 */
@Slf4j
@RestController
@Api(description = "售后单清单管理接口")
@RequestMapping("/xboot/afterPurchase")
@Transactional
public class afterPurchaseController {

    @Autowired
    private IafterPurchaseService iafterPurchaseService;

}
