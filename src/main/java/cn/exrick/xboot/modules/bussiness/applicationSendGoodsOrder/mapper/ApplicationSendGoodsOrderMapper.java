package cn.exrick.xboot.modules.bussiness.applicationSendGoodsOrder.mapper;

import cn.exrick.xboot.modules.bussiness.applicationSendGoodsOrder.VO.AppliSendGoodsVO;
import cn.exrick.xboot.modules.bussiness.sendGoodsOrder.VO.SendGoodsVO;
import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import cn.exrick.xboot.modules.bussiness.applicationSendGoodsOrder.pojo.ApplicationSendGoodsOrder;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.Constants;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * 申请发货单数据处理层
 * @author xiaofei
 */
@Repository
public interface ApplicationSendGoodsOrderMapper extends BaseMapper<ApplicationSendGoodsOrder> {

    @Select("select * from x_application_send_goods_order ${ew.customSqlSegment}")
    Page<AppliSendGoodsVO> getAllList(@Param("page") Page page, @Param(Constants.WRAPPER)Wrapper wrapper);

    /**
     * 自定义全局sql
     * @return
     */
    Object selectLast();

    @Select("SELECT a.* FROM x_application_send_goods_order a INNER JOIN (SELECT create_by FROM x_application_send_goods_order GROUP BY create_by HAVING count(*) > 1 ) b ON a.create_by = b.create_by")
    List<ApplicationSendGoodsOrder> get();

    @Select("select s.id as id,s.order_num as orderNum,s.application_send_goods_id as applicationId,s.create_by,s.create_time," +
            "s.comment as comment,s.create_time as  createTime,s.product_pic as productPic,s.telephone as telephone," +
            "s.arrival_time as arrivalTime from x_send_goods_order s where s.application_send_goods_id = ${applicationSendGoodsId}")
    List<SendGoodsVO> getSendGoodOrderList(@Param("applicationSendGoodsId") String applicationSendGoodsId);
}