package cn.exrick.xboot.modules.base.dao.mapper;

import cn.exrick.xboot.common.sql.SqlProvider;
import cn.exrick.xboot.common.vo.CommonVO;
import cn.exrick.xboot.modules.base.entity.User;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.ResultType;
import org.apache.ibatis.annotations.SelectProvider;
import org.springframework.stereotype.Repository;

import javax.annotation.Resource;

/**
 * @Author : xiaofei
 * @Date: 2019/8/13
 */
@Resource
@Repository
public interface UserMapper extends BaseMapper<User> {


    /**
     * 获取用户详细信息
     * @param userId
     * @return
     */
    @SelectProvider(type = SqlProvider.class,method = "commonMessage")
    @ResultType(CommonVO.class)
    CommonVO getUserMessage(@Param("userId") String userId);

}
