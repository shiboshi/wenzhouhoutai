package cn.exrick.xboot.modules.bussiness.bothSale;

import org.apache.commons.lang3.StringUtils;

public class BothSaleProvider {
    public String getBothSaleCommit(String userName,String id) {
        String sql = "";
        String str = "";
        str += "  select b.create_time,b.order_numb,b.address_id,b.id as id,s.username as userName,(case b.check_code WHEN 1 then '待审核' WHEN 2 THEN '审核成功' WHEN 3 THEN '审核失败' END) as checkCode," +
                "(select  title from  t_department where  id = s.department_id and del_flag = 0) as userDept,(SELECT level_name from t_level where id = l.level_id and del_flag = 0) as userLevel," +
                "b.check_code as checkCode, b.create_time as createTime,b.address as address " +
                "from t_both_sale b  left join  t_user s on b.create_by =  s.id " +
                "left join t_dealer_level l on s.department_id = l.dept_id where b.del_flag=0 ";
        if(StringUtils.isNotBlank(userName)){
            str += " and s.username like '%"+userName+"%'";
        }
        if(StringUtils.isNotBlank(id)){
            str += " and b.id = '"+id+"'";
        }
        sql += str;
        return sql;
    }

    public String getBothSale(String userName,String id) {
        String sql = "";
        String str = "";
        str += "select b.create_time,b.address_id,b.order_numb,b.id as id, s.username as userName,(case b.check_code_dealer WHEN 1 then '待审核' WHEN 2 THEN '审核成功' WHEN 3 THEN '审核失败' END) as checkCodeDealer," +
                "(select  title from  t_department  where  id = s.department_id and del_flag = 0) as userDept,(SELECT level_name from t_level where id = l.level_id and del_flag = 0) as userLevel," +
                "b.check_code as checkCode, b.create_time as createTime,  b.address as address,c.title AS productOrderName," +
                "(SELECT title FROM t_classification WHERE id = c.parent_id and del_flag = 0) AS productModelName," +
                "(SELECT title FROM t_classification WHERE id = (SELECT parent_id FROM t_classification WHERE id = c.parent_id and del_flag = 0) and del_flag = 0) AS productName " +
                "from t_both_sale b  left join  t_user s on b.create_by =  s.id " +
                "left join t_dealer_level l on s.department_id = l.dept_id " +
                "LEFT JOIN x_product_order p on p.id = b.product_order_id " +
                "LEFT JOIN t_classification c on p.kind_id = c.id  where where b.del_flag=0 ";
        if(StringUtils.isNotBlank(userName)){
            str += " and s.username like '%"+userName+"%'";
        }
        if(StringUtils.isNotBlank(id)){
            str += " and b.id = '"+id+"'";
        }
        sql += str;
        return sql;
    }

    public String getBothSaleCommitByUser(String userId,String id,String code) {
        String sql = "";
        String str = "";
        str += "  select b.product_order_ids,b.create_time,b.id as id,b.address_id,b.order_numb, s.username as userName,(case b.check_code WHEN 1 then '待审核' WHEN 2 THEN '审核成功' WHEN 3 THEN '审核失败' END) as checkCode," +
                "(select  title from  t_department where  id = s.department_id and del_flag = 0) as userDept,(SELECT level_name from t_level where id = l.level_id and del_flag = 0) as userLevel," +
                "b.check_code as checkCode, b.create_time as createTime,b.address as address " +
                "from t_both_sale b  left join  t_user s on b.create_by =  s.id " +
                "left join t_dealer_level l on s.department_id = l.dept_id and l.del_flag=0 where b.del_flag=0 and b.create_by = '"+userId+"' ";

        if(StringUtils.isNotBlank(id)){
            str += " and b.id = '"+id+"'";
        }
        if(StringUtils.isNotBlank(code)){
            str += " and b.check_code = "+code+"";
        }
        sql += str;
        return sql;
    }

    public String getBothSaleByUser(String userId,String id, String code) {
        String sql = "";
        String str = "";
        str += "select b.create_time,b.id as id,b.address_id,b.order_numb,s.username as userName,e.username as dealerName,(case b.check_code_dealer WHEN 1 then '待审核' WHEN 2 THEN '审核成功' WHEN 3 THEN '审核失败' END) as checkCodeDealer," +
                "(select  title from  t_department  where  id = s.department_id and del_flag = 0) as userDept,(SELECT level_name from t_level where id = l.level_id and del_flag = 0) as userLevel," +
                "b.check_code as checkCode, b.create_time as createTime,  b.address as address " +
                "from t_both_sale b  left join  t_user s on b.create_by =  s.id and s.del_flag=0 " +
                "left join t_dealer_level l on s.department_id = l.dept_id and l.del_flag=0 " +
                " LEFT JOIN t_user e ON b.dealer_dept_id = e.id and e.del_flag=0 where b.del_flag=0 and b.create_by = '"+userId+"' ";
        if(StringUtils.isNotBlank(id)){
            str += " and b.id = '"+id+"'";
        }
        if(StringUtils.isNotBlank(code)){
            str += " and b.check_code_dealer = "+code+"";
        }
        sql += str;
        return sql;
    }
}
