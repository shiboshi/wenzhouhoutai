package cn.exrick.xboot.modules.bussiness.shiProductComparison.service;

import cn.exrick.xboot.modules.bussiness.shiProductComparison.pojo.productComparisonVo;
import cn.exrick.xboot.modules.bussiness.shiProductComparison.pojo.s_ProductComparison;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;
import org.apache.ibatis.annotations.Param;

/**
 * 产品对比接口
 * @author 石博士
 */
public interface Is_ProductComparisonService extends IService<s_ProductComparison> {


    /**
     * 查询产品对比
     */
    IPage<productComparisonVo> QueryProductComparisonVo(Page page);



    /**
     * 查询产品对比id
     */
    IPage<productComparisonVo> QueryProductComparisonVoID(Page page, @Param("id") String id);


    /**
     * 查询产品对比id
     */
    IPage<productComparisonVo> QueryProductComparisonVoIDList(Page page, @Param("id") String id);




}