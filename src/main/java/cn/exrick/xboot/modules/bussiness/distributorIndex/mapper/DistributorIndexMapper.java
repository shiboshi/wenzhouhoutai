package cn.exrick.xboot.modules.bussiness.distributorIndex.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import cn.exrick.xboot.modules.bussiness.distributorIndex.pojo.DistributorIndex;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * 经销商指标数据处理层
 * @author tqr
 */
@Repository
public interface DistributorIndexMapper extends BaseMapper<DistributorIndex> {

}