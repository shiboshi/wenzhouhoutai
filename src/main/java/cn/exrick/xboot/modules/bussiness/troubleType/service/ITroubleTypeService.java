package cn.exrick.xboot.modules.bussiness.troubleType.service;

import com.baomidou.mybatisplus.extension.service.IService;
import cn.exrick.xboot.modules.bussiness.troubleType.pojo.TroubleType;

import java.util.List;

/**
 * 问题类型表接口
 * @author tqr
 */
public interface ITroubleTypeService extends IService<TroubleType> {

}