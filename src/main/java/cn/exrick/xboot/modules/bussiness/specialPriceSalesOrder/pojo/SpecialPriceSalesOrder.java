package cn.exrick.xboot.modules.bussiness.specialPriceSalesOrder.pojo;

import cn.exrick.xboot.base.XbootBaseEntity;
import cn.exrick.xboot.common.enums.CheckStatus;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModelProperty;
import io.swagger.annotations.ApiModel;
import lombok.Data;
import org.springframework.web.bind.annotation.ModelAttribute;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import java.math.BigDecimal;

/**
 * @author xiaofei
 */
@Data
@Entity
@Table(name = "x_special_price_sales_order")
@TableName("x_special_price_sales_order")
@ApiModel(value = "特价申请销售订单")
public class SpecialPriceSalesOrder extends XbootBaseEntity {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "特价商品采购单id")
    private String purchaseOrderIds;

    @ApiModelProperty(value = "业务流程表id")
    private String businessId;
    @ApiModelProperty(value = "审核状态")
    private CheckStatus checkStatus;

    @ApiModelProperty(value = "申请手机号")
    private String phoneNumber;


}