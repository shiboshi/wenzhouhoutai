package cn.exrick.xboot.modules.bussiness.channel.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import cn.exrick.xboot.modules.bussiness.channel.pojo.Channel;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * 渠道数据处理层
 * @author tqr
 */
@Repository
public interface ChannelMapper extends BaseMapper<Channel> {

}