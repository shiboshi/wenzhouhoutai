package cn.exrick.xboot.common.upay.request;

import com.alibaba.fastjson.JSON;

/**
 * Created by vectoryang on 2017/7/17.
 */
public abstract class RequestBody {

    public abstract String acquireBizChannel();

    public abstract String acquireBizType();

    public abstract boolean validate();

    @Override
    public String toString() {
        if (!validate()) {
            throw new RuntimeException("参数验证失败");
        }
        return JSON.toJSONString(this);
    }
}
