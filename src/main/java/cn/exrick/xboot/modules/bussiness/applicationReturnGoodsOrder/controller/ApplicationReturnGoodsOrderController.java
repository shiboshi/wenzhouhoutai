package cn.exrick.xboot.modules.bussiness.applicationReturnGoodsOrder.controller;

import cn.exrick.xboot.common.enums.OrderType;
import cn.exrick.xboot.common.utils.PageUtil;
import cn.exrick.xboot.common.utils.ResultUtil;
import cn.exrick.xboot.common.utils.ToolUtil;
import cn.exrick.xboot.common.vo.PageVo;
import cn.exrick.xboot.common.vo.Result;
import cn.exrick.xboot.modules.bussiness.appliSendNum.pojo.AppliSendNum;
import cn.exrick.xboot.modules.bussiness.appliSendNum.service.IappliSendNumService;
import cn.exrick.xboot.modules.bussiness.applicationReturnGoodsOrder.VO.AppliReturnGoodsVO;
import cn.exrick.xboot.modules.bussiness.applicationReturnGoodsOrder.pojo.ApplicationReturnGoodsOrder;
import cn.exrick.xboot.modules.bussiness.applicationReturnGoodsOrder.service.IApplicationReturnGoodsOrderService;
import cn.exrick.xboot.modules.bussiness.applicationSendGoodsOrder.VO.AppliParam;
import cn.exrick.xboot.modules.bussiness.applicationSendGoodsOrder.VO.FilterVO;
import cn.exrick.xboot.modules.bussiness.applicationSendGoodsOrder.service.IapplicationSendGoodsOrderService;
import cn.exrick.xboot.modules.bussiness.product.service.IproductOrderService;
import cn.exrick.xboot.modules.bussiness.purchaseOrder.VO.PurchaseOrderVO;
import cn.exrick.xboot.modules.bussiness.purchaseOrder.service.IpurchaseOrderService;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.google.common.collect.Maps;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * @author tqr
 */
@Slf4j
@RestController
@Api(description = "申请退货表管理接口")
@RequestMapping("/xboot/applicationReturnGoodsOrder")
@Transactional
public class ApplicationReturnGoodsOrderController {

    @Autowired
    private IApplicationReturnGoodsOrderService iApplicationReturnGoodsOrderService;
    @Autowired
    private IapplicationSendGoodsOrderService applicationSendGoodsOrderService;
    @Autowired
    private IappliSendNumService appliSendNumService;
    @Autowired
    private IpurchaseOrderService purchaseService;
    @Autowired
    private IproductOrderService productOrderService;

    @PostMapping("/getDetail")
    @ApiOperation(value = "通过id获取申请退货单详情")
    public Result<Map<String, Object>> getDetail(String id, @ModelAttribute PageVo pageVo) {
        Map<String,Object> map = Maps.newHashMap();
        //获取当前申请退货单中的退货详情
        ApplicationReturnGoodsOrder applicationReturnGoodsOrder = iApplicationReturnGoodsOrderService.getById(id);
        List<PurchaseOrderVO> purchaseOrderVOS = new ArrayList<>();
        if(ToolUtil.isNotEmpty(applicationReturnGoodsOrder)){
            purchaseOrderVOS  = purchaseService.getPurchaseList(OrderType.applicationReturnOrder,applicationReturnGoodsOrder.getId());
        }
        map.put("size", purchaseOrderVOS.size());
        List<AppliSendNum> appliSendNums = appliSendNumService.list(Wrappers.<AppliSendNum>lambdaQuery()
                .eq(AppliSendNum::getApplicationSendOrderId,id));
        for(PurchaseOrderVO purchaseOrderVO : purchaseOrderVOS ){
            for (AppliSendNum appliSendNum :appliSendNums){
                if(appliSendNum.getPurchaseId().equals(purchaseOrderVO.getId())){
                    purchaseOrderVO.setDispatchNum(appliSendNum.getSendNum());
                }
            }
        }
        purchaseOrderVOS = PageUtil.listToPage(pageVo,purchaseOrderVOS);
        map.put("data", purchaseOrderVOS);
        return new ResultUtil<Map<String,Object>>().setData(map,"查询成功！");
    }

    @PostMapping("/getList")
    @ApiOperation(value = "获取申请发货列表")
    public Result<Page<AppliReturnGoodsVO>> getList(PageVo pageVo, ApplicationReturnGoodsOrder applicationReturnGoodsOrder) {
        Page<AppliReturnGoodsVO> page = iApplicationReturnGoodsOrderService.getAll(applicationReturnGoodsOrder, pageVo);
        return new ResultUtil<Page<AppliReturnGoodsVO>>().setData(page);
    }

    @PostMapping("/selectById")
    public Result<List<AppliReturnGoodsVO>> selectById(String id) {
        List<AppliReturnGoodsVO> list = new ArrayList<>();
        AppliReturnGoodsVO page = iApplicationReturnGoodsOrderService.selectById(id);
        list.add(page);
        return new ResultUtil<List<AppliReturnGoodsVO>>().setData(list);
    }

    @PostMapping("/insertOrder")
    @ApiOperation(value = "插入申请退货单")
    public Result<?> insertOrder(AppliParam appliParam) {
        //判断当前申请发货的数量是否满足
        List<FilterVO> purchaseOrderFilter = applicationSendGoodsOrderService.filterCanNUm(appliParam);
        if (ToolUtil.isEmpty(purchaseOrderFilter)){
            iApplicationReturnGoodsOrderService.insertOrder(appliParam);
            return new ResultUtil<>().setSuccessMsg("插入订单成功！");
        }else {
            return new ResultUtil<>().setErrorMsg(201, "以下商品不能退货！！！", purchaseOrderFilter);
        }
    }

    @RequestMapping(value = "/delByIds", method = RequestMethod.DELETE)
    @ApiOperation(value = "批量通过id删除")
    public Result<Object> delAllByIds(String id) {
        iApplicationReturnGoodsOrderService.removeById(id);
        return new ResultUtil<Object>().setSuccessMsg("批量通过id删除数据成功");
    }
}
