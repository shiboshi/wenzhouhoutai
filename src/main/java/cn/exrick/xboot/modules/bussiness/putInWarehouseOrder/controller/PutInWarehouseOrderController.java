package cn.exrick.xboot.modules.bussiness.putInWarehouseOrder.controller;

import cn.exrick.xboot.common.enums.Order;
import cn.exrick.xboot.common.utils.*;
import cn.exrick.xboot.common.vo.PageVo;
import cn.exrick.xboot.common.vo.Result;
import cn.exrick.xboot.modules.bussiness.inventoryOrder.pojo.InventoryOrder;
import cn.exrick.xboot.modules.bussiness.inventoryOrder.service.IInventoryOrderService;
import cn.exrick.xboot.modules.bussiness.product.pojo.ProductOrder;
import cn.exrick.xboot.modules.bussiness.product.service.IproductOrderService;
import cn.exrick.xboot.modules.bussiness.product.serviceImp.IproductOrderServiceImpl;
import cn.exrick.xboot.modules.bussiness.putInWarehouse.VO.PutInWarehouseVO;
import cn.exrick.xboot.modules.bussiness.putInWarehouse.mapper.PutInWarehouseMapper;
import cn.exrick.xboot.modules.bussiness.putInWarehouse.pojo.PutInWarehouse;
import cn.exrick.xboot.modules.bussiness.putInWarehouse.service.IPutInWarehouseService;
import cn.exrick.xboot.modules.bussiness.putInWarehouseOrder.pojo.PutInWarehouseOrder;
import cn.exrick.xboot.modules.bussiness.putInWarehouseOrder.pojo.PutInWarehouseOrderVO;
import cn.exrick.xboot.modules.bussiness.putInWarehouseOrder.service.IPutInWarehouseOrderService;
import cn.exrick.xboot.modules.bussiness.shiWarehouse.pojo.ShiWarehouse;
import cn.exrick.xboot.modules.bussiness.shiWarehouse.service.IShiWarehouseService;
import com.alibaba.fastjson.JSON;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.interceptor.TransactionAspectSupport;
import org.springframework.web.bind.annotation.*;
import org.springframework.transaction.annotation.Transactional;

import java.util.*;

/**
 * @author tqr
 */
@Slf4j
@RestController
@Api(description = "入库单集合管理接口")
@RequestMapping("/xboot/putInWarehouseOrder")
@Transactional
public class PutInWarehouseOrderController {

    @Autowired
    private IPutInWarehouseOrderService iPutInWarehouseOrderService;
    @Autowired
    private SecurityUtil securityUtil;
    @Autowired
    private IPutInWarehouseService iPutInWarehouseService;
    @Autowired
    private IInventoryOrderService iInventoryOrderService;
    @Autowired
    private PutInWarehouseMapper putInWarehouseMapper;
    @Autowired
    private IproductOrderService iproductOrderService;

    /**
     * 新增接口
     *
     * @author tqr
     * @Date 2019-06-26
     */
    @PostMapping("/addPutInWarehouseOrder")
    public Result<Object> addPutInWarehouseOrder(String putInWarehouse) {
        List<PutInWarehouse> putInWarehouses = JSON.parseArray(putInWarehouse, PutInWarehouse.class);
        StringBuffer ids = new StringBuffer();
        PutInWarehouseOrder putInWarehouseOrder = new PutInWarehouseOrder();
        try {
            if (ToolUtil.isNotEmpty(putInWarehouses)) {
                List<Map<String,Object>> had = new ArrayList<>();
                for (PutInWarehouse putInWarehouseParam1 : putInWarehouses) {
                    Map<String, Object> map = new HashMap<>();
                    //直接入库
                    String[] seri = null;
                    if (putInWarehouseParam1.getSerialNum().indexOf(" ") != -1) {
                        seri = putInWarehouseParam1.getSerialNum().split("\\s+");
                    } else {
                        seri = putInWarehouseParam1.getSerialNum().split("\\|");
                    }
                    if (ToolUtil.isNotEmpty(seri)) {
                        List<String> hs = new ArrayList<>();
                        //查询序列号是否存在
                        for (String str : seri) {
                            if (ToolUtil.isNotEmpty(iInventoryOrderService.getBaseMapper().selectList(new QueryWrapper<InventoryOrder>().lambda().eq(InventoryOrder::getSerialNum, str)))) {
                                //查询产品品号
                                ProductOrder productOrder = iproductOrderService.getById(putInWarehouseParam1.getProductOrderId());
                                map.put("productName", productOrder.getProductOrderName());
                                hs.add(str);
                            }
                        }
                        if (ToolUtil.isNotEmpty(hs)) {
                            map.put("seri", hs);
                        }
                    }
                    if (ToolUtil.isNotEmpty(map)) {
                        had.add(map);
                    }
                }
                if (ToolUtil.isNotEmpty(had)) {
                    return new ResultUtil<>().setErrorMsg(201, "以下序列号重复", had);
                }
                for (PutInWarehouse putInWarehouseParam : putInWarehouses) {
                    putInWarehouseParam.setCreateBy(securityUtil.getCurrUser().getId());
                    putInWarehouseParam.setCreateTime(new Date());
                    putInWarehouseParam.setDelFlag(0);
                    ids.append(putInWarehouseParam.getId()).append(",");
                    //直接入库
                    String[] seri = null;
                    if (putInWarehouseParam.getSerialNum().indexOf(" ") != -1) {
                        seri = putInWarehouseParam.getSerialNum().split("\\s+");
                    } else {
                        seri = putInWarehouseParam.getSerialNum().split("\\|");
                    }
                    if (ToolUtil.isNotEmpty(seri)) {
                        for (String s : seri) {
                            InventoryOrder inventoryOrder = new InventoryOrder();
                            if (ToolUtil.isNotEmpty(putInWarehouse)) {
                                inventoryOrder.setCreateBy(securityUtil.getCurrUser().getId());
                                inventoryOrder.setDelFlag(0);
                                inventoryOrder.setCreateTime(new Date());
                                inventoryOrder.setProductOrderId(putInWarehouseParam.getProductOrderId());
                                inventoryOrder.setSerialNum(s);
                                inventoryOrder.setBatchNumber(putInWarehouseParam.getBatchNumber());
                                inventoryOrder.setWarhouseId(putInWarehouseParam.getWarehouseId());
                                inventoryOrder.setProductOrderId(putInWarehouseParam.getProductOrderId());
                                inventoryOrder.setAmount(putInWarehouseParam.getAmount());
                                inventoryOrder.setSerialNumOut(1);
                                iInventoryOrderService.save(inventoryOrder);
                            }
                        }
                    }
                    putInWarehouseParam.setStatus(1);
                }
                //添加入库单
                this.iPutInWarehouseService.saveBatch(putInWarehouses);
                //添加入库单集合
                putInWarehouseOrder.setPutInNumb(CommonUtil.createOrderSerial(Order.RK_));
                putInWarehouseOrder.setCreateBy(securityUtil.getCurrUser().getId());
                putInWarehouseOrder.setPutInWarehouseIds(ids.toString());
                iPutInWarehouseOrderService.save(putInWarehouseOrder);
            }
            return new ResultUtil<>().setSuccessMsg("添加成功");
        } catch (Exception e) {
            // 手动回滚事物
            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
            return new ResultUtil<>().setErrorMsg("添加失败");
        }
    }

    /**
     * 查询列表
     *
     * @author tqr
     * @Date 2019-06-26
     */
    @PostMapping("/getPutInWarehouseOrder")
    public Result<Map<String, Object>> getPutInWarehouseOrder(PageVo pageVo, String numb) {
        Map<String, Object> map = new HashMap<>();
        List<PutInWarehouseOrderVO> data = iPutInWarehouseOrderService.getPutInWarehouseOrder(numb);
        map.put("total", data.size());
        data = PageUtil.listToPage(pageVo, data);
        map.put("result", data);
        return new ResultUtil<Map<String, Object>>().setData(map);
    }

    /**
     * 查询详情
     *
     * @author tqr
     * @Date 2019-06-26
     */
    @PostMapping("/getDetail")
    public Result<List<PutInWarehouseVO>> getDetail(String id) {
        //查询集合
        PutInWarehouseOrder putInWarehouseOrder = iPutInWarehouseOrderService.getById(id);
        String[] ids = putInWarehouseOrder.getPutInWarehouseIds().split(",");
        List<PutInWarehouseVO> list = new ArrayList<>();
        for (String s : ids) {
            PutInWarehouseVO putInWarehouseVO = putInWarehouseMapper.getById(s);
            list.add(putInWarehouseVO);
        }
        return new ResultUtil<List<PutInWarehouseVO>>().setData(list);
    }

    /**
     * 删除
     */
    @RequestMapping(value = "/delByIds/{ids}", method = RequestMethod.POST)
    @ApiOperation(value = "批量通过id删除")
    public Result<Object> delAllByIds(@PathVariable String[] ids) {

        for (String id : ids) {
            iPutInWarehouseOrderService.removeById(id);
        }
        return new ResultUtil<Object>().setSuccessMsg("批量通过id删除数据成功");
    }
}
