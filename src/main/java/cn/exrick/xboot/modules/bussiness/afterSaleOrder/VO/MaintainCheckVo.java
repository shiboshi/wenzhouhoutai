package cn.exrick.xboot.modules.bussiness.afterSaleOrder.VO;

import lombok.Data;

import java.math.BigDecimal;

/**
 * @Author : xiaofei
 * @Date: 2019/9/11
 */
@Data
public class MaintainCheckVo {

      /**
     * 收据费用
     */
    private BigDecimal cost;
    /**
     * 配件id
     */
    private String parets;

    /**
     * 售后维修图片
     */
    private String afterPic;


}
