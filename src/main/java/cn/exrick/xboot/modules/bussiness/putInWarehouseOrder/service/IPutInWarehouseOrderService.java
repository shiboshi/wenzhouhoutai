package cn.exrick.xboot.modules.bussiness.putInWarehouseOrder.service;

import cn.exrick.xboot.modules.bussiness.putInWarehouseOrder.pojo.PutInWarehouseOrderVO;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;
import cn.exrick.xboot.modules.bussiness.putInWarehouseOrder.pojo.PutInWarehouseOrder;

import java.util.List;

/**
 * 入库单集合接口
 * @author tqr
 */
public interface IPutInWarehouseOrderService extends IService<PutInWarehouseOrder> {

    List<PutInWarehouseOrderVO> getPutInWarehouseOrder(String numb);
}