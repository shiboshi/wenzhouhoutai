package cn.exrick.xboot.common.upay.response;


import cn.exrick.xboot.common.upay.utils.CipherUtils;
import cn.exrick.xboot.common.upay.utils.StringUtils;
import cn.exrick.xboot.common.upay.utils.Utils;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;

import java.security.interfaces.RSAPublicKey;
import java.util.Map;
import java.util.TreeMap;

/**
 *
 * @author zhou
 * @date 2019-7-30 13:23:54
 */
public class PayResponseBody extends ResponseBody {

    private String appid;
    private String sign;
    private String partnerid;

    public String getAppid() {
        return appid;
    }

    public void setAppid(String appid) {
        this.appid = appid;
    }

    public String getSign() {
        return sign;
    }

    public void setSign(String sign) {
        this.sign = sign;
    }

    public String getPartnerid() {
        return partnerid;
    }

    public void setPartnerid(String partnerid) {
        this.partnerid = partnerid;
    }

    public String getPrepayid() {
        return prepayid;
    }

    public void setPrepayid(String prepayid) {
        this.prepayid = prepayid;
    }

    public String getNoncestr() {
        return noncestr;
    }

    public void setNoncestr(String noncestr) {
        this.noncestr = noncestr;
    }

    public String getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(String timestamp) {
        this.timestamp = timestamp;
    }

    private String prepayid;
    private String noncestr;
    private String timestamp;

}
