package cn.exrick.xboot.modules.bussiness.afterSaleOrder.mapper;

import cn.exrick.xboot.modules.bussiness.afterSaleOrder.pojo.AfterSaleOrder;
import cn.exrick.xboot.modules.bussiness.saleOrder.VO.SalesOrderVO;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * 销售单维修单数据处理层
 *
 * @author fei
 */
@Repository
public interface afterSaleOrderMapper extends BaseMapper<AfterSaleOrder> {


    /**
     * 根据当前的登录人和审核状态为2，
     * 审核成功查询销售订单的数据
     */
    List<SalesOrderVO> QueryAfterSaleOrder(@Param("buyerName") String buyerName);










}