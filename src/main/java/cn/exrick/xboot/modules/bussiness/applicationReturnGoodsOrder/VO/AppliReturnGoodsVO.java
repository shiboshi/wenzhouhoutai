package cn.exrick.xboot.modules.bussiness.applicationReturnGoodsOrder.VO;

import cn.exrick.xboot.common.enums.CheckStatus;
import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import java.util.Date;

/**
 * @Author : xiaofei
 * @Date: 2019/8/9
 */
@Data
public class AppliReturnGoodsVO {

    @ApiModelProperty(value = "主键")
    private String id;

    @ApiModelProperty(value = "订单编号")
    private String orderNum;

    @ApiModelProperty(value = "经销商")
    private String userName;

    @ApiModelProperty(value = "经销商部门")
    private String deptName;

    @ApiModelProperty(value = "经销商部门")
    private String levelName;
    private CheckStatus checkStatus;

    @JsonFormat(timezone = "GMT+8", pattern = "yyyy-MM-dd HH:mm:ss")
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @ApiModelProperty(value = "创建时间")
    private Date createTime;

    private String createBy;
}
