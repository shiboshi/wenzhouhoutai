package cn.exrick.xboot.common.enums;

import com.baomidou.mybatisplus.annotation.EnumValue;
import com.fasterxml.jackson.annotation.JsonValue;

/**
 * @Author : xiaofei
 * @Date: 2019/8/14
 */
public enum PayType {

    BLANCE(0, "账户余额"),
    CRIDIT(1, "信用额度"),
    CRASH(3,"其他支付");
    PayType(final int value, final String desc) {
        this.value = value;
        this.desc = desc;
    }
    @EnumValue
    private final int value;

    public int getValue() {
        return value;
    }

    @JsonValue
    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    private String desc;
}
