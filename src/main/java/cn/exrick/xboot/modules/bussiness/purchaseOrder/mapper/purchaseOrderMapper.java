package cn.exrick.xboot.modules.bussiness.purchaseOrder.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import cn.exrick.xboot.modules.bussiness.purchaseOrder.pojo.PurchaseOrder;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

/**
 * 采购单数据处理层
 * @author xiaofei
 */
@Repository
public interface purchaseOrderMapper extends BaseMapper<PurchaseOrder> {

}