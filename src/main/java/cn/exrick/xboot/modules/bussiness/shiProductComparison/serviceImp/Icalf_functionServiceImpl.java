package cn.exrick.xboot.modules.bussiness.shiProductComparison.serviceImp;

import cn.exrick.xboot.modules.bussiness.shiProductComparison.mapper.calf_functionMapper;
import cn.exrick.xboot.modules.bussiness.shiProductComparison.pojo.calf_function;
import cn.exrick.xboot.modules.bussiness.shiProductComparison.service.Icalf_functionService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

/**
 * 小腿功能表接口实现
 * @author 石博士
 */
@Slf4j
@Service
@Transactional
public class Icalf_functionServiceImpl extends ServiceImpl<calf_functionMapper, calf_function> implements Icalf_functionService {

    @Autowired
    private calf_functionMapper calf_functionMapper;
}