package cn.exrick.xboot.modules.UpayApi.WxPay;

import cn.hutool.core.date.DateUtil;
import lombok.extern.java.Log;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;

/**
 * 时间工具类
 */
@Log
public class DateUtils {
	public final static String YYYY = "yyyy";
	public final static String YYYY_MM_DD = "yyyy-MM-dd";
	public final static String MMDDHHMMSS = "MMddHHmmss";
	public final static String YYYYMMDDHHMMSS = "yyyyMMddHHmmss";
	public final static String YYYY_MM_DD_HH_MM_SS = "yyyy-MM-dd HH:mm:ss";

	public static String formatNow(String pattern) {
		SimpleDateFormat sdf = new SimpleDateFormat(pattern);
		sdf.setTimeZone(TimeZone.getTimeZone("Asia/Shanghai"));
		Date date = DateUtil.date();
		log.info("请求时间："+date);
		return sdf.format(date);
	}

	public static String format(Date date, String pattern) {
		return new SimpleDateFormat(pattern).format(date);
	}

	public static Date time(String time, String type) throws ParseException {
		return new SimpleDateFormat(type).parse(time);
	}
}
