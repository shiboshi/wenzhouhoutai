package cn.exrick.xboot.modules.bussiness.putInWarehouse.pojo;

import cn.exrick.xboot.base.XbootBaseEntity;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.Table;

/**
 * @author tqr
 */
@Data
@Entity
@Table(name = "t_put_in_warehouse")
@TableName("t_put_in_warehouse")
@ApiModel(value = "入库")
public class PutInWarehouse extends XbootBaseEntity {

    private static final long serialVersionUID = 1L;

    /**
     * 批次
     */
    private String batchNumber;

    /**
     * 仓库id
     */
    private String warehouseId;

    /**
     * 序列号
     */
    private String serialNum;

    /**
     * 产品编号id
     */
    private String productOrderId;

    /**
     * 数量
     */
    private Long amount;

    /**
     * 入库状态
     */
    private Integer Status;

}