package cn.exrick.xboot.modules.bussiness.packageSize.VO;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.math.BigDecimal;

@ApiModel("包装尺寸")
@Data
public class PackageSizeVO {
    @ApiModelProperty("id")
    private String id;

    @ApiModelProperty("产品品号id")
    private Long productOrderId;

    @ApiModelProperty("产品大类")
    private String productName;

    @ApiModelProperty("产品型号")
    private String productModelName;

    @ApiModelProperty("产品型号")
    private String productModelId;

    @ApiModelProperty("产品品号")
    private String productOrderName;

    @ApiModelProperty("长")
    private BigDecimal length;

    @ApiModelProperty("宽")
    private BigDecimal wide;

    @ApiModelProperty("高")
    private BigDecimal tall;

    @ApiModelProperty("毛重")
    private BigDecimal roughWeight;

    @ApiModelProperty("净重")
    private BigDecimal netWeight;

    @ApiModelProperty("体积")
    private BigDecimal volume;

    @ApiModelProperty("整体信息")
    private String info;

    @ApiModelProperty("对内型号")
    private String modelNameIn;
}
