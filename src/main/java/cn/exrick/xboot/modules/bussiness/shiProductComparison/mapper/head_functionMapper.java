package cn.exrick.xboot.modules.bussiness.shiProductComparison.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import cn.exrick.xboot.modules.bussiness.shiProductComparison.pojo.head_function;

import java.util.List;

/**
 * 头部功能数据处理层
 * @author 石博士
 */
public interface head_functionMapper extends BaseMapper<head_function> {

}