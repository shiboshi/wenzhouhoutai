package cn.exrick.xboot.modules.bussiness.monthlyIndicators.serviceImp;

import cn.exrick.xboot.modules.bussiness.monthlyIndicators.mapper.MonthlyIndicatorsMapper;
import cn.exrick.xboot.modules.bussiness.monthlyIndicators.pojo.MonthlyIndicators;
import cn.exrick.xboot.modules.bussiness.monthlyIndicators.service.IMonthlyIndicatorsService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

/**
 * 月份指标接口实现
 * @author tqr
 */
@Slf4j
@Service
@Transactional
public class IMonthlyIndicatorsServiceImpl extends ServiceImpl<MonthlyIndicatorsMapper, MonthlyIndicators> implements IMonthlyIndicatorsService {

    @Autowired
    private MonthlyIndicatorsMapper monthlyIndicatorsMapper;
}