package cn.exrick.xboot.modules.bussiness.tApplication.serviceImp;

import cn.exrick.xboot.modules.bussiness.tApplication.mapper.TApplicationMapper;
import cn.exrick.xboot.modules.bussiness.tApplication.pojo.TApplication;
import cn.exrick.xboot.modules.bussiness.tApplication.pojo.TApplicationVO;
import cn.exrick.xboot.modules.bussiness.tApplication.service.ITApplicationService;
import cn.exrick.xboot.modules.shiUtil.JedisDelete;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import lombok.extern.slf4j.Slf4j;
import org.apache.ibatis.annotations.Param;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * 退款申请表接口实现
 * @author tqr
 */
@Slf4j
@Service
@Transactional
public class ITApplicationServiceImpl extends ServiceImpl<TApplicationMapper, TApplication> implements ITApplicationService {

    @Autowired
    private TApplicationMapper tApplicationMapper;



    @Override
    public IPage<TApplication> getByPage(Page page) {
        IPage<TApplication> list = tApplicationMapper.selectPage(page,new QueryWrapper<>());
        return list;
    }



    /**
     * 查询所有集合
     */
    public IPage<TApplicationVO> QueryTapplication(Page page, @Param("username") String username){
        JedisDelete d = new JedisDelete();
        d.Delete();
        return tApplicationMapper.QueryTapplication(page,username);
    }





}