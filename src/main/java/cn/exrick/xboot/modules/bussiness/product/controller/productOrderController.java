package cn.exrick.xboot.modules.bussiness.product.controller;

import cn.exrick.xboot.common.utils.PageUtil;
import cn.exrick.xboot.common.utils.ResultUtil;
import cn.exrick.xboot.common.utils.ToolUtil;
import cn.exrick.xboot.common.vo.PageVo;
import cn.exrick.xboot.common.vo.Result;
import cn.exrick.xboot.modules.bussiness.classification.service.IClassificationService;
import cn.exrick.xboot.modules.bussiness.packageSize.pojo.PackageSize;
import cn.exrick.xboot.modules.bussiness.packageSize.service.IPackageSizeService;
import cn.exrick.xboot.modules.bussiness.product.VO.ProductCommon;
import cn.exrick.xboot.modules.bussiness.product.VO.ProductOrderExtend;
import cn.exrick.xboot.modules.bussiness.product.VO.ProductOrderVo;
import cn.exrick.xboot.modules.bussiness.product.pojo.ProductModel;
import cn.exrick.xboot.modules.bussiness.product.pojo.ProductOrder;
import cn.exrick.xboot.modules.bussiness.product.service.IproductModelService;
import cn.exrick.xboot.modules.bussiness.product.service.IproductOrderService;
import cn.exrick.xboot.modules.bussiness.unit.VO.UnitVO;
import cn.hutool.core.date.DateUtil;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.google.common.collect.Maps;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author xiaofei
 */
@Slf4j
@RestController
@Api(description = "产品型号管理接口")
@RequestMapping("/xboot/productOrder")
@Transactional
public class productOrderController {

    @Autowired
    private IproductOrderService iproductOrderService;
    @Autowired
    private IClassificationService iClassificationService;
    @Autowired
    private IPackageSizeService iPackageSizeService;
    @Autowired
    private IproductModelService iproductModelService;

    @RequestMapping(value = "/get/{id}", method = RequestMethod.GET)
    @ApiOperation(value = "通过id获取")
    public Result<ProductOrder> get(@PathVariable String id) {

        ProductOrder productOrder = iproductOrderService.getById(id);
        return new ResultUtil<ProductOrder>().setData(productOrder);
    }

    @RequestMapping(value = "/getAll", method = RequestMethod.GET)
    @ApiOperation(value = "获取全部数据")
    public Result<List<ProductOrder>> getAll() {

        List<ProductOrder> list = iproductOrderService.list();
        return new ResultUtil<List<ProductOrder>>().setData(list);
    }

    @RequestMapping(value = "/getByPage", method = RequestMethod.GET)
    @ApiOperation(value = "分页获取")
    public Result<IPage<ProductOrder>> getByPage(@ModelAttribute PageVo page) {

        IPage<ProductOrder> data = iproductOrderService.page(PageUtil.initMpPage(page));
        return new ResultUtil<IPage<ProductOrder>>().setData(data);
    }

    @RequestMapping(value = "/insertOrUpdate", method = RequestMethod.POST)
    @ApiOperation(value = "编辑或更新数据")
    public Result<ProductOrder> saveOrUpdate(@ModelAttribute ProductOrder productOrder) {

        if (iproductOrderService.saveOrUpdate(productOrder)) {
            return new ResultUtil<ProductOrder>().setData(productOrder);
        }
        return new ResultUtil<ProductOrder>().setErrorMsg("操作失败");
    }

    @RequestMapping(value = "/delByIds/{ids}", method = RequestMethod.DELETE)
    @ApiOperation(value = "批量通过id删除")
    public Result<Object> delAllByIds(@PathVariable String[] ids) {

        for (String id : ids) {
            ProductOrder productOrder = iproductOrderService.getById(id);
            iproductOrderService.removeById(id);
            iClassificationService.removeById(productOrder.getKindId());
        }
        return new ResultUtil<Object>().setSuccessMsg("批量通过id删除数据成功");
    }

    /**
     * 查询产品(查所有)
     */
    @RequestMapping(value = "/getAllProduct", method = RequestMethod.POST)
    @ApiOperation(value = "获取全部数据")
    public Result<IPage<ProductOrderVo>> getAllProduct(@ModelAttribute PageVo page, String productOrderName) {

        IPage<ProductOrderVo> list = iproductOrderService.getAllProduct(PageUtil.initMpPage(page), productOrderName);
        List<ProductOrderVo> orderVos = list.getRecords();
        for (ProductOrderVo vo : orderVos) {
            vo.setTimeToMarket(DateUtil.format(DateUtil.parse(vo.getTimeToMarket()), "yyyy-MM-dd"));
            StringBuffer stringBuffer = new StringBuffer();
            stringBuffer.append(vo.getColor()).append("/").append(vo.getColorNumber()).append("/").append(vo.getMaterial());
            vo.setAllName(stringBuffer.toString());
            //查寻包装尺寸
            List<PackageSize> packageSize = iPackageSizeService.getBaseMapper().selectList(new QueryWrapper<PackageSize>().lambda().eq(PackageSize::getProductOrderId, vo.getKindId()));
            StringBuilder stringBuilder = new StringBuilder();
            packageSize.forEach(Size -> stringBuilder.append(Size.getLength()).append("*").append(Size.getWide()).append("*")
                    .append(Size.getTall()).append("\n"));
            vo.setPackingSize(stringBuilder.toString());
            //查询对内型号名
            ProductModel p = iproductModelService.getBaseMapper().selectList(new QueryWrapper<ProductModel>().lambda().eq(ProductModel::getKindId,vo.getProductModelId())).get(0);
            vo.setModelNameIn(p.getModelNameIn());
        }
        list.setRecords(orderVos);
        return new ResultUtil<IPage<ProductOrderVo>>().setData(list);
    }

    /**
     * 查询产品
     */
    @RequestMapping(value = "/getAllProductByDist", method = RequestMethod.POST)
    @ApiOperation(value = "获取全部数据")
    public Result<Map<String, Object>> getAllProductByDist(@ModelAttribute PageVo page, String productOrderName, Integer code) {
        Map<String, Object> map = Maps.newHashMap();
        if (ToolUtil.isNotEmpty(iproductOrderService.getAllProductByDist(productOrderName, code))) {
            List<ProductCommon> productList = PageUtil.listToPage(page, iproductOrderService.getAllProductByDist(productOrderName, code));
            map.put("data", productList);
            map.put("size", productList.size());
            return new ResultUtil<Map<String, Object>>().setData(map);
        }
        return new ResultUtil<Map<String, Object>>().setData(null);

    }

    /**
     * 查看商品详情
     */
    @RequestMapping(value = "/getAllProductDetail", method = RequestMethod.POST)
    @ApiOperation(value = "查看商品详情")
    public Result<ProductOrderVo> getAllProductDetail(String id) {

        ProductOrderVo productOrderVo = iproductOrderService.getAllProductDetail(id);
        return new ResultUtil<ProductOrderVo>().setData(productOrderVo);
    }

    /**
     * 查询产品单位
     */
    @RequestMapping(value = "/getProductUnit", method = RequestMethod.POST)
    @ApiOperation(value = "查看商品详情")
    public Result<IPage<UnitVO>> getProductUnit(@ModelAttribute PageVo page, String id) {

        IPage<UnitVO> list = iproductOrderService.getProductUnit(PageUtil.initMpPage(page), id);
        return new ResultUtil<IPage<UnitVO>>().setData(list);
    }

    /**
     * 添加产品
     */
    @RequestMapping(value = "/insertProduct", method = RequestMethod.POST)
    @ApiOperation(value = "添加产品")
    public Result<ProductOrder> insertProduct(@ModelAttribute ProductOrderExtend productOrder) {

        Result<ProductOrder> result = iproductOrderService.insertProduct(productOrder);
        return result;
    }

    /**
     * 修改产品
     */
    @RequestMapping(value = "/updateProduct", method = RequestMethod.POST)
    @ApiOperation(value = "添加产品")
    public Result<ProductOrder> updateProduct(ProductOrderExtend productOrder) {

        Result<ProductOrder> result = iproductOrderService.updateProduct(productOrder);
        return result;
    }

    /**
     * 查询订单产品
     */
    @RequestMapping(value = "/getIndentProduct", method = RequestMethod.POST)
    @ApiOperation(value = "获取全部数据")
    public Result<Map<String, Object>> getIndentProduct(PageVo pageVo, String id) {

        Map<String, Object> map = new HashMap<>();
        List<ProductCommon> list = iproductOrderService.getIndentProduct(id);
        map.put("total", list.size());
        list = PageUtil.listToPage(pageVo, list);
        map.put("result", list);

        return new ResultUtil<Map<String, Object>>().setData(map);
    }
}
