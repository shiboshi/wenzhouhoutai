package cn.exrick.xboot.modules.bussiness.channel.VO;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
@ApiModel("渠道")
public class ChannelVO {

    @ApiModelProperty("id")
    private String id;

    @ApiModelProperty("渠道名称")
    private String channelName;

    @ApiModelProperty("渠道备注")
    private String channelRemark;

}
