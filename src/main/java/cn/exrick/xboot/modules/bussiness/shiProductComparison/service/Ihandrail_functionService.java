package cn.exrick.xboot.modules.bussiness.shiProductComparison.service;

import com.baomidou.mybatisplus.extension.service.IService;
import cn.exrick.xboot.modules.bussiness.shiProductComparison.pojo.handrail_function;

import java.util.List;

/**
 * 臀部功能接口
 * @author 石博士
 */
public interface Ihandrail_functionService extends IService<handrail_function> {

}