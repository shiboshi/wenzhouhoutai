package cn.exrick.xboot.modules.bussiness.specialPriceSalesOrder.listener;

import cn.exrick.xboot.common.enums.CheckStatus;
import cn.exrick.xboot.config.springContext.SpringContextHolder;
import cn.exrick.xboot.modules.bussiness.specialPriceSalesOrder.pojo.SpecialPriceSalesOrder;
import cn.exrick.xboot.modules.bussiness.specialPriceSalesOrder.service.IspecialPriceSalesOrderService;
import org.activiti.engine.delegate.DelegateExecution;
import org.activiti.engine.delegate.ExecutionListener;

/**
 * @Author : xiaofei
 * @Date: 2019/8/28
 */
public class SpecialListener implements ExecutionListener {
    @Override
    public void notify(DelegateExecution delegateExecution) throws Exception {

        String tableId = (String) delegateExecution.getVariable("tableId");
        IspecialPriceSalesOrderService service = SpringContextHolder.getBean(IspecialPriceSalesOrderService.class);
        if (delegateExecution.getEventName().equals("end")){
            SpecialPriceSalesOrder specialPriceSalesOrder = service.getById(tableId);
            specialPriceSalesOrder.setCheckStatus(CheckStatus.sucess);
            service.updateById(specialPriceSalesOrder);
        }
    }
}
