package cn.exrick.xboot.modules.bussiness.level.serviceImp;

import cn.exrick.xboot.modules.bussiness.level.VO.LevelVO;
import cn.exrick.xboot.modules.bussiness.level.mapper.LevelMapper;
import cn.exrick.xboot.modules.bussiness.level.pojo.Level;
import cn.exrick.xboot.modules.bussiness.level.service.ILevelService;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

/**
 * 等级表接口实现
 * @author tqr
 */
@Slf4j
@Service
@Transactional
public class ILevelServiceImpl extends ServiceImpl<LevelMapper, Level> implements ILevelService {

    @Autowired
    private LevelMapper levelMapper;

    @Override
    public IPage<LevelVO> getByPage(Page initMpPage, String levelName) {
        IPage<LevelVO> list = levelMapper.selectPage(initMpPage,new QueryWrapper<Level>().lambda().like(Level::getLevelName,levelName));
        return null;
    }
}