package cn.exrick.xboot.modules.bussiness.shiProductComparison.controller;

import cn.exrick.xboot.common.utils.PageUtil;
import cn.exrick.xboot.common.utils.ResultUtil;
import cn.exrick.xboot.common.vo.PageVo;
import cn.exrick.xboot.common.vo.Result;
import cn.exrick.xboot.modules.bussiness.shiProductComparison.pojo.handrail_function;
import cn.exrick.xboot.modules.bussiness.shiProductComparison.service.Ihandrail_functionService;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * @author 石博士
 */
@Slf4j
@RestController
@Api(description = "臀部功能管理接口")
@RequestMapping("/xboot/handrail_function")
@Transactional
public class handrail_functionController {

    @Autowired
    private Ihandrail_functionService ihandrail_functionService;

    @RequestMapping(value = "/get/{id}", method = RequestMethod.GET)
    @ApiOperation(value = "通过id获取")
    public Result<handrail_function> get(@PathVariable String id){

        handrail_function handrail_function = ihandrail_functionService.getById(id);
        return new ResultUtil<handrail_function>().setData(handrail_function);
    }

    @RequestMapping(value = "/getAll", method = RequestMethod.GET)
    @ApiOperation(value = "获取全部数据")
    public Result<List<handrail_function>> getAll(){

        List<handrail_function> list = ihandrail_functionService.list();
        return new ResultUtil<List<handrail_function>>().setData(list);
    }

    @RequestMapping(value = "/getByPage", method = RequestMethod.GET)
    @ApiOperation(value = "分页获取")
    public Result<IPage<handrail_function>> getByPage(@ModelAttribute PageVo page){

        IPage<handrail_function> data = ihandrail_functionService.page(PageUtil.initMpPage(page));
        return new ResultUtil<IPage<handrail_function>>().setData(data);
    }

    @RequestMapping(value = "/insertOrUpdate", method = RequestMethod.POST)
    @ApiOperation(value = "编辑或更新数据")
    public Result<handrail_function> saveOrUpdate(@ModelAttribute handrail_function handrail_function){

        if(ihandrail_functionService.saveOrUpdate(handrail_function)){
            return new ResultUtil<handrail_function>().setData(handrail_function);
        }
        return new ResultUtil<handrail_function>().setErrorMsg("操作失败");
    }

    @RequestMapping(value = "/delByIds/{ids}", method = RequestMethod.DELETE)
    @ApiOperation(value = "批量通过id删除")
    public Result<Object> delAllByIds(@PathVariable String[] ids){

        for(String id : ids){
            ihandrail_functionService.removeById(id);
        }
        return new ResultUtil<Object>().setSuccessMsg("批量通过id删除数据成功");
    }
}
