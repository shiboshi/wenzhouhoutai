package cn.exrick.xboot.modules.bussiness.saleOrder.Listener;

import cn.exrick.xboot.common.enums.CheckStatus;
import cn.exrick.xboot.common.enums.SendStatus;
import cn.exrick.xboot.config.springContext.SpringContextHolder;
import cn.exrick.xboot.modules.bussiness.saleOrder.pojo.SalesOrder;
import cn.exrick.xboot.modules.bussiness.saleOrder.service.IsalesOrderService;
import org.activiti.engine.delegate.DelegateExecution;
import org.activiti.engine.delegate.ExecutionListener;

/**
 * @Author : xiaofei
 * @Date: 2019/8/28
 */
public class saleOrderListener implements ExecutionListener {
    @Override
    public void notify(DelegateExecution delegateExecution) {

        String tableId = (String) delegateExecution.getVariable("tableId");
        IsalesOrderService service = SpringContextHolder.getBean(IsalesOrderService.class);
        SalesOrder salesOrder = service.getById(tableId);
        if (delegateExecution.getEventName().equals("end")) {
            salesOrder.setSendStatus(SendStatus.DOING);
            salesOrder.setCheckStatus(CheckStatus.sucess);
            service.updateById(salesOrder);
        }
    }
}
