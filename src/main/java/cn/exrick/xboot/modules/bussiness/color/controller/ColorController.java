package cn.exrick.xboot.modules.bussiness.color.controller;

import cn.exrick.xboot.common.constant.CommonConstant;
import cn.exrick.xboot.common.utils.PageUtil;
import cn.exrick.xboot.common.utils.ResultUtil;
import cn.exrick.xboot.common.utils.SecurityUtil;
import cn.exrick.xboot.common.vo.PageVo;
import cn.exrick.xboot.common.vo.Result;
import cn.exrick.xboot.modules.bussiness.classification.pojo.Classification;
import cn.exrick.xboot.modules.bussiness.classification.service.IClassificationService;
import cn.exrick.xboot.modules.bussiness.color.VO.ColorVO;
import cn.exrick.xboot.modules.bussiness.color.pojo.Color;
import cn.exrick.xboot.modules.bussiness.color.service.IcolorService;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.List;

/**
 * @author tqr
 */
@Slf4j
@RestController
@Api(description = "产品颜色管理接口")
@RequestMapping("/xboot/color")
@Transactional
public class ColorController {

    @Autowired
    private SecurityUtil securityUtil;


    @Autowired
    private IcolorService icolorService;

    @RequestMapping(value = "/get/{id}", method = RequestMethod.GET)
    @ApiOperation(value = "通过id获取")
    public Result<Color> get(@PathVariable String id){

        Color color = icolorService.getById(id);
        return new ResultUtil<Color>().setData(color);
    }

    @RequestMapping(value = "/getAll", method = RequestMethod.GET)
    @ApiOperation(value = "获取全部数据")
    public Result<List<Color>> getAll(){

        List<Color> list = icolorService.list();
        return new ResultUtil<List<Color>>().setData(list);
    }

    @RequestMapping(value = "/getByPage", method = RequestMethod.POST)
    @ApiOperation(value = "分页获取")
    public Result<IPage<ColorVO>> getByPage(@ModelAttribute PageVo page, String name){
        IPage<ColorVO> data = new Page<>();
        if(StringUtils.isNotBlank(name)){
            data = icolorService.getByName(PageUtil.initMpPage(page),name);
        } else {
            data = icolorService.page(PageUtil.initMpPage(page));
        }
        return new ResultUtil<IPage<ColorVO>>().setData(data);
    }

    @RequestMapping(value = "/insertOrUpdate", method = RequestMethod.POST)
    @ApiOperation(value = "编辑或更新数据")
    public Result<Color> saveOrUpdate(@ModelAttribute Color color){
        String username = securityUtil.getCurrUser().getUsername();
        color.setCreateBy(username);
        color.setUpdateBy(username);
        color.setCreateTime(new Date());
        color.setUpdateTime(new Date());
        if (!icolorService.saveOrUpdate(color)) {
            return new ResultUtil<Color>().setErrorMsg("添加失败");
        }
        if(!CommonConstant.PARENT_ID.equals(color.getParentId())){
            Color parent = icolorService.getById(color.getParentId());
            if(parent.getIsParent()==null||!parent.getIsParent()){
                parent.setIsParent(true);
                icolorService.saveOrUpdate(parent);
            }
        }
        return new ResultUtil<Color>().setSuccessMsg("添加成功");
    }

    @RequestMapping(value = "/delByIds/{ids}", method = RequestMethod.DELETE)
    @ApiOperation(value = "批量通过id删除")
    public Result<Object> delAllByIds(@PathVariable String[] ids){

        for(String id : ids){
            icolorService.removeById(id);
        }
        return new ResultUtil<Object>().setSuccessMsg("批量通过id删除数据成功");
    }

    @GetMapping("/getByParentId/{parentId}")
    @ApiOperation(value = "通过parentId获取")
    public Result<List<Color>> getByParentId(@PathVariable String parentId) {
            List<Color> list = icolorService.findByParentIdOrderBySortOrder(parentId);
            list.forEach(c -> {
                if(!CommonConstant.PARENT_ID.equals(c.getParentId())){
                    Color parent = icolorService.getById(c.getParentId());
                    c.setParentTitle(parent.getTitle());
                }else{
                    c.setParentTitle("一级分类");
                }
            });
            return new ResultUtil<List<Color>>().setData(list);
        }

    /**
     * 查询颜色
     */
    @RequestMapping(value = "/getColor", method = RequestMethod.POST)
    @ApiOperation(value = "通过id获取")
    public Result<ColorVO> getColor(String id){

        ColorVO color = icolorService.getColor(id);
        return new ResultUtil<ColorVO>().setData(color);
    }
}
