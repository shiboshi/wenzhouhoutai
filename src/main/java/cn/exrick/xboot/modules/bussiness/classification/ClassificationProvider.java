package cn.exrick.xboot.modules.bussiness.classification;

/**
 * 产品分类SQL
 *
 * @author zhou
 */
public class ClassificationProvider {
    /**
     * 产品产品分类SQL
     * @param parentId 父级ID
     * @return
     */
    public String getClassificationSql(String parentId) {
        StringBuffer stringBuffer = new StringBuffer();
        stringBuffer.append("select * from t_classification where parent_id = ")
                .append(parentId).append(" and del_flag = 0");
        return stringBuffer.toString();
    }
}
