package cn.exrick.xboot.modules.base.dao.mapper;

import cn.exrick.xboot.modules.base.entity.Department;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.springframework.stereotype.Repository;

/**
 * @Author : xiaofei
 * @Date: 2019/8/14
 */
@Repository
public interface DepartmentMapper extends BaseMapper<Department> {
}
