package cn.exrick.xboot.modules.bussiness.packageSize.serviceImp;

import cn.exrick.xboot.common.utils.ToolUtil;
import cn.exrick.xboot.modules.bussiness.packageSize.VO.PackageSizeVO;
import cn.exrick.xboot.modules.bussiness.packageSize.mapper.PackageSizeMapper;
import cn.exrick.xboot.modules.bussiness.packageSize.pojo.PackageSize;
import cn.exrick.xboot.modules.bussiness.packageSize.service.IPackageSizeService;
import cn.exrick.xboot.modules.bussiness.product.mapper.productModelMapper;
import cn.exrick.xboot.modules.bussiness.product.pojo.ProductModel;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * 包装尺寸接口实现
 *
 * @author tqr
 */
@Slf4j
@Service
@Transactional
public class IPackageSizeServiceImpl extends ServiceImpl<PackageSizeMapper, PackageSize> implements IPackageSizeService {

    @Autowired
    private PackageSizeMapper packageSizeMapper;
    @Autowired
    private productModelMapper productModelMapper1;

    @Override
    public IPage<PackageSizeVO> getPackageSize(Page initMpPage, String condition) {
        IPage<PackageSizeVO> list = packageSizeMapper.getPackageSize(initMpPage, condition);
        List<PackageSizeVO> sizeVOS = list.getRecords();
        if (sizeVOS.size() > 0) {
            for (PackageSizeVO p : sizeVOS) {
                String name = productModelMapper1.selectOne(new QueryWrapper<ProductModel>().lambda().eq(ProductModel::getKindId, p.getProductModelId())).getModelNameIn();
                p.setModelNameIn(name);
            }
        }
        return list;
    }


    @Override
    public IPage<PackageSizeVO> getByProductId(Page initMpPage, String id) {
        IPage<PackageSizeVO> list = packageSizeMapper.getByProductId(initMpPage, id);
        for (PackageSizeVO p : list.getRecords()) {
            if (ToolUtil.isNotEmpty(p)) {
                String info = "长:" + p.getLength() + "/宽:" + p.getWide() + "/高:" + p.getTall() + "/毛重:" + p.getRoughWeight() + "/净重:" + p.getNetWeight() + "/体积:" + p.getVolume() + "";
                p.setInfo(info);
            }
        }
        return list;
    }
}