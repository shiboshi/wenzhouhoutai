package cn.exrick.xboot.modules.bussiness.saleOrder.controller;

import cn.exrick.xboot.common.utils.PageUtil;
import cn.exrick.xboot.common.utils.ResultUtil;
import cn.exrick.xboot.common.utils.ToolUtil;
import cn.exrick.xboot.common.vo.PageVo;
import cn.exrick.xboot.common.vo.Result;
import cn.exrick.xboot.modules.bussiness.product.VO.ProductCommon;
import cn.exrick.xboot.modules.bussiness.product.service.IproductOrderService;
import cn.exrick.xboot.modules.bussiness.purchaseOrder.VO.PurchaseOrderVO;
import cn.exrick.xboot.modules.bussiness.purchaseOrder.pojo.PurchaseOrder;
import cn.exrick.xboot.modules.bussiness.purchaseOrder.service.IpurchaseOrderService;
import cn.exrick.xboot.modules.bussiness.saleOrder.VO.NeedCheckProcudct;
import cn.exrick.xboot.modules.bussiness.saleOrder.VO.SalesOrderVO;
import cn.exrick.xboot.modules.bussiness.saleOrder.pojo.SalesOrder;
import cn.exrick.xboot.modules.bussiness.saleOrder.service.IsalesOrderService;
import cn.exrick.xboot.modules.bussiness.sendGoodsOrder.pojo.SendGoodsOrder;
import cn.exrick.xboot.modules.bussiness.sendGoodsOrder.service.IsendGoodsOrderService;
import cn.exrick.xboot.modules.bussiness.specialPriceSalesOrder.service.IspecialPriceSalesOrderService;
import cn.exrick.xboot.modules.wx.Wxservice;
import com.alibaba.fastjson.JSON;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.google.common.collect.Maps;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * @author xiaofei
 */
@Slf4j
@RestController
@Api(description = "销售订单管理接口")
@RequestMapping("/xboot/salesOrder")
@Transactional
public class salesOrderController {

    @Autowired
    private IsalesOrderService isalesOrderService;
    @Autowired
    private IspecialPriceSalesOrderService specialPriceSalesOrderService;
    @Autowired
    private IproductOrderService iproductOrderService;
    @Autowired
    private IpurchaseOrderService ipurchaseOrderService;
    @Autowired
    private Wxservice wxservice;
    @Autowired
    private IsendGoodsOrderService isendGoodsOrderService;


    @PostMapping("/getDetail")
    @ApiOperation(value = "通过id获取销售单详情")
    public Result<Map<String, Object>> getDetail(String id, @ModelAttribute PageVo pageVo) {
        List<PurchaseOrderVO> saleOrderDetail = isalesOrderService.getDetail(id);
        for(PurchaseOrderVO p : saleOrderDetail){
            SalesOrder salesOrder = isalesOrderService.getById(id);
            p.setSalesOrder(salesOrder);
        }
        Map<String, Object> map = Maps.newHashMap();
        map.put("data", PageUtil.listToPage(pageVo, saleOrderDetail));
        map.put("size", saleOrderDetail.size());
        return new ResultUtil<Map<String, Object>>().setData(map);
    }

    @PostMapping("/getList")
    @ApiOperation(value = "获取销售订单列表")
    public Result<Page<SalesOrderVO>> getList(@ModelAttribute PageVo pageVo, @ModelAttribute SalesOrderVO salesOrderVO, String userId) {
        Page<SalesOrderVO> lists = isalesOrderService.getList(pageVo, salesOrderVO, userId);
        return new ResultUtil<Page<SalesOrderVO>>().setData(lists);
    }

    @PostMapping("/getUserList")
    @ApiOperation(value = "获取销售订单列表")
    public Result<Page<SalesOrderVO>> getUserList(@ModelAttribute PageVo pageVo, @ModelAttribute SalesOrderVO salesOrderVO, String userId) {
        Page<SalesOrderVO> lists = isalesOrderService.getUserList(pageVo, salesOrderVO, userId);
        List<SalesOrderVO> list = lists.getRecords();
        lists.setTotal(list.size());
        return new ResultUtil<Page<SalesOrderVO>>().setData(lists);
    }

    @PostMapping("/selectById")
    @ApiOperation(value = "获取销售订单")
    public Result<List<SalesOrderVO>> selectById(String id) {
        SalesOrderVO salesOrder = isalesOrderService.selectById(id);
        List<ProductCommon> productCommons = new ArrayList<>();
        if (ToolUtil.isNotEmpty(salesOrder)) {
            String[] ids = salesOrder.getPurchaseOrderIds().split(",");
            if (ids.length > 0) {
                for (String i : ids) {
                    ProductCommon productCommon = iproductOrderService.getProductCommon(ipurchaseOrderService.getById(i).getProductId());
                    productCommon.setPrice(ipurchaseOrderService.getById(i).getPrice());
                    productCommon.setProductNum(ipurchaseOrderService.getById(i).getOrderNumbers());
                    productCommons.add(productCommon);
                }
            }
        }
        salesOrder.setList(productCommons);
        List<SalesOrderVO> list = new ArrayList<>();
        list.add(salesOrder);
        return new ResultUtil<List<SalesOrderVO>>().setData(list);
    }

    @PostMapping("/insertOrder")
    @ApiOperation(value = "插入到销售订单")
    public Result<?> insertOrder(String param, SalesOrder salesOrder,String userId) {
        List<PurchaseOrder> purchaseOrders = JSON.parseArray(param, PurchaseOrder.class);
        if (ToolUtil.isOneEmpty(param, salesOrder)) {
            return new ResultUtil<>().setErrorMsg("传入参数为空！");
        } else {
            if (ToolUtil.isEmpty(purchaseOrders)) {
                return new ResultUtil<>().setErrorMsg("json解析失败!");
            } else {
                List<PurchaseOrder> needCheckList = isalesOrderService.filterNeedCheck(param,salesOrder.getBuyerPhone());
                if (ToolUtil.isNotEmpty(needCheckList)){
                    List<NeedCheckProcudct> needCheckProcudcts = isalesOrderService.getNeedCheck(needCheckList);
                    return new ResultUtil<>().setErrorMsg(201, "以下商品需要审核！", needCheckProcudcts);
                }
                isalesOrderService.commitSend(purchaseOrders, salesOrder, userId);
                return new ResultUtil<>().setData("插入订单成功！");
            }
        }
    }

    @DeleteMapping("/delete")
    @ApiOperation(value = "批量通过id删除")
    public Result<Object> delAllByIds(String[] ids) {
        for (String id : ids) {
            isalesOrderService.removeById(id);
        }
        return new ResultUtil<Object>().setSuccessMsg("批量通过id删除数据成功");
    }

    @PostMapping("/update")
    @ApiOperation(value = "修改")
    public Result<Object> updateOrder(SalesOrder salesOrder) {
        try {
            isalesOrderService.getBaseMapper().updateById(salesOrder);
            return new ResultUtil<Object>().setSuccessMsg("修改成功");
        } catch (Exception e) {
            return new ResultUtil<Object>().setErrorMsg("修改失败");
        }
    }

    /**
     * 销售单发货
     * @return
     */
    @PostMapping("/salesSend")
    public Result<Object> salesSend(SendGoodsOrder sendGoodsOrder,String taskId,String comment,String procInstId){

        List<String> serial = isendGoodsOrderService.filterInventory(sendGoodsOrder.getSerial());
        if (ToolUtil.isNotEmpty(serial)){
            return new ResultUtil<>().setErrorMsg(201,"以下序列号库存为空！",serial);
        }else {
            isalesOrderService.salesSend(sendGoodsOrder,taskId,comment,procInstId);
            return new ResultUtil<>().setSuccessMsg("发货成功！");
        }
    }





}
