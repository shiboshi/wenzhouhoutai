package cn.exrick.xboot.modules.bussiness.purchaseOrder.VO;

import cn.exrick.xboot.common.enums.IsComplete;
import cn.exrick.xboot.modules.bussiness.indentOrder.pojo.IndentOrder;
import cn.exrick.xboot.modules.bussiness.packageSize.pojo.PackageSize;
import cn.exrick.xboot.modules.bussiness.product.VO.ProductOrderVo;
import cn.exrick.xboot.modules.bussiness.saleOrder.pojo.SalesOrder;
import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.math.BigDecimal;
import java.util.Date;

/**
 * @Author : xiaofei
 * @Date: 2019/8/9
 */
@Data
public class PurchaseOrderVO {


    private String id;
    /*产品基础信息*/
    private String productName;
    private String productOrderName;

    @ApiModelProperty("产品型号")
    private String productModelName;


    /**
     * 购买数量
     */
    private BigDecimal orderNumbers;

    /**
     * 购买价格
     */
    private BigDecimal price;

    /**
     * 购买渠道
     */
    private String channelName;

    /**
     * 购买单位
     */
    private String unitName;

    /**
     * 购买包装尺寸
     */
    private PackageSize packageSizeName;

    /**
     * 总金额
     */
    private BigDecimal orderAmount;

    /**
     * 已发数量
     */
    private BigDecimal havaSend;

    /**
     * 未发数量
     */
    private BigDecimal notSend;

    /**
     * 备注
     */
    private String remark;

    /**
     * 退货数量
     */
    private Long returnNum;

    /**
     * 申请发货数量
     */
    private Long dispatchNum;

    /**
     * 订单是否完成1.未完成2.已完成
     */
    private IsComplete isComplete;

    private ProductOrderVo productOrderInfo;

    private String packageSize;
    @ApiModelProperty("净重")
    private String unitWeight;
    @ApiModelProperty("体积")
    private String volume;
    @ApiModelProperty("换算比例")
    private String conversionRatio;
    /**
     * 换算比例分母
     */
    @ApiModelProperty("换算比例分母")
    private String molecule;


    private String modelNameIn;
    private String color;
    private String colorNumber;
    private String material;
    private IndentOrder indentOrder;
    private SalesOrder salesOrder;

    @ApiModelProperty("最小单位")
    private String unitMins;

    @ApiModelProperty("最大单位")
    private String unitMaxs;

    @ApiModelProperty("最低订货数量")
    private BigDecimal minOrderAmount;

    /**
     * 上市时间
     */
    @JsonFormat(timezone = "GMT+8", pattern = "yyyy-MM-dd")
    private Date timeToMarket;




}
