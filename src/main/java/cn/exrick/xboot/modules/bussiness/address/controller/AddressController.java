package cn.exrick.xboot.modules.bussiness.address.controller;

import cn.exrick.xboot.common.constant.CommonConstant;
import cn.exrick.xboot.common.utils.PageUtil;
import cn.exrick.xboot.common.utils.ResultUtil;
import cn.exrick.xboot.common.utils.SecurityUtil;
import cn.exrick.xboot.common.vo.PageVo;
import cn.exrick.xboot.common.vo.Result;
import cn.exrick.xboot.modules.bussiness.address.pojo.Address;
import cn.exrick.xboot.modules.bussiness.address.service.IAddressService;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import java.util.Date;
import java.util.List;

/**
 * @author tqr
 */
@Slf4j
@RestController
@Api(description = "地址管理接口")
@RequestMapping("/xboot/address")
@Transactional
public class AddressController {

    @Autowired
    private IAddressService iAddressService;

    @Autowired
    private SecurityUtil securityUtil;

    @RequestMapping(value = "/get/{id}", method = RequestMethod.GET)
    @ApiOperation(value = "通过id获取")
    public Result<Address> get(@PathVariable String id){

        Address address = iAddressService.getById(id);
        return new ResultUtil<Address>().setData(address);
    }

    @RequestMapping(value = "/getAll", method = RequestMethod.GET)
    @ApiOperation(value = "获取全部数据")
    public Result<List<Address>> getAll(){

        List<Address> list = iAddressService.list();
        return new ResultUtil<List<Address>>().setData(list);
    }

    @RequestMapping(value = "/getByPage", method = RequestMethod.GET)
    @ApiOperation(value = "分页获取")
    public Result<IPage<Address>> getByPage(@ModelAttribute PageVo page){

        IPage<Address> data = iAddressService.page(PageUtil.initMpPage(page));
        return new ResultUtil<IPage<Address>>().setData(data);
    }

    @RequestMapping(value = "/insertOrUpdate", method = RequestMethod.POST)
    @ApiOperation(value = "编辑或更新数据")
    public Result<Address> saveOrUpdate(@ModelAttribute Address address){
        String userId = securityUtil.getCurrUser().getId();
        address.setCreateBy(userId);
        address.setUpdateBy(userId);
        address.setCreateTime(new Date());
        address.setUpdateTime(new Date());
        address.setCreateTime(new Date());
        if(!iAddressService.saveOrUpdate(address)){
            return new ResultUtil<Address>().setErrorMsg("操作失败");
        }
        if(!CommonConstant.PARENT_ID.equals(address.getParentId())){
            Address parent = iAddressService.getById(address.getParentId());
            if(parent.getIsParent()==null||!parent.getIsParent()){
                parent.setIsParent(true);
                iAddressService.saveOrUpdate(parent);
            }
        }
        return new ResultUtil<Address>().setSuccessMsg("添加成功");
    }

    @RequestMapping(value = "/delByIds/{ids}", method = RequestMethod.DELETE)
    @ApiOperation(value = "批量通过id删除")
    public Result<Object> delAllByIds(@PathVariable String[] ids){

        for(String id : ids){
            //查询父级id
            String pId = iAddressService.getById(id).getParentId();
            if("0".equals(pId)){
                return new ResultUtil<Object>().setErrorMsg("请勿删除顶级分类");
            }
            iAddressService.removeById(id);
        }
        return new ResultUtil<Object>().setSuccessMsg("批量通过id删除数据成功");
    }

    @RequestMapping(value = "/getByCondition", method = RequestMethod.POST)
    @ApiOperation(value = "条件获取")
    public Result<IPage<Address>> getByCondition(@ModelAttribute PageVo page,String name){
        IPage<Address> data = new Page<>();
        if(StringUtils.isNotEmpty(name)) {
           data = iAddressService.getByCondition(name, PageUtil.initMpPage(page));
        } else {
           data = iAddressService.getByPage(PageUtil.initMpPage(page));
        }
        return new ResultUtil<IPage<Address>>().setData(data);
    }

    @PostMapping("/getByParentId")
    @ApiOperation(value = "通过parentId获取")
    public Result<List<Address>> getByParentId(String parentId) {
        List<Address> list = iAddressService.findByParentIdOrderBySortOrder(parentId);
        list.forEach(c -> {
            if(!CommonConstant.PARENT_ID.equals(c.getParentId())){
                Address parent = iAddressService.getById(c.getParentId());
                c.setParentTitle(parent.getTitle());
            }else{
                c.setParentTitle("一级分类");
            }
        });
        return new ResultUtil<List<Address>>().setData(list);
    }

}
