package cn.exrick.xboot.modules.bussiness.afterSaleOrder.VO;

import lombok.Data;

import java.math.BigDecimal;
import java.util.List;

/**
 * @Author : xiaofei
 * @Date: 2019/9/10
 */
@Data
public class CheckSaleVO {


    private String id;
    /**
     * 审核人姓名
     */
    private String userName;
    /**
     * 审核人部门
     */
    private String userDept;

    /**
     * 售后维修图片
     */
    private String afterPic;

    /**
     * 售后维修人员的联系方式
     */
    private String maintainerPhone;

    /**
     * 选取配件名称
     */
    private List<String> paretsName;
    /**
     * 维修费用
     */
    private BigDecimal cost;
    /**
     * 评论信息
     */
    private String comment;


}
