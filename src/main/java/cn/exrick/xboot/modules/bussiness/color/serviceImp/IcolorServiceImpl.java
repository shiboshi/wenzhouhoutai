package cn.exrick.xboot.modules.bussiness.color.serviceImp;

import cn.exrick.xboot.common.utils.ToolUtil;
import cn.exrick.xboot.modules.bussiness.classification.pojo.Classification;
import cn.exrick.xboot.modules.bussiness.color.VO.ColorVO;
import cn.exrick.xboot.modules.bussiness.color.mapper.colorMapper;
import cn.exrick.xboot.modules.bussiness.color.pojo.Color;
import cn.exrick.xboot.modules.bussiness.color.service.IcolorService;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.validation.constraints.Max;
import java.util.List;

/**
 * 产品颜色接口实现
 * @author tqr
 */
@Slf4j
@Service
@Transactional
public class IcolorServiceImpl extends ServiceImpl<colorMapper, Color> implements IcolorService {

    @Autowired
    private colorMapper colorMapper;

    @Override
    public IPage<ColorVO> getByName(Page initMpPage, String name) {
        IPage<ColorVO> list = colorMapper.selectPage(initMpPage,new QueryWrapper<Color>().lambda().like(Color::getTitle,name));
        return list;
    }

    @Override
    public List<Color> findByParentIdOrderBySortOrder(String parentId) {
        return colorMapper.findByParentIdOrderBySortOrder(parentId);
    }

    @Override
    public ColorVO getColor(String id) {
        ColorVO colorVO = colorMapper.getColor(id);
        if (ToolUtil.isNotEmpty(colorVO)){
            colorVO.setAllName(colorVO.getColor()+"/"+colorVO.getColorNumber()+"/"+colorVO.getMaterial());
        }
        return colorVO;
    }

}