package cn.exrick.xboot.modules.bussiness.dealerLevel.pojo;

import cn.exrick.xboot.base.XbootBaseEntity;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModelProperty;
import io.swagger.annotations.ApiModel;
import lombok.Data;
import lombok.experimental.Accessors;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import java.math.BigDecimal;

/**
 * @author tqr
 */
@Data
@Entity
@Table(name = "t_dealer_level")
@TableName("t_dealer_level")
@ApiModel(value = "经销商等级")
@Accessors(chain = true)
public class DealerLevel extends XbootBaseEntity {

    private static final long serialVersionUID = 1L;

    /**
     * 经销商等级
     */
    private String levelId;

    /**
     * 经销商名字
     */
    private String name;

    /**
     * 经销商备注
     */
    private String remark;

    /**
     * 关联部门id
     */
    private String deptId;
    /**
     * 经销商信用额度
     */
    @Column(precision = 32,scale = 2)
    private BigDecimal credit;

    /**
     * 经销商详细地址
     */
    @Column(length = 255)
    private String address;

    /**
     * 账号余额
     */
    @Column(precision = 32,scale = 2)
    private BigDecimal balance;

    /**
     * 经销商区域
     */
    private String areaId;
    /**
     * 经销商区域
     */
    @TableField(exist = false)
    @Column(length = 255)
    private String areaName;
}