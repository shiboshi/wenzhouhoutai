package cn.exrick.xboot.modules.bussiness.putInWarehouseOrder.serviceImp;

import cn.exrick.xboot.common.utils.ToolUtil;
import cn.exrick.xboot.common.vo.CommonVO;
import cn.exrick.xboot.modules.base.dao.mapper.UserMapper;
import cn.exrick.xboot.modules.bussiness.product.mapper.ProductOrderMapper;
import cn.exrick.xboot.modules.bussiness.product.pojo.ProductOrder;
import cn.exrick.xboot.modules.bussiness.putInWarehouse.mapper.PutInWarehouseMapper;
import cn.exrick.xboot.modules.bussiness.putInWarehouse.pojo.PutInWarehouse;
import cn.exrick.xboot.modules.bussiness.putInWarehouseOrder.mapper.PutInWarehouseOrderMapper;
import cn.exrick.xboot.modules.bussiness.putInWarehouseOrder.pojo.PutInWarehouseOrder;
import cn.exrick.xboot.modules.bussiness.putInWarehouseOrder.pojo.PutInWarehouseOrderVO;
import cn.exrick.xboot.modules.bussiness.putInWarehouseOrder.service.IPutInWarehouseOrderService;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

/**
 * 入库单集合接口实现
 * @author tqr
 */
@Slf4j
@Service
@Transactional
public class IPutInWarehouseOrderServiceImpl extends ServiceImpl<PutInWarehouseOrderMapper, PutInWarehouseOrder> implements IPutInWarehouseOrderService {

    @Autowired
    private PutInWarehouseOrderMapper putInWarehouseOrderMapper;
    @Autowired
    private UserMapper userMapper;
    @Autowired
    private ProductOrderMapper productOrderMapper;
    @Autowired
    private PutInWarehouseMapper putInWarehouseMapper;

    @Override
    public List<PutInWarehouseOrderVO> getPutInWarehouseOrder(String numb) {
        List<PutInWarehouseOrder> list = new ArrayList<>();
        if(ToolUtil.isNotEmpty(numb)){
            list = putInWarehouseOrderMapper.selectList(new QueryWrapper<PutInWarehouseOrder>().lambda().eq(PutInWarehouseOrder::getPutInNumb,numb).orderByDesc(PutInWarehouseOrder::getCreateTime));
        } else {
            list = putInWarehouseOrderMapper.selectList(new QueryWrapper<PutInWarehouseOrder>().lambda().orderByDesc(PutInWarehouseOrder::getCreateTime));
        }
        List<PutInWarehouseOrderVO> putInWarehouseOrderVOS = new ArrayList<>();
        if(list.size()>0){
            for(PutInWarehouseOrder putInWarehouseOrder : list){
                if(ToolUtil.isNotEmpty(putInWarehouseOrder)){
                    List<String> names = new ArrayList<>();
                    PutInWarehouseOrderVO putInWarehouseOrderVO = new PutInWarehouseOrderVO();
                    putInWarehouseOrderVO.setId(putInWarehouseOrder.getId());
                    putInWarehouseOrderVO.setCreateTime(putInWarehouseOrder.getCreateTime());
                    CommonVO commonVO = userMapper.getUserMessage(putInWarehouseOrder.getCreateBy());
                    ToolUtil.copyProperties(commonVO,putInWarehouseOrderVO);
                    String[] ids = putInWarehouseOrder.getPutInWarehouseIds().split(",");
                    for(String id :ids){
                        PutInWarehouse putInWarehouse = putInWarehouseMapper.selectById(id);
                        if(ToolUtil.isNotEmpty(putInWarehouse)){
                            ProductOrder productOrder = productOrderMapper.selectById(putInWarehouse.getProductOrderId());
                            if(ToolUtil.isEmpty(productOrder)){
                                continue;
                            }
                            names.add(productOrder.getProductOrderName());
                        }
                    }
                    putInWarehouseOrderVO.setProductNames(names);
                    putInWarehouseOrderVO.setPutInNumb(putInWarehouseOrder.getPutInNumb());
                    putInWarehouseOrderVOS.add(putInWarehouseOrderVO);
                }
            }
        }
        return putInWarehouseOrderVOS;
    }
}