package cn.exrick.xboot.modules.bussiness.afterSaleOrder.VO;

import cn.exrick.xboot.common.enums.CheckStatus;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.Date;

/**
 * @Author : xiaofei
 * @Date: 2019/9/10
 */
@Data
public class AfterSaleVO {

    private String id;
    /**
     * 消费者姓名
     */
    private String consumerName;

    /**
     * 联系方式
     */
    private String consumerPhone;

    /**
     * 售后地址
     */
    private String consumerAddress;
    /**
     * 审核状态
     */
    private CheckStatus checkStatus;

    /**
     * 创建时间
     */
    private Date createTime;
    /**
     * 任务节点id
     */
    private String taskId;
}
