package cn.exrick.xboot.modules.bussiness.indentOrder.listener;

import cn.exrick.xboot.common.enums.CheckStatus;
import cn.exrick.xboot.common.enums.IsOpen;
import cn.exrick.xboot.common.utils.SpringContextUtil;
import cn.exrick.xboot.common.utils.ToolUtil;
import cn.exrick.xboot.config.springContext.SpringContextHolder;
import cn.exrick.xboot.modules.activiti.entity.ActBusiness;
import cn.exrick.xboot.modules.activiti.entity.ActProcess;
import cn.exrick.xboot.modules.activiti.service.ActBusinessService;
import cn.exrick.xboot.modules.activiti.service.ActProcessService;
import cn.exrick.xboot.modules.bussiness.indentOrder.pojo.IndentOrder;
import cn.exrick.xboot.modules.bussiness.indentOrder.service.IindentOrderService;
import org.activiti.engine.delegate.DelegateExecution;
import org.activiti.engine.delegate.ExecutionListener;

/**
 * @Author : xiaofei
 * @Date: 2019/8/16
 */
public class IndentListener implements ExecutionListener {
    @Override
    public void notify(DelegateExecution delegateExecution) throws Exception {

        String tableId = (String) delegateExecution.getVariable("tableId");
        IindentOrderService indentOrderService = SpringContextHolder.getBean(IindentOrderService.class);
        IndentOrder indentOrder = indentOrderService.getById(tableId);
        if (delegateExecution.getEventName().equals("end")) {
            if (ToolUtil.isNotEmpty(indentOrder)) {
                indentOrder.setCheckStatus(CheckStatus.sucess);
                indentOrderService.updateById(indentOrder);
            }
        }
    }
}
