package cn.exrick.xboot.modules.bussiness.transferOrder.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import cn.exrick.xboot.modules.bussiness.transferOrder.pojo.TransferOrder;
import org.springframework.stereotype.Repository;

/**
 * 调拨单数据处理层
 * @author fei
 */
@Repository
public interface transferOrderMapper extends BaseMapper<TransferOrder> {

}