package cn.exrick.xboot.modules.bussiness.applicationSendGoodsOrder.VO;

import lombok.Data;
import org.hibernate.procedure.spi.ParameterRegistrationImplementor;

/**
 * @Author : xiaofei
 * @Date: 2019/8/26
 */
@Data
public class FilterVO {
    /**
     * 产品大类
     */
    private String productName;
    /**
     * 产品型号
     */
    private String productModelName;
    /**
     * 产品品号
     */
    private String productOrderName;

    /**
     * 发货/退货数量
     */
    private Long disPathNum;
}
