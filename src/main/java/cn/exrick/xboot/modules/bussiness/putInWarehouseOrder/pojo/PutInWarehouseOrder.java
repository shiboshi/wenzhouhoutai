package cn.exrick.xboot.modules.bussiness.putInWarehouseOrder.pojo;

import cn.exrick.xboot.base.XbootBaseEntity;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModelProperty;
import io.swagger.annotations.ApiModel;
import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

/**
 * @author tqr
 */
@Data
@Entity
@Table(name = "t_put_in_warehouse_order")
@TableName("t_put_in_warehouse_order")
@ApiModel(value = "入库单集合")
public class PutInWarehouseOrder extends XbootBaseEntity {

    private static final long serialVersionUID = 1L;

    @Column(length = 1000)
    private String putInWarehouseIds;

    @Column(length = 255)
    private String putInNumb;

}