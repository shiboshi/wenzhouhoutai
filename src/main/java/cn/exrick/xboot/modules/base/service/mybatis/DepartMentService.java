package cn.exrick.xboot.modules.base.service.mybatis;

import cn.exrick.xboot.modules.base.dao.mapper.DepartmentMapper;
import cn.exrick.xboot.modules.base.entity.Department;
import com.baomidou.mybatisplus.extension.service.IService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * @Author : xiaofei
 * @Date: 2019/8/14
 */
@Service
public class DepartMentService extends ServiceImpl<DepartmentMapper,Department> {
}
