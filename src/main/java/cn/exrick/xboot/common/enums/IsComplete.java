package cn.exrick.xboot.common.enums;

import com.baomidou.mybatisplus.annotation.EnumValue;
import com.baomidou.mybatisplus.core.enums.IEnum;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonValue;

/**
 * @Author : xiaofei
 * @Date: 2019/8/14
 */
public enum IsComplete {

    PENDING(0, "订单未完成"),
    IS_COMPLETE(1, "订单已完成");
    IsComplete(final int value, final String desc) {
        this.value = value;
        this.desc = desc;
    }
    @EnumValue
    private final int value;

    public int getValue() {
        return value;
    }

    @JsonValue
    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    private String desc;
}
