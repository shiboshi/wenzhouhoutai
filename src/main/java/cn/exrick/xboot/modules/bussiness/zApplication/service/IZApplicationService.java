package cn.exrick.xboot.modules.bussiness.zApplication.service;

import cn.exrick.xboot.modules.bussiness.shiVO.ZApplicationOV;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;
import cn.exrick.xboot.modules.bussiness.zApplication.pojo.ZApplication;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * 装修申请接口
 * @author tqr
 */
public interface IZApplicationService extends IService<ZApplication> {




    /**
     * 查询所有集合
     */
    IPage<ZApplicationOV> QueryZapplication(Page page, @Param("username") String username);






}