package cn.exrick.xboot.common.factory;

import cn.exrick.xboot.common.utils.ToolUtil;
import cn.exrick.xboot.config.springContext.SpringContextHolder;
import cn.exrick.xboot.modules.base.dao.DepartmentDao;
import cn.exrick.xboot.modules.base.dao.UserDao;
import cn.exrick.xboot.modules.base.entity.Department;
import cn.exrick.xboot.modules.base.entity.User;
import cn.exrick.xboot.modules.base.service.mybatis.DepartMentService;
import cn.exrick.xboot.modules.bussiness.dealerLevel.mapper.DealerLevelMapper;
import cn.exrick.xboot.modules.bussiness.dealerLevel.pojo.DealerLevel;
import cn.exrick.xboot.modules.bussiness.level.mapper.LevelMapper;
import cn.exrick.xboot.modules.bussiness.level.pojo.Level;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.google.common.base.Joiner;
import com.google.common.base.Splitter;
import com.google.common.collect.Lists;
import org.bouncycastle.jcajce.provider.symmetric.AES;
import org.springframework.context.annotation.DependsOn;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import javax.swing.*;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

/**
 * @Author : xiaofei
 * @Date: 2019/8/9
 */
@Component
@DependsOn("springContextHolder")
@Transactional
public class ConstantFactory implements  IConstantFactory {

    private UserDao userDao = SpringContextHolder.getBean(UserDao.class);
    private DepartmentDao departmentDao = SpringContextHolder.getBean(DepartmentDao.class);
    private LevelMapper levelMapper = SpringContextHolder.getBean(LevelMapper.class);
    private DealerLevelMapper dealerLevelMapper = SpringContextHolder.getBean(DealerLevelMapper.class);
    private DepartMentService departMentService = SpringContextHolder.getBean(DepartMentService.class);

    public static IConstantFactory me() {
        return SpringContextHolder.getBean("constantFactory");
    }
    public static  final Splitter SPLITTER = Splitter.on(",").trimResults().omitEmptyStrings();
    public static  final Joiner JOINER = Joiner.on(",").skipNulls();

    @Override
    public String getUserName(String userId) {
        if (ToolUtil.isNotEmpty(userId)) {
            return userDao.getOne(userId).getUsername();
        }
        return "";
    }

    @Override
    public String getUserLevelName(String userId) {
        if (ToolUtil.isNotEmpty(userId)) {
            User user = userDao.getOne(userId);
            if (ToolUtil.isNotEmpty(user)) {
                List<DealerLevel> dealerLevel = dealerLevelMapper.selectList(Wrappers.<DealerLevel>lambdaQuery()
                        .eq(DealerLevel::getDeptId, user.getDepartmentId()));
                if (ToolUtil.isNotEmpty(dealerLevel)) {
                    Level level = levelMapper.selectById(dealerLevel.get(0).getLevelId());
                    if (ToolUtil.isNotEmpty(level)) {
                        return level.getLevelName();
                    }
                }
            }
        }
        return "";
    }

    @Override
    public String getUserDeptName(String userId) {

        if (ToolUtil.isNotEmpty(userId)) {
            User user = userDao.getOne(userId);
            if (ToolUtil.isNotEmpty(user)) {
               Department department = departMentService.getById(user.getDepartmentId());
               if (ToolUtil.isNotEmpty(department)){
                   return department.getTitle();
               }
            }
        }
        return "";
    }
}
