package cn.exrick.xboot.modules.bussiness.shiProductComparison.controller;

import cn.exrick.xboot.common.utils.PageUtil;
import cn.exrick.xboot.common.utils.ResultUtil;
import cn.exrick.xboot.common.vo.PageVo;
import cn.exrick.xboot.common.vo.Result;
import cn.exrick.xboot.modules.bussiness.shiProductComparison.pojo.head_function;
import cn.exrick.xboot.modules.bussiness.shiProductComparison.service.Ihead_functionService;
import com.baomidou.mybatisplus.core.metadata.IPage;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * @author 石博士
 */
@Slf4j
@RestController
@Api(description = "头部功能管理接口")
@RequestMapping("/xboot/head_function")
@Transactional
public class head_functionController {

    @Autowired
    private Ihead_functionService ihead_functionService;

    @RequestMapping(value = "/get/{id}", method = RequestMethod.GET)
    @ApiOperation(value = "通过id获取")
    public Result<head_function> get(@PathVariable String id){

        head_function head_function = ihead_functionService.getById(id);
        return new ResultUtil<head_function>().setData(head_function);
    }

    @RequestMapping(value = "/getAll", method = RequestMethod.GET)
    @ApiOperation(value = "获取全部数据")
    public Result<List<head_function>> getAll(){

        List<head_function> list = ihead_functionService.list();
        return new ResultUtil<List<head_function>>().setData(list);
    }

    @RequestMapping(value = "/getByPage", method = RequestMethod.GET)
    @ApiOperation(value = "分页获取")
    public Result<IPage<head_function>> getByPage(@ModelAttribute PageVo page){

        IPage<head_function> data = ihead_functionService.page(PageUtil.initMpPage(page));
        return new ResultUtil<IPage<head_function>>().setData(data);
    }

    @RequestMapping(value = "/insertOrUpdate", method = RequestMethod.POST)
    @ApiOperation(value = "编辑或更新数据")
    public Result<head_function> saveOrUpdate(@ModelAttribute head_function head_function){

        if(ihead_functionService.saveOrUpdate(head_function)){
            return new ResultUtil<head_function>().setData(head_function);
        }
        return new ResultUtil<head_function>().setErrorMsg("操作失败");
    }

    @RequestMapping(value = "/delByIds/{ids}", method = RequestMethod.DELETE)
    @ApiOperation(value = "批量通过id删除")
    public Result<Object> delAllByIds(@PathVariable String[] ids){

        for(String id : ids){
            ihead_functionService.removeById(id);
        }
        return new ResultUtil<Object>().setSuccessMsg("批量通过id删除数据成功");
    }
}
