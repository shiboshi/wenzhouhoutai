package cn.exrick.xboot.modules.bussiness.afterIndentOrder.service;

import cn.exrick.xboot.common.vo.PageVo;
import cn.exrick.xboot.modules.bussiness.inventoryOrder.pojo.InventoryOrderResult;
import cn.exrick.xboot.modules.bussiness.product.VO.ProductCommon;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;
import cn.exrick.xboot.modules.bussiness.afterIndentOrder.pojo.AfterIndentOrder;

import java.util.List;
import java.util.Map;

/**
 * 订货单维修单接口
 * @author xiaofei
 */
public interface IafterIndentOrderService extends IService<AfterIndentOrder> {


    //1.插入订货单售后单中
    void insertOrder(String applicationAfterSaleOrderId, String inventoryOrderIds,String procDefId);

    //3.获取订货单列表
    Map<String,Object> getList(PageVo pageVo,String checkStatus,String numb);

    void insertAfterIndentSend(String id,String procDefId);

    List<ProductCommon> getDetail(Page initMpPage, String id);
}