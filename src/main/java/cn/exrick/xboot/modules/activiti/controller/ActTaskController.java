package cn.exrick.xboot.modules.activiti.controller;

import cn.exrick.xboot.common.constant.ActivitiConstant;
import cn.exrick.xboot.common.enums.CheckStatus;
import cn.exrick.xboot.common.enums.IsOpen;
import cn.exrick.xboot.common.utils.ResultUtil;
import cn.exrick.xboot.common.utils.SecurityUtil;
import cn.exrick.xboot.common.utils.SnowFlakeUtil;
import cn.exrick.xboot.common.utils.ToolUtil;
import cn.exrick.xboot.common.vo.PageVo;
import cn.exrick.xboot.common.vo.Result;
import cn.exrick.xboot.common.vo.SearchVo;
import cn.exrick.xboot.modules.activiti.entity.ActBusiness;
import cn.exrick.xboot.modules.activiti.entity.ActProcess;
import cn.exrick.xboot.modules.activiti.service.ActBusinessService;
import cn.exrick.xboot.modules.activiti.service.ActProcessService;
import cn.exrick.xboot.modules.activiti.service.mybatis.IHistoryIdentityService;
import cn.exrick.xboot.modules.activiti.utils.MessageUtil;
import cn.exrick.xboot.modules.activiti.vo.ActPage;
import cn.exrick.xboot.modules.activiti.vo.Assignee;
import cn.exrick.xboot.modules.activiti.vo.HistoricTaskVo;
import cn.exrick.xboot.modules.activiti.vo.TaskVo;
import cn.exrick.xboot.modules.base.dao.mapper.UserRoleMapper;
import cn.exrick.xboot.modules.base.entity.Department;
import cn.exrick.xboot.modules.base.entity.User;
import cn.exrick.xboot.modules.base.entity.UserRole;
import cn.exrick.xboot.modules.base.service.DepartmentService;
import cn.exrick.xboot.modules.base.service.UserService;
import cn.exrick.xboot.modules.bussiness.applicationAfterSaleOrder.service.IapplicationAfterSaleOrderService;
import cn.exrick.xboot.modules.bussiness.applicationReturnGoodsOrder.pojo.ApplicationReturnGoodsOrder;
import cn.exrick.xboot.modules.bussiness.applicationReturnGoodsOrder.service.IApplicationReturnGoodsOrderService;
import cn.exrick.xboot.modules.bussiness.applicationSendGoodsOrder.pojo.ApplicationSendGoodsOrder;
import cn.exrick.xboot.modules.bussiness.applicationSendGoodsOrder.service.IapplicationSendGoodsOrderService;
import cn.exrick.xboot.modules.bussiness.dealerLevel.pojo.DealerLevel;
import cn.exrick.xboot.modules.bussiness.dealerLevel.service.IDealerLevelService;
import cn.exrick.xboot.modules.bussiness.indentOrder.pojo.IndentOrder;
import cn.exrick.xboot.modules.bussiness.indentOrder.service.IindentOrderService;
import cn.exrick.xboot.modules.bussiness.roleAddress.mapper.AddressRoleMapper;
import cn.exrick.xboot.modules.bussiness.roleAddress.pojo.AddressRole;
import cn.hutool.core.date.DateUtil;
import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.google.common.collect.Lists;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import lombok.extern.slf4j.Slf4j;
import org.activiti.engine.*;
import org.activiti.engine.history.HistoricIdentityLink;
import org.activiti.engine.history.HistoricProcessInstance;
import org.activiti.engine.history.HistoricTaskInstance;
import org.activiti.engine.history.HistoricTaskInstanceQuery;
import org.activiti.engine.impl.interceptor.Command;
import org.activiti.engine.impl.interceptor.CommandContext;
import org.activiti.engine.impl.persistence.entity.ExecutionEntity;
import org.activiti.engine.impl.persistence.entity.ProcessDefinitionEntity;
import org.activiti.engine.impl.pvm.process.ActivityImpl;
import org.activiti.engine.impl.pvm.process.ProcessDefinitionImpl;
import org.activiti.engine.runtime.ProcessInstance;
import org.activiti.engine.task.Comment;
import org.activiti.engine.task.IdentityLink;
import org.activiti.engine.task.Task;
import org.activiti.engine.task.TaskQuery;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.Date;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.stream.Collectors;

/**
 * @author Exrick
 */
@Slf4j
@RestController
@Api(description = "任务管理接口")
@RequestMapping("/xboot/actTask")
@Transactional
public class ActTaskController {

    String count = null;

    @Autowired
    private TaskService taskService;

    @Autowired
    private HistoryService historyService;

    @Autowired
    private RuntimeService runtimeService;

    @Autowired
    private RepositoryService repositoryService;

    @Autowired
    private ManagementService managementService;

    @Autowired
    private ActProcessService actProcessService;

    @Autowired
    private ActBusinessService actBusinessService;

    @Autowired
    private UserService userService;

    @Autowired
    private IHistoryIdentityService iHistoryIdentityService;

    @Autowired
    private SecurityUtil securityUtil;

    @Autowired
    private MessageUtil messageUtil;
    @Autowired
    private IDealerLevelService dealerLevelService;
    @Autowired
    private AddressRoleMapper addressRoleMapper;
    @Autowired
    private DepartmentService departmentService;
    @Autowired
    private UserRoleMapper  userRoleMapper;
    @Autowired
    private IindentOrderService iindentOrderService;
    @Autowired
    private IapplicationSendGoodsOrderService applicationSendGoodsOrderService;
    @Autowired
    private IApplicationReturnGoodsOrderService applicationReturnGoodsOrderService;
    @Autowired
    private IapplicationAfterSaleOrderService applicationAfterSaleOrderService;

    @RequestMapping(value = "/todoList", method = RequestMethod.GET)
    @ApiOperation(value = "代办列表")
    public Result<Object> todoList(@RequestParam(required = false) String name,
                                   @RequestParam(required = false) String categoryId,
                                   @RequestParam(required = false) Integer priority,
                                   @ModelAttribute SearchVo searchVo,
                                   @ModelAttribute PageVo pageVo){
        ActPage<TaskVo> page = new ActPage<TaskVo>();
        List<TaskVo> list = new ArrayList<>();

        //根据用户id查询代办列表
        String userId = securityUtil.getCurrUser().getId();
        TaskQuery query = taskService.createTaskQuery().taskCandidateOrAssigned(userId);

        // 多条件搜索
        //条件搜索
        if("createTime".equals(pageVo.getSort())&&"asc".equals(pageVo.getOrder())){
            query.orderByTaskCreateTime().asc();
        }else if("priority".equals(pageVo.getSort())&&"asc".equals(pageVo.getOrder())){
            query.orderByTaskPriority().asc();
        }else if("priority".equals(pageVo.getSort())&&"desc".equals(pageVo.getOrder())){
            query.orderByTaskPriority().desc();
        }else{
            query.orderByTaskCreateTime().desc();
        }
        if(StrUtil.isNotBlank(name)){
            query.taskNameLike("%"+name+"%");
        }
        if(StrUtil.isNotBlank(categoryId)){
            query.taskCategory(categoryId);
        }
        if(priority!=null){
            query.taskPriority(priority);
        }
        if(StrUtil.isNotBlank(searchVo.getStartDate())&&StrUtil.isNotBlank(searchVo.getEndDate())){
            Date start = DateUtil.parse(searchVo.getStartDate());
            Date end = DateUtil.parse(searchVo.getEndDate());
            query.taskCreatedAfter(start);
            query.taskCreatedBefore(DateUtil.endOfDay(end));
        }

        page.setTotalElements(query.count());
        int first =  (pageVo.getPageNumber()-1) * pageVo.getPageSize();
        List<Task> taskList = query.listPage(first, pageVo.getPageSize());
        //条件搜索结束

        //对数据处理
        if (ToolUtil.isNotEmpty(taskList)){
        //判断模型表中是否开启了区域过滤和 权限过滤
        ActProcess actProcessUse = actProcessService.get(taskList.get(0).getProcessDefinitionId());
        //获取流程定义id,通过流程定义id获取 act_process表的设置信息
        if (ToolUtil.isNotEmpty(actProcessUse)) {
            List<Task> tasks = Lists.newArrayList();
            if (actProcessUse.getAreaStatus() == IsOpen.OPEN.getCode() && actProcessUse.getPermissionStauts() == IsOpen.NOTOPEN.getCode()) {
                tasks = taskList.stream().filter(task -> filterArea(task) == true).collect(Collectors.toList());
            }
            if (actProcessUse.getAreaStatus() == IsOpen.NOTOPEN.getCode() && actProcessUse.getPermissionStauts() == IsOpen.OPEN.getCode()) {
                tasks = taskList.stream().filter(task -> filterPermission(task) == true).collect(Collectors.toList());
            }
            if (actProcessUse.getAreaStatus() == IsOpen.OPEN.getCode() && actProcessUse.getPermissionStauts() == IsOpen.OPEN.getCode()) {
                tasks = taskList.stream().filter(task -> filterArea(task) == true)
                        .filter(task -> filterPermission(task) == true)
                        .collect(Collectors.toList());
            }
            if (actProcessUse.getAreaStatus() == IsOpen.NOTOPEN.getCode() && actProcessUse.getPermissionStauts() == IsOpen.NOTOPEN.getCode()) {
                tasks = taskList;
            }
            // 转换vo
            tasks.forEach(e -> {
                TaskVo tv = new TaskVo(e);

                // 关联委托人
                if (StrUtil.isNotBlank(tv.getOwner())) {
                    tv.setOwner(userService.get(tv.getOwner()).getUsername());
                }
                List<IdentityLink> identityLinks = runtimeService.getIdentityLinksForProcessInstance(tv.getProcInstId());
                for (IdentityLink ik : identityLinks) {
                    //关联发起人
                    if ("starter".equals(ik.getType()) && StrUtil.isNotBlank(ik.getUserId())) {
                        tv.setApplyer(userService.get(ik.getUserId()).getUsername());
                    }
                }
                // 关联流程信息
                ActProcess actProcess = actProcessService.get(tv.getProcDefId());
                if (actProcess != null) {
                    tv.setProcessName(actProcess.getName());
                    tv.setRouteName(actProcess.getRouteName());
                }
                // 关联业务key
                ProcessInstance pi = runtimeService.createProcessInstanceQuery().processInstanceId(tv.getProcInstId()).singleResult();
                tv.setBusinessKey(pi.getBusinessKey());
                ActBusiness actBusiness = actBusinessService.get(pi.getBusinessKey());
                if (actBusiness != null) {
                    tv.setTableId(actBusiness.getTableId());
                }
                list.add(tv);
            });
            page.setContent(list);
            return new ResultUtil<Object>().setData(page);
        }
        }
        //数据处理结束
        return null;
    }

    /**
     * 过滤区域
     * @param task
     */
    public Boolean filterArea(Task task) {

        log.info("檔期那名字："+task.getName());
        if ("服务专员".equals(task.getName())) {
            String proInstanceId = task.getProcessInstanceId();
            if (ToolUtil.isNotEmpty(proInstanceId)) {
                List<IdentityLink> identityLinks = runtimeService.getIdentityLinksForProcessInstance(proInstanceId);
                for (IdentityLink tk : identityLinks) {
                    if ("starter".equals(tk.getType()) && ToolUtil.isNotEmpty(tk.getUserId())) {
                        User user = userService.get(tk.getUserId());
                        if (ToolUtil.isNotEmpty(user)) {
                            DealerLevel dealerLevel = dealerLevelService.getOne(Wrappers.<DealerLevel>lambdaQuery()
                                    .eq(DealerLevel::getDeptId, user.getDepartmentId()));
                            if (ToolUtil.isNotEmpty(dealerLevel)) {
                                //获取当前登录角色的区域信息
                                //获取当前登录者的角色
                                String userId = securityUtil.getCurrUser().getId();
                                UserRole userRole = userRoleMapper.selectOne(Wrappers.<UserRole>lambdaQuery()
                                        .eq(UserRole::getUserId, userId));
                                if (ToolUtil.isNotEmpty(userRole)) {
                                    List<AddressRole> addressRoles = addressRoleMapper.selectList(Wrappers.<AddressRole>lambdaQuery()
                                            .eq(AddressRole::getRoleId, userRole.getRoleId()));
                                    List<String> ids = addressRoles.stream().map(role -> role.getAddressId()).collect(Collectors.toList());
                                    if (ids.contains(dealerLevel.getAreaId())) {
                                        return true;
                                    }
                                }
                            }
                        }
                    }
                }
            }
            return false;
        }
        return  true;
    }
    /**
     * 过滤权限
     * @param task
     * @return
     */
    public  Boolean filterPermission(Task task){

       String proceInstance = task.getProcessInstanceId();
       if (ToolUtil.isNotEmpty(proceInstance)){
           log.info("当前任务节点的名字是："+task.getName());
           List<IdentityLink> identityLinks = runtimeService.getIdentityLinksForProcessInstance(proceInstance);
           for (IdentityLink identityLink : identityLinks) {
               if ("starter".equals(identityLink.getType())&&ToolUtil.isNotEmpty(identityLink.getUserId())){
                   //当前流程总共有三个节点，如果是第二个节点，则流程发起人的部门的上一级部门=当前登录的部门
                   if ("店长审批".equals(task.getName())){
                       //流程发起人的部门,如果当前登录人的角色有店长返回true
                       User user = userService.get(identityLink.getUserId());
                       if (ToolUtil.isNotEmpty(user)){
                           if (user.getDepartmentId().equals(securityUtil.getCurrUser().getDepartmentId())){
                               return true;
                           }
                       }
                   }else if ("经销商".equals(task.getName()) || "经销商仓库发货人员".equals(task.getName())){
                       User userDealer = userService.get(identityLink.getUserId());
                       if (ToolUtil.isNotEmpty(userDealer)) {
                           Department departmentAssi = departmentService.get(userDealer.getDepartmentId());
                           //当前审批人的部门
                           Department departmentNow = departmentService.get(securityUtil.getCurrUser().getDepartmentId());
                           if (!ToolUtil.isOneEmpty(departmentAssi, departmentNow)) {
                               if (departmentAssi.getParentId().equals(departmentNow.getId())) {
                                   return true;
                               }
                           }
                       }
                   }
               }
           }

       }
       return false;
    }

    @RequestMapping(value = "/doneList", method = RequestMethod.GET)
    @ApiOperation(value = "已办列表")
    public Result<Object> doneList(@RequestParam(required = false) String name,
                                   @RequestParam(required = false) String categoryId,
                                   @RequestParam(required = false) Integer priority,
                                   @ModelAttribute SearchVo searchVo,
                                   @ModelAttribute PageVo pageVo){

        ActPage<HistoricTaskVo> page = new ActPage<HistoricTaskVo>();
        List<HistoricTaskVo> list = new ArrayList<>();

        String userId = securityUtil.getCurrUser().getId();
        HistoricTaskInstanceQuery query = historyService.createHistoricTaskInstanceQuery().or().taskCandidateUser(userId).
                taskAssignee(userId).endOr().finished();

        // 多条件搜索
        if("createTime".equals(pageVo.getSort())&&"asc".equals(pageVo.getOrder())){
            query.orderByHistoricTaskInstanceEndTime().asc();
        }else if("priority".equals(pageVo.getSort())&&"asc".equals(pageVo.getOrder())){
            query.orderByTaskPriority().asc();
        }else if("priority".equals(pageVo.getSort())&&"desc".equals(pageVo.getOrder())){
            query.orderByTaskPriority().desc();
        }else if("duration".equals(pageVo.getSort())&&"asc".equals(pageVo.getOrder())){
            query.orderByHistoricTaskInstanceDuration().asc();
        }else if("duration".equals(pageVo.getSort())&&"desc".equals(pageVo.getOrder())){
            query.orderByHistoricTaskInstanceDuration().desc();
        }else{
            query.orderByHistoricTaskInstanceEndTime().desc();
        }
        if(StrUtil.isNotBlank(name)){
            query.taskNameLike("%"+name+"%");
        }
        if(StrUtil.isNotBlank(categoryId)){
            query.taskCategory(categoryId);
        }
        if(priority!=null){
            query.taskPriority(priority);
        }
        if(StrUtil.isNotBlank(searchVo.getStartDate())&&StrUtil.isNotBlank(searchVo.getEndDate())){
            Date start = DateUtil.parse(searchVo.getStartDate());
            Date end = DateUtil.parse(searchVo.getEndDate());
            query.taskCompletedAfter(start);
            query.taskCompletedBefore(DateUtil.endOfDay(end));
        }

        page.setTotalElements((long) query.list().size());
        int first =  (pageVo.getPageNumber()-1) * pageVo.getPageSize();
        List<HistoricTaskInstance> taskList = query.listPage(first, pageVo.getPageSize());
        // 转换vo
        taskList.forEach(e -> {
            HistoricTaskVo htv = new HistoricTaskVo(e);
            // 关联委托人
            if(StrUtil.isNotBlank(htv.getOwner())){
                htv.setOwner(userService.get(htv.getOwner()).getUsername());
            }
            List<HistoricIdentityLink> identityLinks = historyService.getHistoricIdentityLinksForProcessInstance(htv.getProcInstId());
            for(HistoricIdentityLink hik : identityLinks){
                // 关联发起人
                if("starter".equals(hik.getType())&&StrUtil.isNotBlank(hik.getUserId())){
                    htv.setApplyer(userService.get(hik.getUserId()).getUsername());
                }
            }
            // 关联审批意见
            List<Comment> comments = taskService.getTaskComments(htv.getId(), "comment");
            if(comments!=null&&comments.size()>0){
                htv.setComment(comments.get(0).getFullMessage());
            }
            // 关联流程信息
            ActProcess actProcess = actProcessService.get(htv.getProcDefId());
            if(actProcess!=null){
                htv.setProcessName(actProcess.getName());
                htv.setRouteName(actProcess.getRouteName());
            }
            // 关联业务key
            HistoricProcessInstance hpi = historyService.createHistoricProcessInstanceQuery().processInstanceId(htv.getProcInstId()).singleResult();
            htv.setBusinessKey(hpi.getBusinessKey());
            ActBusiness actBusiness = actBusinessService.get(hpi.getBusinessKey());
            if(actBusiness!=null){
                htv.setTableId(actBusiness.getTableId());
            }

            list.add(htv);
        });
        page.setContent(list);
        return new ResultUtil<Object>().setData(page);
    }

    @RequestMapping(value = "/historicFlow/{id}", method = RequestMethod.GET)
    @ApiOperation(value = "流程流转历史")
    public Result<Object> historicFlow(@ApiParam("流程实例id") @PathVariable String id){

        List<HistoricTaskVo> list = new ArrayList<>();

        List<HistoricTaskInstance> taskList = historyService.createHistoricTaskInstanceQuery()
                .processInstanceId(id).orderByHistoricTaskInstanceEndTime().asc().list();

        // 转换vo
        taskList.forEach(e -> {
            HistoricTaskVo htv = new HistoricTaskVo(e);
            List<Assignee> assignees = new ArrayList<>();
            // 关联分配人（委托用户时显示该人）
            if(StrUtil.isNotBlank(htv.getAssignee())){
                String username = userService.get(htv.getAssignee()).getUsername();
                htv.setAssignee(username);
                assignees.add(new Assignee(username, true));
            }
            List<HistoricIdentityLink> identityLinks = historyService.getHistoricIdentityLinksForTask(e.getId());
            StringBuilder candidateBuilder = new StringBuilder();
            // 获取实际审批用户id
            String userId = iHistoryIdentityService.findUserIdByTypeAndTaskId(ActivitiConstant.EXECUTOR_TYPE, e.getId());
            for(HistoricIdentityLink hik : identityLinks){
                // 关联候选用户（分配的候选用户审批人）
                if("candidate".equals(hik.getType())&&StrUtil.isNotBlank(hik.getUserId())){
                    String username = userService.get(hik.getUserId()).getUsername();
                    Assignee assignee = new Assignee(username, false);
                    if(StrUtil.isNotBlank(userId)&&userId.equals(hik.getUserId())){
                        assignee.setIsExecutor(true);
                        username+="(实际审批)";
                    }
                    if(StrUtil.isBlank(candidateBuilder.toString())){
                        candidateBuilder.append(username);
                    }else{
                        candidateBuilder.append("、" + username);
                    }
                    assignees.add(assignee);
                }
            }
            if(StrUtil.isBlank(htv.getAssignee())&&StrUtil.isNotBlank(candidateBuilder.toString())){
                htv.setAssignee(candidateBuilder.toString());
                htv.setAssignees(assignees);
            }
            // 关联审批意见
            List<Comment> comments = taskService.getTaskComments(htv.getId(), "comment");
            if(comments!=null&&comments.size()>0){
                htv.setComment(comments.get(0).getFullMessage());
            }
            list.add(htv);
        });
        return new ResultUtil<Object>().setData(list);
    }

    @RequestMapping(value = "/pass", method = RequestMethod.POST)
    @ApiOperation(value = "任务节点审批通过")
    public Result<Object> pass(@ApiParam("任务id") @RequestParam String id,
                               @ApiParam("流程实例id") @RequestParam String procInstId,
                               @ApiParam("下个节点审批人") @RequestParam(required = false) String[] assignees,
                               @ApiParam("优先级") @RequestParam(required = false) Integer priority,
                               @ApiParam("意见评论") @RequestParam(required = false) String comment,
                               @ApiParam("是否发送站内消息") @RequestParam(defaultValue = "false") Boolean sendMessage,
                               @ApiParam("是否发送短信通知") @RequestParam(defaultValue = "false") Boolean sendSms,
                               @ApiParam("是否发送邮件通知") @RequestParam(defaultValue = "false") Boolean sendEmail){

        if(StrUtil.isBlank(comment)){
            comment = "";
        }
        taskService.addComment(id, procInstId, comment);
        ProcessInstance pi = runtimeService.createProcessInstanceQuery().processInstanceId(procInstId).singleResult();
        Task task = taskService.createTaskQuery().taskId(id).singleResult();
        if(StrUtil.isNotBlank(task.getOwner())&&!("RESOLVED").equals(task.getDelegationState().toString())){
            // 未解决的委托任务 先resolve
            taskService.resolveTask(id);
        }
        taskService.complete(id);
        List<Task> tasks = taskService.createTaskQuery().processInstanceId(procInstId).list();
        // 判断下一个节点
        if(tasks!=null&&tasks.size()>0){
            for(Task t : tasks){
                if(assignees==null||assignees.length<1){
                    // 如果分配审批人为空 设置默认审批用户
                    List<User> users = actProcessService.getNode(t.getTaskDefinitionKey()).getUsers();
                    for(User user : users){
                        //根据设置的角色获取审批人
                        taskService.addCandidateUser(t.getId(), user.getId());
                        // 异步发消息
                        messageUtil.sendActMessage(user.getId(), ActivitiConstant.MESSAGE_TODO_CONTENT, sendMessage, sendSms, sendEmail);
                    }
                }
                for(String assignee : assignees){
                    taskService.addCandidateUser(t.getId(), assignee);
                    // 异步发消息
                    messageUtil.sendActMessage(assignee, ActivitiConstant.MESSAGE_TODO_CONTENT, sendMessage, sendSms, sendEmail);
                }
                if(priority!=null){
                    taskService.setPriority(t.getId(), priority);
                }
            }
        } else {
            ActBusiness actBusiness = actBusinessService.get(pi.getBusinessKey());
            actBusiness.setStatus(ActivitiConstant.STATUS_FINISH);
            actBusiness.setResult(ActivitiConstant.RESULT_PASS);
            actBusinessService.update(actBusiness);
            // 异步发消息
            messageUtil.sendActMessage(actBusiness.getUserId(), ActivitiConstant.MESSAGE_PASS_CONTENT, sendMessage, sendSms, sendEmail);
        }
        // 记录实际审批人员
        iHistoryIdentityService.insert(String.valueOf(SnowFlakeUtil.getFlowIdInstance().nextId()),
                ActivitiConstant.EXECUTOR_TYPE, securityUtil.getCurrUser().getId(), id, procInstId);
        return new ResultUtil<Object>().setSuccessMsg("操作成功");
    }

    @RequestMapping(value = "/passAll/{ids}", method = RequestMethod.POST)
    @ApiOperation(value = "批量通过")
    public Result<Object> passAll(@ApiParam("任务ids") @PathVariable String[] ids,
                                  @ApiParam("是否发送站内消息") @RequestParam(defaultValue = "false") Boolean sendMessage,
                                  @ApiParam("是否发送短信通知") @RequestParam(defaultValue = "false") Boolean sendSms,
                                  @ApiParam("是否发送邮件通知") @RequestParam(defaultValue = "false") Boolean sendEmail){

        for(String id:ids){
            Task task = taskService.createTaskQuery().taskId(id).singleResult();
            ProcessInstance pi = runtimeService.createProcessInstanceQuery().processInstanceId(task.getProcessInstanceId()).singleResult();
            if(StrUtil.isNotBlank(task.getOwner())&&!("RESOLVED").equals(task.getDelegationState().toString())){
                // 未解决的委托任务 先resolve
                taskService.resolveTask(id);
            }
            taskService.complete(id);
            List<Task> tasks = taskService.createTaskQuery().processInstanceId(task.getProcessInstanceId()).list();
            // 判断下一个节点
            if(tasks!=null&&tasks.size()>0){
                for(Task t : tasks){
                    List<User> users = actProcessService.getNode(t.getTaskDefinitionKey()).getUsers();
                    for(User user : users){
                        taskService.addCandidateUser(t.getId(), user.getId());
                        // 异步发消息
                        messageUtil.sendActMessage(user.getId(), ActivitiConstant.MESSAGE_TODO_CONTENT, sendMessage, sendSms, sendEmail);
                    }
                    taskService.setPriority(t.getId(), task.getPriority());
                }
            } else {
                ActBusiness actBusiness = actBusinessService.get(pi.getBusinessKey());
                actBusiness.setStatus(ActivitiConstant.STATUS_FINISH);
                actBusiness.setResult(ActivitiConstant.RESULT_PASS);
                actBusinessService.update(actBusiness);
                // 异步发消息
                messageUtil.sendActMessage(actBusiness.getUserId(), ActivitiConstant.MESSAGE_PASS_CONTENT, sendMessage, sendSms, sendEmail);
            }
            // 记录实际审批人员
            iHistoryIdentityService.insert(String.valueOf(SnowFlakeUtil.getFlowIdInstance().nextId()),
                    ActivitiConstant.EXECUTOR_TYPE, securityUtil.getCurrUser().getId(), id, pi.getId());
        }
        return new ResultUtil<Object>().setSuccessMsg("操作成功");
    }

    @RequestMapping(value = "/getBackList/{procInstId}", method = RequestMethod.GET)
    @ApiOperation(value = "获取可返回的节点")
    public Result<Object> getBackList(@PathVariable String procInstId){

        List<HistoricTaskVo> list = new ArrayList<>();
        List<HistoricTaskInstance> taskInstanceList = historyService.createHistoricTaskInstanceQuery().processInstanceId(procInstId)
                .finished().list();

        taskInstanceList.forEach(e -> {
            HistoricTaskVo htv = new HistoricTaskVo(e);
            list.add(htv);
        });

        // 去重
        LinkedHashSet<String> set = new LinkedHashSet<String>(list.size());
        List<HistoricTaskVo> newList = new ArrayList<>();
        list.forEach(e->{
            if(set.add(e.getName())){
                newList.add(e);
            }
        });

        return new ResultUtil<Object>().setData(newList);
    }

    @RequestMapping(value = "/backToTask", method = RequestMethod.POST)
    @ApiOperation(value = "任务节点审批驳回至指定历史节点")
    public Result<Object> backToTask(@ApiParam("任务id") @RequestParam String id,
                                     @ApiParam("驳回指定节点key") @RequestParam String backTaskKey,
                                     @ApiParam("流程实例id") @RequestParam String procInstId,
                                     @ApiParam("流程定义id") @RequestParam String procDefId,
                                     @ApiParam("原节点审批人") @RequestParam(required = false) String[] assignees,
                                     @ApiParam("优先级") @RequestParam(required = false) Integer priority,
                                     @ApiParam("意见评论") @RequestParam(required = false) String comment,
                                     @ApiParam("是否发送站内消息") @RequestParam(defaultValue = "false") Boolean sendMessage,
                                     @ApiParam("是否发送短信通知") @RequestParam(defaultValue = "false") Boolean sendSms,
                                     @ApiParam("是否发送邮件通知") @RequestParam(defaultValue = "false") Boolean sendEmail){


        if(StrUtil.isBlank(comment)){
            comment = "";
        }
        taskService.addComment(id, procInstId, comment);
        // 取得流程定义
        ProcessDefinitionEntity definition = (ProcessDefinitionEntity) repositoryService.getProcessDefinition(procDefId);
        // 获取历史任务的Activity
        ActivityImpl hisActivity = definition.findActivity(backTaskKey);
        // 实现跳转
        managementService.executeCommand(new JumpTask(procInstId, hisActivity.getId()));
        // 重新分配原节点审批人
        List<Task> tasks = taskService.createTaskQuery().processInstanceId(procInstId).list();
        if(tasks!=null&&tasks.size()>0){
            tasks.forEach(e->{
                for(String assignee:assignees){
                    taskService.addCandidateUser(e.getId(), assignee);
                    // 异步发消息
                    messageUtil.sendActMessage(assignee, ActivitiConstant.MESSAGE_TODO_CONTENT, sendMessage, sendSms, sendEmail);
                }
                if(priority!=null){
                    taskService.setPriority(e.getId(), priority);
                }
            });
        }
        // 记录实际审批人员
        iHistoryIdentityService.insert(String.valueOf(SnowFlakeUtil.getFlowIdInstance().nextId()),
                ActivitiConstant.EXECUTOR_TYPE, securityUtil.getCurrUser().getId(), id, procInstId);
        return new ResultUtil<Object>().setSuccessMsg("操作成功");
    }

    public class JumpTask implements Command<ExecutionEntity> {

        private String procInstId;
        private String activityId;

        public JumpTask(String procInstId, String activityId) {
            this.procInstId = procInstId;
            this.activityId = activityId;
        }

        @Override
        public ExecutionEntity execute(CommandContext commandContext) {

            ExecutionEntity executionEntity = commandContext.getExecutionEntityManager().findExecutionById(procInstId);
            executionEntity.destroyScope("backed");
            ProcessDefinitionImpl processDefinition = executionEntity.getProcessDefinition();
            ActivityImpl activity = processDefinition.findActivity(activityId);
            executionEntity.executeActivity(activity);

            return executionEntity;
        }
    }

    @RequestMapping(value = "/back", method = RequestMethod.POST)
    @ApiOperation(value = "任务节点审批驳回至发起人")
    public Result<Object> back(@ApiParam("任务id") @RequestParam String id,
                               @ApiParam("流程实例id") @RequestParam String procInstId,
                               @ApiParam("意见评论") @RequestParam(required = false) String comment,
                               @ApiParam("是否发送站内消息") @RequestParam(defaultValue = "false") Boolean sendMessage,
                               @ApiParam("是否发送短信通知") @RequestParam(defaultValue = "false") Boolean sendSms,
                               @ApiParam("是否发送邮件通知") @RequestParam(defaultValue = "false") Boolean sendEmail){


        if(StrUtil.isBlank(comment)){
            comment = "";
        }
        taskService.addComment(id, procInstId, comment);
        ProcessInstance pi = runtimeService.createProcessInstanceQuery().processInstanceId(procInstId).singleResult();
        // 删除流程实例
        runtimeService.deleteProcessInstance(procInstId, "backed");
        ActBusiness actBusiness = actBusinessService.get(pi.getBusinessKey());
        actBusiness.setStatus(ActivitiConstant.STATUS_FINISH);
        actBusiness.setResult(ActivitiConstant.RESULT_FAIL);
        actBusinessService.update(actBusiness);

        /*--------------各订单的审核失败实现层--------------------------------------*/
        if (actBusiness.getProcDefId().contains("indentOrder")){
            IndentOrder indentOrder = iindentOrderService.getById(actBusiness.getTableId());
            if (ToolUtil.isNotEmpty(indentOrder)){
                indentOrder.setCheckStatus(CheckStatus.fail);
                iindentOrderService.updateById(indentOrder);
            }
        }
        if (actBusiness.getProcDefId().contains("aplicationSendOrder")){
            ApplicationSendGoodsOrder applicationSendGoodsOrder = applicationSendGoodsOrderService.getById(actBusiness.getTableId());
            if (ToolUtil.isNotEmpty(applicationSendGoodsOrder)){
                applicationSendGoodsOrder.setCheckStatus(CheckStatus.fail);
                applicationSendGoodsOrderService.updateById(applicationSendGoodsOrder);
            }
        }
        if (actBusiness.getProcDefId().contains("applicationReturn")){
            ApplicationReturnGoodsOrder applicationReturnGoodsOrder = applicationReturnGoodsOrderService.getById(actBusiness.getTableId());
            if (ToolUtil.isNotEmpty(applicationReturnGoodsOrder)){
                applicationReturnGoodsOrder.setCheckStatus(CheckStatus.fail);
                applicationReturnGoodsOrderService.updateById(applicationReturnGoodsOrder);
            }
        }

        // 异步发消息
        messageUtil.sendActMessage(actBusiness.getUserId(), ActivitiConstant.MESSAGE_BACK_CONTENT, sendMessage, sendSms, sendEmail);
        // 记录实际审批人员
        iHistoryIdentityService.insert(String.valueOf(SnowFlakeUtil.getFlowIdInstance().nextId()),
                ActivitiConstant.EXECUTOR_TYPE, securityUtil.getCurrUser().getId(), id, procInstId);
        return new ResultUtil<Object>().setSuccessMsg("操作成功");
    }

    @RequestMapping(value = "/backAll/{procInstIds}", method = RequestMethod.POST)
    @ApiOperation(value = "批量驳回至发起人")
    public Result<Object> backAll(@ApiParam("流程实例ids") @PathVariable String[] procInstIds,
                                  @ApiParam("是否发送站内消息") @RequestParam(defaultValue = "false") Boolean sendMessage,
                                  @ApiParam("是否发送短信通知") @RequestParam(defaultValue = "false") Boolean sendSms,
                                  @ApiParam("是否发送邮件通知") @RequestParam(defaultValue = "false") Boolean sendEmail){

        for(String procInstId:procInstIds){
            // 记录实际审批人员
            List<Task> tasks = taskService.createTaskQuery().processInstanceId(procInstId).list();
            tasks.forEach(t->{
                iHistoryIdentityService.insert(String.valueOf(SnowFlakeUtil.getFlowIdInstance().nextId()),
                        ActivitiConstant.EXECUTOR_TYPE, securityUtil.getCurrUser().getId(), t.getId(), procInstId);
            });
            ProcessInstance pi = runtimeService.createProcessInstanceQuery().processInstanceId(procInstId).singleResult();
            // 删除流程实例
            runtimeService.deleteProcessInstance(procInstId, ActivitiConstant.BACKED_FLAG);
            ActBusiness actBusiness = actBusinessService.get(pi.getBusinessKey());
            actBusiness.setStatus(ActivitiConstant.STATUS_FINISH);
            actBusiness.setResult(ActivitiConstant.RESULT_FAIL);
            actBusinessService.update(actBusiness);
            // 异步发消息
            messageUtil.sendActMessage(actBusiness.getUserId(), ActivitiConstant.MESSAGE_BACK_CONTENT, sendMessage, sendSms, sendEmail);
        }
        return new ResultUtil<Object>().setSuccessMsg("操作成功");
    }

    @RequestMapping(value = "/delegate", method = RequestMethod.POST)
    @ApiOperation(value = "委托他人代办")
    public Result<Object> delegate(@ApiParam("任务id") @RequestParam String id,
                                   @ApiParam("委托用户id") @RequestParam String userId,
                                   @ApiParam("流程实例id") @RequestParam String procInstId,
                                   @ApiParam("意见评论") @RequestParam(required = false) String comment,
                                   @ApiParam("是否发送站内消息") @RequestParam(defaultValue = "false") Boolean sendMessage,
                                   @ApiParam("是否发送短信通知") @RequestParam(defaultValue = "false") Boolean sendSms,
                                   @ApiParam("是否发送邮件通知") @RequestParam(defaultValue = "false") Boolean sendEmail){

        if(StrUtil.isBlank(comment)){
            comment = "";
        }
        taskService.addComment(id, procInstId, comment);
        taskService.delegateTask(id, userId);
        taskService.setOwner(id, securityUtil.getCurrUser().getId());
        // 异步发消息
        messageUtil.sendActMessage(userId, ActivitiConstant.MESSAGE_DELEGATE_CONTENT, sendMessage, sendSms, sendEmail);
        return new ResultUtil<Object>().setSuccessMsg("操作成功");
    }

    @RequestMapping(value = "/delete/{ids}", method = RequestMethod.DELETE)
    @ApiOperation(value = "删除任务")
    public Result<Object> delete(@ApiParam("任务id") @PathVariable String[] ids,
                                 @ApiParam("原因") @RequestParam(required = false) String reason){

        if(StrUtil.isBlank(reason)){
            reason = "";
        }
        for(String id : ids){
            taskService.deleteTask(id, reason);
        }
        return new ResultUtil<Object>().setSuccessMsg("操作成功");
    }

    @RequestMapping(value = "/deleteHistoric/{ids}", method = RequestMethod.DELETE)
    @ApiOperation(value = "删除任务历史")
    public Result<Object> deleteHistoric(@ApiParam("任务id") @PathVariable String[] ids){

        for(String id : ids){
            historyService.deleteHistoricTaskInstance(id);
        }
        return new ResultUtil<Object>().setSuccessMsg("操作成功");
    }
}
