package cn.exrick.xboot.modules.bussiness.tApplication.listener;

import cn.exrick.xboot.common.utils.ToolUtil;
import cn.exrick.xboot.config.springContext.SpringContextHolder;
import cn.exrick.xboot.modules.activiti.service.ActBusinessService;
import cn.exrick.xboot.modules.activiti.service.ActProcessService;
import cn.exrick.xboot.modules.base.entity.Department;
import cn.exrick.xboot.modules.base.entity.User;
import cn.exrick.xboot.modules.base.service.IDepartmentService;
import cn.exrick.xboot.modules.base.service.IUserService;
import cn.exrick.xboot.modules.bussiness.dealerLevel.pojo.DealerLevel;
import cn.exrick.xboot.modules.bussiness.dealerLevel.service.IDealerLevelService;
import cn.exrick.xboot.modules.bussiness.tApplication.pojo.TApplication;
import cn.exrick.xboot.modules.bussiness.tApplication.service.ITApplicationService;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import org.activiti.engine.delegate.DelegateExecution;
import org.activiti.engine.delegate.ExecutionListener;

import java.math.BigDecimal;

public class tApplicationlistener implements ExecutionListener {


    @Override
    public void notify(DelegateExecution delegateExecution) throws Exception {

        String tableId = (String) delegateExecution.getVariable("tableId");
        ITApplicationService iTApplicationService = SpringContextHolder.getBean(ITApplicationService.class);
        //经销商service
        IDealerLevelService iDealerLevelService = SpringContextHolder.getBean(IDealerLevelService.class);
        //用户service
        IUserService iUserService = SpringContextHolder.getBean(IUserService.class);
        //部门service
        IDepartmentService iDepartmentService = SpringContextHolder.getBean(IDepartmentService.class);
        ActProcessService actProcessService = SpringContextHolder.getBean(ActProcessService.class);
        ActBusinessService actBusinessService = SpringContextHolder.getBean(ActBusinessService.class);
        TApplication tApplication = iTApplicationService.getById(tableId);
        if(delegateExecution.getEventName().equals("end")){
            if(ToolUtil.isNotEmpty(tApplication)){
                tApplication.setCompletionStatus("1");
                tApplication.setTStatus("1");
                iTApplicationService.updateById(tApplication);

                //通过退款申请人的id,查询出用户表的部门id
                User user = iUserService.getById(tApplication.getUserId());

                //通过用户表的部门id，查出部门
                Department department = iDepartmentService.getById(user.getDepartmentId());

                QueryWrapper<DealerLevel> queryWrapper = new QueryWrapper<>();
                queryWrapper.eq("dept_id", department.getId());

                //根据部门表的id查询经销商
                DealerLevel d = iDealerLevelService.getOne(queryWrapper);

                //退款申请的金额
                BigDecimal number = new BigDecimal(tApplication.getRemoney());

                //经销商原有的金额
                BigDecimal balance = new BigDecimal(d.getBalance().toString());

                //计算总金额
                BigDecimal balances = number.add(balance);
                d.setBalance(balances);
                iDealerLevelService.updateById(d);

                //把退款申请的金额减掉
//                BigDecimal numbers = new BigDecimal(tApplication.getRemoney());
//                BigDecimal balances2 = number.subtract(numbers);
//                tApplication.setRemoney(balances2.toString());
//
//                //再次更新自己相减的值
//                iTApplicationService.updateById(tApplication);


            }

        }
    }
}
