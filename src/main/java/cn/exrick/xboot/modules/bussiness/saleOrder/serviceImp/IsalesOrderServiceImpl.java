package cn.exrick.xboot.modules.bussiness.saleOrder.serviceImp;

import cn.exrick.xboot.common.constant.ActivitiConstant;
import cn.exrick.xboot.common.enums.*;
import cn.exrick.xboot.common.utils.*;
import cn.exrick.xboot.common.vo.CommonVO;
import cn.exrick.xboot.common.vo.PageVo;
import cn.exrick.xboot.modules.activiti.entity.ActBusiness;
import cn.exrick.xboot.modules.activiti.service.ActBusinessService;
import cn.exrick.xboot.modules.activiti.service.ActProcessService;
import cn.exrick.xboot.modules.activiti.service.mybatis.IHistoryIdentityService;
import cn.exrick.xboot.modules.activiti.utils.MessageUtil;
import cn.exrick.xboot.modules.base.dao.mapper.UserMapper;
import cn.exrick.xboot.modules.base.entity.User;
import cn.exrick.xboot.modules.base.service.UserService;
import cn.exrick.xboot.modules.bussiness.applicationSendGoodsOrder.mapper.ApplicationSendGoodsOrderMapper;
import cn.exrick.xboot.modules.bussiness.applicationSendGoodsOrder.pojo.ApplicationSendGoodsOrder;
import cn.exrick.xboot.modules.bussiness.inventoryOrder.pojo.InventoryOrder;
import cn.exrick.xboot.modules.bussiness.inventoryOrder.service.IInventoryOrderService;
import cn.exrick.xboot.modules.bussiness.product.VO.ProductCommon;
import cn.exrick.xboot.modules.bussiness.product.pojo.ProductOrder;
import cn.exrick.xboot.modules.bussiness.product.service.IproductOrderService;
import cn.exrick.xboot.modules.bussiness.purchaseOrder.VO.PurchaseOrderVO;
import cn.exrick.xboot.modules.bussiness.purchaseOrder.pojo.PurchaseOrder;
import cn.exrick.xboot.modules.bussiness.purchaseOrder.service.IpurchaseOrderService;
import cn.exrick.xboot.modules.bussiness.saleOrder.VO.NeedCheckProcudct;
import cn.exrick.xboot.modules.bussiness.saleOrder.VO.SalesOrderVO;
import cn.exrick.xboot.modules.bussiness.saleOrder.mapper.salesOrderMapper;
import cn.exrick.xboot.modules.bussiness.saleOrder.pojo.SalesOrder;
import cn.exrick.xboot.modules.bussiness.saleOrder.service.IsalesOrderService;
import cn.exrick.xboot.modules.bussiness.sendGoodsOrder.mapper.sendGoodsOrderMapper;
import cn.exrick.xboot.modules.bussiness.sendGoodsOrder.pojo.SendGoodsOrder;
import cn.exrick.xboot.modules.bussiness.shiWarehouse.service.IShiWarehouseService;
import cn.exrick.xboot.modules.bussiness.specialPriceSalesOrder.service.IspecialPriceSalesOrderService;
import cn.exrick.xboot.modules.wx.VO.EditNodeIdUserVO;
import cn.exrick.xboot.modules.wx.Wxservice;
import com.alibaba.fastjson.JSON;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.google.common.base.Splitter;
import lombok.extern.slf4j.Slf4j;
import org.activiti.engine.TaskService;
import org.activiti.engine.task.Task;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Nullable;
import java.math.BigDecimal;
import java.util.*;
import java.util.stream.Collectors;

/**
 * 销售订单接口实现
 *
 * @author xiaofei
 */
@Slf4j
@Service
@Transactional
public class IsalesOrderServiceImpl extends ServiceImpl<salesOrderMapper, SalesOrder> implements IsalesOrderService {

    @Autowired
    private salesOrderMapper salesOrderMapper;
    @Autowired
    private SecurityUtil securityUtil;
    @Autowired
    private IpurchaseOrderService purchaseOrderService;
    @Autowired
    private ActBusinessService actBusinessService;
    @Autowired
    private IproductOrderService productOrderService;
    @Autowired
    private UserMapper userMapper;
    @Autowired
    private IspecialPriceSalesOrderService specialPriceSalesOrderService;
    @Autowired
    private ApplicationSendGoodsOrderMapper applicationSendGoodsOrderMapper;
    @Autowired
    private Wxservice wxservice;
    @Autowired
    private IShiWarehouseService shiWarehouseService;
    @Autowired
    private IInventoryOrderService inventoryOrderService;
    @Autowired
    private UserService userService;
    @Autowired
    private IsalesOrderService isalesOrderService;
    @Autowired
    private sendGoodsOrderMapper sendGoodsOrderMapper1;
    @Autowired
    private TaskService taskService;
    @Autowired
    private ActProcessService actProcessService;
    @Autowired
    private MessageUtil messageUtil;
    @Autowired
    private IHistoryIdentityService historyIdentityService;

    private static final Splitter SPLITTER = Splitter.on(",").trimResults().omitEmptyStrings();

    @Override
    @Nullable
    @Transactional(propagation = Propagation.SUPPORTS)
    public Page<SalesOrderVO> getList(PageVo pageVo, SalesOrderVO salesOrderVO, String userId) {

        if (ToolUtil.isEmpty(userId)) {
            userId = securityUtil.getCurrUser().getId();
        }
        CommonVO commonVO = userMapper.getUserMessage(userId);
        Page<SalesOrderVO> salesList = salesOrderMapper.getList(PageUtil.initMpPage(pageVo), salesOrderVO.getOrderNum(), userId, salesOrderVO.getCheckStatus(), salesOrderVO.getWarehouseType());
        List<SalesOrderVO> salesOrderVOS = salesList.getRecords();
        salesList.setRecords(changeMessage(commonVO, salesOrderVOS));
        return salesList;
    }

    @Override
    public SalesOrderVO selectById(String id) {
        SalesOrderVO salesOrder = salesOrderMapper.getById(id);
        CommonVO commonVO = userMapper.getUserMessage(salesOrder.getCreateBy());
        ToolUtil.copyProperties(commonVO, salesOrder);
        return salesOrder;
    }

    @Override
    public List<NeedCheckProcudct> getNeedCheck(List<PurchaseOrder> purchaseOrders) {

        if (ToolUtil.isNotEmpty(purchaseOrders)) {
            List<NeedCheckProcudct> needCheckProcudcts = purchaseOrders.stream().map(purchaseOrder -> {
                NeedCheckProcudct needCheckProcudct = new NeedCheckProcudct();
                ProductCommon productCommon = productOrderService.getProductCommon(purchaseOrder.getProductId());
                ToolUtil.copyProperties(productCommon, needCheckProcudct);
                return needCheckProcudct;
            }).collect(Collectors.toList());
            return needCheckProcudcts;
        }
        return null;
    }

    @Override
    public List<PurchaseOrderVO> getDetail(String salsOrderId) {
        List<PurchaseOrderVO> purchaseOrders = purchaseOrderService.getPurchaseList(OrderType.salsOrder, salsOrderId);
        return purchaseOrders;
    }

    /**
     * 减少门店库存
     *
     * @param inventoryIds
     */
    public void reduceInventory(String inventoryIds) {
        Set<String> ids = new HashSet<>(SPLITTER.splitToList(inventoryIds));
        if (ToolUtil.isNotEmpty(ids)) {
            ids.stream().forEach(s -> {
                InventoryOrder inventoryOrder = inventoryOrderService.getById(s);
                if (ToolUtil.isNotEmpty(inventoryOrder)) {
                    inventoryOrder.setSerialNumOut(2);
                    inventoryOrderService.updateById(inventoryOrder);
                }
            });
        }
    }

    @Override
    public ActBusiness commitSend(List<PurchaseOrder> purchaseOrders, SalesOrder salesOrder, String userId) {
        String userid = ToolUtil.isNotEmpty(userId) ? userId : securityUtil.getCurrUser().getId();
        if (ToolUtil.isNotEmpty(salesOrder)) {
            SalesOrder salesOrderUse = new SalesOrder();
            ToolUtil.copyProperties(salesOrder, salesOrderUse);
            salesOrderUse.setPurchaseOrderIds(purchaseOrderService.getPurchaseIds(purchaseOrders, userid));
            //获取最后一条记录
            salesOrderUse.setOrderNum(CommonUtil.createOrderSerial(Order.XS_));
            if (salesOrder.getWarehouseType() == 1) {
                salesOrderUse.setCheckStatus(CheckStatus.sucess);
                salesOrderUse.setSendStatus(SendStatus.DOING);
                reduceInventory(salesOrder.getInventoryOrderIds());
            } else if (salesOrder.getWarehouseType() == 2) {
                salesOrderUse.setCheckStatus(CheckStatus.pengindg);
                salesOrderUse.setSendStatus(SendStatus.PENDING);
            } else if (salesOrder.getWarehouseType() == 3) {
                salesOrderUse.setCheckStatus(CheckStatus.pengindg);
                salesOrderUse.setSendStatus(SendStatus.PENDING);
            }
            salesOrderUse.setCreateBy(userid);
            this.save(salesOrderUse);

            //获取最新的一条记录
            SalesOrder salesOrderLast = isalesOrderService.getOne(Wrappers.<SalesOrder>lambdaQuery()
            .orderByDesc(SalesOrder::getId)
            .last("limit 1"));
            //3.插入到流程审核单中
            ActBusiness actBusiness = new ActBusiness();
            actBusiness.setTableId(salesOrderLast.getId());
            switch (salesOrder.getWarehouseType()) {
                case 1:
                    actBusiness.setStatus(ActivitiConstant.STATUS_FINISH);
                    break;
                case 2:
                    actBusiness.setProcDefId(wxservice.getProcDefId(ActivitiConstant.salesOrder));
                    break;
                case 3:
                    //获取最新的一条记录
                    SalesOrder salesOrderNew = this.getOne(Wrappers.<SalesOrder>lambdaQuery()
                            .orderByDesc(SalesOrder::getId)
                            .last("limit 1"));
                    //1.插入申请发货单
                    ApplicationSendGoodsOrder applicationSendGoodsOrder = new ApplicationSendGoodsOrder();
                    applicationSendGoodsOrder.setSalesOrderId(salesOrderNew.getId());
                    applicationSendGoodsOrder.setPurchaseOrderIds(salesOrderNew.getPurchaseOrderIds());
                    applicationSendGoodsOrder.setCreateBy(userId);
                    applicationSendGoodsOrder.setCheckStatus(CheckStatus.pengindg);
                    applicationSendGoodsOrder.setSendStatus(SendStatus.PENDING);
                    applicationSendGoodsOrder.setOrderNum(CommonUtil.createOrderSerial(Order.SQ_));
                    applicationSendGoodsOrderMapper.insert(applicationSendGoodsOrder);
                    String procDefIdApplication = wxservice.getProcDefId(ActivitiConstant.aplicationSendOrder);
                    actBusiness.setProcDefId(procDefIdApplication);
                    break;
            }
            actBusiness.setUserId(userId);
            ActBusiness actBusinessIns = actBusinessService.save(actBusiness);

            //4.更新销售订单
            salesOrderLast.setBussinessId(actBusinessIns.getId());
            isalesOrderService.updateById(salesOrderLast);
            return actBusinessIns;
        }
        return null;
    }

    @Override
    public List<PurchaseOrder> filterNeedCheck(String param, String phone) {

        List<PurchaseOrder> purchaseOrders = JSON.parseArray(param, PurchaseOrder.class);
        List<PurchaseOrder> needSpecial = purchaseOrders.stream().filter(purchaseOrder -> purchaseOrder.getPrice()
                .compareTo(isalesOrderService.getproductPrice(purchaseOrder.getProductId())) == -1).collect(Collectors.toList());
        if (ToolUtil.isNotEmpty(needSpecial)) {
            List<Map<String, Object>> result = specialPriceSalesOrderService.getMyPassList(phone);
            if (ToolUtil.isNotEmpty(result)) {
                List<PurchaseOrder> needResult = needSpecial.stream().filter(purchaseOrder -> filterNeedCheck(purchaseOrder, result) == true)
                        .collect(Collectors.toList());
                return needResult;
            } else {
                return needSpecial;
            }
        }
        return null;
    }

    @Override
    public Page<SalesOrderVO> getUserList(PageVo pageVo, SalesOrderVO salesOrderVO, String userId) {
        String id = null;
        Page<SalesOrderVO> salesList = salesOrderMapper.getList(PageUtil.initMpPage(pageVo), salesOrderVO.getOrderNum(), id, salesOrderVO.getCheckStatus(), salesOrderVO.getWarehouseType());
        List<SalesOrderVO> list = salesList.getRecords();
        if (ToolUtil.isNotEmpty(list)) {
            User user = userMapper.selectById(userId);
            if (ToolUtil.isNotEmpty(user)) {
                list = list.stream().filter(l -> user.getMobile().equals(l.getBuyerPhone())).filter(i -> i.getCheckStatus().equals("审核成功")).collect(Collectors.toList());
            }
            for (SalesOrderVO s : list) {
                CommonVO commonVO = userMapper.getUserMessage(s.getCreateBy());
                ToolUtil.copyProperties(commonVO, s);
            }
            salesList.setRecords(list);
        }
        return salesList;
    }

    @Override
    public void salesSend(SendGoodsOrder sendGoodsOrder,String taskId,String comment,String procInstId) {

        sendGoodsOrder.setCreateBy(securityUtil.getCurrUser().getId());
        sendGoodsOrder.setCreateTime(new Date());
        sendGoodsOrder.setOrderNum(CommonUtil.createOrderSerial(Order.FH_));
        sendGoodsOrderMapper1.insert(sendGoodsOrder);
//启动流程
        EditNodeIdUserVO editNodeIdUserVO = new EditNodeIdUserVO();
        editNodeIdUserVO.setTaskId(taskId);
        editNodeIdUserVO.setProcInstId(procInstId);
        if (ToolUtil.isEmpty(comment) ){
            editNodeIdUserVO.setComment("");
        } else {
            editNodeIdUserVO.setComment(comment);
        }
        //activity消息提醒
        taskService.addComment(editNodeIdUserVO.getTaskId(), editNodeIdUserVO.getProcInstId(), editNodeIdUserVO.getComment());
        Task taskCheck = taskService.createTaskQuery().taskId(editNodeIdUserVO.getTaskId()).singleResult();
        taskService.complete(taskCheck.getId());

        //分配审批节点
        List<Task> tasks = taskService.createTaskQuery().processInstanceId(editNodeIdUserVO.getProcInstId()).list();
        if (ToolUtil.isNotEmpty(tasks)) {

            tasks.stream().forEach(t -> {
                List<User> users = actProcessService.getNode(t.getTaskDefinitionKey()).getUsers();
                for (User user : users) {
                    //根据设置的角色获取审批人
                    taskService.addCandidateUser(t.getId(), user.getId());
                    // 异步发消息
                    messageUtil.sendActMessage(user.getId(), ActivitiConstant.MESSAGE_TODO_CONTENT, false, false, false);
                }
            });
        }

        // 记录实际审批人员
        historyIdentityService.insert(String.valueOf(SnowFlakeUtil.getFlowIdInstance().nextId()),
                ActivitiConstant.EXECUTOR_TYPE, securityUtil.getCurrUser().getId(), editNodeIdUserVO.getTaskId(), editNodeIdUserVO.getProcInstId());

    }

    public boolean filterNeedCheck(PurchaseOrder purchaseOrder, List<Map<String, Object>> maps) {

        for (Map<String, Object> map : maps) {
            if (map.get("id").equals(purchaseOrder.getProductId()) && map.get("price").equals(purchaseOrder.getPrice().doubleValue())) {
                return false;
            }
        }
        return true;
    }

    /**
     * 获取产品的最低销售价价格
     * 提供者
     *
     * @param productOrderId
     * @return
     */
    public BigDecimal getproductPrice(String productOrderId) {
        ProductOrder productOrder = productOrderService.getById(productOrderId);
        if (ToolUtil.isNotEmpty(productOrder)) {
            return productOrder.getMinSellingPrice();
        } else {
            return null;
        }
    }

    /**
     * 转移公共类
     *
     * @param commonVO
     * @param salesOrderVOS
     * @return
     */
    public List<SalesOrderVO> changeMessage(CommonVO commonVO, List<SalesOrderVO> salesOrderVOS) {
        if (!ToolUtil.isOneEmpty(commonVO, salesOrderVOS)) {
            List<SalesOrderVO> salesOrderVOSReturn = salesOrderVOS.stream().map(order -> {
                ToolUtil.copyProperties(commonVO, order);
                return order;
            }).collect(Collectors.toList());
            return salesOrderVOSReturn;
        }
        return null;
    }
}