package cn.exrick.xboot.modules.bussiness.afterPurchase.service;

import cn.exrick.xboot.modules.bussiness.afterPurchase.VO.AfterPurchaseVO;
import com.baomidou.mybatisplus.extension.service.IService;
import cn.exrick.xboot.modules.bussiness.afterPurchase.pojo.AfterPurchase;

import java.util.List;

/**
 * 售后单清单接口
 * @author xiaofei
 */
public interface IafterPurchaseService extends IService<AfterPurchase> {


    /**
     * 插入售后采购单
     * @param afterPurchase
     */
    String  insertAfterPurchase(List<AfterPurchase> afterPurchase);

    /**
     * 获取售后详情
     * @param afterIndentId
     * @return
     */
    List<AfterPurchaseVO> getList(String afterIndentId);

}