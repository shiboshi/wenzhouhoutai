package cn.exrick.xboot.modules.bussiness.color.service;

import cn.exrick.xboot.modules.bussiness.classification.pojo.Classification;
import cn.exrick.xboot.modules.bussiness.color.VO.ColorVO;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;
import cn.exrick.xboot.modules.bussiness.color.pojo.Color;

import java.util.List;

/**
 * 产品颜色接口
 * @author tqr
 */
public interface IcolorService extends IService<Color> {

    IPage<ColorVO> getByName(Page initMpPage, String name);

    List<Color> findByParentIdOrderBySortOrder(String parentId);

    ColorVO getColor(String id);
}