package cn.exrick.xboot.modules.bussiness.shiProductComparison.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import cn.exrick.xboot.modules.bussiness.shiProductComparison.pojo.calf_function;

import java.util.List;

/**
 * 小腿功能表数据处理层
 * @author 石博士
 */
public interface calf_functionMapper extends BaseMapper<calf_function> {

}