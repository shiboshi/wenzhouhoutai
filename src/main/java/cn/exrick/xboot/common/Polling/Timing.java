package cn.exrick.xboot.common.Polling;

import cn.exrick.xboot.common.utils.SecurityUtil;
import cn.exrick.xboot.common.utils.ToolUtil;

import cn.exrick.xboot.modules.activiti.entity.ActBusiness;
import cn.exrick.xboot.modules.activiti.service.ActBusinessService;
import cn.exrick.xboot.modules.bussiness.applicationSendGoodsOrder.mapper.ApplicationSendGoodsOrderMapper;
import cn.exrick.xboot.modules.bussiness.applicationSendGoodsOrder.pojo.ApplicationSendGoodsOrder;
import cn.exrick.xboot.modules.bussiness.applicationSendGoodsOrder.service.IapplicationSendGoodsOrderService;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

public class Timing {
    @Autowired
    private IapplicationSendGoodsOrderService iapplicationSendGoodsOrderService;
    @Autowired
    private SecurityUtil securityUtil;
    @Autowired
    private ActBusinessService actBusinessService;
    @Autowired
    private ApplicationSendGoodsOrderMapper applicationSendGoodsOrderMapper;

    public void ApplicationSendOrder() {
        List<ApplicationSendGoodsOrder> list = iapplicationSendGoodsOrderService.get();
        Map<String, List<ApplicationSendGoodsOrder>> result = list.stream().collect(Collectors.groupingBy(ApplicationSendGoodsOrder::getCreateBy));
        if (result.size() > 0) {
            for (int i = 0; i < result.size(); i++) {
                Set<String> first = result.keySet();
                for (String f : first) {
                    ApplicationSendGoodsOrder newApplicat = new ApplicationSendGoodsOrder();
                    List<ApplicationSendGoodsOrder> applicatList = result.get(f);
                    List<ApplicationSendGoodsOrder> newList = list.stream().sorted(Comparator.comparing(ApplicationSendGoodsOrder::getCreateTime).reversed()).collect(Collectors.toList());
                    ToolUtil.copyProperties(newList.get(0), newApplicat);
                    for (ApplicationSendGoodsOrder a : applicatList) {
                        actBusinessService.delete(a.getBussinessId());
                    }
                    iapplicationSendGoodsOrderService.save(newApplicat);
                    //掺入业务流程表
                    ApplicationSendGoodsOrder applicationSendGoodsOrderLast = iapplicationSendGoodsOrderService.getBaseMapper().selectOne(new QueryWrapper<ApplicationSendGoodsOrder>().orderByDesc("create_time").last(" limit 1 "));
                    ActBusiness actBusiness = new ActBusiness();
                    actBusiness.setUserId(applicationSendGoodsOrderLast.getCreateBy());
                    actBusiness.setTableId(applicationSendGoodsOrderLast.getId());
                    ActBusiness actBusinessInsert = actBusinessService.save(actBusiness);
                    //更新业务表
                    applicationSendGoodsOrderLast.setBussinessId(actBusinessInsert.getId());
                    iapplicationSendGoodsOrderService.updateById(applicationSendGoodsOrderLast);
                }
            }
        }
    }
}
