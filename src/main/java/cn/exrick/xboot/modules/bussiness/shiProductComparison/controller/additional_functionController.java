package cn.exrick.xboot.modules.bussiness.shiProductComparison.controller;

import cn.exrick.xboot.common.utils.PageUtil;
import cn.exrick.xboot.common.utils.ResultUtil;
import cn.exrick.xboot.common.vo.PageVo;
import cn.exrick.xboot.common.vo.Result;
import cn.exrick.xboot.modules.bussiness.shiProductComparison.pojo.additional_function;
import cn.exrick.xboot.modules.bussiness.shiProductComparison.service.Iadditional_functionService;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * @author 石博士
 */
@Slf4j
@RestController
@Api(description = "附加功能表管理接口")
@RequestMapping("/xboot/additional_function")
@Transactional
public class additional_functionController {

    @Autowired
    private Iadditional_functionService iadditional_functionService;

    @RequestMapping(value = "/get/{id}", method = RequestMethod.GET)
    @ApiOperation(value = "通过id获取")
    public Result<additional_function> get(@PathVariable String id){

        additional_function additional_function = iadditional_functionService.getById(id);
        return new ResultUtil<additional_function>().setData(additional_function);
    }

    @RequestMapping(value = "/getAll", method = RequestMethod.GET)
    @ApiOperation(value = "获取全部数据")
    public Result<List<additional_function>> getAll(){

        List<additional_function> list = iadditional_functionService.list();
        return new ResultUtil<List<additional_function>>().setData(list);
    }

    @RequestMapping(value = "/getByPage", method = RequestMethod.GET)
    @ApiOperation(value = "分页获取")
    public Result<IPage<additional_function>> getByPage(@ModelAttribute PageVo page){

        IPage<additional_function> data = iadditional_functionService.page(PageUtil.initMpPage(page));
        return new ResultUtil<IPage<additional_function>>().setData(data);
    }

    @RequestMapping(value = "/insertOrUpdate", method = RequestMethod.POST)
    @ApiOperation(value = "编辑或更新数据")
    public Result<additional_function> saveOrUpdate(@ModelAttribute additional_function additional_function){

        if(iadditional_functionService.saveOrUpdate(additional_function)){
            return new ResultUtil<additional_function>().setData(additional_function);
        }
        return new ResultUtil<additional_function>().setErrorMsg("操作失败");
    }

    @RequestMapping(value = "/delByIds/{ids}", method = RequestMethod.DELETE)
    @ApiOperation(value = "批量通过id删除")
    public Result<Object> delAllByIds(@PathVariable String[] ids){

        for(String id : ids){
            iadditional_functionService.removeById(id);
        }
        return new ResultUtil<Object>().setSuccessMsg("批量通过id删除数据成功");
    }
}
