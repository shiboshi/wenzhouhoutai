package cn.exrick.xboot.modules.bussiness.applicationSendGoodsOrder.VO;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @Author : xiaofei
 * @Date: 2019/8/26
 */
@Data
public class AppliParam {
    private String param;
    private String indentOrderId;
    private String procDefId;
    private String userId;
    private String telephone;
    private String productPic;
    private String arrivalTime;
    /**
     * 收货地址
     */
    private String address;
}
