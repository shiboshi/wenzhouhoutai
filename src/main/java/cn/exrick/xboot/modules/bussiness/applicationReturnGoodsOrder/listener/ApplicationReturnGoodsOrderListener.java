package cn.exrick.xboot.modules.bussiness.applicationReturnGoodsOrder.listener;

import cn.exrick.xboot.common.enums.CheckStatus;
import cn.exrick.xboot.common.enums.Order;
import cn.exrick.xboot.common.utils.ToolUtil;
import cn.exrick.xboot.config.springContext.SpringContextHolder;
import cn.exrick.xboot.modules.bussiness.appliSendNum.pojo.AppliSendNum;
import cn.exrick.xboot.modules.bussiness.appliSendNum.service.IappliSendNumService;
import cn.exrick.xboot.modules.bussiness.applicationReturnGoodsOrder.pojo.ApplicationReturnGoodsOrder;
import cn.exrick.xboot.modules.bussiness.applicationReturnGoodsOrder.service.IApplicationReturnGoodsOrderService;
import cn.exrick.xboot.modules.bussiness.purchaseOrder.pojo.PurchaseOrder;
import cn.exrick.xboot.modules.bussiness.purchaseOrder.service.IpurchaseOrderService;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.google.common.base.Splitter;
import com.google.common.collect.Lists;
import org.activiti.engine.delegate.DelegateExecution;
import org.activiti.engine.delegate.ExecutionListener;

import java.math.BigDecimal;
import java.util.List;
import java.util.stream.Collectors;

/**
 * @Author : xiaofei
 * @Date: 2019/8/22
 */
public class ApplicationReturnGoodsOrderListener implements ExecutionListener {

    private static final Splitter SPLITTER = Splitter.on(",").trimResults().omitEmptyStrings();
    @Override
    public void notify(DelegateExecution delegateExecution) throws Exception {

        String tableId = (String) delegateExecution.getVariable("tableId");
        IApplicationReturnGoodsOrderService applicationReturnGoodsOrderService = SpringContextHolder.getBean(IApplicationReturnGoodsOrderService.class);
        IpurchaseOrderService purchaseOrderService = SpringContextHolder.getBean(IpurchaseOrderService.class);
        IappliSendNumService appliSendNumService = SpringContextHolder.getBean(IappliSendNumService.class);

         if (delegateExecution.getEventName().equals("end")){
            ApplicationReturnGoodsOrder applicationReturnGoodsOrder = applicationReturnGoodsOrderService.getById(tableId);
            applicationReturnGoodsOrder.setCheckStatus(CheckStatus.sucess);
            applicationReturnGoodsOrderService.updateById(applicationReturnGoodsOrder);

            //修改数量
             List<AppliSendNum> appliSendNums = appliSendNumService.list(Wrappers.<AppliSendNum>lambdaQuery()
                     .eq(AppliSendNum::getApplicationSendOrderId,tableId));

            List<PurchaseOrder> purchaseOrders = purchaseOrderService.list(Wrappers.<PurchaseOrder>lambdaQuery()
            .in(PurchaseOrder::getId, Lists.newArrayList(SPLITTER.split(applicationReturnGoodsOrder.getPurchaseOrderIds()))));
            if (ToolUtil.isNotEmpty(purchaseOrders)){
                purchaseOrders.stream()
                        .filter(Order -> ToolUtil.isNotEmpty(Order))
                        .forEach(order -> {
                         //1.修改订购数量
                           List<AppliSendNum> appliSendNumsFilter = appliSendNums.stream()
                                   .filter(appliSendNum -> appliSendNum.getPurchaseId().equals(order.getId()))
                                   .collect(Collectors.toList());
                           order.setOrderNumbers(order.getOrderNumbers().subtract(new BigDecimal(appliSendNumsFilter
                           .get(0).getSendNum())));
                         //2.修改未发数量
                            order.setNotSend(order.getOrderNumbers());
                            purchaseOrderService.updateById(order);
                });
            }
        }
    }
}
