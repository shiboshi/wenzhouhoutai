package cn.exrick.xboot.modules.bussiness.product.VO;

import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiModelProperty;
import lombok.Builder;
import lombok.Data;
import lombok.experimental.Accessors;
import org.quartz.SimpleTrigger;
import org.springframework.format.annotation.DateTimeFormat;

import java.util.Date;

/**
 * @Author : xiaofei
 * @Date: 2019/8/8
 */
@Data
@Accessors(chain = true)
public class ProductVO {

    /**
     * id
     */
    private String id;
    /**
     * 产品分类ID
     */
    @ApiModelProperty("产品分类ID")
    private String kindId;

    /**
     * 产品大类名称
     */
    @ApiModelProperty("产品大类名称")
    private String productName;

    /**
     * 创建人
     */
    @ApiModelProperty("创建人")
    private String createBy;

    /**
     * 创建时间
     */
    @ApiModelProperty("创建时间")
    @JsonFormat(timezone = "GMT+8", pattern = "yyyy-MM-dd HH:mm:ss")
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date createTime;

    private String userName;
}
