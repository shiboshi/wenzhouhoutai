package cn.exrick.xboot.modules.bussiness.productPrice;

import org.apache.commons.lang3.StringUtils;

public class ProductPriceProvider {
    public String getProductPrice(String condition) {
        String sql = "";
        String str = "";
        str += " select p.create_time,p.id AS id, p.product_price AS productPrice,s.title as productOrderName ," +
                "(select title from t_classification where id = s.parent_id) as productModelName,(select id from t_classification where id = s.parent_id) as productModelId," +
                "(select title from  t_classification where id = (select  parent_id from t_classification where id = s.parent_id)) as productName," +
                "l.level_name as level from  " +
                "t_product_price p left join t_classification s on p.product_order_id = s.id and s.del_flag = 0 " +
                "left join t_level l on p.level_id = l.id and l.del_flag = 0 where p.del_flag = 0 ";
        if(StringUtils.isNotBlank(condition)){
            str += " and (p.product_price like '%"+condition+"%' or s.title like '%"+condition+"%' or (select title from  t_classification where id = (select  parent_id from t_classification where id = s.parent_id)) like '%"+condition+"%' or (select title from t_classification where id = s.parent_id) like '%"+condition+"%' or l.level_name like '%"+condition+"%')";
        }
        sql += str;
        return sql;
    }
}
