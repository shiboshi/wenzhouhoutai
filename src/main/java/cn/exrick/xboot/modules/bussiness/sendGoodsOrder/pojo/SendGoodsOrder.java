package cn.exrick.xboot.modules.bussiness.sendGoodsOrder.pojo;

import cn.exrick.xboot.base.XbootBaseEntity;
import cn.exrick.xboot.common.enums.IsComplete;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.Table;

/**
 * @author xiaofei
 */
@Data
@Entity
@Table(name = "x_send_goods_order")
@TableName("x_send_goods_order")
@ApiModel(value = "发货单")
public class SendGoodsOrder extends XbootBaseEntity {

    private static final long serialVersionUID = 1L;

    /**
     * 关联申请发货单id
     */
    private String applicationSendGoodsId;

    /**
     * 关联库存id
     */
    private String inventoryOrderIds;

    /**
     *序列号id
     */
    private String serial;

    /**
     * 关联销售单id
     */
    private String saleOrderId;

    /**
     * 订单编号
     */
    private String orderNum;

    /**
     * 评论信息
     */
    private String comment;

   /* *
     * 订单是否完成1.未完成2.已完成*/

    private IsComplete isComplete;

    /**
     * 咨询电话
     */
    private String telephone;

    /**
     * 产品图片
     */
    private String productPic;

    /**
     * 到货时间
     */
    private String arrivalTime;


}