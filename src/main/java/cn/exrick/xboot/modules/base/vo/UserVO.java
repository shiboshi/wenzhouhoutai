package cn.exrick.xboot.modules.base.vo;

import lombok.Data;

/**
 * @Author : xiaofei
 * @Date: 2019/7/8
 */
@Data
public class UserVO {

    private String nickName;
    private String password;
    private String username;
    //private Date birthday;
    private String sex;
    private String email;
    private String mobile;
    private String serials;

}
