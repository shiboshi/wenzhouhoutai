package cn.exrick.xboot.modules.bussiness.size.pojo;

import cn.exrick.xboot.base.XbootBaseEntity;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModelProperty;
import io.swagger.annotations.ApiModel;
import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

/**
 * @author tqr
 */
@Data
@Entity
@Table(name = "t_size")
@TableName("t_size")
@ApiModel(value = "产品尺寸")
public class size extends XbootBaseEntity {

    private static final long serialVersionUID = 1L;

    @Column(length=60)
    private String twenty;

    @Column(length=60)
    private String forty;

    @Column(length=60)
    private String fortyHQ;

    @Column(length=60)
    private String name;

}