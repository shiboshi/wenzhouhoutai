package cn.exrick.xboot.modules.bussiness.putInWarehouse.serviceImp;

import cn.exrick.xboot.common.utils.ToolUtil;
import cn.exrick.xboot.modules.bussiness.putInWarehouse.VO.PutInWarehouseVO;
import cn.exrick.xboot.modules.bussiness.putInWarehouse.mapper.PutInWarehouseMapper;
import cn.exrick.xboot.modules.bussiness.putInWarehouse.pojo.PutInWarehouse;
import cn.exrick.xboot.modules.bussiness.putInWarehouse.service.IPutInWarehouseService;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

/**
 * 入库接口实现
 * @author tqr
 */
@Slf4j
@Service
@Transactional
public class IPutInWarehouseServiceImpl extends ServiceImpl<PutInWarehouseMapper, PutInWarehouse> implements IPutInWarehouseService {

    @Autowired
    private PutInWarehouseMapper putInWarehouseMapper;

    @Override
    public IPage<PutInWarehouseVO> getPutInWarehouse(Page initMpPage,String productOrderName,String status) {
        IPage<PutInWarehouseVO> data = putInWarehouseMapper.getPutInWarehouse(initMpPage,productOrderName,status);
        List<PutInWarehouseVO> list = data.getRecords();
        for(PutInWarehouseVO putInWarehouseVO :list){
            if(ToolUtil.isNotEmpty(putInWarehouseVO)){
                if(1 == (putInWarehouseVO.getStatus())){
                    putInWarehouseVO.setIsPutIn("已入库");
                } else if(0 == (putInWarehouseVO.getStatus())){
                    putInWarehouseVO.setIsPutIn("未入库");
                }
                putInWarehouseVO.setAllName(putInWarehouseVO.getColor()+"/"+putInWarehouseVO.getColorNumber()+"/"+putInWarehouseVO.getMaterial());
            }
        }
        return data;
    }
}