package cn.exrick.xboot.modules.bussiness.afterSaleOrder.service;

import cn.exrick.xboot.modules.bussiness.afterSaleOrder.pojo.AfterSaleOrder;
import cn.exrick.xboot.modules.bussiness.saleOrder.VO.SalesOrderVO;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;

/**
 * 销售单维修单接口
 * @author fei
 */
public interface IafterSaleOrderService extends IService<AfterSaleOrder> {


    /**
     * 根据当前的登录人和审核状态为2，
     * 审核成功查询销售订单的数据
     */
    List<SalesOrderVO> QueryAfterSaleOrder(String buyerName);







}