package cn.exrick.xboot.modules.bussiness.specialPriceSalesOrder.service;

import cn.exrick.xboot.modules.activiti.entity.ActBusiness;
import cn.exrick.xboot.modules.bussiness.purchaseOrder.VO.PurchaseOrderVO;
import cn.exrick.xboot.modules.bussiness.purchaseOrder.pojo.PurchaseOrder;
import cn.exrick.xboot.modules.bussiness.specialPriceSalesOrder.VO.SpecialPriceSalesOrderVO;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;
import cn.exrick.xboot.modules.bussiness.specialPriceSalesOrder.pojo.SpecialPriceSalesOrder;

import java.util.List;
import java.util.Map;

/**
 * 特价申请销售订单接口
 * @author xiaofei
 */
public interface IspecialPriceSalesOrderService extends IService<SpecialPriceSalesOrder> {

    /**
     * 插入到特价申请单中
     * @param purchaseOrders
     */
    ActBusiness insertSecialPriceOrder(List<PurchaseOrder> purchaseOrders, String proDefId, String userId,String phone);

    /**
     * 获取当前用户审核通过的产品id
     * @return
     */
    List<Map<String,Object>> getMyPassList(String buyerPhone);


    IPage<SpecialPriceSalesOrderVO> getList(Page initMpPage,String userId,String checkStatus);

    List<PurchaseOrderVO> getDetail(String id);
}