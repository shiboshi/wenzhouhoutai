package cn.exrick.xboot.modules.bussiness.specialPriceSalesOrder;

import cn.exrick.xboot.common.utils.ToolUtil;

import java.util.Map;

public class SpecialPriceSalesOrderProvider {
    public String getList(Map<String,Object> map){
        String sql = "";
        String str ="";
        str += "select * from x_special_price_sales_order where del_flag = 0 ";
        if(ToolUtil.isNotEmpty(map.get("userId"))){
            str += " and create_by = '"+map.get("userId")+"' ";
        }
        if (ToolUtil.isNotEmpty(map.get("checkStatus"))){
            str += " and check_status = '"+map.get("checkStatus")+"' ";
        }
        sql += str;
        return sql;
    }
}
