package cn.exrick.xboot.modules.bussiness.shiProductComparison.pojo;

import cn.exrick.xboot.base.XbootBaseEntity;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.Table;

/**
 * @author 石博士
 */
@Data
@Entity
@Table(name = "s_basic_parameter")
@TableName("s_basic_parameter")
@ApiModel(value = "基本参数表")
public class basic_parameter extends XbootBaseEntity {

    private static final long serialVersionUID = 1L;



    @ApiModelProperty("额定电压，手写")
    private String voltage;

    @ApiModelProperty("额定频率，手写")
    private String frequency;

    @ApiModelProperty("最小气压，手写")
    private String minimum_pressure;

    @ApiModelProperty("待机功率，手写")
    private String standby_power;

    @ApiModelProperty("负离子浓度，手写")
    private String concentration;

    @ApiModelProperty("额定功率，手写")
    private String rated_power;

    @ApiModelProperty("噪音，手写")
    private String noise;

    @ApiModelProperty("最大气压，手写")
    private String maximum_pressure;

    @ApiModelProperty("磁场强度，手写")
    private String strength;

    @ApiModelProperty("气袋总数，手写")
    private String total;

    @ApiModelProperty("电磁阀总数，手写")
    private String total_electricity;

    @ApiModelProperty("气泵总数，手写")
    private String total_gas;












}