package cn.exrick.xboot.modules.bussiness.tApplication.service;

import cn.exrick.xboot.modules.bussiness.srecharge.pojo.Srecharge;
import cn.exrick.xboot.modules.bussiness.tApplication.pojo.TApplicationVO;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;
import cn.exrick.xboot.modules.bussiness.tApplication.pojo.TApplication;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * 退款申请表接口
 * @author tqr
 */
public interface ITApplicationService extends IService<TApplication> {



    IPage<TApplication> getByPage(Page page);


    /**
     * 查询所有集合
     */
    IPage<TApplicationVO> QueryTapplication(Page page, @Param("username") String username);




}