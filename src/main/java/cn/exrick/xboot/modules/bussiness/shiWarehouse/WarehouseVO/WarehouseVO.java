package cn.exrick.xboot.modules.bussiness.shiWarehouse.WarehouseVO;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.List;

@Data
@ApiModel("仓库")
public class WarehouseVO {

    @ApiModelProperty("id")
    private String id;

    @ApiModelProperty("创建时间")
    private String createTime;

    @ApiModelProperty("编号")
    private String serialNum;

    @ApiModelProperty("仓库名称")
    private String wName;

    @ApiModelProperty("部门id")
    private String deptId;

    @ApiModelProperty("部门名称")
    private String deptName;

}
