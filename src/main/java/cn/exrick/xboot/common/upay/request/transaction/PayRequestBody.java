package cn.exrick.xboot.common.upay.request.transaction;

import cn.exrick.xboot.common.upay.enums.BizChannel;
import cn.exrick.xboot.common.upay.enums.BizType;
import cn.exrick.xboot.common.upay.request.RequestBody;
import cn.exrick.xboot.common.upay.utils.StringUtils;


/**
 * TODO 条码支付请求参数
 *
 * @author zhou
 * @date 2019-7-30 09:55:30
 */
public class PayRequestBody extends RequestBody {

    private String ext_no;
    private String auth_code;
    private String total_amount;
    private String discountable_amount;
    private String undiscountable_amount;
    private String currency = "CNY";
    private String subject = "银联商务浙江";
    private String body;
    private String attach;
    private String goods_detail;
    private String timeout_express = "5m";

    public PayRequestBody() {
    }

    @Override
    public boolean validate() {

        if (StringUtils.isEmpty(ext_no)) {
            throw new RuntimeException("商户订单号[ext_no]不可为空");
        }

        if (StringUtils.isEmpty(total_amount)) {
            throw new RuntimeException("订单总金额[total_amount]不可为空");
        }

        if (StringUtils.isEmpty(currency)) {
            throw new RuntimeException("金额币种[currency]不可为空");
        }

        if (StringUtils.isEmpty(subject)) {
            throw new RuntimeException("订单主题[subject]不可为空");
        }

        return true;
    }

    public String getExt_no() {
        return ext_no;
    }

    public void setExt_no(String ext_no) {
        this.ext_no = ext_no;
    }

    public String getAuth_code() {
        return auth_code;
    }

    public void setAuth_code(String auth_code) {
        this.auth_code = auth_code;
    }

    public String getTotal_amount() {
        return total_amount;
    }

    public void setTotal_amount(String total_amount) {
        this.total_amount = total_amount;
    }

    public String getDiscountable_amount() {
        return discountable_amount;
    }

    public void setDiscountable_amount(String discountable_amount) {
        this.discountable_amount = discountable_amount;
    }

    public String getUndiscountable_amount() {
        return undiscountable_amount;
    }

    public void setUndiscountable_amount(String undiscountable_amount) {
        this.undiscountable_amount = undiscountable_amount;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public String getBody() {
        return body;
    }

    public void setBody(String body) {
        this.body = body;
    }

    public String getAttach() {
        return attach;
    }

    public void setAttach(String attach) {
        this.attach = attach;
    }

    public String getGoods_detail() {
        return goods_detail;
    }

    public void setGoods_detail(String goods_detail) {
        this.goods_detail = goods_detail;
    }

    public String getTimeout_express() {
        return timeout_express;
    }

    public void setTimeout_express(String timeout_express) {
        this.timeout_express = timeout_express;
    }

    @Override
    public String acquireBizChannel() {
        return BizChannel.UMSZJ_CHANNLE_MIN.getEn();
    }

    @Override
    public String acquireBizType() {
        return BizType.UMSZJ_TRADE_PRECREATE.getEn();
    }
}
