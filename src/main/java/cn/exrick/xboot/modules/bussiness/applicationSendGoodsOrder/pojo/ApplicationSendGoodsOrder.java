package cn.exrick.xboot.modules.bussiness.applicationSendGoodsOrder.pojo;

import cn.exrick.xboot.base.XbootBaseEntity;
import cn.exrick.xboot.common.enums.CheckStatus;
import cn.exrick.xboot.common.enums.SendStatus;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiModelProperty;
import io.swagger.annotations.ApiModel;
import lombok.Builder;
import lombok.Data;
import lombok.experimental.Accessors;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import java.util.Date;

/**
 * @author xiaofei
 */
@Data
@Entity
@Table(name = "x_application_send_goods_order")
@TableName("x_application_send_goods_order")
@ApiModel(value = "申请发货单")
@Accessors(chain = true)
public class ApplicationSendGoodsOrder extends XbootBaseEntity {

    private static final long serialVersionUID = 1L;

    /**
     * 订单编号
     */
    @ApiModelProperty(value = "订单编号")
    private String orderNum;
    /**
     * 订单id
     */
    @ApiModelProperty(value = "订单id")
    private String indentOrderId;

    /**
     * 采购单id
     */
    @ApiModelProperty(value = "采购单id")
    @Column(length = 1000)
    private String purchaseOrderIds;

    /**
     * 审核状态
     */
    private CheckStatus checkStatus;
    /**
     * 发货状态
     */
    private SendStatus sendStatus;

    /**
     * 业务流程id
     */
    @ApiModelProperty(value = "业务流程id")
    private String bussinessId;

    /**
     * 销售订单id
     */
    private String salesOrderId;

    private String afterIndentOrderId;

    /**
     * 收货地址
     */
    private String address;
}