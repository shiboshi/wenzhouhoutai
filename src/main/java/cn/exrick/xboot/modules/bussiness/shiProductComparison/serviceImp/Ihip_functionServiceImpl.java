package cn.exrick.xboot.modules.bussiness.shiProductComparison.serviceImp;

import cn.exrick.xboot.modules.bussiness.shiProductComparison.mapper.hip_functionMapper;
import cn.exrick.xboot.modules.bussiness.shiProductComparison.pojo.hip_function;
import cn.exrick.xboot.modules.bussiness.shiProductComparison.service.Ihip_functionService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

/**
 * 臀部功能接口实现
 * @author 石博士
 */
@Slf4j
@Service
@Transactional
public class Ihip_functionServiceImpl extends ServiceImpl<hip_functionMapper, hip_function> implements Ihip_functionService {

    @Autowired
    private hip_functionMapper hip_functionMapper;
}