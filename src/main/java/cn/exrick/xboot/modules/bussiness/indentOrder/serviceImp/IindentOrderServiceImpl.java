package cn.exrick.xboot.modules.bussiness.indentOrder.serviceImp;

import cn.exrick.xboot.common.constant.CommonConstant;
import cn.exrick.xboot.common.enums.*;
import cn.exrick.xboot.common.factory.ConstantFactory;
import cn.exrick.xboot.common.utils.CommonUtil;
import cn.exrick.xboot.common.utils.PageUtil;
import cn.exrick.xboot.common.utils.SecurityUtil;
import cn.exrick.xboot.common.utils.ToolUtil;
import cn.exrick.xboot.common.vo.PageVo;
import cn.exrick.xboot.modules.activiti.entity.ActBusiness;
import cn.exrick.xboot.modules.activiti.service.ActBusinessService;
import cn.exrick.xboot.modules.bussiness.applicationAfterSaleOrder.mapper.applicationAfterSaleOrderMapper;
import cn.exrick.xboot.modules.bussiness.applicationAfterSaleOrder.pojo.ApplicationAfterSaleOrder;
import cn.exrick.xboot.modules.bussiness.indentOrder.VO.IndentOrderVO;
import cn.exrick.xboot.modules.bussiness.indentOrder.mapper.IndentOrderMapper;
import cn.exrick.xboot.modules.bussiness.indentOrder.pojo.IndentOrder;
import cn.exrick.xboot.modules.bussiness.indentOrder.service.IindentOrderService;
import cn.exrick.xboot.modules.bussiness.purchaseOrder.VO.PurchaseOrderVO;
import cn.exrick.xboot.modules.bussiness.purchaseOrder.pojo.PurchaseOrder;
import cn.exrick.xboot.modules.bussiness.purchaseOrder.service.IpurchaseOrderService;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.google.common.base.Splitter;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.lang.Nullable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.concurrent.atomic.AtomicReference;

/**
 * 订货单接口实现
 *
 * @author xiaofei
 */
@Slf4j
@Service
@Transactional
public class IindentOrderServiceImpl extends ServiceImpl<IndentOrderMapper, IndentOrder> implements IindentOrderService {

    @Autowired
    private SecurityUtil securityUtil;
    @Autowired
    private IpurchaseOrderService purchaseOrderService;
    @Autowired
    private ActBusinessService actBusinessService;
    @Autowired
    private IndentOrderMapper indentOrderMapper;
    @Autowired
    private applicationAfterSaleOrderMapper applicationAfterSaleOrderMapper1;


    private static  final Splitter SPLITTER = Splitter.on(",").trimResults().omitEmptyStrings();

    @Override
    public Page<IndentOrderVO> getAll(IndentOrder indentOrder, PageVo pageVo, Integer code, String userId) {
        userId = ToolUtil.isEmpty(userId) ? securityUtil.getCurrUser().getId() : userId;
        Page<IndentOrderVO> indentOrderVOPage = PageUtil.initMpPage(pageVo);
        if (CommonConstant.INDENTORDER.equals(code)) {
            indentOrderVOPage = indentOrderMapper.selectIndentOrderList(PageUtil.initMpPage(pageVo), CommonConstant.INDENTORDER, userId, indentOrder.getOrderNum());
        } else if (CommonConstant.APPLICATIONSEND.equals(code)) {
            indentOrderVOPage = indentOrderMapper.selectIndentOrderList(PageUtil.initMpPage(pageVo), CommonConstant.APPLICATIONSEND, userId, indentOrder.getOrderNum());
        } else if (CommonConstant.ENDORDER.equals(code)) {
            indentOrderVOPage = indentOrderMapper.selectIndentOrderList(PageUtil.initMpPage(pageVo), CommonConstant.ENDORDER, userId, indentOrder.getOrderNum());
            List<IndentOrderVO> indentOrderVOS = indentOrderVOPage.getRecords();
            List<IndentOrderVO> orderVOS = new ArrayList<>();
            for (IndentOrderVO vo : indentOrderVOS) {
                Integer count = applicationAfterSaleOrderMapper1.selectCount(new QueryWrapper<ApplicationAfterSaleOrder>().lambda().eq(ApplicationAfterSaleOrder::getIndentOrderId, vo.getId()));
                if (count == 0) {
                    orderVOS.add(vo);
                }
            }
            indentOrderVOPage.setRecords(orderVOS);
        } else if (4 == (code) || 5 == (code) || 6 == (code)) {
            indentOrderVOPage = indentOrderMapper.selectIndentOrderList(PageUtil.initMpPage(pageVo), code, userId, indentOrder.getOrderNum());
        }
        List<IndentOrderVO> indentOrderVOS = indentOrderVOPage.getRecords();
        for (IndentOrderVO order : indentOrderVOS) {
            order.setUserName(ConstantFactory.me().getUserName(order.getCreateBy()));
            order.setUserLevel(ConstantFactory.me().getUserLevelName(order.getCreateBy()));
            order.setUserDept(ConstantFactory.me().getUserDeptName(order.getCreateBy()));
            order.setCode(juegeSendOver(order.getId()));
        }
        indentOrderVOPage.setRecords(indentOrderVOS);
        return indentOrderVOPage;
    }

    @Override
    public IndentOrderVO selectById(String id) {
        IndentOrderVO indentOrderVOPage = indentOrderMapper.getById(id);

        indentOrderVOPage.setUserName(ConstantFactory.me().getUserName(indentOrderVOPage.getCreateBy()));
        indentOrderVOPage.setUserLevel(ConstantFactory.me().getUserLevelName(indentOrderVOPage.getCreateBy()));
        indentOrderVOPage.setUserDept(ConstantFactory.me().getUserDeptName(indentOrderVOPage.getCreateBy()));

        return indentOrderVOPage;
    }

    @Override
    public List<PurchaseOrderVO> getIndentOrderDetail(String indentOrderId) {
        List<PurchaseOrderVO> purchaseOrderVOS = purchaseOrderService.getPurchaseList(OrderType.indentOrder, indentOrderId);
        return purchaseOrderVOS;
    }


    @Override
    @Transactional(propagation = Propagation.SUPPORTS)
    @Nullable
    public ActBusiness insertIndentOrder(List<PurchaseOrder> purchaseOrders, String procDefId, String userId, Integer payType,String indentOrderType) {

        String userid = ToolUtil.isNotEmpty(userId) ? userId : securityUtil.getCurrUser().getId();
        IndentOrder indentOrder = new IndentOrder();
        indentOrder.setIndentOrderType(indentOrderType);
        indentOrder.setPurchaseOrderIds(purchaseOrderService.getPurchaseIds(purchaseOrders, userId));
        indentOrder.setOrderNum(CommonUtil.createOrderSerial(Order.NM_));
        indentOrder.setCreateBy(userid);
        if (ToolUtil.isNotEmpty(payType)) {
            switch (payType) {
                case 0:
                    indentOrder.setPayType(PayType.BLANCE);
                    break;
                case 1:
                    indentOrder.setPayType(PayType.CRIDIT);
                    break;
                case 2:
                    indentOrder.setPayType(PayType.CRASH);
                    break;
            }
        }
        indentOrder.setIsComplete(IsComplete.PENDING);
        indentOrder.setCheckStatus(CheckStatus.pengindg);
        this.save(indentOrder);

        //获取最新插入的一条数据
        IndentOrder indentOrderLast = this.getOne(Wrappers.<IndentOrder>lambdaQuery()
                .orderByDesc(IndentOrder::getId)
                .last("limit 1"));
        ActBusiness actBusiness = new ActBusiness();
        actBusiness.setUserId(userid);
        actBusiness.setTableId(indentOrderLast.getId());
        actBusiness.setProcDefId(procDefId);
        ActBusiness actBusinessNew = actBusinessService.save(actBusiness);
        //找到最新插入的数据
        indentOrderLast.setBussinessId(actBusinessNew.getId());
        this.updateById(indentOrderLast);
        return actBusinessNew;
    }

    @Override
    @Transactional(propagation = Propagation.SUPPORTS)
    @Nullable
    public void editIndentOrder(List<PurchaseOrder> purchaseOrders, String procDefId, String userId, String id, Integer payType) {

        //查询老的订货单数据
        IndentOrder old = indentOrderMapper.selectById(id);
        if (ToolUtil.isNotEmpty(old)) {
            String userid = ToolUtil.isNotEmpty(userId) ? userId : securityUtil.getCurrUser().getId();
            IndentOrder indentOrder = new IndentOrder();
            if (ToolUtil.isNotEmpty(payType)) {
                if (payType == PayType.BLANCE.getValue()) {
                    indentOrder.setPayType(PayType.BLANCE);
                } else if (payType == PayType.CRIDIT.getValue()) {
                    indentOrder.setPayType(PayType.CRIDIT);
                } else if (payType == PayType.CRASH.getValue()) {
                    indentOrder.setPayType(PayType.CRASH);
                }
            } else {
                indentOrder.setPayType(old.getPayType());
            }
            indentOrder.setPurchaseOrderIds(purchaseOrderService.getPurchaseIds(purchaseOrders, userId));
            indentOrder.setOrderNum(old.getOrderNum());
            indentOrder.setCreateBy(old.getCreateBy());
            indentOrder.setUpdateBy(userid);
            indentOrder.setUpdateTime(new Date());
            indentOrder.setCreateTime(old.getCreateTime());
            indentOrder.setIsComplete(old.getIsComplete());
            indentOrder.setCheckStatus(old.getCheckStatus());
            indentOrder.setId(id);
            indentOrder.setDelFlag(0);
            indentOrder.setBussinessId(old.getBussinessId());
            //删除老数据
            indentOrderMapper.deleteIndent(id);
            //保存新数据
            this.save(indentOrder);

        }
    }

    /**
     * 判断 当前订货单是否所有商品都发完 1.未发完 2.发完
     * @param indentId
     * @return
     */
    public Integer juegeSendOver(String indentId) {

        AtomicReference<Integer> code = new AtomicReference<>(2);
        IndentOrder indentOrder = indentOrderMapper.selectById(indentId);
        Optional.ofNullable(indentOrder).ifPresent(indentOrderUse -> {
            //1.获取当前订货单中的所有采购单
            List<String> purIds = SPLITTER.splitToList(indentOrderUse.getPurchaseOrderIds());
            purIds.forEach(ids -> {
                PurchaseOrder purchaseOrder = purchaseOrderService.getById(ids);
                Optional.ofNullable(purchaseOrder).ifPresent(order -> {
                    if (order.getNotSend().intValue() > 0){
                        code.set(1);
                    }
                });
            });
        });

        return code.get();
    }

    @Override
    public IndentOrderVO formItem(String indentOrderId) {

        String userId = securityUtil.getCurrUser().getId();
        IndentOrder indentOrder = this.getById(indentOrderId);
        IndentOrderVO indentOrderVO = new IndentOrderVO();
        if (ToolUtil.isNotEmpty(indentOrder)) {
            ToolUtil.copyProperties(indentOrder, indentOrderVO);
            indentOrderVO.setUserName(ConstantFactory.me().getUserName(userId));
            indentOrderVO.setUserDept(ConstantFactory.me().getUserDeptName(userId));
            indentOrderVO.setUserLevel(ConstantFactory.me().getUserLevelName(userId));
        }
        return indentOrderVO;
    }

}