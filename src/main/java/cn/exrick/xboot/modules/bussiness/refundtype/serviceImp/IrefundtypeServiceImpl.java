package cn.exrick.xboot.modules.bussiness.refundtype.serviceImp;

import cn.exrick.xboot.modules.bussiness.refundtype.mapper.refundtypeMapper;
import cn.exrick.xboot.modules.bussiness.refundtype.pojo.refundtype;
import cn.exrick.xboot.modules.bussiness.refundtype.service.IrefundtypeService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

/**
 * 退款类型接口实现
 * @author 石博士
 */
@Slf4j
@Service
@Transactional
public class IrefundtypeServiceImpl extends ServiceImpl<refundtypeMapper, refundtype> implements IrefundtypeService {

    @Autowired
    private refundtypeMapper refundtypeMapper;
}