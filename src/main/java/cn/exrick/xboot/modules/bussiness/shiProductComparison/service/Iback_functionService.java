package cn.exrick.xboot.modules.bussiness.shiProductComparison.service;

import com.baomidou.mybatisplus.extension.service.IService;
import cn.exrick.xboot.modules.bussiness.shiProductComparison.pojo.back_function;

import java.util.List;

/**
 * 背部功能接口
 * @author 石博士
 */
public interface Iback_functionService extends IService<back_function> {

}