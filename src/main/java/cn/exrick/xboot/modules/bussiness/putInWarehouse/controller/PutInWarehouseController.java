package cn.exrick.xboot.modules.bussiness.putInWarehouse.controller;

import cn.exrick.xboot.common.utils.PageUtil;
import cn.exrick.xboot.common.utils.ResultUtil;
import cn.exrick.xboot.common.utils.SecurityUtil;
import cn.exrick.xboot.common.utils.ToolUtil;
import cn.exrick.xboot.common.vo.PageVo;
import cn.exrick.xboot.common.vo.Result;
import cn.exrick.xboot.modules.base.service.DepartmentService;
import cn.exrick.xboot.modules.bussiness.inventoryOrder.pojo.InventoryOrder;
import cn.exrick.xboot.modules.bussiness.inventoryOrder.service.IInventoryOrderService;
import cn.exrick.xboot.modules.bussiness.putInWarehouse.VO.PutInWarehouseVO;
import cn.exrick.xboot.modules.bussiness.putInWarehouse.pojo.PutInWarehouse;
import cn.exrick.xboot.modules.bussiness.putInWarehouse.service.IPutInWarehouseService;
import com.alibaba.fastjson.JSON;
import com.baomidou.mybatisplus.core.metadata.IPage;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.List;

/**
 * @author tqr
 */
@Slf4j
@RestController
@Api(description = "入库管理接口")
@RequestMapping("/xboot/putInWarehouse")
@Transactional
public class PutInWarehouseController {

    @Autowired
    private IPutInWarehouseService iPutInWarehouseService;
    @Autowired
    private SecurityUtil securityUtil;
    @Autowired
    private DepartmentService departmentService;
    @Autowired
    private IInventoryOrderService iInventoryOrderService;

    @RequestMapping(value = "/get/{id}", method = RequestMethod.GET)
    @ApiOperation(value = "通过id获取")
    public Result<PutInWarehouse> get(@PathVariable String id) {

        PutInWarehouse putInWarehouse = iPutInWarehouseService.getById(id);
        return new ResultUtil<PutInWarehouse>().setData(putInWarehouse);
    }

    @RequestMapping(value = "/getAll", method = RequestMethod.GET)
    @ApiOperation(value = "获取全部数据")
    public Result<List<PutInWarehouse>> getAll() {

        List<PutInWarehouse> list = iPutInWarehouseService.list();
        return new ResultUtil<List<PutInWarehouse>>().setData(list);
    }

    @RequestMapping(value = "/getByPage", method = RequestMethod.GET)
    @ApiOperation(value = "分页获取")
    public Result<IPage<PutInWarehouse>> getByPage(@ModelAttribute PageVo page) {

        IPage<PutInWarehouse> data = iPutInWarehouseService.page(PageUtil.initMpPage(page));
        return new ResultUtil<IPage<PutInWarehouse>>().setData(data);
    }

    @RequestMapping(value = "/insertOrUpdate", method = RequestMethod.POST)
    @ApiOperation(value = "编辑或更新数据")
    public Result<PutInWarehouse> saveOrUpdate(@ModelAttribute PutInWarehouse putInWarehouse) {

        if (iPutInWarehouseService.saveOrUpdate(putInWarehouse)) {
            return new ResultUtil<PutInWarehouse>().setData(putInWarehouse);
        }
        return new ResultUtil<PutInWarehouse>().setErrorMsg("操作失败");
    }

    @RequestMapping(value = "/delByIds/{ids}", method = RequestMethod.DELETE)
    @ApiOperation(value = "批量通过id删除")
    public Result<Object> delAllByIds(@PathVariable String[] ids) {

        for (String id : ids) {
            iPutInWarehouseService.removeById(id);
        }
        return new ResultUtil<Object>().setSuccessMsg("批量通过id删除数据成功");
    }

    @RequestMapping(value = "/getPutInWarehouse", method = RequestMethod.POST)
    @ApiOperation(value = "分页获取")
    public Result<IPage<PutInWarehouseVO>> getPutInWarehouse(@ModelAttribute PageVo page, String productOrderName, String status) {

        IPage<PutInWarehouseVO> data = iPutInWarehouseService.getPutInWarehouse(PageUtil.initMpPage(page), productOrderName, status);
        return new ResultUtil<IPage<PutInWarehouseVO>>().setData(data);
    }

    /**
     * 新增接口
     *
     * @author 小飞
     * @Date 2019-06-26
     */
    @PostMapping("/addItem")
    public Result<Object> addItem(String putInWarehouse) {
        List<PutInWarehouse> putInWarehouses = JSON.parseArray(putInWarehouse, PutInWarehouse.class);
        if (ToolUtil.isNotEmpty(putInWarehouses)) {
            for (PutInWarehouse putInWarehouseParam : putInWarehouses) {
                putInWarehouseParam.setCreateBy(securityUtil.getCurrUser().getId());
                putInWarehouseParam.setCreateTime(new Date());
                putInWarehouseParam.setStatus(0);
                putInWarehouseParam.setDelFlag(0);
            }
        }
        this.iPutInWarehouseService.saveBatch(putInWarehouses);
        return new ResultUtil<>().setSuccessMsg("添加成功");
    }

    /**
     * 入库接口
     *
     * @param id
     * @return
     */
    @PostMapping("/putInWarhouse")
    public Result<Object> putInWarhouse(String id) {

        //查出当前入库信息
        PutInWarehouse putInWarehouse = iPutInWarehouseService.getById(id);
        if (putInWarehouse.getStatus() == 1) {
            return new ResultUtil<Object>().setErrorMsg("请勿重复入库！");
        }
        InventoryOrder inventoryOrder = new InventoryOrder();
        if (ToolUtil.isNotEmpty(putInWarehouse)) {

            inventoryOrder.setCreateBy(securityUtil.getCurrUser().getId());
            inventoryOrder.setDelFlag(0);
            inventoryOrder.setCreateTime(new Date());
            inventoryOrder.setProductOrderId(putInWarehouse.getProductOrderId());
            inventoryOrder.setSerialNum(putInWarehouse.getSerialNum());
            inventoryOrder.setBatchNumber(putInWarehouse.getBatchNumber());
            inventoryOrder.setWarhouseId(putInWarehouse.getWarehouseId());
            inventoryOrder.setProductOrderId(putInWarehouse.getProductOrderId());
            inventoryOrder.setAmount(1L);
            inventoryOrder.setSerialNumOut(1);
            iInventoryOrderService.save(inventoryOrder);

            //更改库库存信息的状态
            putInWarehouse.setStatus(1);
            iPutInWarehouseService.getBaseMapper().updateById(putInWarehouse);
            return new ResultUtil<Object>().setSuccessMsg("成功");
        } else {
            return new ResultUtil<Object>().error(202, "暂无当前入库信息！");
        }
    }

}
