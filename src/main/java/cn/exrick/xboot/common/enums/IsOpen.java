package cn.exrick.xboot.common.enums;

import com.baomidou.mybatisplus.annotation.EnumValue;
import com.fasterxml.jackson.annotation.JsonValue;

/**
 * @Author : xiaofei
 * @Date: 2019/8/18
 */
public enum  IsOpen  {


    OPEN(1,"开启"),
    NOTOPEN(2,"不开启");

    @EnumValue
    private final int code;
    private String desc;

    public int getCode() {
        return code;
    }

    IsOpen(int code, String desc) {
        this.code = code;
        this.desc = desc;
    }

    @JsonValue
    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }
}
