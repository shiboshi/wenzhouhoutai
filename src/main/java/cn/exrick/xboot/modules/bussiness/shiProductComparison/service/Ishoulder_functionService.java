package cn.exrick.xboot.modules.bussiness.shiProductComparison.service;

import com.baomidou.mybatisplus.extension.service.IService;
import cn.exrick.xboot.modules.bussiness.shiProductComparison.pojo.shoulder_function;

import java.util.List;

/**
 * 肩部功能接口
 * @author 石博士
 */
public interface Ishoulder_functionService extends IService<shoulder_function> {

}