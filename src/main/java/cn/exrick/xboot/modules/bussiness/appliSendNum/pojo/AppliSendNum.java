package cn.exrick.xboot.modules.bussiness.appliSendNum.pojo;

import cn.exrick.xboot.base.XbootBaseEntity;
import cn.exrick.xboot.common.enums.CheckStatus;
import cn.exrick.xboot.common.enums.SendStatus;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModelProperty;
import io.swagger.annotations.ApiModel;
import lombok.Data;
import lombok.experimental.Accessors;

import javax.persistence.Entity;
import javax.persistence.Table;

/**
 * @author fei
 */
@Data
@Entity
@Table(name = "x_appli_send_num")
@TableName("x_appli_send_num")
@ApiModel(value = "申请发货")
@Accessors(chain = true)
public class AppliSendNum extends XbootBaseEntity {

    private static final long serialVersionUID = 1L;

    /**
     *申请发货单 id
     */
    private String applicationSendOrderId;
    /**
     * 采购单id
     */
    private String purchaseId;

    /**
     * 发货数量
     */
    private Long sendNum;


}