package cn.exrick.xboot.modules.bussiness.shiProductComparison.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import cn.exrick.xboot.modules.bussiness.shiProductComparison.pojo.additional_function;

import java.util.List;

/**
 * 附加功能表数据处理层
 * @author 石博士
 */
public interface additional_functionMapper extends BaseMapper<additional_function> {

}