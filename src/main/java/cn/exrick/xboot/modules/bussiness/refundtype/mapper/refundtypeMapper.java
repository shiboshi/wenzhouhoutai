package cn.exrick.xboot.modules.bussiness.refundtype.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import cn.exrick.xboot.modules.bussiness.refundtype.pojo.refundtype;

import java.util.List;

/**
 * 退款类型数据处理层
 * @author 石博士
 */
public interface refundtypeMapper extends BaseMapper<refundtype> {

}