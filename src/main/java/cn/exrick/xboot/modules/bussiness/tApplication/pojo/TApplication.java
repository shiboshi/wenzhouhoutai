package cn.exrick.xboot.modules.bussiness.tApplication.pojo;

import cn.exrick.xboot.base.XbootBaseEntity;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.Table;

/**
 * @author tqr
 */
@Data
@Entity
@Table(name = "t_t_application")
@TableName("t_t_application")
@ApiModel(value = "退款申请表")
public class TApplication extends XbootBaseEntity {

    private static final long serialVersionUID = 1L;

    /**
     * 类型，关联类型表 refundtype
     */
    private String type;

    /**
     * 退款金额
     */
    private String remoney;

    /**
     * 附件
     */
    private String enclosu;

    /**
     * 备注
     */
    private String remarks;

    //申请人id,为当前登录人id
    private String userId;

    //关联订货单查询
    private String indentOrderId;

    /**
     * 申请状态
     */
    private String tStatus;

    /**
     * 流程id
     */
    private String businessId;



    @ApiModelProperty("完成状态")
    private String completionStatus;










}