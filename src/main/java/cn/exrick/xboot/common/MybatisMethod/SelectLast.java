package cn.exrick.xboot.common.MybatisMethod;

import com.baomidou.mybatisplus.core.injector.AbstractMethod;
import com.baomidou.mybatisplus.core.metadata.TableInfo;
import org.apache.ibatis.mapping.MappedStatement;
import org.apache.ibatis.mapping.SqlSource;

import javax.swing.*;
import java.util.List;

/**
 * @Author : xiaofei
 * @Date: 2019/8/10
 */
public class SelectLast extends AbstractMethod {
    @Override
    public MappedStatement injectMappedStatement(Class<?> mapperClass, Class<?> modelClass, TableInfo tableInfo) {

        String sql = "select * from" + tableInfo.getTableName();
        String method = "selectLast";
        SqlSource sqlSource = languageDriver.createSqlSource(configuration,sql,modelClass);
        return this.addSelectMappedStatement(mapperClass,method,sqlSource,modelClass,tableInfo);
    }
}
