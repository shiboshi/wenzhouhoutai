package cn.exrick.xboot.modules.bussiness.shiProductComparison.controller;

import cn.exrick.xboot.common.utils.PageUtil;
import cn.exrick.xboot.common.utils.ResultUtil;
import cn.exrick.xboot.common.vo.PageVo;
import cn.exrick.xboot.common.vo.Result;
import cn.exrick.xboot.modules.bussiness.shiProductComparison.pojo.back_function;
import cn.exrick.xboot.modules.bussiness.shiProductComparison.service.Iback_functionService;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * @author 石博士
 */
@Slf4j
@RestController
@Api(description = "背部功能管理接口")
@RequestMapping("/xboot/back_function")
@Transactional
public class back_functionController {

    @Autowired
    private Iback_functionService iback_functionService;

    @RequestMapping(value = "/get/{id}", method = RequestMethod.GET)
    @ApiOperation(value = "通过id获取")
    public Result<back_function> get(@PathVariable String id){

        back_function back_function = iback_functionService.getById(id);
        return new ResultUtil<back_function>().setData(back_function);
    }

    @RequestMapping(value = "/getAll", method = RequestMethod.GET)
    @ApiOperation(value = "获取全部数据")
    public Result<List<back_function>> getAll(){

        List<back_function> list = iback_functionService.list();
        return new ResultUtil<List<back_function>>().setData(list);
    }

    @RequestMapping(value = "/getByPage", method = RequestMethod.GET)
    @ApiOperation(value = "分页获取")
    public Result<IPage<back_function>> getByPage(@ModelAttribute PageVo page){

        IPage<back_function> data = iback_functionService.page(PageUtil.initMpPage(page));
        return new ResultUtil<IPage<back_function>>().setData(data);
    }

    @RequestMapping(value = "/insertOrUpdate", method = RequestMethod.POST)
    @ApiOperation(value = "编辑或更新数据")
    public Result<back_function> saveOrUpdate(@ModelAttribute back_function back_function){

        if(iback_functionService.saveOrUpdate(back_function)){
            return new ResultUtil<back_function>().setData(back_function);
        }
        return new ResultUtil<back_function>().setErrorMsg("操作失败");
    }

    @RequestMapping(value = "/delByIds/{ids}", method = RequestMethod.DELETE)
    @ApiOperation(value = "批量通过id删除")
    public Result<Object> delAllByIds(@PathVariable String[] ids){

        for(String id : ids){
            iback_functionService.removeById(id);
        }
        return new ResultUtil<Object>().setSuccessMsg("批量通过id删除数据成功");
    }
}
