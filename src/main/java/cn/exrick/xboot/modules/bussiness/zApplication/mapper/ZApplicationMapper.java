package cn.exrick.xboot.modules.bussiness.zApplication.mapper;

import cn.exrick.xboot.modules.bussiness.shiVO.ZApplicationOV;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import cn.exrick.xboot.modules.bussiness.zApplication.pojo.ZApplication;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

/**
 * 装修申请数据处理层
 * @author tqr
 */
@Repository
public interface ZApplicationMapper extends BaseMapper<ZApplication> {



    /**
     * 查询所有集合
     */
    IPage<ZApplicationOV> QueryZapplication(Page page, @Param("username") String username);





}