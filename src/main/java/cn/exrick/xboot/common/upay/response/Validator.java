package cn.exrick.xboot.common.upay.response;

/**
 * Author: jcouyang@chinaums.com
 * Date: 2018/7/27 11:34
 */
public abstract class Validator {
    public abstract boolean validate();
}
