package cn.exrick.xboot.modules.bussiness.unit.controller;

import cn.exrick.xboot.common.utils.PageUtil;
import cn.exrick.xboot.common.utils.ResultUtil;
import cn.exrick.xboot.common.vo.PageVo;
import cn.exrick.xboot.common.vo.Result;
import cn.exrick.xboot.modules.bussiness.unit.VO.UnitVO;
import cn.exrick.xboot.modules.bussiness.unit.pojo.Unit;
import cn.exrick.xboot.modules.bussiness.unit.service.IUnitService;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * @author tqr
 */
@Slf4j
@RestController
@Api(description = "产品单位管理接口")
@RequestMapping("/xboot/unit")
@Transactional
public class UnitController {

    @Autowired
    private IUnitService iUnitService;

    @RequestMapping(value = "/get/{id}", method = RequestMethod.GET)
    @ApiOperation(value = "通过id获取")
    public Result<Unit> get(@PathVariable String id){

        Unit unit = iUnitService.getById(id);
        return new ResultUtil<Unit>().setData(unit);
    }

    @RequestMapping(value = "/getAll", method = RequestMethod.GET)
    @ApiOperation(value = "获取全部数据")
    public Result<List<Unit>> getAll(){

        List<Unit> list = iUnitService.list();
        return new ResultUtil<List<Unit>>().setData(list);
    }

    @RequestMapping(value = "/getByPage", method = RequestMethod.GET)
    @ApiOperation(value = "分页获取")
    public Result<IPage<UnitVO>> getByPage(@ModelAttribute PageVo page, String unitName){
        IPage<UnitVO> data = new Page<>();
        if (StringUtils.isNotBlank(unitName)){
            data = iUnitService.getByName(PageUtil.initMpPage(page),unitName);
        } else {
            data = iUnitService.page(PageUtil.initMpPage(page));
        }
        return new ResultUtil<IPage<UnitVO>>().setData(data);
    }

    @RequestMapping(value = "/insertOrUpdate", method = RequestMethod.POST)
    @ApiOperation(value = "编辑或更新数据")
    public Result<Unit> saveOrUpdate(@ModelAttribute Unit unit){

        if(iUnitService.saveOrUpdate(unit)){
            return new ResultUtil<Unit>().setData(unit);
        }
        return new ResultUtil<Unit>().setErrorMsg("操作失败");
    }

    @RequestMapping(value = "/delByIds/{ids}", method = RequestMethod.DELETE)
    @ApiOperation(value = "批量通过id删除")
    public Result<Object> delAllByIds(@PathVariable String[] ids){

        for(String id : ids){
            iUnitService.removeById(id);
        }
        return new ResultUtil<Object>().setSuccessMsg("批量通过id删除数据成功");
    }
}
