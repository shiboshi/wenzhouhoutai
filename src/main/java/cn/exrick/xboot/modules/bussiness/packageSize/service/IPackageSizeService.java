package cn.exrick.xboot.modules.bussiness.packageSize.service;

import cn.exrick.xboot.modules.bussiness.packageSize.VO.PackageSizeVO;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;
import cn.exrick.xboot.modules.bussiness.packageSize.pojo.PackageSize;

import java.util.List;

/**
 * 包装尺寸接口
 * @author tqr
 */
public interface IPackageSizeService extends IService<PackageSize> {

    IPage<PackageSizeVO> getPackageSize(Page initMpPage, String condition);

    IPage<PackageSizeVO> getByProductId(Page initMpPage, String id);
}