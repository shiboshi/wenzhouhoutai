package cn.exrick.xboot.modules.bussiness.productPrice.VO;

import lombok.Data;

import java.math.BigDecimal;

@Data
public class LevelPrice {
    private String levelId;
    private BigDecimal productPrice;
}
