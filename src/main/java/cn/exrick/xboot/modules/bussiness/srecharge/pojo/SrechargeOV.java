package cn.exrick.xboot.modules.bussiness.srecharge.pojo;

import lombok.Data;

import java.math.BigDecimal;

/**
 * 代充值返回值实体类
 */
@Data
public class SrechargeOV {

    private static final long serialVersionUID = 1L;

    private String id;

    private String createBy;

    private String createTime;

    private String updateBy;

    private String updateTime;

    private Integer delFlag;

    /**
     *类型
     */
    private String type;

    /**
     * 备注
     */
    private String remarks;

    /**
     * 申请人id
     */
    private String userId;

    /**
     *充值金额
     */
    private BigDecimal number;

    /**
     *审核状态
     */
    private Long srechargeStatus;


    /**
     * 充值类型返回值字段
     */
    private String types;

    /**
     * 申请人名称返回值
     */
    private String username;


    /**
     * 审核状态返回值
     */
    private String shisrechargeStatus;


    /**
     * 申请人部门返回值
     */
    private String titles;


    /**
     * 完成状态返回值
     */
    private String completionStatus;


    /**
     * 经销商名称返回值
     */
    private String dealerLevelName;


    /**
     * 代充值图片
     */
    private String rechargePicture;









}






