package cn.exrick.xboot.modules.bussiness.applicationReturnGoodsOrder.mapper;

import cn.exrick.xboot.modules.bussiness.applicationReturnGoodsOrder.VO.AppliReturnGoodsVO;
import cn.exrick.xboot.modules.bussiness.applicationSendGoodsOrder.VO.AppliSendGoodsVO;
import cn.exrick.xboot.modules.bussiness.applicationSendGoodsOrder.pojo.ApplicationSendGoodsOrder;
import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import cn.exrick.xboot.modules.bussiness.applicationReturnGoodsOrder.pojo.ApplicationReturnGoodsOrder;
import com.baomidou.mybatisplus.core.toolkit.Constants;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * 申请退货表数据处理层
 * @author tqr
 */
@Repository
public interface ApplicationReturnGoodsOrderMapper extends BaseMapper<ApplicationReturnGoodsOrder> {

    @Select("select * from t_application_return_goods_order ${ew.customSqlSegment}")
    Page<AppliReturnGoodsVO> getAllList(@Param("page") Page page, @Param(Constants.WRAPPER)Wrapper wrapper);


    @Select("SELECT a.* FROM t_application_return_goods_order a INNER JOIN (SELECT create_by FROM t_application_return_goods_order GROUP BY create_by HAVING count(*) > 1 ) b ON a.create_by = b.create_by")
    List<ApplicationReturnGoodsOrder> get();
}