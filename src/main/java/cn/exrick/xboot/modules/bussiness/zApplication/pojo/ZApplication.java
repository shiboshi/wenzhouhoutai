package cn.exrick.xboot.modules.bussiness.zApplication.pojo;

import cn.exrick.xboot.base.XbootBaseEntity;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.Table;

/**
 * @author tqr
 */
@Data
@Entity
@Table(name = "t_z_application")
@TableName("t_z_application")
@ApiModel(value = "装修申请")
public class ZApplication extends XbootBaseEntity {

    private static final long serialVersionUID = 1L;

    /**
     * 附件
     */
    private String enclosu;

    /**
     * 备注
     */
    private String remarks;

    /**
     * 申请人id
     */
    private String userId;


    /**
     * 流程定义id
     */
    private String businessId;


    /**
     * 装修申请，审核状态
     */
    private String decorationStatus;


    /**
     * 流程id
     */
    @ApiModelProperty("流id")
    private String bussinessId;



    @ApiModelProperty("完成状态")
    private String completionStatus;







}