package cn.exrick.xboot.modules.bussiness.shiProductComparison.pojo;

import cn.exrick.xboot.base.XbootBaseEntity;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.Table;

/**
 * @author 石博士
 */
@Data
@Entity
@Table(name = "s_head_function")
@TableName("s_head_function")
@ApiModel(value = "头部功能")
public class head_function extends XbootBaseEntity {

    private static final long serialVersionUID = 1L;


    @ApiModelProperty("头靠调节,0没有，1有，2选配")
    private String head_adjustment;

    @ApiModelProperty("颈部热疗，0没有，1有，2选配")
    private String neck_h_t;

    @ApiModelProperty("颈部凸点按摩，0没有，1有，2选配")
    private String neck_bump_massage;

    @ApiModelProperty("颈部气压按摩，0没有，1有，2选配")
    private String heck_pressure_massage;

    @ApiModelProperty("中药香薰，0没有，1有，2选配")
    private String medicinal_aromatherapy;








}