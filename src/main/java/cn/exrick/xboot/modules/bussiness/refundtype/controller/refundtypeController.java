package cn.exrick.xboot.modules.bussiness.refundtype.controller;

import cn.exrick.xboot.common.utils.PageUtil;
import cn.exrick.xboot.common.utils.ResultUtil;
import cn.exrick.xboot.common.vo.PageVo;
import cn.exrick.xboot.common.vo.Result;
import cn.exrick.xboot.modules.bussiness.refundtype.pojo.refundtype;
import cn.exrick.xboot.modules.bussiness.refundtype.service.IrefundtypeService;
import com.baomidou.mybatisplus.core.metadata.IPage;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * @author 石博士
 */
@Slf4j
@RestController
@Api(description = "退款类型管理接口")
@RequestMapping("/xboot/refundtype")
@Transactional
public class refundtypeController {

    @Autowired
    private IrefundtypeService irefundtypeService;


    @RequestMapping(value = "/get/{id}", method = RequestMethod.POST)
    @ApiOperation(value = "通过id获取")
    public Result<refundtype> get(@PathVariable String id){

        refundtype refundtype = irefundtypeService.getById(id);
        return new ResultUtil<refundtype>().setData(refundtype);
    }


    @RequestMapping(value = "/getAll", method = RequestMethod.POST)
    @ApiOperation(value = "获取全部数据")
    public Result<List<refundtype>> getAll(){

        List<refundtype> list = irefundtypeService.list();
        return new ResultUtil<List<refundtype>>().setData(list);
    }


    @RequestMapping(value = "/getByPage", method = RequestMethod.POST)
    @ApiOperation(value = "分页获取")
    public Result<IPage<refundtype>> getByPage(@ModelAttribute PageVo page){

        IPage<refundtype> data = irefundtypeService.page(PageUtil.initMpPage(page));
        return new ResultUtil<IPage<refundtype>>().setData(data);
    }


    @RequestMapping(value = "/insertOrUpdate", method = RequestMethod.POST)
    @ApiOperation(value = "编辑或更新数据")
    public Result<refundtype> saveOrUpdate(@ModelAttribute refundtype refundtype){

        if(irefundtypeService.saveOrUpdate(refundtype)){
            return new ResultUtil<refundtype>().setData(refundtype);
        }
        return new ResultUtil<refundtype>().setErrorMsg("操作失败");
    }



    @RequestMapping(value = "/delAllByIds", method = RequestMethod.DELETE)
    @ApiOperation(value = "批量通过id删除")
    public Result<Object> delAllByIds(String id){
            irefundtypeService.removeById(id);
        return new ResultUtil<Object>().setSuccessMsg("删除数据成功");
    }
















}
