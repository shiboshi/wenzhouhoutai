package cn.exrick.xboot.modules.bussiness.classification.service;

import cn.exrick.xboot.modules.bussiness.classification.VO.ClassificationVo;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;
import cn.exrick.xboot.modules.bussiness.classification.pojo.Classification;

import java.util.List;

/**
 * 产品分类表接口
 * @author tqr
 */
public interface IClassificationService extends IService<Classification> {

    IPage<ClassificationVo> getByName(Page initMpPage, String name);

    /**
     * 通过父id获取
     * @param parentId
     * @return
     */
    List<Classification> findByParentIdOrderBySortOrder(String parentId);
}