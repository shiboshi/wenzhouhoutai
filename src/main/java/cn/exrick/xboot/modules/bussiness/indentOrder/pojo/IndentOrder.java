package cn.exrick.xboot.modules.bussiness.indentOrder.pojo;

import cn.exrick.xboot.base.XbootBaseEntity;
import cn.exrick.xboot.common.enums.CheckStatus;
import cn.exrick.xboot.common.enums.IsComplete;
import cn.exrick.xboot.common.enums.PayType;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.Table;

/**
 * @author xiaofei
 */
@Data
@Entity
@Table(name = "x_indent_order")
@TableName("x_indent_order")
@ApiModel(value = "订货单")
public class IndentOrder extends XbootBaseEntity {

    private static final long serialVersionUID = 1L;

    /**
     * 关联流程业务表id
     */
    private String bussinessId;
    /**
     * 订货单编号
     */
    private String orderNum;

    /**
     * 采购单ids
     */
    private String purchaseOrderIds;

    /**
     * 备注
     */
    private String remark;
    /**
     * 付款类型
     */
    private PayType payType;
    /**
     * 局部注解序列化
     */
    private IsComplete isComplete;

    /**
     * 审核状态
     */
    private CheckStatus checkStatus;

    /**
     * 支付类型
     */
    private String indentOrderType;

}