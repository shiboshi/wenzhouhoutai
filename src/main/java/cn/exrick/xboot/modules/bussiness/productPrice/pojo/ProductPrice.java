package cn.exrick.xboot.modules.bussiness.productPrice.pojo;

import cn.exrick.xboot.base.XbootBaseEntity;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModelProperty;
import io.swagger.annotations.ApiModel;
import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.Table;
import java.math.BigDecimal;

/**
 * @author tqr
 */
@Data
@Entity
@Table(name = "t_product_price")
@TableName("t_product_price")
@ApiModel(value = "产品价格表")
public class ProductPrice extends XbootBaseEntity {

    private static final long serialVersionUID = 1L;

    /**
     * 产品编码Id
     */
    private String productOrderId;

    /**
     * 经销商等级
     */
    private String levelId;

    /**
     * 产品价格
     */
    private BigDecimal productPrice;
}