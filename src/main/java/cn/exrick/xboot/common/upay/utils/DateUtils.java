package cn.exrick.xboot.common.upay.utils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * 时间工具类
 */
public class DateUtils {
	public final static String YYYY = "yyyy";
	public final static String SSSS = "SSSS";
	public final static String YYYY_MM_DD = "yyyy-MM-dd";
	public final static String MMDDHHMMSS = "MMddHHmmss";
	public final static String YYYYMMDDHHMMSS = "yyyyMMddHHmmss";
	public final static String YYYY_MM_DD_HH_MM_SS = "yyyy-MM-dd HH:mm:ss";

	public static String formatNow(String pattern) {
		SimpleDateFormat sdf = new SimpleDateFormat(pattern);
		return sdf.format(new Date());
	}

	public static String format(Date date, String pattern) {
		return new SimpleDateFormat(pattern).format(date);
	}

	public static Date time(String time, String type) throws ParseException {
		return new SimpleDateFormat(type).parse(time);
	}
}
