package cn.exrick.xboot.modules.bussiness.bothSale.serviceImp;

import cn.exrick.xboot.common.constant.ActivitiConstant;
import cn.exrick.xboot.common.enums.CheckStatus;
import cn.exrick.xboot.common.enums.Order;
import cn.exrick.xboot.common.exception.XbootException;
import cn.exrick.xboot.common.utils.*;
import cn.exrick.xboot.common.vo.Result;
import cn.exrick.xboot.modules.activiti.entity.ActBusiness;
import cn.exrick.xboot.modules.activiti.entity.ActNode;
import cn.exrick.xboot.modules.activiti.service.ActBusinessService;
import cn.exrick.xboot.modules.activiti.service.ActNodeService;
import cn.exrick.xboot.modules.activiti.service.ActProcessService;
import cn.exrick.xboot.modules.activiti.service.mybatis.IHistoryIdentityService;
import cn.exrick.xboot.modules.activiti.utils.MessageUtil;
import cn.exrick.xboot.modules.activiti.vo.ProcessNodeVo;
import cn.exrick.xboot.modules.base.dao.mapper.UserMapper;
import cn.exrick.xboot.modules.base.entity.User;
import cn.exrick.xboot.modules.bussiness.address.mapper.AddressMapper;
import cn.exrick.xboot.modules.bussiness.address.pojo.Address;
import cn.exrick.xboot.modules.bussiness.bothSale.mapper.BothSaleMapper;
import cn.exrick.xboot.modules.bussiness.bothSale.pojo.BothSale;
import cn.exrick.xboot.modules.bussiness.bothSale.service.IBothSaleService;
import cn.exrick.xboot.modules.bussiness.dealerLevel.VO.DealerLevelVO;
import cn.exrick.xboot.modules.bussiness.dealerLevel.mapper.DealerLevelMapper;
import cn.exrick.xboot.modules.bussiness.inventoryOrder.VO.BothSaleVO;
import cn.exrick.xboot.modules.bussiness.inventoryOrder.mapper.InventoryOrderMapper;
import cn.exrick.xboot.modules.bussiness.inventoryOrder.pojo.InventoryOrder;
import cn.exrick.xboot.modules.bussiness.product.VO.ProductCommon;
import cn.exrick.xboot.modules.bussiness.product.service.IproductOrderService;
import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import lombok.extern.slf4j.Slf4j;
import org.activiti.engine.TaskService;
import org.activiti.engine.task.Task;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.transaction.interceptor.TransactionAspectSupport;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

/**
 * 两地销售接口实现
 *
 * @author tqr
 */
@Slf4j
@Service
@Transactional
public class IBothSaleServiceImpl extends ServiceImpl<BothSaleMapper, BothSale> implements IBothSaleService {

    @Autowired
    private BothSaleMapper bothSaleMapper;
    @Autowired
    private SecurityUtil securityUtil;
    @Autowired
    private ActBusinessService actBusinessService;
    @Autowired
    private IproductOrderService iproductOrderService;
    @Autowired
    private ActProcessService actProcessService;
    @Autowired
    private ActNodeService actNodeService;
    @Autowired
    private AddressMapper addressMapper;
    @Autowired
    private DealerLevelMapper dealerLevelMapper;
    @Autowired
    private UserMapper userMapper;
    @Autowired
    private InventoryOrderMapper inventoryOrderMapper;

    @Override
    public Result<BothSale> insertBothSaleCommit(BothSaleVO bothSaleVO, String procDefId) {
        Result<BothSale> result = new Result<>();
        BothSale bothSale = new BothSale();
        try {
            ToolUtil.copyProperties(bothSaleVO, bothSale);

            if (ToolUtil.isNotEmpty(securityUtil.getCurrUser())) {
                bothSale.setCreateBy(securityUtil.getCurrUser().getId());
            }
            bothSale.setDelFlag(0);
            bothSale.setCheckCode(CheckStatus.pengindg);
            bothSale.setCreateTime(new Date());
            bothSale.setOrderNumb(CommonUtil.createOrderSerial(Order.LD_));

//            String[] ltl = bothSaleVO.getLongitudeLatitude().split(",");
//            if (ltl.length >= 2) {
//                bothSale.setLongitude(ltl[0]);
//                bothSale.setLatitude(ltl[1]);
//            }
            bothSaleMapper.insert(bothSale);

            //获取最新插入的一条数据
            BothSale last = this.getOne(Wrappers.<BothSale>lambdaQuery()
                    .orderByDesc(BothSale::getCreateTime)
                    .last("limit 1"));
            ActBusiness actBusiness = new ActBusiness();
            actBusiness.setUserId(securityUtil.getCurrUser().getId());
            actBusiness.setTableId(last.getId());
            actBusiness.setProcDefId(procDefId);
            ActBusiness actBusinessNew = actBusinessService.save(actBusiness);

            //找到最新插入的数据
            last.setBusinessId(actBusinessNew.getId());
            this.updateById(last);
            result.setSuccess(true);
            result.setCode(200);
            result.setMessage("添加成功");
            result.setResult(bothSale);
        } catch (Exception e) {
            result.setCode(500);
            result.setMessage("添加失败");
        }
        return result;
    }

    @Override
    public IPage<BothSaleVO> getBothSaleCommit(Page initMpPage, String userName, String id) {
        IPage<BothSaleVO> list = bothSaleMapper.getBothSaleCommit(initMpPage, userName, id);
        return list;
    }

    @Override
    public Result<BothSale> insertBothSale(String id, String procDefId, String productIds) {
        Result<BothSale> result = new Result<>();
        try {
            BothSale bothSale = bothSaleMapper.selectById(id);
            if (ToolUtil.isNotEmpty(bothSale)) {
                if (ToolUtil.isNotEmpty(securityUtil.getCurrUser())) {
                    bothSale.setUpdateBy(securityUtil.getCurrUser().getId());
                }
                bothSale.setProductOrderIds(productIds);
                bothSale.setDelFlag(0);
                bothSale.setUpdateTime(new Date());
                bothSale.setCheckCodeDealer(CheckStatus.pengindg);

                bothSaleMapper.updateById(bothSale);

                //获取最新插入的一条数据
                BothSale last = this.getOne(Wrappers.<BothSale>lambdaQuery()
                        .orderByDesc(BothSale::getUpdateTime)
                        .last("limit 1"));
                ActBusiness actBusiness = new ActBusiness();
                actBusiness.setUserId(securityUtil.getCurrUser().getId());
                actBusiness.setTableId(last.getId());
                ActBusiness actBusinessNew = actBusinessService.save(actBusiness);
                String[] dealer = new String[1];
                if (ToolUtil.isNotEmpty(bothSale.getDealerDeptId())) {
                    dealer[0] = bothSale.getDealerDeptId();
                }
                actBusiness.setAssignees(dealer);
                actBusiness.setProcDefId(procDefId);
                //找到最新插入的数据
                last.setBusinessId(actBusinessNew.getId());
                this.updateById(last);
//开启流程
                ActBusiness actBusiness1 = actBusinessService.get(actBusiness.getId());
                actBusiness.setTableId(actBusiness1.getTableId());
                // 根据你的业务需求放入相应流程所需变量
                actBusiness = putParams(actBusiness);
                String processInstanceId = actProcessService.startProcess(actBusiness);
                actBusiness1.setProcDefId(actBusiness.getProcDefId());
                actBusiness1.setTitle(actBusiness.getTitle());
                actBusiness1.setProcInstId(processInstanceId);
                actBusiness1.setStatus(ActivitiConstant.STATUS_DEALING);
                actBusiness1.setResult(ActivitiConstant.RESULT_DEALING);
                actBusiness1.setApplyTime(new Date());
                actBusinessService.update(actBusiness1);

                //配置流程下一节点
                ProcessNodeVo processNodeVo = actProcessService.getFirstNode(actBusiness1.getProcDefId());
                actNodeService.deleteByNodeId(processNodeVo.getId());
                ActNode actNode = new ActNode();
                actNode.setNodeId(processNodeVo.getId());
                actNode.setRelateId(bothSale.getDealerDeptId());
                actNode.setType(ActivitiConstant.NODE_USER);
                actNodeService.save(actNode);

                result.setCode(200);
                result.setMessage("添加成功");
                result.setSuccess(true);
                result.setResult(bothSale);
            }
        } catch (Exception e) {
            // 手动回滚事物
            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
            result.setCode(500);
            result.setMessage("添加失败");
        }
        return result;
    }

    /**
     * 放入相应流程所需变量
     *
     * @param act
     * @return
     */
    public ActBusiness putParams(ActBusiness act) {

        if (StrUtil.isBlank(act.getTableId())) {
            throw new XbootException("关联业务表TableId不能为空");
        }
        //
        // 放入变量

        return act;
    }

    @Override
    public IPage<BothSaleVO> getBothSale(Page initMpPage, String userName, String id) {
        IPage<BothSaleVO> list = bothSaleMapper.getBothSale(initMpPage, userName, id);
        List<BothSaleVO> bothSaleVOS = list.getRecords();
        if (bothSaleVOS.size() > 0) {
            for (BothSaleVO b : bothSaleVOS) {
                Address p = addressMapper.selectById(b.getAddressId());
                Address c = addressMapper.selectById(p.getParentId());
                String address = p.getTitle() + c.getTitle() + b.getAddress();
                b.setAddress(address);
            }
        }
        list.setRecords(bothSaleVOS);
        return list;
    }

    @Override
    public Result<BothSale> updateById(BothSaleVO bothSaleVO) {
        Result<BothSale> result = new Result<>();
        BothSale bothSale = new BothSale();
        try {
            ToolUtil.copyProperties(bothSaleVO, bothSale);

            if (ToolUtil.isNotEmpty(securityUtil.getCurrUser())) {
                bothSale.setUpdateBy(securityUtil.getCurrUser().getId());
            }
            bothSale.setDelFlag(0);
            bothSale.setUpdateTime(new Date());

            bothSaleMapper.updateById(bothSale);
            result.setCode(200);
            result.setMessage("修改成功");
            result.setResult(bothSale);
        } catch (Exception e) {
            result.setCode(500);
            result.setMessage("修改失败");
        }
        return result;
    }

    @Override
    public IPage<BothSaleVO> getBothSaleCommitByUser(Page initMpPage, String id, String code) {

        String userId = null;
        userId = securityUtil.getCurrUser().getId();
        IPage<BothSaleVO> list = bothSaleMapper.getBothSaleCommitByUser(initMpPage, userId, id, code);
        if ("2".equals(code)) {
            List<BothSaleVO> list1 = list.getRecords();
            list1 = list1.stream().filter(l -> ToolUtil.isEmpty(l.getProductOrderIds())).collect(Collectors.toList());
            list.setRecords(list1);
        }
        return list;
    }

    @Override
    public IPage<BothSaleVO> getBothSaleByUser(Page initMpPage, String id, String code) {
        String userId = null;
        userId = securityUtil.getCurrUser().getId();
        IPage<BothSaleVO> list = bothSaleMapper.getBothSaleByUser(initMpPage, userId, id, code);
        return list;
    }

    @Override
    public List<ProductCommon> getBothSaleDetail(Page initMpPage, String id) {
        BothSale bothSale = bothSaleMapper.selectById(id);
        List<ProductCommon> list = new ArrayList<>();
        if (ToolUtil.isNotEmpty(bothSale)) {
            String[] ids = bothSale.getProductOrderIds().split(",");
            if (ToolUtil.isNotEmpty(ids)) {
                for (String i : ids) {
                    InventoryOrder inventoryOrder = inventoryOrderMapper.selectById(i);
                    if (ToolUtil.isNotEmpty(inventoryOrder)) {
                        ProductCommon productCommon = iproductOrderService.getProductCommon(inventoryOrder.getProductOrderId());
                        if (ToolUtil.isNotEmpty(productCommon)) {
                            list.add(productCommon);
                        }
                    }
                }
            }
            return list;
        }
        return null;
    }

    @Override
    public void choose(String id, String dealerDeptId, String procInstId) {
        //查询
        BothSale bothSale = bothSaleMapper.selectById(id);
        if (ToolUtil.isNotEmpty(bothSale)) {
            bothSale.setDealerDeptId(dealerDeptId);
        }
        //配置流程下一节点
        ProcessNodeVo processNodeVo = actProcessService.getNextNode(procInstId);
        if (ToolUtil.isNotEmpty(processNodeVo)) {
            actNodeService.deleteByNodeId(processNodeVo.getId());
            ActNode actNode = new ActNode();
            actNode.setNodeId(processNodeVo.getId());
            actNode.setRelateId(dealerDeptId);
            actNode.setType(ActivitiConstant.NODE_DEPARTMENT);
            actNodeService.save(actNode);
        }
    }

    @Override
    public List<User> getDealer(Page page, String id) {
        String name = null;
        IPage<DealerLevelVO> list = dealerLevelMapper.getDealer(page, name);
        List<DealerLevelVO> dealerLevelVOS = list.getRecords();
        List<User> users = new ArrayList<>();
        if (ToolUtil.isNotEmpty(dealerLevelVOS)) {
            //查询两地销售申请
            BothSale bothSale = bothSaleMapper.selectById(id);
            dealerLevelVOS = dealerLevelVOS.stream().filter(d -> d.getAreaId().equals(bothSale.getAddressId())).collect(Collectors.toList());
            for (DealerLevelVO d : dealerLevelVOS) {
                List<User> userList = userMapper.selectList(new QueryWrapper<User>().lambda().eq(User::getDepartmentId, d.getDeptId()));
                users.addAll(userList);
            }
        }
        return users;
    }


}