package cn.exrick.xboot.modules.bussiness.transferOrder.pojo;

import cn.exrick.xboot.base.XbootBaseEntity;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModelProperty;
import io.swagger.annotations.ApiModel;
import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.Table;

/**
 * @author fei
 */
@Data
@Entity
@Table(name = "x_transfer_order")
@TableName("x_transfer_order")
@ApiModel(value = "调拨单")
public class TransferOrder extends XbootBaseEntity {

    private static final long serialVersionUID = 1L;

    /**
     * 库存ids
     */
    private String inventoryIds;

    /**
     * 转移到仓库id
     */
    private String warhouseId;

    private String oldwarhouseId;

    /**
     * 调拨单号
     */
    private String transferOrderNum;

    /**
     * 确认收货状态 0:未确认 1：已确认
     */
    private Integer status;

}