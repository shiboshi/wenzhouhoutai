package cn.exrick.xboot.modules.bussiness.applicationSendGoodsOrder.VO;

import lombok.Data;

import java.math.BigDecimal;

/**
 * @Author : xiaofei
 * @Date: 2019/8/25
 */
@Data
public class SendDistinctVO {

    private String id;
    private BigDecimal num;
}
