package cn.exrick.xboot.modules.bussiness.product.service;

import cn.exrick.xboot.common.vo.Result;
import cn.exrick.xboot.modules.bussiness.product.VO.ProductCommon;
import cn.exrick.xboot.modules.bussiness.product.VO.ProductOrderExtend;
import cn.exrick.xboot.modules.bussiness.product.VO.ProductOrderVo;
import cn.exrick.xboot.modules.bussiness.unit.VO.UnitVO;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;
import cn.exrick.xboot.modules.bussiness.product.pojo.ProductOrder;

import java.util.List;


/**
 * 产品型号接口
 * @author xiaofei
 */
public interface IproductOrderService extends IService<ProductOrder> {

    /**
     * 获取产品品号的公共信息
     * @param productOrderId
     * @return
     */
    ProductCommon getProductCommon(String productOrderId);

    IPage<ProductOrderVo> getAllProduct(Page page,String productModelName);

    List<ProductCommon> getAllProductByDist(String productOrderName,Integer code);

    ProductOrderVo getAllProductDetail(String id);

    IPage<UnitVO> getProductUnit(Page page,String id);

    Result<ProductOrder> insertProduct(ProductOrderExtend productOrder);

    Result<ProductOrder> updateProduct(ProductOrderExtend productOrder);

    List<ProductCommon> getIndentProduct(String id);
}