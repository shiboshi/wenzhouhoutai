package cn.exrick.xboot.modules.UpayApi.test.qrcode;


import org.apache.commons.codec.digest.DigestUtils;

import java.io.UnsupportedEncodingException;


public class UnionPayH5Test{
	
	public static void main(String[] args) {
		String sign = "instMid=QRPAYDEFAULT&mid=898340149000005&msgSrc=WWW.TEST.COM&msgType=bills.getQRCode&requestTimestamp=2018-11-26 15:20:06&tid=88880001";
		String key ="fcAmtnx7MwismjWNhNKdHC44mNXtnEQeJkRrhKJwyrW2ysRR";

        String signText = DigestUtils.md5Hex(getContentBytes(sign + key)).toUpperCase();
        System.out.println("signText：" + signText);

	}


    public static byte[] getContentBytes(String content) {
        try {
            return content.getBytes("UTF-8");
        } catch (UnsupportedEncodingException e) {
            throw new RuntimeException("签名过程中出现错误");
        }
    }
	
}
