package cn.exrick.xboot.modules.bussiness.classification.serviceImp;

import cn.exrick.xboot.modules.bussiness.classification.VO.ClassificationVo;
import cn.exrick.xboot.modules.bussiness.classification.mapper.ClassificationMapper;
import cn.exrick.xboot.modules.bussiness.classification.pojo.Classification;
import cn.exrick.xboot.modules.bussiness.classification.service.IClassificationService;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.sql.Wrapper;
import java.util.ArrayList;
import java.util.List;

/**
 * 产品分类表接口实现
 * @author tqr
 */
@Slf4j
@Service
@Transactional
public class IClassificationServiceImpl extends ServiceImpl<ClassificationMapper, Classification> implements IClassificationService {

    @Autowired
    private ClassificationMapper classificationMapper;

    @Override
    public IPage<ClassificationVo> getByName(Page initMpPage, String name) {
        IPage<ClassificationVo> list = classificationMapper.selectPage(initMpPage,new QueryWrapper<Classification>().like("name",name));
        return list;
    }

    @Override
    public List<Classification> findByParentIdOrderBySortOrder(String parentId) {
        return classificationMapper.findByParentIdOrderBySortOrder(parentId);
    }
}