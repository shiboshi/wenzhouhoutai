package cn.exrick.xboot.modules.bussiness.afterSaleOrder.pojo;

import cn.exrick.xboot.base.XbootBaseEntity;
import cn.exrick.xboot.common.enums.CheckStatus;
import cn.exrick.xboot.common.enums.IsComplete;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import lombok.Data;
import lombok.experimental.Accessors;

import javax.persistence.Entity;
import javax.persistence.Table;
import java.math.BigDecimal;
import java.util.Date;

/**
 * @author fei
 */
@Data
@Entity
@Table(name = "x_after_sale_order")
@TableName("x_after_sale_order")
@ApiModel(value = "销售单维修单")
@Accessors(chain = true)
public class AfterSaleOrder extends XbootBaseEntity {

    private static final long serialVersionUID = 1L;

    /**
     * 消费者姓名
     */
    private String consumerName;

    /**
     * 联系方式
     */
    private String consumerPhone;

    /**
     * 售后地址
     */
    private String consumerAddress;


    /**
     * 序列号，分割
     */
    private String serials;


    /**
     * 维修人员id
     */
    private String userId;

    /**
     * 售后维修人员的联系方式
     */
    private String maintainerPhone;

    /**
     * 预计到场时间
     */
    private Date predictArrivalTime;


    /**
     * 维修前图片
     */
    private String beforePic;

    /**
     * 售后维修图片
     */
    private String afterPic;

    /**
     * 保修时间
     */
    private Date  warrantyPeriod;
    /**
     * 收据费用
     */
    private BigDecimal cost;
    /**
     * 配件id
     */
    private String parets;

    /**
     * 评论信息
     */
    private String comment;

    /**
     * 销售订单id
     */
    private String saleOrderId;

    /**
     * 流程定义id
     */
    private String bussinessId;
    /**
     * 审核状态
     */
    private CheckStatus checkStatus;
    /**
     * 订单完成状态
     */
    private IsComplete isComplete;

}