package cn.exrick.xboot.common.factory;

/**
 * @Author : xiaofei
 * @Date: 2019/8/9
 */
public interface IConstantFactory {


    /**userId
     * 获取当前登录者姓名
     * @return
     */
    String getUserName(String userId);

    /**
     * 获取当前登录者等级
     * @param userId
     * @return
     */
    String getUserLevelName(String userId);


    /**
     * 获取当前登陆者部门名称
     * @param userId
     * @return
     */
    String getUserDeptName(String userId);
}
