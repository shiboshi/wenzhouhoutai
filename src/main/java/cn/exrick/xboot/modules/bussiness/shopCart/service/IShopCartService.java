package cn.exrick.xboot.modules.bussiness.shopCart.service;

import com.baomidou.mybatisplus.extension.service.IService;
import cn.exrick.xboot.modules.bussiness.shopCart.pojo.ShopCart;

import java.util.List;

/**
 * 购物车接口
 * @author tqr
 */
public interface IShopCartService extends IService<ShopCart> {

}