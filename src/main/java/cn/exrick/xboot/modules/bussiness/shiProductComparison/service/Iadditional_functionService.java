package cn.exrick.xboot.modules.bussiness.shiProductComparison.service;

import com.baomidou.mybatisplus.extension.service.IService;
import cn.exrick.xboot.modules.bussiness.shiProductComparison.pojo.additional_function;

import java.util.List;

/**
 * 附加功能表接口
 * @author 石博士
 */
public interface Iadditional_functionService extends IService<additional_function> {

}