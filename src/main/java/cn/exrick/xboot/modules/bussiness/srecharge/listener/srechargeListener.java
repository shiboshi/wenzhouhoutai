package cn.exrick.xboot.modules.bussiness.srecharge.listener;

import cn.exrick.xboot.common.utils.ToolUtil;
import cn.exrick.xboot.config.springContext.SpringContextHolder;
import cn.exrick.xboot.modules.activiti.service.ActBusinessService;
import cn.exrick.xboot.modules.activiti.service.ActProcessService;
import cn.exrick.xboot.modules.base.entity.Department;
import cn.exrick.xboot.modules.base.entity.User;
import cn.exrick.xboot.modules.base.service.IDepartmentService;
import cn.exrick.xboot.modules.base.service.IUserService;
import cn.exrick.xboot.modules.bussiness.dealerLevel.pojo.DealerLevel;
import cn.exrick.xboot.modules.bussiness.dealerLevel.service.IDealerLevelService;
import cn.exrick.xboot.modules.bussiness.srecharge.pojo.Srecharge;
import cn.exrick.xboot.modules.bussiness.srecharge.service.ISrechargeService;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import org.activiti.engine.delegate.DelegateExecution;
import org.activiti.engine.delegate.ExecutionListener;

import java.math.BigDecimal;

public class srechargeListener implements ExecutionListener {


    @Override
    public void notify(DelegateExecution delegateExecution) throws Exception {

        String tableId = (String) delegateExecution.getVariable("tableId");
        ISrechargeService iSrechargeService = SpringContextHolder.getBean(ISrechargeService.class);
        //经销商service
        IDealerLevelService iDealerLevelService = SpringContextHolder.getBean(IDealerLevelService.class);
        //用户service
        IUserService iUserService = SpringContextHolder.getBean(IUserService.class);
        //部门service
        IDepartmentService iDepartmentService = SpringContextHolder.getBean(IDepartmentService.class);

        ActProcessService actProcessService = SpringContextHolder.getBean(ActProcessService.class);
        ActBusinessService actBusinessService = SpringContextHolder.getBean(ActBusinessService.class);
        Srecharge srecharge = iSrechargeService.getById(tableId);
        if(delegateExecution.getEventName().equals("end")){
           if(ToolUtil.isNotEmpty(srecharge)){
               Long srechargestatus = new Long((long)1);
               srecharge.setSrechargeStatus(srechargestatus);
               srecharge.setCompletionStatus("1");
               iSrechargeService.updateById(srecharge);

               //通过退款申请人的id,查询出用户表的部门id
               User user = iUserService.getById(srecharge.getUserId());

               //通过用户表的部门id，查出部门
               Department department = iDepartmentService.getById(user.getDepartmentId());

               QueryWrapper<DealerLevel> queryWrapper = new QueryWrapper<>();
               queryWrapper.eq("dept_id", department.getId());
               //根据部门表的id查询经销商
               DealerLevel d = iDealerLevelService.getOne(queryWrapper);

               //代充值的金额
               BigDecimal number = new BigDecimal(srecharge.getNumber().toString());

               //如果选择的类型是信用额度调整，就把钱冲到经销商的额度里面
               if(srecharge.getType().equals("182334009580195840")){
                   //查询经销商原有的额度
                   BigDecimal credit = new BigDecimal(d.getCredit().toString());
                   //计算总金额
                   BigDecimal credits = number.add(credit);
                   d.setCredit(credits);
                   iDealerLevelService.updateById(d);
               }else{
                   //查询经销商原有的金额
                   BigDecimal balance = new BigDecimal(d.getBalance().toString());
                   //计算总金额
                   BigDecimal balances = number.add(balance);
                   d.setBalance(balances);
                   iDealerLevelService.updateById(d);
               }


               //把自己代充值的金额减掉
//               BigDecimal numbers = new BigDecimal(srecharge.getNumber().toString());
//               BigDecimal balances2 = number.subtract(numbers);
//               srecharge.setNumber(balances2);
//               //再次更新自己相减的值
//               iSrechargeService.updateById(srecharge);

           }
        }




















    }
}
