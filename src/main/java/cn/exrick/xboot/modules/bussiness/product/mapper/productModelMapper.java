package cn.exrick.xboot.modules.bussiness.product.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import cn.exrick.xboot.modules.bussiness.product.pojo.ProductModel;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

/**
 * 产品品号数据处理层
 * @author xiaofei
 */
@Repository
public interface productModelMapper extends BaseMapper<ProductModel> {

}