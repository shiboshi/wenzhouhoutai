package cn.exrick.xboot.modules.bussiness.afterPurchase.serviceImp;

import cn.exrick.xboot.common.utils.ToolUtil;
import cn.exrick.xboot.modules.bussiness.afterIndentOrder.pojo.AfterIndentOrder;
import cn.exrick.xboot.modules.bussiness.afterIndentOrder.service.IafterIndentOrderService;
import cn.exrick.xboot.modules.bussiness.afterPurchase.VO.AfterPurchaseVO;
import cn.exrick.xboot.modules.bussiness.afterPurchase.mapper.afterPurchaseMapper;
import cn.exrick.xboot.modules.bussiness.afterPurchase.pojo.AfterPurchase;
import cn.exrick.xboot.modules.bussiness.afterPurchase.service.IafterPurchaseService;
import cn.exrick.xboot.modules.bussiness.product.VO.ProductCommon;
import cn.exrick.xboot.modules.bussiness.product.VO.ProductOrderVo;
import cn.exrick.xboot.modules.bussiness.product.service.IproductOrderService;
import cn.exrick.xboot.modules.bussiness.purchaseOrder.pojo.PurchaseOrder;
import cn.exrick.xboot.modules.bussiness.troubleType.service.ITroubleTypeService;
import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.bean.copier.CopyOptions;
import cn.hutool.core.bean.copier.ValueProvider;
import cn.hutool.core.thread.ThreadUtil;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.google.common.base.Joiner;
import com.google.common.base.Splitter;
import com.google.common.collect.Lists;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.thymeleaf.expression.Ids;

import java.lang.reflect.Type;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Future;
import java.util.stream.Collectors;

/**
 * 售后单清单接口实现
 * @author xiaofei
 */
@Slf4j
@Service
@Transactional
public class IafterPurchaseServiceImpl extends ServiceImpl<afterPurchaseMapper, AfterPurchase> implements IafterPurchaseService {


    @Autowired
    private IafterIndentOrderService afterIndentOrderService;
    @Autowired
    private IproductOrderService productOrderService;
    @Autowired
    private ITroubleTypeService troubleTypeService;


    public  static  final ExecutorService EXECUTOR_SERVICE = ThreadUtil.newExecutor(10);
    public  static  final Joiner JOINER = Joiner.on(",").skipNulls();
    public  static  final Splitter SPLITTER = Splitter.on(",").trimResults().omitEmptyStrings();


    /**
     * 插入采购单消费者任务
     */
    @Data
    class providerInsert implements Callable<String>{

        @Autowired
        private IafterPurchaseService afterPurchaseService;
        private AfterPurchase afterPurchase;
        public providerInsert(AfterPurchase afterPurchase) {
            this.afterPurchase = afterPurchase;
        }
        @Override
        public String call() throws Exception {
            AfterPurchase afterPurchaseUse = new AfterPurchase();
            ToolUtil.copyProperties(afterPurchase,afterPurchaseUse);
            afterPurchaseService.save(afterPurchase);

            //获取最新的一条数据
            AfterPurchase afterPurchaseNew = afterPurchaseService
                    .getOne(Wrappers.<AfterPurchase>lambdaQuery()
                    .orderByDesc(AfterPurchase::getCreateTime)
                    .last("limit 1"));
            Thread.sleep(2000);
            return afterPurchaseNew.getId();
        }
    }



    @Override
    public String insertAfterPurchase(List<AfterPurchase> afterPurchase) {

        if (ToolUtil.isNotEmpty(afterPurchase)){
          List<String> ids =  afterPurchase.stream().map(purchase -> {

               log.info("/*--------线程开启-----------*/");
                Future id = EXECUTOR_SERVICE.submit(new providerInsert(purchase));
                return id.toString();
            }).collect(Collectors.toList());
            return  JOINER.join(ids);
        }else {
            return  "";
        }
    }


    /**
     * 获取售后表详情
     * @param afterIndentId
     * @return
     */
    @Override
    public List<AfterPurchaseVO> getList(String afterIndentId) {

        AfterIndentOrder afterIndentOrder = afterIndentOrderService.getById(afterIndentId);
        if (ToolUtil.isNotEmpty(afterIndentOrder)){
            List<AfterPurchase> afterPurchases = this.list(Wrappers.<AfterPurchase>lambdaQuery()
            .in(AfterPurchase::getId,SPLITTER.split(afterIndentOrder.getApplicationAfterSaleOrderId())));
            if (ToolUtil.isNotEmpty(afterPurchases)){
                List<AfterPurchaseVO> afterPurchaseVOS = afterPurchases.stream()
                        .map(purchase ->{
                            ProductCommon productCommon = productOrderService.getProductCommon(purchase.getProductOrderId());
                 AfterPurchaseVO afterPurchaseVO = BeanUtil.fillBean(new AfterPurchaseVO(), new ValueProvider<String>() {
                     @Override
                     public Object value(String s, Type type) {
                         switch (s){
                             case "id":
                                 return purchase.getId();
                             case "productName":
                                 return  productCommon.getProductName();
                             case "productModelName":
                                 return  productCommon.getProductModelName();
                             case "productOrderName":
                                 return  productCommon.getProductOrderName();
                             case "saleType":
                                 return  purchase.getAfterSaleType().getDesc();
                             case "amount":
                                 return  purchase.getAmount();
                             case "reason":
                                 return  purchase.getReason();
                             case "troubleTypeId":
                                 if (ToolUtil.isNotEmpty(troubleTypeService.getById(purchase.getTroubleTypeId()))){
                                     return troubleTypeService.getById(purchase.getTroubleTypeId()).getTroubleType();
                                 }
                             case "picBefore":
                                 return purchase.getPicBefore();
                             case "picAfter":
                                 return purchase.getPicAfter();
                         }
                         return null;
                     }

                     //是否总是存在key
                     @Override
                     public boolean containsKey(String s) {
                         return true;
                     }
                 }, CopyOptions.create());

                 return  afterPurchaseVO;
                }).collect(Collectors.toList());

               return afterPurchaseVOS;
            }
        }
        return null;
    }
}