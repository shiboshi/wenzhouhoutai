package cn.exrick.xboot.modules.bussiness.appliSendNum.serviceImp;

import cn.exrick.xboot.common.utils.SecurityUtil;
import cn.exrick.xboot.common.utils.ToolUtil;
import cn.exrick.xboot.modules.bussiness.appliSendNum.mapper.appliSendNumMapper;
import cn.exrick.xboot.modules.bussiness.appliSendNum.pojo.AppliSendNum;
import cn.exrick.xboot.modules.bussiness.appliSendNum.service.IappliSendNumService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * 申请发货接口实现
 * @author fei
 */
@Slf4j
@Service
@Transactional
public class IappliSendNumServiceImpl extends ServiceImpl<appliSendNumMapper, AppliSendNum> implements IappliSendNumService {

    @Autowired
    private SecurityUtil securityUtil;

    @Override
    public void insert(String applicationSendOrderId, String purchaseId,Long num) {
        if (!ToolUtil.isOneEmpty(applicationSendOrderId,purchaseId)){
            AppliSendNum appliSendNum = new AppliSendNum();
            appliSendNum.setSendNum(num)
                    .setApplicationSendOrderId(applicationSendOrderId)
                    .setPurchaseId(purchaseId);
            this.save(appliSendNum);
        }
    }
}