package cn.exrick.xboot.modules.bussiness.product.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import cn.exrick.xboot.modules.bussiness.product.pojo.Product;
import io.swagger.annotations.ApiModel;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

/**
 * 产品大类数据处理层
 * @author xiaofei
 */
@Repository
public interface productMapper extends BaseMapper<Product> {

}