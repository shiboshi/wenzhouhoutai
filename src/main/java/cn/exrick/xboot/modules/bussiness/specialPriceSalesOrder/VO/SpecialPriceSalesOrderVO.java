package cn.exrick.xboot.modules.bussiness.specialPriceSalesOrder.VO;

import cn.exrick.xboot.common.enums.CheckStatus;
import cn.exrick.xboot.common.vo.CommonVO;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
public class SpecialPriceSalesOrderVO extends CommonVO {

    @ApiModelProperty(value = "特价商品采购单id")
    private String purchaseOrderIds;

    @ApiModelProperty(value = "业务流程表id")
    private String businessId;

    @ApiModelProperty(value = "申请人凭证")
    private String phoneNumber;

    private String checkStatus;
}
