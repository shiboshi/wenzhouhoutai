package cn.exrick.xboot.modules.bussiness.shiProductComparison.pojo;

import cn.exrick.xboot.base.XbootBaseEntity;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.Table;

/**
 * @author 石博士
 */
@Data
@Entity
@Table(name = "s_back_function")
@TableName("s_back_function")
@ApiModel(value = "背部功能")
public class back_function extends XbootBaseEntity {

    private static final long serialVersionUID = 1L;



    @ApiModelProperty("机械手，手工输入")
    private String manipulator;

    @ApiModelProperty("扭腰功能，0没有，1有，2选配")
    private String twist_the_waist;

    @ApiModelProperty("背部振动，0没有，1有，2选配")
    private String back_vibration;

    @ApiModelProperty("背部气压，手工输入")
    private String back_air_pressure;

    @ApiModelProperty("顶腰功能，0没有，1有，2选配")
    private String parietal;

    @ApiModelProperty("热疗功能，0没有，1有，2选配")
    private String hyperthermic;

    @ApiModelProperty("靠背升降，0没有，1有，2选配")
    private String back_lift;

    @ApiModelProperty("导轨（L/S），0表示L，1表示S")
    private String guideway;

    @ApiModelProperty("腰部牵引，0没有，1有，2选配")
    private String lumbar_traction;

    @ApiModelProperty("背部磁疗，手工输入")
    private String magnetotherapy;








}