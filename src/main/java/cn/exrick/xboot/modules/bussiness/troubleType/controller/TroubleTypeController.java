package cn.exrick.xboot.modules.bussiness.troubleType.controller;

import cn.exrick.xboot.common.utils.PageUtil;
import cn.exrick.xboot.common.utils.ResultUtil;
import cn.exrick.xboot.common.vo.PageVo;
import cn.exrick.xboot.common.vo.Result;
import cn.exrick.xboot.modules.bussiness.troubleType.pojo.TroubleType;
import cn.exrick.xboot.modules.bussiness.troubleType.service.ITroubleTypeService;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * @author tqr
 */
@Slf4j
@RestController
@Api(description = "问题类型表管理接口")
@RequestMapping("/xboot/troubleType")
@Transactional
public class TroubleTypeController {

    @Autowired
    private ITroubleTypeService iTroubleTypeService;

    @RequestMapping(value = "/get/{id}", method = RequestMethod.GET)
    @ApiOperation(value = "通过id获取")
    public Result<TroubleType> get(@PathVariable String id){

        TroubleType troubleType = iTroubleTypeService.getById(id);
        return new ResultUtil<TroubleType>().setData(troubleType);
    }

    @RequestMapping(value = "/getAll", method = RequestMethod.GET)
    @ApiOperation(value = "获取全部数据")
    public Result<List<TroubleType>> getAll(){

        List<TroubleType> list = iTroubleTypeService.list();
        return new ResultUtil<List<TroubleType>>().setData(list);
    }

    @RequestMapping(value = "/getByPage", method = RequestMethod.GET)
    @ApiOperation(value = "分页获取")
    public Result<IPage<TroubleType>> getByPage(@ModelAttribute PageVo page){

        IPage<TroubleType> data = iTroubleTypeService.page(PageUtil.initMpPage(page));
        return new ResultUtil<IPage<TroubleType>>().setData(data);
    }

    @RequestMapping(value = "/insertOrUpdate", method = RequestMethod.POST)
    @ApiOperation(value = "编辑或更新数据")
    public Result<TroubleType> saveOrUpdate(@ModelAttribute TroubleType troubleType){

        if(iTroubleTypeService.saveOrUpdate(troubleType)){
            return new ResultUtil<TroubleType>().setData(troubleType);
        }
        return new ResultUtil<TroubleType>().setErrorMsg("操作失败");
    }

    @RequestMapping(value = "/delByIds/{ids}", method = RequestMethod.DELETE)
    @ApiOperation(value = "批量通过id删除")
    public Result<Object> delAllByIds(@PathVariable String[] ids){

        for(String id : ids){
            iTroubleTypeService.removeById(id);
        }
        return new ResultUtil<Object>().setSuccessMsg("批量通过id删除数据成功");
    }
}
