package cn.exrick.xboot.modules.bussiness.purchaseOrder.VO;

import cn.exrick.xboot.modules.bussiness.packageSize.pojo.PackageSize;
import lombok.Data;

import java.math.BigDecimal;

/**
 * @Author : xiaofei
 * @Date: 2019/8/21
 */
@Data
public class SpecialPurchaseVO {

    private String id;
    /*产品基础信息*/
    private String productName;
    private String productOrderName;
    private String productModelName;


    /**
     * 购买数量
     */
    private BigDecimal orderNumbers;

    /**
     * 购买价格
     */
    private  BigDecimal price;

    /**
     * 购买渠道
     */
    private  String channelName;

    /**
     * 购买单位
     */
    private String unitName;

    /**
     * 购买包装尺寸
     */
    private PackageSize packageSizeName;

    /**
     * 总金额
     */
    private BigDecimal orderAmount;



}
