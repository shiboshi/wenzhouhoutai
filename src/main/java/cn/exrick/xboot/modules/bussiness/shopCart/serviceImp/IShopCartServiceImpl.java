package cn.exrick.xboot.modules.bussiness.shopCart.serviceImp;

import cn.exrick.xboot.modules.bussiness.shopCart.mapper.ShopCartMapper;
import cn.exrick.xboot.modules.bussiness.shopCart.pojo.ShopCart;
import cn.exrick.xboot.modules.bussiness.shopCart.service.IShopCartService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

/**
 * 购物车接口实现
 * @author tqr
 */
@Slf4j
@Service
@Transactional
public class IShopCartServiceImpl extends ServiceImpl<ShopCartMapper, ShopCart> implements IShopCartService {

    @Autowired
    private ShopCartMapper shopCartMapper;
}