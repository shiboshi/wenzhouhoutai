package cn.exrick.xboot.modules.bussiness.shiProductComparison.serviceImp;

import cn.exrick.xboot.modules.bussiness.shiProductComparison.mapper.back_functionMapper;
import cn.exrick.xboot.modules.bussiness.shiProductComparison.pojo.back_function;
import cn.exrick.xboot.modules.bussiness.shiProductComparison.service.Iback_functionService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

/**
 * 背部功能接口实现
 * @author 石博士
 */
@Slf4j
@Service
@Transactional
public class Iback_functionServiceImpl extends ServiceImpl<back_functionMapper, back_function> implements Iback_functionService {

    @Autowired
    private back_functionMapper back_functionMapper;
}