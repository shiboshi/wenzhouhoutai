package cn.exrick.xboot.modules.bussiness.channel.controller;

import ch.qos.logback.core.util.TimeUtil;
import cn.exrick.xboot.common.utils.PageUtil;
import cn.exrick.xboot.common.utils.ResultUtil;
import cn.exrick.xboot.common.utils.SecurityUtil;
import cn.exrick.xboot.common.vo.PageVo;
import cn.exrick.xboot.common.vo.Result;
import cn.exrick.xboot.modules.base.entity.User;
import cn.exrick.xboot.modules.bussiness.channel.VO.ChannelVO;
import cn.exrick.xboot.modules.bussiness.channel.pojo.Channel;
import cn.exrick.xboot.modules.bussiness.channel.service.IChannelService;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.List;

/**
 * @author tqr
 */
@Slf4j
@RestController
@Api(description = "渠道管理接口")
@RequestMapping("/xboot/channel")
@Transactional
public class ChannelController {

    @Autowired
    private IChannelService iChannelService;
    @Autowired
    private SecurityUtil securityUtil;

    @RequestMapping(value = "/get/{id}", method = RequestMethod.GET)
    @ApiOperation(value = "通过id获取")
    public Result<Channel> get(@PathVariable String id) {

        Channel channel = iChannelService.getById(id);
        return new ResultUtil<Channel>().setData(channel);
    }

    @RequestMapping(value = "/getAll", method = RequestMethod.GET)
    @ApiOperation(value = "获取全部数据")
    public Result<List<Channel>> getAll() {

        List<Channel> list = iChannelService.list();
        return new ResultUtil<List<Channel>>().setData(list);
    }

    @RequestMapping(value = "/getByPage", method = RequestMethod.POST)
    @ApiOperation(value = "分页获取")
    public Result<IPage<ChannelVO>> getByPage(@ModelAttribute PageVo page, String channelName) {
        IPage<ChannelVO> data = new Page<>();
        if (StringUtils.isNotBlank(channelName)) {
            data = iChannelService.getByPage(PageUtil.initMpPage(page), channelName);
        } else {
            data = iChannelService.page(PageUtil.initMpPage(page));
        }
        return new ResultUtil<IPage<ChannelVO>>().setData(data);
    }

    @RequestMapping(value = "/insertOrUpdate", method = RequestMethod.POST)
    @ApiOperation(value = "编辑或更新数据")
    public Result<Channel> saveOrUpdate(@ModelAttribute Channel channel) {

        channel.setCreateTime(new Date());
        channel.setCreateBy(securityUtil.getCurrUser().getId());
        channel.setUpdateBy(securityUtil.getCurrUser().getId());
        if (iChannelService.saveOrUpdate(channel)) {
            return new ResultUtil<Channel>().setData(channel);
        }
        return new ResultUtil<Channel>().setErrorMsg("操作失败");
    }

    @RequestMapping(value = "/delByIds/{ids}", method = RequestMethod.DELETE)
    @ApiOperation(value = "批量通过id删除")
    public Result<Object> delAllByIds(@PathVariable String[] ids) {

        for (String id : ids) {
            iChannelService.removeById(id);
        }
        return new ResultUtil<Object>().setSuccessMsg("批量通过id删除数据成功");
    }
}
