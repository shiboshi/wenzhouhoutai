package cn.exrick.xboot.modules.bussiness.afterIndentOrder.serviceImp;

import cn.exrick.xboot.common.enums.CheckStatus;
import cn.exrick.xboot.common.enums.IsComplete;
import cn.exrick.xboot.common.enums.Order;
import cn.exrick.xboot.common.enums.SendStatus;
import cn.exrick.xboot.common.factory.ConstantFactory;
import cn.exrick.xboot.common.utils.CommonUtil;
import cn.exrick.xboot.common.utils.PageUtil;
import cn.exrick.xboot.common.utils.SecurityUtil;
import cn.exrick.xboot.common.utils.ToolUtil;
import cn.exrick.xboot.common.vo.PageVo;
import cn.exrick.xboot.modules.activiti.entity.ActBusiness;
import cn.exrick.xboot.modules.activiti.service.ActBusinessService;
import cn.exrick.xboot.modules.base.dao.mapper.UserMapper;
import cn.exrick.xboot.modules.bussiness.afterIndentOrder.VO.AfterIndentVO;
import cn.exrick.xboot.modules.bussiness.afterIndentOrder.mapper.afterIndentOrderMapper;
import cn.exrick.xboot.modules.bussiness.afterIndentOrder.pojo.AfterIndentOrder;
import cn.exrick.xboot.modules.bussiness.afterIndentOrder.service.IafterIndentOrderService;
import cn.exrick.xboot.modules.bussiness.applicationAfterSaleOrder.mapper.applicationAfterSaleOrderMapper;
import cn.exrick.xboot.modules.bussiness.applicationAfterSaleOrder.pojo.ApplicationAfterSaleOrder;
import cn.exrick.xboot.modules.bussiness.applicationSendGoodsOrder.mapper.ApplicationSendGoodsOrderMapper;
import cn.exrick.xboot.modules.bussiness.applicationSendGoodsOrder.pojo.ApplicationSendGoodsOrder;
import cn.exrick.xboot.modules.bussiness.classification.mapper.ClassificationMapper;
import cn.exrick.xboot.modules.bussiness.classification.pojo.Classification;
import cn.exrick.xboot.modules.bussiness.inventoryOrder.mapper.InventoryOrderMapper;
import cn.exrick.xboot.modules.bussiness.inventoryOrder.pojo.InventoryOrder;
import cn.exrick.xboot.modules.bussiness.inventoryOrder.pojo.InventoryOrderResult;
import cn.exrick.xboot.modules.bussiness.inventoryOrder.service.IInventoryOrderService;
import cn.exrick.xboot.modules.bussiness.product.VO.ProductCommon;
import cn.exrick.xboot.modules.bussiness.product.pojo.ProductModel;
import cn.exrick.xboot.modules.bussiness.product.pojo.ProductOrder;
import cn.exrick.xboot.modules.bussiness.product.service.IproductModelService;
import cn.exrick.xboot.modules.bussiness.product.service.IproductOrderService;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.core.toolkit.support.SFunction;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.google.common.collect.Maps;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.*;
import java.util.stream.Collectors;

/**
 * 订货单维修单接口实现
 *
 * @author xiaofei
 */
@Slf4j
@Service
@Transactional
public class IafterIndentOrderServiceImpl extends ServiceImpl<afterIndentOrderMapper, AfterIndentOrder> implements IafterIndentOrderService {

    @Autowired
    private afterIndentOrderMapper afterIndentOrderMapper;
    @Autowired
    private ActBusinessService actBusinessService;
    @Autowired
    private SecurityUtil securityUtil;
    @Autowired
    private applicationAfterSaleOrderMapper applicationAfterSaleOrderMapper1;
    @Autowired
    private InventoryOrderMapper inventoryOrderMapper;
    @Autowired
    private UserMapper userMapper;
    @Autowired
    private ApplicationSendGoodsOrderMapper applicationSendGoodsOrderMapper;
    @Autowired
    private IInventoryOrderService iInventoryOrderService;
    @Autowired
    private IproductModelService iproductModelService;
    @Autowired
    private IproductOrderService iproductOrderService;
    @Autowired
    private ClassificationMapper classificationMapper;

    @Override
    public void insertOrder(String applicationAfterSaleOrderId, String inventoryOrderIds, String procDefId) {

        AfterIndentOrder afterIndentOrder = new AfterIndentOrder();
        afterIndentOrder.setApplicationAfterSaleOrderId(applicationAfterSaleOrderId);
        afterIndentOrder.setDelFlag(0);
        afterIndentOrder.setCreateBy(securityUtil.getCurrUser().getId());
        afterIndentOrder.setCreateTime(new Date());
        afterIndentOrder.setIsComplete(IsComplete.PENDING);
        //获取最新的一个售后单
        afterIndentOrder.setOrderNum(CommonUtil.createOrderSerial(Order.SH_));
        this.save(afterIndentOrder);

        //插入业务流程表
        ActBusiness actBusiness = new ActBusiness();
        //获取最新插入的业务流程表
        AfterIndentOrder afterIndentOrderNew = afterIndentOrderMapper.selectOne(new QueryWrapper<AfterIndentOrder>().lambda().orderByDesc(AfterIndentOrder::getCreateTime).last(" limit 1 "));
        actBusiness.setUserId(securityUtil.getCurrUser().getId());
        actBusiness.setTableId(afterIndentOrderNew.getId());
        actBusiness.setProcDefId(procDefId);
        ActBusiness actBusinessUse = actBusinessService.save(actBusiness);

        //更新业务表
        afterIndentOrderNew.setBusinessId(actBusinessUse.getId());
        this.updateById(afterIndentOrderNew);

    }


    @Override
    public Map<String, Object> getList(PageVo pageVo, String checkStatus, String numb) {
        Map<SFunction<AfterIndentOrder, ?>, Object> map = Maps.newHashMap();
        if (ToolUtil.isNotEmpty(checkStatus)) {
            if (IsComplete.IS_COMPLETE.getValue() == Integer.parseInt(checkStatus)) {
                map.put(AfterIndentOrder::getIsComplete, IsComplete.IS_COMPLETE);
            } else if (IsComplete.PENDING.getValue() == Integer.parseInt(checkStatus)) {
                map.put(AfterIndentOrder::getIsComplete, IsComplete.PENDING);
            }
        }
        if (ToolUtil.isNotEmpty(numb)) {
            map.put(AfterIndentOrder::getOrderNum, numb);
        }
        Map<String, Object> result = new HashMap<>();
        List<AfterIndentOrder> afterIndentOrders = this.list(Wrappers.<AfterIndentOrder>lambdaQuery()
                .allEq(true, map, false));

        if (ToolUtil.isNotEmpty(afterIndentOrders)) {
            List<AfterIndentVO> afterIndentVOS = afterIndentOrders.stream().map(order -> {
                AfterIndentVO afterIndentVO = new AfterIndentVO();
                ToolUtil.copyProperties(order, afterIndentVO);
                afterIndentVO.setUserName(ConstantFactory.me().getUserName(order.getCreateBy()));
                afterIndentVO.setUserDept(ConstantFactory.me().getUserDeptName(order.getCreateBy()));
                afterIndentVO.setUserLevel(ConstantFactory.me().getUserLevelName(order.getCreateBy()));
                return afterIndentVO;
            }).collect(Collectors.toList());
            result.put("total", afterIndentVOS.size());
            result.put("result", PageUtil.listToPage(pageVo, afterIndentVOS));
            return result;
        } else {
            return null;
        }
    }

    @Override
    public List<ProductCommon> getDetail(Page initMpPage, String id) {
        //查询申请发货单
        List<ProductCommon> list = new ArrayList<>();
        ApplicationAfterSaleOrder applicationAfterSaleOrder = applicationAfterSaleOrderMapper1.selectById(afterIndentOrderMapper.selectById(id).getApplicationAfterSaleOrderId());
        if (ToolUtil.isNotEmpty(applicationAfterSaleOrder)) {
            if (ToolUtil.isNotEmpty(applicationAfterSaleOrder)) {
                String[] ids = applicationAfterSaleOrder.getInventoryIds().split(",");
                if (ToolUtil.isNotEmpty(ids)) {
                    for (String i : ids) {
                        InventoryOrder inventoryOrder = inventoryOrderMapper.selectById(i);
                        if (ToolUtil.isNotEmpty(inventoryOrder)) {
                            ProductCommon productCommon = iproductOrderService.getProductCommon(inventoryOrder.getProductOrderId());
                            if (ToolUtil.isNotEmpty(productCommon)) {
                                list.add(productCommon);
                            }
                        }
                    }
                }
            }
        }
        return list;
    }

    @Override
    public void insertAfterIndentSend(String id, String procDefId) {
        //查询申请售后单
        ApplicationAfterSaleOrder applicationAfterSaleOrder = applicationAfterSaleOrderMapper1.selectById(afterIndentOrderMapper.selectById(id).getApplicationAfterSaleOrderId());
        if (ToolUtil.isNotEmpty(applicationAfterSaleOrder)) {
            String userId = applicationAfterSaleOrder.getCreateBy();

            //1.插入申请发货单
            ApplicationSendGoodsOrder applicationSendGoodsOrder = new ApplicationSendGoodsOrder();
            applicationSendGoodsOrder.setAfterIndentOrderId(id);
            applicationSendGoodsOrder.setCreateBy(userId);

            applicationSendGoodsOrder.setCheckStatus(CheckStatus.pengindg);
            applicationSendGoodsOrder.setSendStatus(SendStatus.PENDING);
            applicationSendGoodsOrder.setOrderNum(CommonUtil.createOrderSerial(Order.SQ_));
            applicationSendGoodsOrderMapper.insert(applicationSendGoodsOrder);

            //获取最新的一条记录
            ApplicationSendGoodsOrder applicationSendGoodsOrderNew = applicationSendGoodsOrderMapper.selectOne(Wrappers.<ApplicationSendGoodsOrder>lambdaQuery()
                    .orderByDesc(ApplicationSendGoodsOrder::getId)
                    .last("limit 1"));


            //3.插入到流程审核单中
            ActBusiness actBusiness = new ActBusiness();
            actBusiness.setTableId(applicationSendGoodsOrderNew.getId());
            actBusiness.setProcDefId(procDefId);
            actBusiness.setUserId(userId);
            ActBusiness actBusinessIns = actBusinessService.save(actBusiness);

            //4.更新申请发货单
            applicationSendGoodsOrderNew.setBussinessId(actBusinessIns.getId());
            applicationSendGoodsOrderMapper.updateById(applicationSendGoodsOrderNew);
        }
    }
}