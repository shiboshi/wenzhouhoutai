package cn.exrick.xboot.common.sql;

import org.apache.ibatis.jdbc.SQL;

import java.util.Map;

/**
 * @Author : xiaofei
 * @Date: 2019/8/15
 */
public class SqlProvider {

    /**
     * 获取登录者的信息
     * @param map
     * @return
     */
    public String commonMessage(Map<String,Object> map){
       return new SQL(){{
            SELECT("u.username as userName,d.title as userDept,(SELECT name FROM t_level WHERE id = l.level_id and del_flag = 0) as userLevel");
            FROM("t_user u");
            LEFT_OUTER_JOIN("t_department d on u.department_id = d.id and d.del_flag=0");
            LEFT_OUTER_JOIN("t_dealer_level l on u.department_id = l.dept_id and l.del_flag=0");
            WHERE("u.id = " + map.get("userId").toString());
            WHERE("u.del_flag = 0");
        }}.toString();
    }
}
