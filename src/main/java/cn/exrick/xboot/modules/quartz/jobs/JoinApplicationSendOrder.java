package cn.exrick.xboot.modules.quartz.jobs;

import cn.exrick.xboot.common.constant.ActivitiConstant;
import cn.exrick.xboot.common.enums.SendStatus;
import cn.exrick.xboot.common.utils.ToolUtil;
import cn.exrick.xboot.config.springContext.SpringContextHolder;
import cn.exrick.xboot.modules.activiti.entity.ActBusiness;
import cn.exrick.xboot.modules.activiti.service.mybatis.IActbusinessService;
import cn.exrick.xboot.modules.bussiness.appliSendNum.pojo.AppliSendNum;
import cn.exrick.xboot.modules.bussiness.appliSendNum.service.IappliSendNumService;
import cn.exrick.xboot.modules.bussiness.applicationSendGoodsOrder.VO.AppliParam;
import cn.exrick.xboot.modules.bussiness.applicationSendGoodsOrder.pojo.ApplicationSendGoodsOrder;
import cn.exrick.xboot.modules.bussiness.applicationSendGoodsOrder.service.IapplicationSendGoodsOrderService;
import cn.exrick.xboot.modules.bussiness.purchaseOrder.VO.UpdatePurchaseOrderVO;
import cn.exrick.xboot.modules.wx.Wxservice;
import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.google.common.base.Joiner;
import com.google.common.collect.Lists;
import lombok.extern.java.Log;
import org.activiti.engine.RuntimeService;
import org.activiti.engine.TaskService;
import org.activiti.engine.task.Task;
import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * 合并申请发货轮询
 * springboot 单线程定时任务
 * @Author : xiaofei
 * @Date: 2019/8/29
 */
@Log
@Component
@Configuration
@EnableScheduling
public class JoinApplicationSendOrder {

    @Autowired
    private IapplicationSendGoodsOrderService applicationSendGoodsOrderService;
    @Autowired
    private TaskService taskService;
    @Autowired
    private IActbusinessService actbusinessService;
    @Autowired
    private RuntimeService runtimeService;
    @Autowired
    private IappliSendNumService appliSendNumService;

    private static final Joiner JOINER = Joiner.on(",").skipNulls();

    /**
     * 过滤出第一个节点的
     * @param proInstanceId
     * @return
     */
    public boolean getTaskName(String proInstanceId){
        Task task = taskService.createTaskQuery().processInstanceId(proInstanceId).singleResult();
        if (task.getName().equals("服务专员")){
            return true;
        }
      return false;
    }

    /**
     * 获取过滤后的流程业务表
     * @param applicationSendGoodsOrders
     * @return
     */
    public List<ApplicationSendGoodsOrder> getFilterList(List<ApplicationSendGoodsOrder> applicationSendGoodsOrders){
        List<String> tableId = applicationSendGoodsOrders.stream().map(order -> order.getId()).collect(Collectors.toList());
        List<ActBusiness> actBusinesses = actbusinessService.list(Wrappers.<ActBusiness>lambdaQuery()
        .in(ActBusiness::getTableId,tableId));
        List<String> orderIds = actBusinesses.stream().filter(actBusiness -> getTaskName(actBusiness.getProcInstId()))
                .map(ActBusiness::getTableId)
                .collect(Collectors.toList());
        if (ToolUtil.isNotEmpty(orderIds)) {
            List<ApplicationSendGoodsOrder> fiterList = applicationSendGoodsOrderService.list(Wrappers.<ApplicationSendGoodsOrder>
                    lambdaQuery().in(ApplicationSendGoodsOrder::getId, orderIds));
            return fiterList;
        }
        return null;
    }

    /**
     * 获取分组后的数据
     * @param applicationSendGoodsOrders
     * @return
     */
    public Map<String,List<ApplicationSendGoodsOrder>> getDistinctList(List<ApplicationSendGoodsOrder> applicationSendGoodsOrders){

        if (ToolUtil.isNotEmpty(applicationSendGoodsOrders)) {
            Map<String, List<ApplicationSendGoodsOrder>> goupBy = applicationSendGoodsOrders.stream()
                    .collect(Collectors.groupingBy(ApplicationSendGoodsOrder::getIndentOrderId));
            return goupBy;
        }
        return null;
    }

    /**
     * 撤销流程
     * @param procInstId
     * @param id
     * @param reason
     */
    public void  deleteProcess(String procInstId,String id,String reason){
        if(StrUtil.isBlank(reason)){
            reason = "";
        }
        runtimeService.deleteProcessInstance(procInstId, "canceled-"+reason);
        ActBusiness actBusiness = actbusinessService.getById(id);
        actBusiness.setStatus(ActivitiConstant.STATUS_CANCELED);
        actBusiness.setResult(ActivitiConstant.RESULT_TO_SUBMIT);
        actbusinessService.updateById(actBusiness);
    }


    /**
     * 合并订单，并且生成一张提交的申请发货单
     * @param appliSendNums
     * @param userId
     * @return
     */
    public void joinOrder(List<AppliSendNum> appliSendNums,String userId){

         Map<String,List<AppliSendNum>> group = appliSendNums.stream()
                 .collect(Collectors.groupingBy(AppliSendNum::getPurchaseId));

         List<UpdatePurchaseOrderVO> updatePurchaseOrderVOS = Lists.newArrayList();
        AppliParam appliParam = new AppliParam();
        appliParam.setProcDefId(Wxservice.me().getProcDefId(ActivitiConstant.aplicationSendOrder));
        appliParam.setUserId(userId);
        //获取indentOrderId
        ApplicationSendGoodsOrder applicationSendGoodsOrder = applicationSendGoodsOrderService.getById(appliSendNums.get(0).getApplicationSendOrderId());
        appliParam.setIndentOrderId(applicationSendGoodsOrder.getIndentOrderId());
        ActBusiness actBusiness = applicationSendGoodsOrderService.insertOrder(appliParam);
        //获取最新的一条数据
        ApplicationSendGoodsOrder applicationSendGoodsOrderNew = applicationSendGoodsOrderService.getById(actBusiness.getTableId());


        for (Map.Entry<String,List<AppliSendNum>> entry : group.entrySet()){
            UpdatePurchaseOrderVO updatePurchaseOrderVO = new UpdatePurchaseOrderVO();
            List<AppliSendNum> appliSendNumsGet = entry.getValue();
            updatePurchaseOrderVO.setDispatchNum(appliSendNumsGet.stream()
            .mapToLong(AppliSendNum::getSendNum).sum());
            updatePurchaseOrderVO.setId(entry.getKey());
            updatePurchaseOrderVOS.add(updatePurchaseOrderVO);

            //插入发货数量单
            AppliSendNum appliSendNum = new AppliSendNum();
            appliSendNum.setSendNum(updatePurchaseOrderVO.getDispatchNum());
            appliSendNum.setPurchaseId(updatePurchaseOrderVO.getId());
            appliSendNum.setApplicationSendOrderId(applicationSendGoodsOrderNew.getId());
            appliSendNumService.save(appliSendNum);
         }

        List<String> ids = updatePurchaseOrderVOS.stream().map(UpdatePurchaseOrderVO::getId).collect(Collectors.toList());
        applicationSendGoodsOrderNew.setPurchaseOrderIds(JOINER.join(ids));
        applicationSendGoodsOrderService.updateById(applicationSendGoodsOrderNew);

        //启动流程
        ActBusiness actBusinessNew = Wxservice.me().checkUser(actBusiness, ActivitiConstant.aplicationSendOrder);
        Wxservice.me().start(actBusinessNew, appliParam.getUserId());

    }

    /**
     * 开启定时任务
     */
    @Scheduled(cron = "0 0 0/3 * * ?")
    public void execute(){

        log.info("/*-------------------触发定时任务！！！-------------------------*/");
        List<ApplicationSendGoodsOrder> applicationSendGoodsOrders = applicationSendGoodsOrderService.list(Wrappers.<ApplicationSendGoodsOrder>lambdaQuery()
                .eq(ApplicationSendGoodsOrder::getSendStatus, SendStatus.PENDING.getCdoe()));
        //合并
        Map<String,List<ApplicationSendGoodsOrder>> map = getDistinctList(getFilterList(applicationSendGoodsOrders));
        if (ToolUtil.isNotEmpty(map)){

            for (Map.Entry<String,List<ApplicationSendGoodsOrder>> entry : map.entrySet()){
                List<ApplicationSendGoodsOrder> resoveData = entry.getValue();
                String userId  = resoveData.get(0).getCreateBy();
                if (resoveData != null && resoveData.size()>1){
                    List<String> appliIds = resoveData.stream().map(Order -> Order.getId()).collect(Collectors.toList());
                    List<AppliSendNum> appliSendNum = appliSendNumService.list(Wrappers.<AppliSendNum>lambdaQuery()
                            .in(AppliSendNum::getApplicationSendOrderId,appliIds));
                    joinOrder(appliSendNum, userId);
                    //批量删除申请发货单，发货数量单
                    appliSendNumService.removeByIds(appliSendNum.stream().map(AppliSendNum::getId).collect(Collectors.toList()));
                    applicationSendGoodsOrderService.removeByIds(appliIds);

                    //撤销当前申请发货单的流程
                    List<ActBusiness> actBusinesses = actbusinessService.list(Wrappers.<ActBusiness>lambdaQuery()
                            .in(ActBusiness::getTableId,appliIds));
                    actBusinesses.forEach(actBusiness -> deleteProcess(actBusiness.getProcInstId(), actBusiness.getId(), "合并订单") );
                }

            }
        }
        log.info("/*---------------定时任务结束!!!--------------------*/");
    }
}




