package cn.exrick.xboot.modules.bussiness.dealerLevel.mapper;

import cn.exrick.xboot.modules.bussiness.dealerLevel.DealerProvider;
import cn.exrick.xboot.modules.bussiness.dealerLevel.VO.DealerLevelVO;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import cn.exrick.xboot.modules.bussiness.dealerLevel.pojo.DealerLevel;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.SelectProvider;
import org.springframework.security.access.method.P;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * 经销商等级数据处理层
 * @author tqr
 */
@Repository
public interface DealerLevelMapper extends BaseMapper<DealerLevel> {

    @SelectProvider(type = DealerProvider.class,method = "getDealer")
    IPage<DealerLevelVO> getDealer(@Param("initMpPage") Page initMpPage,@Param("name") String name);
}