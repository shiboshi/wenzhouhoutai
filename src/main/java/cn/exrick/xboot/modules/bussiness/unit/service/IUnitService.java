package cn.exrick.xboot.modules.bussiness.unit.service;

import cn.exrick.xboot.modules.bussiness.unit.VO.UnitVO;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;
import cn.exrick.xboot.modules.bussiness.unit.pojo.Unit;

import java.util.List;

/**
 * 产品单位接口
 * @author tqr
 */
public interface IUnitService extends IService<Unit> {

    IPage<UnitVO> getByName(Page initMpPage,String unitName);
}