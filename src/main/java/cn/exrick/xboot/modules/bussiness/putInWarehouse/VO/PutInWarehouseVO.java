package cn.exrick.xboot.modules.bussiness.putInWarehouse.VO;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@ApiModel("入库")
@Data
public class PutInWarehouseVO {

    @ApiModelProperty("id")
    private String id;

    @ApiModelProperty("入库时间")
    private String createTime;

    @ApiModelProperty("操作人")
    private String userName;

    @ApiModelProperty("部门名称")
    private String deptName;

    /**
     * 批次
     */
    @ApiModelProperty("批次")
    private String batchNumber;

    /**
     * 仓库id
     */
    @ApiModelProperty("仓库id")
    private String warehouseId;

    @ApiModelProperty("仓库名称")
    private String warehouseName;

    /**
     * 产品编号id
     */
    private String productOrderId;

    @ApiModelProperty("产品大类")
    private String productName;

    @ApiModelProperty("产品型号")
    private String productModelName;

    @ApiModelProperty("产品品号")
    private String productOrderName;

    /**
     * 数量
     */
    private Long amount;

    /**
     * 入库状态
     */
    private Integer Status;

    private String isPutIn;

    @ApiModelProperty("颜色名称拼接")
    private String allName;

    @ApiModelProperty("产品颜色")
    private String color;

    @ApiModelProperty("产品色号")
    private String colorNumber;

    @ApiModelProperty("产品材质")
    private String material;

    private String unitWeight;

    @ApiModelProperty("毛重")
    private String grossWeight;

    @ApiModelProperty("体积")
    private String volume;

    private String unitName;
}
