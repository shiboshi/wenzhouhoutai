package cn.exrick.xboot.common.upay.request.manage;


import cn.exrick.xboot.common.upay.enums.BizChannel;
import cn.exrick.xboot.common.upay.enums.BizType;
import cn.exrick.xboot.common.upay.request.RequestBody;
import cn.exrick.xboot.common.upay.utils.StringUtils;


public class SignInRequestBody extends RequestBody {
    private String union_request;
    private String manufacturer;
    private String model;
    private String serial;

    public SignInRequestBody() {

    }

    public SignInRequestBody(String manufacturer, String model, String serial) {
        this.manufacturer = manufacturer;
        this.model = model;
        this.serial = serial;
    }

    @Override
    public boolean validate() {

        if (StringUtils.isEmpty(manufacturer)) {
            throw new RuntimeException("设备厂商[manufacturer]不可为空");
        }

        if (StringUtils.isEmpty(model)) {
            throw new RuntimeException("产品型号[model]不可为空");
        }

        if (StringUtils.isEmpty(serial)) {
            throw new RuntimeException("设备序列号[serial]不可为空");
        }

        return true;
    }

    public String getUnion_request() {
        return union_request;
    }

    public void setUnion_request(String union_request) {
        this.union_request = union_request;
    }

    public String getManufacturer() {
        return manufacturer;
    }

    public void setManufacturer(String manufacturer) {
        this.manufacturer = manufacturer;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public String getSerial() {
        return serial;
    }

    public void setSerial(String serial) {
        this.serial = serial;
    }

    @Override
    public String acquireBizChannel() {
        return BizChannel.UMSZJ_CHANNEL_MANAGE.getEn();
    }

    @Override
    public String acquireBizType() {
        return BizType.UMSZJ_TRADE_SIGN.getEn();
    }

}
