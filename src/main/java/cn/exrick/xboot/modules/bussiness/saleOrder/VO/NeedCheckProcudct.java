package cn.exrick.xboot.modules.bussiness.saleOrder.VO;

import lombok.Data;

import java.math.BigDecimal;

/**
 * @Author : xiaofei
 * @Date: 2019/8/28
 */
@Data
public class NeedCheckProcudct {

    private String productName;
    private String productModelName;
    private String productOrderName;

    /**
     * 最低销售价
     */
    private BigDecimal minSellingPrice;


}
