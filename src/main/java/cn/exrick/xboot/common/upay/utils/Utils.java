package cn.exrick.xboot.common.upay.utils;

import org.apache.commons.codec.binary.Base64;

import java.beans.BeanInfo;
import java.beans.Introspector;
import java.beans.PropertyDescriptor;
import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.Map;
import java.util.TreeMap;

/**
 * TODO 大杂烩工具
 * 
 * @author jcouyang@chinaums.com
 * @date 2017年6月16日 下午2:40:53
 */
public class Utils {

	/**
	 * 将一个Bean对象转化为一个Map 忽略掉为空的属性，默认不排序
	 * @param bean
	 * @return
	 * @throws Exception
	 */
	public static Map<String, String> convertBean(Object bean) throws Exception {
		return convertBean(bean, false);
	}

	/**
	 *
	 * @param bean
	 * @param ordered
	 * @return
	 * @throws Exception
	 */
	public static Map<String, String> convertBean(Object bean, boolean ordered) throws Exception {
		Map<String, String> returnMap;
		if (ordered) {
            returnMap=new TreeMap<>();
        } else {
            returnMap=new HashMap<>();
        }

		BeanInfo beanInfo = Introspector.getBeanInfo(bean.getClass());
		//获取属性描述器
		PropertyDescriptor[] propertyDescriptors = beanInfo.getPropertyDescriptors();

		for (PropertyDescriptor descriptor : propertyDescriptors) {
			String propertyName = descriptor.getName();

			if (!propertyName.equals("class")) {
				Method readMethod = descriptor.getReadMethod();
				String result = (String) readMethod.invoke(bean);

				if (!StringUtils.isEmpty(result)) {
					returnMap.put(propertyName, result);
				}

			}
		}

		return returnMap;
	}

	/**
	 * 获取文件的真实媒体类型。目前只支持JPG, GIF, PNG, BMP四种图片文件。
	 * 
	 * @param bytes
	 *            文件字节流
	 * @return 媒体类型(MEME-TYPE)
	 */
	public static String getMimeType(byte[] bytes) {
		String suffix = getFileSuffix(bytes);
		String mimeType;

		if ("JPG".equals(suffix)) {
			mimeType = "image/jpeg";
		} else if ("GIF".equals(suffix)) {
			mimeType = "image/gif";
		} else if ("PNG".equals(suffix)) {
			mimeType = "image/png";
		} else if ("BMP".equals(suffix)) {
			mimeType = "image/bmp";
		} else {
			mimeType = "application/octet-stream";
		}

		return mimeType;
	}

	/**
	 * 获取文件的真实后缀名。目前只支持JPG, GIF, PNG, BMP四种图片文件。
	 * 
	 * @param bytes
	 *            文件字节流
	 * @return JPG, GIF, PNG or null
	 */
	public static String getFileSuffix(byte[] bytes) {
		if (bytes == null || bytes.length < 10) {
			return null;
		}

		if (bytes[0] == 'G' && bytes[1] == 'I' && bytes[2] == 'F') {
			return "GIF";
		} else if (bytes[1] == 'P' && bytes[2] == 'N' && bytes[3] == 'G') {
			return "PNG";
		} else if (bytes[6] == 'J' && bytes[7] == 'F' && bytes[8] == 'I' && bytes[9] == 'F') {
			return "JPG";
		} else if (bytes[0] == 'B' && bytes[1] == 'M') {
			return "BMP";
		} else {
			return null;
		}
	}
	
	/**
	 * BASE64解密
	 * @param content
	 * @return
	 */
	public static byte[] decodeBase64(String content){
    	return Base64.decodeBase64(content);
    }

}
