package cn.exrick.xboot.modules.bussiness.shiWarehouse.mapper;

import cn.exrick.xboot.modules.bussiness.shiWarehouse.WarehouseProvider;
import cn.exrick.xboot.modules.bussiness.shiWarehouse.WarehouseVO.WarehouseVO;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import cn.exrick.xboot.modules.bussiness.shiWarehouse.pojo.ShiWarehouse;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.SelectProvider;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * 仓库表数据处理层
 * @author tqr
 */
@Repository
public interface ShiWarehouseMapper extends BaseMapper<ShiWarehouse> {

    @SelectProvider(type = WarehouseProvider.class,method = "getByName")
    List<WarehouseVO> getWarehouse(@Param("deptId") String deptId);

}