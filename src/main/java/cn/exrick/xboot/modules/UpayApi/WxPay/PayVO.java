package cn.exrick.xboot.modules.UpayApi.WxPay;

import lombok.Data;

import java.math.BigDecimal;

/**
 * @Author : xiaofei
 * @Date: 2019/9/5
 */
@Data
public class PayVO {
    private Integer payType;
    private String param;
    private String userId;
    private BigDecimal totalAmount;
    private String openId;
    private String indentOrderType;
}
