package cn.exrick.xboot.modules.bussiness.shiWarehouse;

import org.apache.commons.lang3.StringUtils;

public class WarehouseProvider {
    public String getByName(String deptId) {
        String sql = "";
        String str = "";
        str += "select a.dept_id, a.id,a.create_time,a.serial_num,a.w_name,b.title as deptName from " +
                "t_shi_warehouse a LEFT JOIN t_department b on a.dept_id = b.id and b.del_flag = 0 where 1=1 and a.del_flag = 0 ";
        if(StringUtils.isNotBlank(deptId)){
            str += " and b.id = '"+deptId+"'";
        }
        sql += str;
        return sql;
    }
}
