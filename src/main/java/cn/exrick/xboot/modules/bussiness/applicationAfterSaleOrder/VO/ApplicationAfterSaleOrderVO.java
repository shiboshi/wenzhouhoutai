package cn.exrick.xboot.modules.bussiness.applicationAfterSaleOrder.VO;

import cn.exrick.xboot.common.enums.AfterSaleType;
import cn.exrick.xboot.common.enums.CheckStatus;
import cn.exrick.xboot.common.vo.CommonVO;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @Author : xiaofei
 * @Date: 2019/8/11
 */
@Data
public class ApplicationAfterSaleOrderVO extends CommonVO {

    @ApiModelProperty("订货单编号")
    private String orderNum;

    private String inventoryIds;

    @ApiModelProperty("售后类型")
    private String afterSaleType;

    @ApiModelProperty("订货单详情")
    private String indentOrderId;

    private String createBy;

    @ApiModelProperty("审核状态")
    private String checkStatus;

    private String appliAfterNumb;
}
