package cn.exrick.xboot.modules.bussiness.inventoryOrder.pojo;

import cn.exrick.xboot.base.XbootBaseEntity;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModelProperty;
import io.swagger.annotations.ApiModel;
import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

/**
 * @author tqr
 */
@Data
@Entity
@Table(name = "t_inventory_order")
@TableName("t_inventory_order")
@ApiModel(value = "库存表")
public class InventoryOrder extends XbootBaseEntity {

    private static final long serialVersionUID = 1L;

    /**
     * 数量
     */
    private Long amount;

    /**
     * 批次
     */
    private String batchNumber;

    /**
     * 仓库id
     */
    private String warhouseId;

    /**
     * 序列号
     */
    @Column(length = 1000)
    private String serialNum;

    /**
     * 产品编号id
     */
    private String productOrderId;

    /**
     * 状态值1.未出库 2.已出库
     */
    private Integer  serialNumOut;

}