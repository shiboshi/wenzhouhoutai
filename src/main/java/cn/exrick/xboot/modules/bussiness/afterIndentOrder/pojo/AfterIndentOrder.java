package cn.exrick.xboot.modules.bussiness.afterIndentOrder.pojo;

import cn.exrick.xboot.base.XbootBaseEntity;
import cn.exrick.xboot.common.enums.CheckStatus;
import cn.exrick.xboot.common.enums.IsComplete;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModelProperty;
import io.swagger.annotations.ApiModel;
import lombok.Builder;
import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.Table;
import java.math.BigDecimal;

/**
 * @author xiaofei
 */
@Data
@Entity
@Table(name = "after_indent_order")
@TableName("after_indent_order")
@ApiModel(value = "订货单售后")
public class AfterIndentOrder extends XbootBaseEntity {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty("售后申请单id")
    private String applicationAfterSaleOrderId;

    @ApiModelProperty("业务流程id")
    private String businessId;

    @ApiModelProperty("订单完成状态")
    private IsComplete isComplete;

    @ApiModelProperty("订单编号")
    private String orderNum;

    @ApiModelProperty("评价")
    private String comment;

    @ApiModelProperty("审核状态")
    private CheckStatus checkStatus;

    @ApiModelProperty("退货总价")
    private BigDecimal returnMoney;
}