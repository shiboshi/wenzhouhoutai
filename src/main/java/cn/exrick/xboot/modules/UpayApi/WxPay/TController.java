package cn.exrick.xboot.modules.UpayApi.WxPay;

import cn.exrick.xboot.common.utils.ResultUtil;
import cn.exrick.xboot.common.vo.Result;
import cn.exrick.xboot.modules.UpayApi.test.test.WxpayService;
import cn.hutool.http.HttpUtil;
import com.alibaba.fastjson.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @Author : xiaofei
 * @Date: 2019/9/6
 */
@RestController
@RequestMapping("/xboot/pay")
public class TController {

    @Autowired
    private WxpayService wx;

    @PostMapping("/pa")
    public Result<String> pay(PayVO payVO){

        String string = wx.pay(payVO);
        return new ResultUtil<String>().setData(string, "支付成功！");
    }

    /**
     * 获取小程序openId
     * @param openIdRequest
     * @return
     */
    @PostMapping("/getOpenId")
    public Result<String> getOpenId(OpenIdRequest openIdRequest){

        StringBuilder url = new StringBuilder();
        url.append(WxInterface.code2Session).append("?appid="+openIdRequest.getAppid()+"&")
                .append("secret="+openIdRequest.getSecret()+"&")
                .append("js_code=" + openIdRequest.getJs_code()+"&")
                .append("grant_type=" +openIdRequest.getGrant_type());
        String response = HttpUtil.get(url.toString());
        OpenIdResponse openIdResponse = JSONObject.parseObject(response,OpenIdResponse.class);
        return new ResultUtil<String>().setData(openIdResponse.getOpenid(), "获取成功！！");
    }
}
