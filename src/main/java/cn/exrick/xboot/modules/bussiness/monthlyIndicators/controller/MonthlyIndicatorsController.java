package cn.exrick.xboot.modules.bussiness.monthlyIndicators.controller;

import cn.exrick.xboot.common.utils.PageUtil;
import cn.exrick.xboot.common.utils.ResultUtil;
import cn.exrick.xboot.common.vo.PageVo;
import cn.exrick.xboot.common.vo.Result;
import cn.exrick.xboot.modules.bussiness.monthlyIndicators.pojo.MonthlyIndicators;
import cn.exrick.xboot.modules.bussiness.monthlyIndicators.service.IMonthlyIndicatorsService;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * @author tqr
 */
@Slf4j
@RestController
@Api(description = "月份指标管理接口")
@RequestMapping("/xboot/monthlyIndicators")
@Transactional
public class MonthlyIndicatorsController {

    @Autowired
    private IMonthlyIndicatorsService iMonthlyIndicatorsService;

    @RequestMapping(value = "/get/{id}", method = RequestMethod.GET)
    @ApiOperation(value = "通过id获取")
    public Result<MonthlyIndicators> get(@PathVariable String id){

        MonthlyIndicators monthlyIndicators = iMonthlyIndicatorsService.getById(id);
        return new ResultUtil<MonthlyIndicators>().setData(monthlyIndicators);
    }

    @RequestMapping(value = "/getAll", method = RequestMethod.GET)
    @ApiOperation(value = "获取全部数据")
    public Result<List<MonthlyIndicators>> getAll(){

        List<MonthlyIndicators> list = iMonthlyIndicatorsService.list();
        return new ResultUtil<List<MonthlyIndicators>>().setData(list);
    }

    @RequestMapping(value = "/getByPage", method = RequestMethod.GET)
    @ApiOperation(value = "分页获取")
    public Result<IPage<MonthlyIndicators>> getByPage(@ModelAttribute PageVo page){

        IPage<MonthlyIndicators> data = iMonthlyIndicatorsService.page(PageUtil.initMpPage(page));
        return new ResultUtil<IPage<MonthlyIndicators>>().setData(data);
    }

    @RequestMapping(value = "/insertOrUpdate", method = RequestMethod.POST)
    @ApiOperation(value = "编辑或更新数据")
    public Result<MonthlyIndicators> saveOrUpdate(@ModelAttribute MonthlyIndicators monthlyIndicators){

        if(iMonthlyIndicatorsService.saveOrUpdate(monthlyIndicators)){
            return new ResultUtil<MonthlyIndicators>().setData(monthlyIndicators);
        }
        return new ResultUtil<MonthlyIndicators>().setErrorMsg("操作失败");
    }

    @RequestMapping(value = "/delByIds/{ids}", method = RequestMethod.DELETE)
    @ApiOperation(value = "批量通过id删除")
    public Result<Object> delAllByIds(@PathVariable String[] ids){

        for(String id : ids){
            iMonthlyIndicatorsService.removeById(id);
        }
        return new ResultUtil<Object>().setSuccessMsg("批量通过id删除数据成功");
    }
}
