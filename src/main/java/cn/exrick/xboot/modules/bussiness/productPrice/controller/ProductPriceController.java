package cn.exrick.xboot.modules.bussiness.productPrice.controller;

import cn.exrick.xboot.common.utils.PageUtil;
import cn.exrick.xboot.common.utils.ResultUtil;
import cn.exrick.xboot.common.utils.SecurityUtil;
import cn.exrick.xboot.common.utils.ToolUtil;
import cn.exrick.xboot.common.vo.PageVo;
import cn.exrick.xboot.common.vo.Result;
import cn.exrick.xboot.modules.bussiness.productPrice.VO.LevelPrice;
import cn.exrick.xboot.modules.bussiness.productPrice.VO.ProductPriceVO;
import cn.exrick.xboot.modules.bussiness.productPrice.pojo.ProductPrice;
import cn.exrick.xboot.modules.bussiness.productPrice.service.IProductPriceService;
import cn.exrick.xboot.modules.bussiness.purchaseOrder.pojo.PurchaseOrder;
import com.alibaba.fastjson.JSON;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * @author tqr
 */
@Slf4j
@RestController
@Api(description = "产品价格表管理接口")
@RequestMapping("/xboot/productPrice")
@Transactional
public class ProductPriceController {

    @Autowired
    private IProductPriceService iProductPriceService;
    @Autowired
    private SecurityUtil securityUtil;

    @RequestMapping(value = "/get/{id}", method = RequestMethod.GET)
    @ApiOperation(value = "通过id获取")
    public Result<ProductPrice> get(@PathVariable String id){

        ProductPrice productPrice = iProductPriceService.getById(id);
        return new ResultUtil<ProductPrice>().setData(productPrice);
    }

    @RequestMapping(value = "/getAll", method = RequestMethod.GET)
    @ApiOperation(value = "获取全部数据")
    public Result<List<ProductPrice>> getAll(){

        List<ProductPrice> list = iProductPriceService.list();
        return new ResultUtil<List<ProductPrice>>().setData(list);
    }

    @RequestMapping(value = "/getByPage", method = RequestMethod.GET)
    @ApiOperation(value = "分页获取")
    public Result<IPage<ProductPrice>> getByPage(@ModelAttribute PageVo page){

        IPage<ProductPrice> data = iProductPriceService.page(PageUtil.initMpPage(page));
        return new ResultUtil<IPage<ProductPrice>>().setData(data);
    }

    @RequestMapping(value = "/insertOrUpdate", method = RequestMethod.POST)
    @ApiOperation(value = "编辑或更新数据")
    public Result<ProductPrice> saveOrUpdate(@ModelAttribute ProductPrice productPrice){

        productPrice.setCreateBy(securityUtil.getCurrUser().getId());
        productPrice.setUpdateBy(securityUtil.getCurrUser().getId());
        productPrice.setDelFlag(0);
        productPrice.setCreateTime(new Date());
        productPrice.setUpdateTime(new Date());
        if(iProductPriceService.saveOrUpdate(productPrice)){
            return new ResultUtil<ProductPrice>().setData(productPrice);
        }
        return new ResultUtil<ProductPrice>().setErrorMsg("操作失败");
    }

    @RequestMapping(value = "/delByIds/{ids}", method = RequestMethod.DELETE)
    @ApiOperation(value = "批量通过id删除")
    public Result<Object> delAllByIds(@PathVariable String[] ids){

        for(String id : ids){
            iProductPriceService.removeById(id);
        }
        return new ResultUtil<Object>().setSuccessMsg("批量通过id删除数据成功");
    }

    @RequestMapping(value = "/getProductPrice", method = RequestMethod.POST)
    @ApiOperation(value = "分页获取")
    public Result<IPage<ProductPriceVO>> getProductPrice(@ModelAttribute PageVo page,String condition){

        IPage<ProductPriceVO> data = iProductPriceService.getProductPrice(PageUtil.initMpPage(page),condition);
        return new ResultUtil<IPage<ProductPriceVO>>().setData(data);
    }

    @RequestMapping(value = "/insertPrice", method = RequestMethod.POST)
    @ApiOperation(value = "添加")
    public Result<ProductPrice> insertPrice(String productOrderId,String levelprice){
        List<LevelPrice> levelPrices = JSON.parseArray(levelprice,LevelPrice.class);
        List<ProductPrice> list = new ArrayList<>();
        for (LevelPrice lp : levelPrices){
            if(ToolUtil.isNotEmpty(lp.getProductPrice())) {
                if(ToolUtil.isEmpty(iProductPriceService.getByCondition(productOrderId,lp.getLevelId()))) {
                    ProductPrice productPrice = new ProductPrice();
                    productPrice.setProductPrice(lp.getProductPrice());
                    productPrice.setLevelId(lp.getLevelId());
                    productPrice.setProductOrderId(productOrderId);
                    productPrice.setCreateTime(new Date());
                    productPrice.setDelFlag(0);
                    productPrice.setCreateBy(securityUtil.getCurrUser().getId());
                    list.add(productPrice);
                } else {
                    return new ResultUtil<ProductPrice>().setErrorMsg("重复添加");
                }
            }
        }
        if(iProductPriceService.insertPrice(list)){
            return new ResultUtil<ProductPrice>().setSuccessMsg("操作成功");
        }
        return new ResultUtil<ProductPrice>().setErrorMsg("操作失败");
    }

    @RequestMapping(value = "/update", method = RequestMethod.POST)
    @ApiOperation(value = "更新数据")
    public Result<ProductPrice> update(@ModelAttribute ProductPrice productPrice){

        productPrice.setDelFlag(0);
        productPrice.setUpdateTime(new Date());
        productPrice.setUpdateBy(securityUtil.getCurrUser().getId());
        iProductPriceService.getBaseMapper().updateById(productPrice);
        return new ResultUtil<ProductPrice>().setSuccessMsg("操作成功");
    }
}
