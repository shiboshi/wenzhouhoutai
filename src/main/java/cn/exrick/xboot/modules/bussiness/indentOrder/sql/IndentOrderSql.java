package cn.exrick.xboot.modules.bussiness.indentOrder.sql;

import cn.exrick.xboot.common.enums.CheckStatus;
import cn.exrick.xboot.common.enums.IsComplete;
import cn.exrick.xboot.common.utils.ToolUtil;
import org.apache.ibatis.jdbc.SQL;

import java.util.Map;

/**
 * @Author : xiaofei
 * @Date: 2019/8/16
 */
public class IndentOrderSql {

    public String selectList(Map<String, Object> map) {

        return new SQL() {{

            SELECT("order_num as orderNum,create_time as createTime,create_by,id as id,(case check_status when 1 then '待审核' when 2 then '审核成功' when 3 then '审核失败'end) as checkStatus," +
                    "(case is_complete when 0 then '订单未完成' when 1 then '订单已完成' end) as iscomplete");
            FROM("x_indent_order");
            String userId = map.get("userId").toString();
            WHERE("create_by = " + userId);
            WHERE("del_flag = " + 0);
            if (ToolUtil.isNotEmpty(map.get("orderNum"))) {
                WHERE("locate(" + map.get("orderNum").toString() + ",order_num)>0");
            }
            if ((Integer) map.get("code") == 2) {
                WHERE("check_status = " + CheckStatus.sucess.getCode());
                WHERE("is_complete = " + IsComplete.PENDING.getValue());
            }
            if ((Integer) map.get("code") == 3) {
                WHERE("is_complete = " + IsComplete.IS_COMPLETE.getValue());
            }
            if ((Integer) map.get("code") == 4) {
                WHERE("check_status = " + CheckStatus.pengindg.getCode());
            }
            if ((Integer) map.get("code") == 5) {
                WHERE("check_status = " + CheckStatus.sucess.getCode());
            }
            if ((Integer) map.get("code") == 6) {
                WHERE("check_status = " + CheckStatus.fail.getCode());
            }
            ORDER_BY("create_time desc");

        }}.toString();
    }

    public String getById(String id) {

        return new SQL() {{
            SELECT("order_num as orderNum,create_time as createTime,create_by,id as id,(case check_status when 1 then '待审核' when 2 then '审核成功' when 3 then '审核失败'end) as checkStatus," +
                    "(case is_complete when 0 then '订单未完成' when 1 then '订单已完成' end) as iscomplete");
            FROM("x_indent_order");
            WHERE("del_flag = " + 0);
            WHERE("id = '" + id + "'");

        }}.toString();
    }

    public String updateIsComplete(String indentOrderId) {
        String sql = "";
        String str = " UPDATE x_indent_order set is_complete = 1,update_time = NOW() where id = '" + indentOrderId + "' ";
        sql += str;
        return sql;
    }

}
