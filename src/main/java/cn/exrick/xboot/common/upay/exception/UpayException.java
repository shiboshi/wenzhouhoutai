package cn.exrick.xboot.common.upay.exception;

/**
 * 异常
 * @author zhou
 */
public class UpayException extends Exception {

    private String code;
    private String msg;

    public UpayException() {
        super();
    }

    public UpayException(Throwable cause) {
        super(cause);
    }

    public UpayException(String msg) {
        super(msg);
        this.msg = msg;
    }

    public UpayException(String code, String msg) {
        super(code + ":" + msg);
        this.code = code;
        this.msg = msg;
    }

    public UpayException(String code, String msg, Throwable cause) {
        super(cause);
        this.code = code;
        this.msg = msg;
    }

    public String getCode() {
        return code;
    }

    public String getMsg() {
        return msg;
    }

}
