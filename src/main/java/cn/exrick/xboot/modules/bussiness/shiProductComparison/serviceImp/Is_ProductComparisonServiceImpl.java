package cn.exrick.xboot.modules.bussiness.shiProductComparison.serviceImp;

import cn.exrick.xboot.modules.bussiness.shiProductComparison.mapper.s_ProductComparisonMapper;
import cn.exrick.xboot.modules.bussiness.shiProductComparison.pojo.productComparisonVo;
import cn.exrick.xboot.modules.bussiness.shiProductComparison.pojo.s_ProductComparison;
import cn.exrick.xboot.modules.bussiness.shiProductComparison.service.Is_ProductComparisonService;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import lombok.extern.slf4j.Slf4j;
import org.apache.ibatis.annotations.Param;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * 产品对比接口实现
 * @author 石博士
 */
@Slf4j
@Service
@Transactional
public class Is_ProductComparisonServiceImpl extends ServiceImpl<s_ProductComparisonMapper, s_ProductComparison> implements Is_ProductComparisonService {

    @Autowired
    private s_ProductComparisonMapper s_ProductComparisonMapper;




    /**
     * 查询产品对比
     */
    public IPage<productComparisonVo> QueryProductComparisonVo(Page page){
        return s_ProductComparisonMapper.QueryProductComparisonVo(page);
    }



    /**
     * 查询产品对比id
     */
    public IPage<productComparisonVo> QueryProductComparisonVoID(Page page, @Param("id") String id){
        return s_ProductComparisonMapper.QueryProductComparisonVoID(page,id);
    }


    /**
     * 查询产品对比id
     */
    public IPage<productComparisonVo> QueryProductComparisonVoIDList(Page page, @Param("id") String id){
        return s_ProductComparisonMapper.QueryProductComparisonVoIDList(page,id);
    }











}