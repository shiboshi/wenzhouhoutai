package cn.exrick.xboot.modules.bussiness.shiProductComparison.controller;

import cn.exrick.xboot.common.utils.PageUtil;
import cn.exrick.xboot.common.utils.ResultUtil;
import cn.exrick.xboot.common.vo.PageVo;
import cn.exrick.xboot.common.vo.Result;
import cn.exrick.xboot.modules.bussiness.shiProductComparison.pojo.basic_parameter;
import cn.exrick.xboot.modules.bussiness.shiProductComparison.service.Ibasic_parameterService;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * @author 石博士
 */
@Slf4j
@RestController
@Api(description = "基本参数表管理接口")
@RequestMapping("/xboot/basic_parameter")
@Transactional
public class basic_parameterController {

    @Autowired
    private Ibasic_parameterService ibasic_parameterService;

    @RequestMapping(value = "/get/{id}", method = RequestMethod.GET)
    @ApiOperation(value = "通过id获取")
    public Result<basic_parameter> get(@PathVariable String id){

        basic_parameter basic_parameter = ibasic_parameterService.getById(id);
        return new ResultUtil<basic_parameter>().setData(basic_parameter);
    }

    @RequestMapping(value = "/getAll", method = RequestMethod.GET)
    @ApiOperation(value = "获取全部数据")
    public Result<List<basic_parameter>> getAll(){

        List<basic_parameter> list = ibasic_parameterService.list();
        return new ResultUtil<List<basic_parameter>>().setData(list);
    }

    @RequestMapping(value = "/getByPage", method = RequestMethod.GET)
    @ApiOperation(value = "分页获取")
    public Result<IPage<basic_parameter>> getByPage(@ModelAttribute PageVo page){

        IPage<basic_parameter> data = ibasic_parameterService.page(PageUtil.initMpPage(page));
        return new ResultUtil<IPage<basic_parameter>>().setData(data);
    }

    @RequestMapping(value = "/insertOrUpdate", method = RequestMethod.POST)
    @ApiOperation(value = "编辑或更新数据")
    public Result<basic_parameter> saveOrUpdate(@ModelAttribute basic_parameter basic_parameter){

        if(ibasic_parameterService.saveOrUpdate(basic_parameter)){
            return new ResultUtil<basic_parameter>().setData(basic_parameter);
        }
        return new ResultUtil<basic_parameter>().setErrorMsg("操作失败");
    }

    @RequestMapping(value = "/delByIds/{ids}", method = RequestMethod.DELETE)
    @ApiOperation(value = "批量通过id删除")
    public Result<Object> delAllByIds(@PathVariable String[] ids){

        for(String id : ids){
            ibasic_parameterService.removeById(id);
        }
        return new ResultUtil<Object>().setSuccessMsg("批量通过id删除数据成功");
    }
}
