package cn.exrick.xboot.modules.bussiness.putInWarehouse.service;

import cn.exrick.xboot.modules.bussiness.putInWarehouse.VO.PutInWarehouseVO;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;
import cn.exrick.xboot.modules.bussiness.putInWarehouse.pojo.PutInWarehouse;

import java.util.List;

/**
 * 入库接口
 * @author tqr
 */
public interface IPutInWarehouseService extends IService<PutInWarehouse> {

    IPage<PutInWarehouseVO> getPutInWarehouse(Page initMpPage,String productOrderName,String status);
}