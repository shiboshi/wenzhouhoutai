package cn.exrick.xboot.modules.bussiness.tApplication.controller;

import cn.exrick.xboot.common.utils.PageUtil;
import cn.exrick.xboot.common.utils.ResultUtil;
import cn.exrick.xboot.common.utils.SecurityUtil;
import cn.exrick.xboot.common.vo.PageVo;
import cn.exrick.xboot.common.vo.Result;
import cn.exrick.xboot.modules.activiti.entity.ActBusiness;
import cn.exrick.xboot.modules.activiti.service.ActBusinessService;
import cn.exrick.xboot.modules.base.entity.User;
import cn.exrick.xboot.modules.bussiness.indentOrder.pojo.IndentOrder;
import cn.exrick.xboot.modules.bussiness.indentOrder.service.IindentOrderService;
import cn.exrick.xboot.modules.bussiness.refundtype.pojo.refundtype;
import cn.exrick.xboot.modules.bussiness.refundtype.service.IrefundtypeService;
import cn.exrick.xboot.modules.bussiness.tApplication.pojo.TApplication;
import cn.exrick.xboot.modules.bussiness.tApplication.pojo.TApplicationVO;
import cn.exrick.xboot.modules.bussiness.tApplication.service.ITApplicationService;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpSession;
import java.util.List;

/**
 * @author tqr
 */
@Slf4j
@RestController
@Api(description = "退款申请表管理接口")
@RequestMapping("/xboot/tApplication")
@Transactional
public class TApplicationController {


    @Autowired
    private ITApplicationService iTApplicationService;

    @Autowired
    private IrefundtypeService irefundtypeService;

    @Autowired
    private SecurityUtil securityUtil;

    @Autowired
    private ActBusinessService actBusinessService;

    @Autowired
    private IindentOrderService iindentOrderService;




    @RequestMapping(value = "/get/{id}", method = RequestMethod.POST)
    @ApiOperation(value = "通过id获取")
    public Result<TApplication> get(@PathVariable String id){

        TApplication tApplication = iTApplicationService.getById(id);
        return new ResultUtil<TApplication>().setData(tApplication);
    }

    @RequestMapping(value = "/getAll", method = RequestMethod.POST)
    @ApiOperation(value = "获取全部数据")
    public Result<List<TApplication>> getAll(){

        List<TApplication> list = iTApplicationService.list();
        return new ResultUtil<List<TApplication>>().setData(list);
    }

    @RequestMapping(value = "/getByPage", method = RequestMethod.POST)
    @ApiOperation(value = "分页获取")
    public Result<IPage<TApplicationVO>> getByPage(@ModelAttribute PageVo page, HttpSession session, @RequestParam(value="procDefId") String procDefId){
            session.setAttribute("procDefId",procDefId);
        return new ResultUtil<IPage<TApplicationVO>>().setData(iTApplicationService.QueryTapplication(PageUtil.initMpPage(page),securityUtil.getCurrUser().getUsername()));
    }

    @RequestMapping(value = "/insertOrUpdate", method = RequestMethod.POST)
    @ApiOperation(value = "编辑或更新数据")
    public Result<TApplication> saveOrUpdate(@ModelAttribute TApplication tApplication,HttpSession session){

        //获取当前登录人 ID
        User u = securityUtil.getCurrUser();
        tApplication.setUserId(u.getId());
        //增加审核状态
        tApplication.setTStatus("0");
        tApplication.setCompletionStatus("0");
        if(iTApplicationService.saveOrUpdate(tApplication)){

            ActBusiness actBusiness = new ActBusiness();
            actBusiness.setProcDefId(session.getAttribute("procDefId").toString());
            actBusiness.setUserId(u.getId());
            tApplication = iTApplicationService.getOne(Wrappers.<TApplication>lambdaQuery()
            .orderByDesc(TApplication::getId).last("limit 1"));
            actBusiness.setTableId(tApplication.getId());
            ActBusiness actBusinessUse = actBusinessService.save(actBusiness);
            tApplication.setBusinessId(actBusinessUse.getId());
            iTApplicationService.updateById(tApplication);

            return new ResultUtil<TApplication>().setData(tApplication);
        }
        return new ResultUtil<TApplication>().setErrorMsg("操作失败");
    }

    @RequestMapping(value = "/delByIds", method = RequestMethod.DELETE)
    @ApiOperation(value = "批量通过id删除")
    public Result<Object> delAllByIds(String id){
            iTApplicationService.removeById(id);
        return new ResultUtil<Object>().setSuccessMsg("批量通过id删除数据成功");
    }



    @RequestMapping(value = "/refundTypeName", method = RequestMethod.POST)
    @ApiOperation(value = "查询退款类型")
    public Result<IPage<refundtype>> refundTypeName(@ModelAttribute PageVo page){

        IPage<refundtype> data = irefundtypeService.page(PageUtil.initMpPage(page));
        return new ResultUtil<IPage<refundtype>>().setData(data);
    }




    @RequestMapping(value = "/getAllIndentOrder", method = RequestMethod.POST)
    @ApiOperation(value = "获取订货单全部数据,取订货单名称")
    public Result<IPage<IndentOrder>> getAllIndentOrder(@ModelAttribute PageVo page){
        IPage<IndentOrder> list = iindentOrderService.page(PageUtil.initMpPage(page));
        return new ResultUtil<IPage<IndentOrder>>().setData(list);
    }






}








