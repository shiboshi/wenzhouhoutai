package cn.exrick.xboot.modules.activiti.vo;

import lombok.Data;
import lombok.experimental.Accessors;

import javax.annotation.security.DenyAll;

/**
 * @Author : xiaofei
 * @Date: 2019/8/23
 */
@Data
@Accessors(chain = true)
public class ActProcessVo {

    private String desc;
    private String proceDefId;


}
