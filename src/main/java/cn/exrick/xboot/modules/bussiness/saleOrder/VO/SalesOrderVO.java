package cn.exrick.xboot.modules.bussiness.saleOrder.VO;

import cn.exrick.xboot.common.enums.IsComplete;
import cn.exrick.xboot.common.vo.CommonVO;
import cn.exrick.xboot.modules.bussiness.product.VO.ProductCommon;
import cn.exrick.xboot.modules.bussiness.purchaseOrder.VO.PurchaseOrderVO;
import lombok.Data;
import lombok.experimental.Accessors;

import javax.persistence.Column;
import java.util.List;

/**
 * 销售单VO
 * @Author : xiaofei
 * @Date: 2019/8/10
 */
@Data
@Accessors(chain = true)
public class SalesOrderVO extends CommonVO {

    /**
     *订单编号
     */
    private String orderNum;

    private String purchaseOrderIds;

    /**
     * 购买者姓名
     */
    private String buyerName;

    /**
     * 购买者手机号
     */
    private String buyerPhone;

    /**
     * 购买者收货地址
     */
    private String buyerAddr;

    /**
     * 销售类型1.门店仓库2.经销商仓库
     */
    private String warehouseType;

    private String checkStatus;
    private String sendStatus;
    private String createBy;
    /**
     * 购买者收货地址id
     */
    private String buyerAddrId;

    /**
     * 购买者收货详细地址
     */
    private String buyerDetailAddr;

    private List<ProductCommon> list;
}
