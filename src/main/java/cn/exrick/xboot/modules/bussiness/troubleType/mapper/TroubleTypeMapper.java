package cn.exrick.xboot.modules.bussiness.troubleType.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import cn.exrick.xboot.modules.bussiness.troubleType.pojo.TroubleType;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

/**
 * 问题类型表数据处理层
 * @author tqr
 */
@Mapper
public interface TroubleTypeMapper extends BaseMapper<TroubleType> {

}