package cn.exrick.xboot.modules.bussiness.distributorIndex.service;

import cn.exrick.xboot.modules.bussiness.distributorIndex.pojo.DistributorIndexVO;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;
import cn.exrick.xboot.modules.bussiness.distributorIndex.pojo.DistributorIndex;

import java.util.List;

/**
 * 经销商指标接口
 * @author tqr
 */
public interface IDistributorIndexService extends IService<DistributorIndex> {

    List<DistributorIndexVO> select(Page initMpPage, String deptName);
}