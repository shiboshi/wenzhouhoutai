package cn.exrick.xboot.modules.bussiness.purchaseOrder.controller;

import cn.exrick.xboot.common.utils.PageUtil;
import cn.exrick.xboot.common.utils.ResultUtil;
import cn.exrick.xboot.common.utils.SecurityUtil;
import cn.exrick.xboot.common.vo.PageVo;
import cn.exrick.xboot.common.vo.Result;
import cn.exrick.xboot.modules.bussiness.purchaseOrder.pojo.PurchaseOrder;
import cn.exrick.xboot.modules.bussiness.purchaseOrder.service.IpurchaseOrderService;
import com.alibaba.fastjson.JSON;
import com.baomidou.mybatisplus.core.metadata.IPage;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.json.simple.parser.JSONParser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.List;

/**
 * @author xiaofei
 */
@Slf4j
@RestController
@Api(description = "采购单管理接口")
@RequestMapping("/xboot/purchaseOrder")
@Transactional
public class purchaseOrderController {

    @Autowired
    private IpurchaseOrderService ipurchaseOrderService;
    @Autowired
    private SecurityUtil securityUtil;

    @RequestMapping(value = "/get/{id}", method = RequestMethod.GET)
    @ApiOperation(value = "通过id获取")
    public Result<PurchaseOrder> get(@PathVariable String id) {

        PurchaseOrder purchaseOrder = ipurchaseOrderService.getById(id);
        return new ResultUtil<PurchaseOrder>().setData(purchaseOrder);
    }

    @RequestMapping(value = "/getAll", method = RequestMethod.GET)
    @ApiOperation(value = "获取全部数据")
    public Result<List<PurchaseOrder>> getAll() {

        List<PurchaseOrder> list = ipurchaseOrderService.list();
        return new ResultUtil<List<PurchaseOrder>>().setData(list);
    }

    @RequestMapping(value = "/getByPage", method = RequestMethod.GET)
    @ApiOperation(value = "分页获取")
    public Result<IPage<PurchaseOrder>> getByPage(@ModelAttribute PageVo page) {

        IPage<PurchaseOrder> data = ipurchaseOrderService.page(PageUtil.initMpPage(page));
        return new ResultUtil<IPage<PurchaseOrder>>().setData(data);
    }

    @RequestMapping(value = "/insertOrUpdate", method = RequestMethod.POST)
    @ApiOperation(value = "编辑或更新数据")
    public Result<PurchaseOrder> saveOrUpdate(@ModelAttribute PurchaseOrder purchaseOrder) {

        if (ipurchaseOrderService.saveOrUpdate(purchaseOrder)) {
            return new ResultUtil<PurchaseOrder>().setData(purchaseOrder);
        }
        return new ResultUtil<PurchaseOrder>().setErrorMsg("操作失败");
    }

    @RequestMapping(value = "/delByIds/{ids}", method = RequestMethod.DELETE)
    @ApiOperation(value = "批量通过id删除")
    public Result<Object> delAllByIds(@PathVariable String[] ids) {

        for (String id : ids) {
            ipurchaseOrderService.removeById(id);
        }
        return new ResultUtil<Object>().setSuccessMsg("批量通过id删除数据成功");
    }

    @RequestMapping(value = "/chooseChanel", method = RequestMethod.POST)
    @ApiOperation(value = "选择渠道")
    public Result<PurchaseOrder> chooseChanel(String purchaseOrder) {
        List<PurchaseOrder> list = JSON.parseArray(purchaseOrder, PurchaseOrder.class);
        for (PurchaseOrder order : list){
            order.setUpdateBy(securityUtil.getCurrUser().getId());
            order.setUpdateTime(new Date());
        }
        try {
            ipurchaseOrderService.updateBatchById(list);
            return new ResultUtil<PurchaseOrder>().setSuccessMsg("操作成功");
        } catch (Exception e) {
            return new ResultUtil<PurchaseOrder>().setErrorMsg("操作失败");
        }

    }
}
