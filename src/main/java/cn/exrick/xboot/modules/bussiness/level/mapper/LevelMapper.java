package cn.exrick.xboot.modules.bussiness.level.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import cn.exrick.xboot.modules.bussiness.level.pojo.Level;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * 等级表数据处理层
 * @author tqr
 */
@Repository
public interface LevelMapper extends BaseMapper<Level> {

}