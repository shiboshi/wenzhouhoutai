package cn.exrick.xboot.modules.UpayApi.WxPay;

import lombok.Data;

/**
 * @Author : xiaofei
 * @Date: 2019/9/6
 */
@Data
public class OpenIdRequest {

    /**
     * 小程序 appId
     */
    private String appid;
    /**
     * appSecret
     */
    private String secret;
    /**
     * code
     */
    private String js_code;
    /**
     * 授权类型，此处只需填写 authorization_code
     */
    private String grant_type = "authorization_code";
}
