package cn.exrick.xboot.modules.bussiness.transferOrder.VO;

import cn.exrick.xboot.common.vo.CommonVO;
import cn.exrick.xboot.modules.bussiness.inventoryOrder.VO.InventoryVO;
import cn.exrick.xboot.modules.bussiness.inventoryOrder.pojo.InventoryOrderResult;
import lombok.Data;

/**
 * @Author : xiaofei
 * @Date: 2019/8/27
 */
@Data
public class TransferVO extends CommonVO {

    /**
     * 转移仓库
     */
    private String warhouseName;

    /**
     * 转移仓库
     */
    private String oldWarhouseName;

    private String transferOrderNum;

    private InventoryOrderResult inventoryVO;

    private String warhouseId;
    private String oldwarhouseId;

    private String inventoryIds;
    /**
     * 确认收货状态 0:未确认 1：已确认
     */
    private Integer status;
}
