package cn.exrick.xboot.modules.activiti.controller;

import cn.exrick.xboot.common.constant.ActivitiConstant;
import cn.exrick.xboot.common.enums.PayType;
import cn.exrick.xboot.common.exception.XbootException;
import cn.exrick.xboot.common.utils.PageUtil;
import cn.exrick.xboot.common.utils.ResultUtil;
import cn.exrick.xboot.common.utils.ToolUtil;
import cn.exrick.xboot.common.vo.PageVo;
import cn.exrick.xboot.common.vo.Result;
import cn.exrick.xboot.common.vo.SearchVo;
import cn.exrick.xboot.modules.activiti.entity.ActBusiness;
import cn.exrick.xboot.modules.activiti.entity.ActProcess;
import cn.exrick.xboot.modules.activiti.service.ActBusinessService;
import cn.exrick.xboot.modules.activiti.service.ActProcessService;
import cn.exrick.xboot.modules.activiti.service.business.LeaveService;
import cn.exrick.xboot.modules.activiti.service.mybatis.IActService;
import cn.exrick.xboot.modules.base.service.UserService;
import cn.exrick.xboot.modules.bussiness.applicationSendGoodsOrder.service.IapplicationSendGoodsOrderService;
import cn.exrick.xboot.modules.bussiness.dealerLevel.mapper.DealerLevelMapper;
import cn.exrick.xboot.modules.bussiness.dealerLevel.pojo.DealerLevel;
import cn.exrick.xboot.modules.bussiness.dealerLevel.service.IDealerLevelService;
import cn.exrick.xboot.modules.bussiness.indentOrder.mapper.IndentOrderMapper;
import cn.exrick.xboot.modules.bussiness.indentOrder.pojo.IndentOrder;
import cn.exrick.xboot.modules.bussiness.purchaseOrder.pojo.PurchaseOrder;
import cn.exrick.xboot.modules.bussiness.purchaseOrder.service.IpurchaseOrderService;
import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.activiti.engine.RuntimeService;
import org.activiti.engine.TaskService;
import org.activiti.engine.task.Task;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * @author Exrick
 */
@Slf4j
@RestController
@Api(description = "业务申请管理接口")
@RequestMapping("/xboot/actBusiness")
@Transactional
public class ActBusinessController {

    @Autowired
    private ActBusinessService actBusinessService;

    @Autowired
    private IActService iActService;

    @Autowired
    private ActProcessService actProcessService;

    @Autowired
    private TaskService taskService;

    @Autowired
    private RuntimeService runtimeService;

    @Autowired
    private LeaveService leaveService;
    @Autowired
    private IndentOrderMapper indentOrderMapper;
    @Autowired
    private DealerLevelMapper dealerLevelMapper;
    @Autowired
    private UserService userService;
    @Autowired
    private IapplicationSendGoodsOrderService applicationSendGoodsOrderService;
    @Autowired
    private IpurchaseOrderService ipurchaseOrderService;
    @Autowired
    private IDealerLevelService dealerLevelService;

    @RequestMapping(value = "/getByCondition", method = RequestMethod.GET)
    @ApiOperation(value = "多条件分页获取申请列表")
    public Result<Page<ActBusiness>> getByCondition(@ModelAttribute ActBusiness actBusiness,
                                                    @ModelAttribute SearchVo searchVo,
                                                    @ModelAttribute PageVo pageVo) {

        Page<ActBusiness> page = actBusinessService.findByCondition(actBusiness, searchVo, PageUtil.initPage(pageVo));
        page.getContent().forEach(e -> {
            /*if (StrUtil.isNotBlank(e.getProcDefId())) {*/
            if (e.getProcDefId()!=null){
                ActProcess actProcess = actProcessService.get(e.getProcDefId());
                e.setRouteName(actProcess.getRouteName());
                e.setProcessName(actProcess.getName());
            }
            if (ActivitiConstant.STATUS_DEALING.equals(e.getStatus())) {
                // 关联当前任务
                List<Task> task = taskService.createTaskQuery().processInstanceId(e.getProcInstId()).list();
                if (null != task & task.size() >=0 ) {
                    Task task1=task.get(0);
                    e.setCurrTaskName(task1.getName());
                }
            }
        });
        return new ResultUtil<Page<ActBusiness>>().setData(page);
    }

    @RequestMapping(value = "/apply", method = RequestMethod.POST)
    @ApiOperation(value = "提交申请 启动流程")
    public Result<Object> apply(@ModelAttribute ActBusiness act) {

        if (ToolUtil.isNotEmpty(act.getProcDefId())) {
            if (act.getProcDefId().contains("x_indent_order")) {
                if (ToolUtil.isNotEmpty(act.getId())) {
                    //查询订货单
                    IndentOrder indentOrder = indentOrderMapper.selectOne(new QueryWrapper<IndentOrder>().lambda().eq(IndentOrder::getBussinessId, act.getId()));
                    if (ToolUtil.isNotEmpty(indentOrder)) {
                        String user = indentOrder.getCreateBy();
                        List<PurchaseOrder> purchaseOrders = new ArrayList<>();
                        String[] ids = indentOrder.getPurchaseOrderIds().split(",");
                        for (String id : ids) {
                            PurchaseOrder purchaseOrder = ipurchaseOrderService.getById(id);
                            if (ToolUtil.isNotEmpty(purchaseOrder)) {
                                purchaseOrders.add(purchaseOrder);
                            }
                        }
                        BigDecimal totalAmout = new BigDecimal(purchaseOrders.stream().mapToDouble(p -> p.getPrice().multiply(p.getOrderNumbers()).setScale(2, BigDecimal.ROUND_HALF_UP).doubleValue()).sum());
                        DealerLevel dealerLevel = dealerLevelService.getOne(Wrappers.<DealerLevel>lambdaQuery()
                                .eq(DealerLevel::getDeptId, userService.get(user).getDepartmentId()));
                        if (ToolUtil.isNotEmpty(indentOrder.getPayType())) {

                            if (indentOrder.getPayType().getValue() == (PayType.BLANCE.getValue())) {
                                if (dealerLevel.getBalance().compareTo(totalAmout) == -1) {
                                    return new ResultUtil<>().setErrorMsg(201, "账户余额不足，请走代充值渠道！！！");
                                }
                            } else if (indentOrder.getPayType().getValue() == (PayType.CRIDIT.getValue())) {
                                if (dealerLevel.getCredit().compareTo(totalAmout) == -1) {
                                    return new ResultUtil<>().setErrorMsg(202, "信用额度不足，请走代充值渠道!!! ");
                                }
                            }
                        }
                    }
                }
            }
        }
        ActBusiness actBusiness = actBusinessService.get(act.getId());
        act.setTableId(actBusiness.getTableId());
        // 根据你的业务需求放入相应流程所需变量
        act = putParams(act);
        String processInstanceId = actProcessService.startProcess(act);
        actBusiness.setProcInstId(processInstanceId);
        actBusiness.setStatus(ActivitiConstant.STATUS_DEALING);
        actBusiness.setResult(ActivitiConstant.RESULT_DEALING);
        actBusiness.setApplyTime(new Date());
        actBusinessService.update(actBusiness);
        return new ResultUtil<Object>().setSuccessMsg("操作成功");
    }

    @RequestMapping(value = "/start", method = RequestMethod.POST)
    @ApiOperation(value = "流程选择组件启动流程")
    public Result<Object> start(@ModelAttribute ActBusiness act) {

        ActBusiness actBusiness = actBusinessService.get(act.getId());
        act.setTableId(actBusiness.getTableId());
        // 根据你的业务需求放入相应流程所需变量
        act = putParams(act);
        String processInstanceId = actProcessService.startProcess(act);
        actBusiness.setProcDefId(act.getProcDefId());
        actBusiness.setTitle(act.getTitle());
        actBusiness.setProcInstId(processInstanceId);
        actBusiness.setStatus(ActivitiConstant.STATUS_DEALING);
        actBusiness.setResult(ActivitiConstant.RESULT_DEALING);
        actBusiness.setApplyTime(new Date());
        actBusinessService.update(actBusiness);
        return new ResultUtil<Object>().setSuccessMsg("操作成功");
    }

    /**
     * 放入相应流程所需变量
     *
     * @param act
     * @return
     */
    public ActBusiness putParams(ActBusiness act) {

        if (StrUtil.isBlank(act.getTableId())) {
            throw new XbootException("关联业务表TableId不能为空");
        }
        //
        // 放入变量

        return act;
    }

    @RequestMapping(value = "/cancel", method = RequestMethod.POST)
    @ApiOperation(value = "撤回申请")
    public Result<Object> cancel(@RequestParam String id,
                                 @RequestParam String procInstId,
                                 @RequestParam(required = false) String reason) {

        if (StrUtil.isBlank(reason)) {
            reason = "";
        }
        runtimeService.deleteProcessInstance(procInstId, "canceled-" + reason);
        ActBusiness actBusiness = actBusinessService.get(id);
        actBusiness.setStatus(ActivitiConstant.STATUS_CANCELED);
        actBusiness.setResult(ActivitiConstant.RESULT_TO_SUBMIT);
        actBusinessService.update(actBusiness);
        return new ResultUtil<Object>().setSuccessMsg("操作成功");
    }

    @RequestMapping(value = "/delByIds/{ids}", method = RequestMethod.DELETE)
    @ApiOperation(value = "通过id删除草稿状态申请")
    public Result<Object> delByIds(@PathVariable String[] ids) {

        for (String id : ids) {
            ActBusiness actBusiness = actBusinessService.get(id);
            if (!ActivitiConstant.STATUS_TO_APPLY.equals(actBusiness.getStatus())) {
                return new ResultUtil<>().setErrorMsg("删除失败, 仅能删除草稿状态的申请");
            }
            // 删除关联业务表
            ActProcess actProcess = actProcessService.get(actBusiness.getProcDefId());
            iActService.deleteBusiness(actProcess.getBusinessTable(), actBusiness.getTableId());
            actBusinessService.delete(id);
        }
        return new ResultUtil<Object>().setSuccessMsg("删除成功");
    }
}
