package cn.exrick.xboot.modules.bussiness.applicationAfterSaleOrder.controller;

import cn.exrick.xboot.common.enums.AfterSaleType;
import cn.exrick.xboot.common.utils.PageUtil;
import cn.exrick.xboot.common.utils.ResultUtil;
import cn.exrick.xboot.common.vo.PageVo;
import cn.exrick.xboot.common.vo.Result;
import cn.exrick.xboot.modules.bussiness.applicationAfterSaleOrder.VO.ApplicationAfterSaleOrderVO;
import cn.exrick.xboot.modules.bussiness.applicationAfterSaleOrder.pojo.ApplicationAfterSaleOrder;
import cn.exrick.xboot.modules.bussiness.applicationAfterSaleOrder.service.IapplicationAfterSaleOrderService;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeoutException;

/**
 * @author xiaofei
 */
@Slf4j
@RestController
@Api(description = "申请售后单管理接口")
@RequestMapping("/xboot/applicationAfterSaleOrder")
@Transactional
public class applicationAfterSaleOrderController {

    @Autowired
    private IapplicationAfterSaleOrderService iapplicationAfterSaleOrderService;

    /**
     * 查询全部数据
     * @param applicationAfterSaleOrderVO
     * @param pageVo
     * @return
     */
    @PostMapping("/getAll")
    @ApiOperation(value = "获取全部申请售后单")
    public Result<Page<ApplicationAfterSaleOrderVO>> getAll(ApplicationAfterSaleOrderVO applicationAfterSaleOrderVO, PageVo pageVo,String userId,String checkStatus,String numb) {
        Page<ApplicationAfterSaleOrderVO> lists = iapplicationAfterSaleOrderService.getList(applicationAfterSaleOrderVO,pageVo,userId,checkStatus,numb);
        return new ResultUtil<Page<ApplicationAfterSaleOrderVO>>().setData(lists,"查询申请售后单成功！");
    }

    /**插入申请售后单
     * @param afterSaleType
     * @return
     */
    @PostMapping("/insertApplicationAfteSale")
    public Result<Object> insertApplicationAfteSale(String inventoryIds, Integer afterSaleType,String procDefId){
        iapplicationAfterSaleOrderService.commitApplication(inventoryIds,afterSaleType == 1 ? AfterSaleType.SaleReturn:AfterSaleType.maintain,procDefId);
        return new ResultUtil<>().setSuccessMsg("插入成功！");
    }

    @RequestMapping(value = "/delByIds/{ids}", method = RequestMethod.DELETE)
    @ApiOperation(value = "批量通过id删除")
    public Result<Object> delAllByIds(@PathVariable String[] ids){

        for(String id : ids){
            iapplicationAfterSaleOrderService.removeById(id);
        }
        return new ResultUtil<Object>().setSuccessMsg("批量通过id删除数据成功");
    }
}
