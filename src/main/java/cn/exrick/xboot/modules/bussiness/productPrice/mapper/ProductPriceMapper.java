package cn.exrick.xboot.modules.bussiness.productPrice.mapper;

import cn.exrick.xboot.modules.bussiness.productPrice.ProductPriceProvider;
import cn.exrick.xboot.modules.bussiness.productPrice.VO.ProductPriceVO;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import cn.exrick.xboot.modules.bussiness.productPrice.pojo.ProductPrice;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;

import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.SelectProvider;
import org.springframework.stereotype.Repository;



/**
 * 产品价格表数据处理层
 * @author tqr
 */
@Repository
public interface ProductPriceMapper extends BaseMapper<ProductPrice> {

    @SelectProvider(type = ProductPriceProvider.class,method = "getProductPrice")
    IPage<ProductPriceVO> getProductPrice(@Param("initMpPage") Page initMpPage,@Param("condition") String condition);
}