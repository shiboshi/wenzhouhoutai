package cn.exrick.xboot.modules.bussiness.shiWarehouse.controller;

import cn.exrick.xboot.common.utils.PageUtil;
import cn.exrick.xboot.common.utils.ResultUtil;
import cn.exrick.xboot.common.vo.PageVo;
import cn.exrick.xboot.common.vo.Result;
import cn.exrick.xboot.modules.bussiness.inventoryOrder.pojo.InventoryOrder;
import cn.exrick.xboot.modules.bussiness.inventoryOrder.service.IInventoryOrderService;
import cn.exrick.xboot.modules.bussiness.shiWarehouse.WarehouseVO.WarehouseVO;
import cn.exrick.xboot.modules.bussiness.shiWarehouse.pojo.ShiWarehouse;
import cn.exrick.xboot.modules.bussiness.shiWarehouse.service.IShiWarehouseService;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.google.common.collect.Maps;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.transaction.annotation.Transactional;

import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author tqr
 */
@Slf4j
@RestController
@Api(description = "仓库表管理接口")
@RequestMapping("/xboot/shiWarehouse")
@Transactional
public class ShiWarehouseController {

    @Autowired
    private IShiWarehouseService iShiWarehouseService;
    @Autowired
    private IInventoryOrderService iInventoryOrderService;

    @RequestMapping(value = "/get/{id}", method = RequestMethod.GET)
    @ApiOperation(value = "通过id获取")
    public Result<ShiWarehouse> get(@PathVariable String id) {

        ShiWarehouse shiWarehouse = iShiWarehouseService.getById(id);
        return new ResultUtil<ShiWarehouse>().setData(shiWarehouse);
    }

    @RequestMapping(value = "/getAll", method = RequestMethod.GET)
    @ApiOperation(value = "获取全部数据")
    public Result<List<ShiWarehouse>> getAll() {

        List<ShiWarehouse> list = iShiWarehouseService.list();
        return new ResultUtil<List<ShiWarehouse>>().setData(list);
    }

    @RequestMapping(value = "/insertOrUpdate", method = RequestMethod.POST)
    @ApiOperation(value = "编辑或更新数据")
    public Result<ShiWarehouse> saveOrUpdate(@ModelAttribute ShiWarehouse shiWarehouse) {

        if (iShiWarehouseService.saveOrUpdate(shiWarehouse)) {
            return new ResultUtil<ShiWarehouse>().setData(shiWarehouse);
        }
        return new ResultUtil<ShiWarehouse>().setErrorMsg("操作失败");
    }

    @RequestMapping(value = "/delByIds/{ids}", method = RequestMethod.DELETE)
    @ApiOperation(value = "批量通过id删除")
    public Result<Object> delAllByIds(@PathVariable String[] ids) {

        for (String id : ids) {
            if (iInventoryOrderService.getBaseMapper().selectList(new QueryWrapper<InventoryOrder>().lambda().eq(InventoryOrder::getWarhouseId, id)).size() > 0) {
                return new ResultUtil<Object>().setErrorMsg("该仓库有库存，不允许删除");
            }
            iShiWarehouseService.removeById(id);
        }
        return new ResultUtil<Object>().setSuccessMsg("批量通过id删除数据成功");
    }

    /**
     * 新建仓库
     */
    @RequestMapping(value = "/insertWarehouse", method = RequestMethod.POST)
    @ApiOperation(value = "编辑或更新数据新建仓库库")
    public Result<ShiWarehouse> insertWarehouse(@ModelAttribute ShiWarehouse shiWarehouse, HttpServletRequest request) {
        shiWarehouse.setWName(request.getParameter("wname"));
        if (iShiWarehouseService.insertWarehouse(shiWarehouse)) {
            return new ResultUtil<ShiWarehouse>().setData(shiWarehouse);
        }
        return new ResultUtil<ShiWarehouse>().setErrorMsg("操作失败");
    }

    /**
     * 查询
     *
     * @param page
     * @return
     */
    @RequestMapping(value = "/getWarehouse", method = RequestMethod.POST)
    @ApiOperation(value = "分页获取")
    public Result<Map<String, Object>> getWarehouse(@ModelAttribute PageVo page, String wName) {
        Map<String, Object> map = new HashMap<>();
        List<WarehouseVO> data = iShiWarehouseService.getWarehouse(wName);
        map.put("total",data.size());
        data = PageUtil.listToPage(page,data);
        map.put("data", data);
        return new ResultUtil<Map<String, Object>>().setData(map);
    }

    /**
     * 修改
     */
    @RequestMapping(value = "/editWarehouse", method = RequestMethod.POST)
    @ApiOperation(value = "分页获取")
    public Result<ShiWarehouse> editWarehouse(ShiWarehouse warehouse, HttpServletRequest request) {
        warehouse.setWName(request.getParameter("wname"));
        Result<ShiWarehouse> data = iShiWarehouseService.editWarehouse(warehouse);

        return data;
    }

    /**
     * 入库时查询仓库
     */
    @RequestMapping(value = "/getWarehouseByDept", method = RequestMethod.POST)
    public Result<List<ShiWarehouse>> getWarehouseByDept() {
        Result<List<ShiWarehouse>> data = iShiWarehouseService.getWarehouseByDept();
        return data;
    }

    /**
     * 调拨单时查询仓库
     */
    @RequestMapping(value = "/getTransferWarehouse", method = RequestMethod.POST)
    public Result<Map<String,Object>> getTransferWarehouse(@ModelAttribute PageVo page) {
        Map<String,Object> map = Maps.newHashMap();
        List<WarehouseVO> data = iShiWarehouseService.getTransferWarehouse();
        map.put("total", data.size());
        data = PageUtil.listToPage(page,data);
        map.put("data", data);
        return new ResultUtil<Map<String,Object>>().setData(map);
    }
}
