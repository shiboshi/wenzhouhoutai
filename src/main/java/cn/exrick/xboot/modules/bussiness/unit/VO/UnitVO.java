package cn.exrick.xboot.modules.bussiness.unit.VO;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Column;

@ApiModel("单位")
@Data
@NoArgsConstructor
@AllArgsConstructor
public class UnitVO {

    @ApiModelProperty("id")
    private String id;

    @ApiModelProperty("单位")
    private String unitName;

    private Boolean selected;
}
