package cn.exrick.xboot.modules.bussiness.srecharge.service;

import cn.exrick.xboot.modules.bussiness.srecharge.pojo.Srecharge;
import cn.exrick.xboot.modules.bussiness.srecharge.pojo.SrechargeOV;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;
import org.apache.ibatis.annotations.Param;

/**
 * 代充值接口
 * @author tqr
 */
public interface ISrechargeService extends IService<Srecharge> {



    IPage<Srecharge> getByPage(Page page);


    /**
     * 查询所有集合
     */
    IPage<SrechargeOV> QuerySrecharge(Page page, @Param("id") String id, @Param("username") String username);


    /**
     * 根据id查询详情
     */
    IPage<SrechargeOV> getByPageId(Page page, @Param("id") String id);




}