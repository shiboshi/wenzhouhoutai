package cn.exrick.xboot.config.mybatisplus;

import cn.exrick.xboot.common.utils.CommonUtil;
import cn.exrick.xboot.common.utils.SecurityUtil;
import cn.exrick.xboot.config.springContext.SpringContextHolder;
import cn.hutool.core.date.DateUtil;
import com.alibaba.druid.sql.visitor.functions.Now;
import com.baomidou.mybatisplus.core.handlers.MetaObjectHandler;
import org.apache.ibatis.reflection.MetaObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.DependsOn;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import javax.xml.crypto.Data;
import java.util.Date;

/**
 * @Author : xiaofei
 * @Date: 2019/8/9
 */
@Component
public class MyMetaObjectHandler implements MetaObjectHandler {

    @Override
    public void insertFill(MetaObject metaObject) {
        //设置更新时间
        Date now = DateUtil.parse(DateUtil.now());
        Object createTime = getFieldValByName("createTime",metaObject);
        if (CommonUtil.isEmpty(createTime)) {
            setFieldValByName("createTime", now, metaObject);
        }

        //从缓存中拿到当前登录用户

    }

    @Override
    public void updateFill(MetaObject metaObject) {
        //设置更新时间
        Date now = DateUtil.parse(DateUtil.now());
        setFieldValByName("updateTime",now,metaObject);
    }
}
