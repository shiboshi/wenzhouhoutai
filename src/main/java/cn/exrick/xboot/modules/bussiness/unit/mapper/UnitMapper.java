package cn.exrick.xboot.modules.bussiness.unit.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import cn.exrick.xboot.modules.bussiness.unit.pojo.Unit;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * 产品单位数据处理层
 * @author tqr
 */
@Repository
public interface UnitMapper extends BaseMapper<Unit> {

}