package cn.exrick.xboot.modules.conditionMethod;

import cn.exrick.xboot.common.utils.SecurityUtil;
import cn.exrick.xboot.common.utils.SpringContextUtil;
import cn.exrick.xboot.common.utils.ToolUtil;
import cn.exrick.xboot.modules.base.service.DepartmentService;
import cn.exrick.xboot.modules.bussiness.dealerLevel.pojo.DealerLevel;
import cn.exrick.xboot.modules.bussiness.dealerLevel.service.IDealerLevelService;
import cn.exrick.xboot.modules.bussiness.roleAddress.pojo.AddressRole;
import cn.exrick.xboot.modules.bussiness.roleAddress.service.IaddressRoleService;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import org.springframework.context.annotation.Bean;

import javax.annotation.Resource;
import java.sql.Wrapper;
import java.util.List;
import java.util.stream.Collectors;

/**
 * @Author : xiaofei
 * @Date: 2019/8/17
 */
@Resource
public class Method {

    /**
     * 注入bean
     */
    public  SecurityUtil securityUtil = SpringContextUtil.getBean(SecurityUtil.class);
    public  IDealerLevelService dealerLevelService = SpringContextUtil.getBean(IDealerLevelService.class);
    public  IaddressRoleService addressRoleService = SpringContextUtil.getBean(IaddressRoleService.class);

    /**
     * 根据审批人来判断审核区域
     * @return
     */
    public boolean Mybean(){
        //判断当前登录的经销商的地址 是否在审核人负责的区域内
        DealerLevel  dealerLevel =  dealerLevelService.getOne(Wrappers.<DealerLevel>lambdaQuery()
        .eq(DealerLevel::getDeptId,securityUtil.getCurrUser().getDepartmentId()));
        //获取当前角色的所对应的区域
        List<AddressRole> addressRoles = addressRoleService.list(Wrappers.<AddressRole>lambdaQuery()
        .eq(AddressRole::getRoleId,"175131207976095744"));
        List<String> addressIds = addressRoles.stream().map(role -> role.getAddressId()).collect(Collectors.toList());

        if (ToolUtil.isNotEmpty(dealerLevel)){
            if (addressIds.contains(dealerLevel.getAreaId())){
                return true;
            }
        }
        return false;
    }

}
