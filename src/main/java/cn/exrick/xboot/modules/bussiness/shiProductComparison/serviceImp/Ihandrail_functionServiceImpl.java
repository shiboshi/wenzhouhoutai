package cn.exrick.xboot.modules.bussiness.shiProductComparison.serviceImp;

import cn.exrick.xboot.modules.bussiness.shiProductComparison.mapper.handrail_functionMapper;
import cn.exrick.xboot.modules.bussiness.shiProductComparison.pojo.handrail_function;
import cn.exrick.xboot.modules.bussiness.shiProductComparison.service.Ihandrail_functionService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

/**
 * 臀部功能接口实现
 * @author 石博士
 */
@Slf4j
@Service
@Transactional
public class Ihandrail_functionServiceImpl extends ServiceImpl<handrail_functionMapper, handrail_function> implements Ihandrail_functionService {

    @Autowired
    private handrail_functionMapper handrail_functionMapper;
}