package cn.exrick.xboot.common.utils;

import cn.exrick.xboot.common.constant.SettingConstant;
import cn.exrick.xboot.common.enums.Order;
import org.apache.commons.lang3.StringUtils;

import java.lang.reflect.Field;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 * @author Exrickx
 */
public class CommonUtil {

    /**
     * 以UUID重命名
     *
     * @param fileName
     * @return
     */
    public static String renamePic(String fileName) {
        String extName = fileName.substring(fileName.lastIndexOf("."));
        return UUID.randomUUID().toString().replace("-", "") + extName;
    }

    /**
     * 获得短信模版Key
     *
     * @param type
     * @return
     */
    public static String getSmsTemplate(Integer type) {
        switch (type) {
            case 0:
                return SettingConstant.ALI_SMS_COMMON;
            case 1:
                return SettingConstant.ALI_SMS_REGIST;
            case 2:
                return SettingConstant.ALI_SMS_LOGIN;
            case 3:
                return SettingConstant.ALI_SMS_CHANGE_MOBILE;
            case 4:
                return SettingConstant.ALI_SMS_CHANG_PASS;
            case 5:
                return SettingConstant.ALI_SMS_RESET_PASS;
            case 6:
                return SettingConstant.ALI_SMS_ACTIVITI;
            default:
                return SettingConstant.ALI_SMS_COMMON;
        }
    }

    /**
     * 随机6位数生成
     */
    public static String getRandomNum() {

        Random random = new Random();
        int num = random.nextInt(999999);
        //不足六位前面补0
        String str = String.format("%06d", num);
        return str;
    }

    /**
     * 批量递归删除时 判断target是否在ids中 避免重复删除
     *
     * @param target
     * @param ids
     * @return
     */
    public static Boolean judgeIds(String target, String[] ids) {

        Boolean flag = false;
        for (String id : ids) {
            if (id.equals(target)) {
                flag = true;
                break;
            }
        }
        return flag;
    }

    public static String getTimeStr() {
        //获取日历单例
        Calendar c = Calendar.getInstance();
        //格式化
        SimpleDateFormat f = new SimpleDateFormat("yyyyMMddHHmmss");
        //截取
        return f.format(c.getTime());
    }

    /**
     * 自动生成编号
     *
     * @param type
     * @return
     */
    public static String createOrderSerial(Order type) {
        return type + getTimeStr();
    }

    /**
     * 判断对象中属性值是否全为空
     *
     * @param object
     * @return
     */
    public static boolean isEmpty(Object object) {
        if (null == object) {
            return true;
        }

        try {
            for (Field f : object.getClass().getDeclaredFields()) {
                f.setAccessible(true);

                System.out.print(f.getName() + ":");
                System.out.println(f.get(object));

                if (f.get(object) != null && StringUtils.isNotBlank(f.get(object).toString())) {
                    return false;
                }

            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return true;
    }

    /**
     * 序列号分割
     *
     * @return
     */
    public static List<String> getSerial(String serialNumber) {
        if (ToolUtil.isEmpty(serialNumber)) {
            return null;
        }

        try {
            String[] seri = null;
            if (serialNumber.indexOf(" ") != -1) {
                seri = serialNumber.split("\\s+");
            } else {
                seri = serialNumber.split("\\|");
            }
            if (ToolUtil.isNotEmpty(seri)) {
                List<String> resultList = new ArrayList<>(Arrays.asList(seri));
                return resultList;
            } else {
                return null;
            }
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

}
