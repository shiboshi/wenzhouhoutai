package cn.exrick.xboot.modules.bussiness.shiProductComparison.controller;

import cn.exrick.xboot.common.utils.PageUtil;
import cn.exrick.xboot.common.utils.ResultUtil;
import cn.exrick.xboot.common.vo.PageVo;
import cn.exrick.xboot.common.vo.Result;
import cn.exrick.xboot.modules.bussiness.shiProductComparison.pojo.shoulder_function;
import cn.exrick.xboot.modules.bussiness.shiProductComparison.service.Ishoulder_functionService;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * @author 石博士
 */
@Slf4j
@RestController
@Api(description = "肩部功能管理接口")
@RequestMapping("/xboot/shoulder_function")
@Transactional
public class shoulder_functionController {

    @Autowired
    private Ishoulder_functionService ishoulder_functionService;

    @RequestMapping(value = "/get/{id}", method = RequestMethod.GET)
    @ApiOperation(value = "通过id获取")
    public Result<shoulder_function> get(@PathVariable String id){

        shoulder_function shoulder_function = ishoulder_functionService.getById(id);
        return new ResultUtil<shoulder_function>().setData(shoulder_function);
    }

    @RequestMapping(value = "/getAll", method = RequestMethod.GET)
    @ApiOperation(value = "获取全部数据")
    public Result<List<shoulder_function>> getAll(){

        List<shoulder_function> list = ishoulder_functionService.list();
        return new ResultUtil<List<shoulder_function>>().setData(list);
    }

    @RequestMapping(value = "/getByPage", method = RequestMethod.GET)
    @ApiOperation(value = "分页获取")
    public Result<IPage<shoulder_function>> getByPage(@ModelAttribute PageVo page){

        IPage<shoulder_function> data = ishoulder_functionService.page(PageUtil.initMpPage(page));
        return new ResultUtil<IPage<shoulder_function>>().setData(data);
    }

    @RequestMapping(value = "/insertOrUpdate", method = RequestMethod.POST)
    @ApiOperation(value = "编辑或更新数据")
    public Result<shoulder_function> saveOrUpdate(@ModelAttribute shoulder_function shoulder_function){

        if(ishoulder_functionService.saveOrUpdate(shoulder_function)){
            return new ResultUtil<shoulder_function>().setData(shoulder_function);
        }
        return new ResultUtil<shoulder_function>().setErrorMsg("操作失败");
    }

    @RequestMapping(value = "/delByIds/{ids}", method = RequestMethod.DELETE)
    @ApiOperation(value = "批量通过id删除")
    public Result<Object> delAllByIds(@PathVariable String[] ids){

        for(String id : ids){
            ishoulder_functionService.removeById(id);
        }
        return new ResultUtil<Object>().setSuccessMsg("批量通过id删除数据成功");
    }
}
