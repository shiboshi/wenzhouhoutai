package cn.exrick.xboot.modules.bussiness.afterIndentOrder.VO;

import cn.exrick.xboot.common.enums.IsComplete;
import cn.exrick.xboot.common.vo.CommonVO;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

/**
 * @Author : xiaofei
 * @Date: 2019/8/11
 */
@Data
@Accessors(chain = true)
public class AfterIndentVO extends CommonVO {

    @ApiModelProperty("订单编号")
    private String orderNum;

    @ApiModelProperty("完成状态")
    private IsComplete isComplete;

    @ApiModelProperty("评论信息")
    private String comment;
}
