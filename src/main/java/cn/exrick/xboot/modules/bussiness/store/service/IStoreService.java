package cn.exrick.xboot.modules.bussiness.store.service;

import cn.exrick.xboot.modules.bussiness.shiVO.StoreVO;
import cn.exrick.xboot.modules.bussiness.store.pojo.Store;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;
import org.apache.ibatis.annotations.Param;

/**
 * 门店表接口
 * @author tqr
 */
public interface IStoreService extends IService<Store> {



    /**
     * 查询所有集合
     */
    IPage<StoreVO> QueryStore(Page page, @Param("username") String username);





}