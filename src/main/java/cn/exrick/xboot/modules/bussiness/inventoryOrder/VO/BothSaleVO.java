package cn.exrick.xboot.modules.bussiness.inventoryOrder.VO;

import cn.exrick.xboot.common.vo.CommonVO;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.Date;

@ApiModel("两地销售")
@Data
public class BothSaleVO extends CommonVO {

    @ApiModelProperty("id")
    private String id;
    private String addressId;
//    /**
//     * 经度
//     */
//    @ApiModelProperty("经度")
//    private String longitude;
//
//    /**
//     * 纬度
//     */
//    @ApiModelProperty("纬度")
//    private String latitude;
//
//    @ApiModelProperty("经纬度")
//    private String longitudeLatitude;

    /**
     * 申请状态
     */
    @ApiModelProperty("申请状态")
    private String checkCode;

    /**
     * 指定的经销商部门id
     */
    @ApiModelProperty("指定的经销商部门id")
    private String dealerDeptId;

    @ApiModelProperty("指定的经销商部门")
    private String dealerDept;

    /**
     * 详细地址
     */
    @ApiModelProperty("详细地址")
    private String address;

    /**
     * 经销商审核状态
     */
    @ApiModelProperty("经销商审核状态")
    private String checkCodeDealer;

    /**
     * 产品id
     */
    @ApiModelProperty("产品id")
    private String productOrderIds;

    /**
     * 产品序列号
     */
    @ApiModelProperty("产品序列号")
    private String serialNumber;

    @ApiModelProperty("产品大类")
    private String productName;

    @ApiModelProperty("产品型号")
    private String productModelName;

    @ApiModelProperty("产品品号")
    private String productOrderName;

    private String orderNumb;

    private String dealerName;
}
