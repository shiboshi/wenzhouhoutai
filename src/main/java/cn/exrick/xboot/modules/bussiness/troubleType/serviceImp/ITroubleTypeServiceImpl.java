package cn.exrick.xboot.modules.bussiness.troubleType.serviceImp;

import cn.exrick.xboot.modules.bussiness.troubleType.mapper.TroubleTypeMapper;
import cn.exrick.xboot.modules.bussiness.troubleType.pojo.TroubleType;
import cn.exrick.xboot.modules.bussiness.troubleType.service.ITroubleTypeService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

/**
 * 问题类型表接口实现
 * @author tqr
 */
@Slf4j
@Service
@Transactional
public class ITroubleTypeServiceImpl extends ServiceImpl<TroubleTypeMapper, TroubleType> implements ITroubleTypeService {

    @Autowired
    private TroubleTypeMapper troubleTypeMapper;
}