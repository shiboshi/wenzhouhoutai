package cn.exrick.xboot.modules.bussiness.product.service;

import cn.exrick.xboot.common.vo.PageVo;
import cn.exrick.xboot.common.vo.Result;
import cn.exrick.xboot.modules.bussiness.product.VO.ProductModelVo;
import cn.exrick.xboot.modules.bussiness.product.pojo.ProductOrder;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;
import cn.exrick.xboot.modules.bussiness.product.pojo.ProductModel;

import java.util.List;
import java.util.Map;

/**
 * 产品品号接口
 * @author xiaofei
 */
public interface IproductModelService extends IService<ProductModel> {


    Result<Map<String,Object>> getByName(PageVo page, String productModelName);

    Result<ProductModel> insertProduct(ProductModelVo productModelVo);

    Result<ProductModel> updateProduct(ProductModelVo productModelVo);
}