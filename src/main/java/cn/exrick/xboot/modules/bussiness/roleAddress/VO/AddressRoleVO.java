package cn.exrick.xboot.modules.bussiness.roleAddress.VO;

import cn.exrick.xboot.common.vo.CommonVO;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.springframework.web.bind.annotation.ModelAttribute;

/**
 * @Author : xiaofei
 * @Date: 2019/8/17
 */
@Data
public class AddressRoleVO extends CommonVO {

    @ApiModelProperty("区域名称")
    public String addressName;

    @ApiModelProperty("角色名称")
    public String roleName;

}
