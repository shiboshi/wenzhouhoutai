package cn.exrick.xboot.modules.bussiness.applicationReturnGoodsOrder.pojo;

import cn.exrick.xboot.base.XbootBaseEntity;
import cn.exrick.xboot.common.enums.CheckStatus;
import cn.exrick.xboot.common.enums.ReturnStatus;
import cn.exrick.xboot.common.enums.SendStatus;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModelProperty;
import io.swagger.annotations.ApiModel;
import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.Table;

/**
 * @author tqr
 */
@Data
@Entity
@Table(name = "t_application_return_goods_order")
@TableName("t_application_return_goods_order")
@ApiModel(value = "申请退货表")
public class ApplicationReturnGoodsOrder extends XbootBaseEntity {

    private static final long serialVersionUID = 1L;

    /**
     * 订单编号
     */
    @ApiModelProperty(value = "订单编号")
    private String orderNum;

    /**
     * 业务流程id
     */
    @ApiModelProperty(value = "业务流程id")
    private String bussinessId;

    /**
     * 订单id
     */
    @ApiModelProperty(value = "订单id")
    private String indentOrderId;

    /**
     * 采购单id
     */
    @ApiModelProperty(value = "采购单id")
    private String purchaseOrderIds;

    /**
     * 审核状态
     */
    private CheckStatus checkStatus;

    /**
     * 退货状态
     */
    private ReturnStatus returnStatus;

}