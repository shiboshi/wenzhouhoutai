package cn.exrick.xboot.modules.bussiness.inventoryOrder.VO;

import cn.exrick.xboot.modules.bussiness.product.VO.ProductCommon;
import cn.exrick.xboot.modules.bussiness.sendGoodsOrder.pojo.SendGoodsOrder;
import lombok.Data;

/**
 * @Author : xiaofei
 * @Date: 2019/8/21
 */
@Data
public class InventoryVO {

    private String id;
    /**
     * 数量
     */
    private Long amount;


    /**
     * 批次
     */
    private String batchNumber;


    /**
     * 仓库id
     */
    private String warhouseName;

    /**
     * 序列号
     */
    private String serialNum;


    private ProductCommon productCommon;
    private SendGoodsOrder sendGoodsOrder;

    /**
     * 收货数量
     */
    private String quantityReceived;



}
