package cn.exrick.xboot.modules.bussiness.inventoryOrder;

import cn.exrick.xboot.common.utils.ToolUtil;

public class InventoryProvider {
    public String getByPage(String deptId,String condition) {
        String sql = "";
        String str = "";
        str += "select i.create_time,i.id as id,i.warhouse_id,i.batch_number as batchNumber, " +
                "s.w_name as warhouseName, i.serial_num as serialNum, i.product_order_id as productOrderId," +
                " c.title as productOrderName, (select title from t_classification where id = c.parent_id) as productModelName, " +
                "(case i.serial_num_out when 1 then '未出库' when 2 then '已出库' end) as serialNumOut," +
                "(select title from t_classification where id = (select parent_id from t_classification where id = c.parent_id )) as productName" +
                " from t_inventory_order i " +
                "left join t_shi_warehouse s on i.warhouse_id = s.id and s.del_flag = 0 " +
                "left join x_product_order p on i.product_order_id = p.id and p.del_flag = 0 " +
                "left join t_classification c on c.id = p.kind_id and c.del_flag = 0 where (i.del_flag = 0 or i.del_flag is null) ";
        if(ToolUtil.isNotEmpty(deptId)){
            str += " and s.dept_id = '"+deptId+"'";
        }
        if(ToolUtil.isNotEmpty(condition)){
            str += " and (s.w_name like '%"+condition+"%' or (select title from t_classification where id = c.parent_id) like '%"+condition+"%' or i.serial_num like '%"+condition+"%' or c.title like '%"+condition+"%') ";
        }
        sql += str;
        return sql;
    }

    public String getMainWarehouse(String id,String condition) {
        String sql = "";
        String str = "";
        str += "select i.warhouse_id,i.create_time,i.id as id,i.amount as amount,i.batch_number as batchNumber, " +
                "s.w_name as warhouseName, i.serial_num as serialNum, i.product_order_id as productOrderId," +
                " c.title as productOrderName, (select title from t_classification where id = c.parent_id) as productModelName, " +
                "(case i.serial_num_out when 1 then '未出库' when 2 then '已出库' end) as serialNumOut," +
                "(select title from t_classification where id = (select parent_id from t_classification where id = c.parent_id )) as productName" +
                " from t_inventory_order i " +
                "left join t_shi_warehouse s on i.warhouse_id = s.id and s.del_flag = 0 " +
                "left join x_product_order p on i.product_order_id = p.id and p.del_flag = 0 " +
                "left join t_classification c on c.id = p.kind_id and c.del_flag = 0 where (i.del_flag = 0 or i.del_flag is null) ";
        if(ToolUtil.isNotEmpty(id)){
            str += " and i.warhouse_id = '"+id+"'";
        }
        if(ToolUtil.isNotEmpty(condition)){
            str += " and (s.w_name like '%"+condition+"%' or (select title from t_classification where id = c.parent_id) like '%"+condition+"%' or i.serial_num like '%"+condition+"%' or c.title like '%"+condition+"%') ";
        }
        sql += str;
        return sql;
    }

    public String getInventoryOrder(String deptId,String condition) {
        String sql = "";
        String str = "";
        str += "select COUNT(i.product_order_id) as amount,i.create_time,i.id as id,i.batch_number as batchNumber, " +
                "s.w_name as warhouseName, i.serial_num as serialNum, i.product_order_id as productOrderId," +
                " c.title as productOrderName, (select title from t_classification where id = c.parent_id) as productModelName, " +
                "(case i.serial_num_out when 1 then '未出库' when 2 then '已出库' end) as serialNumOut," +
                "(select title from t_classification where id = (select parent_id from t_classification where id = c.parent_id )) as productName" +
                " from t_inventory_order i " +
                "left join t_shi_warehouse s on i.warhouse_id = s.id and s.del_flag = 0 " +
                "left join x_product_order p on i.product_order_id = p.id and p.del_flag = 0 " +
                "left join t_classification c on c.id = p.kind_id and c.del_flag = 0 where (i.del_flag = 0 or i.del_flag is null) and i.serial_num_out='1' ";
        if(ToolUtil.isNotEmpty(deptId)){
            str += " and s.dept_id = '"+deptId+"'";
        }
        if(ToolUtil.isNotEmpty(condition)){
            str += " and (s.w_name like '%"+condition+"%' or (select title from t_classification where id = c.parent_id) like '%"+condition+"%' or i.serial_num like '%"+condition+"%' or c.title like '%"+condition+"%') ";
        }
        str += " GROUP BY i.warhouse_id,i.product_order_id ORDER BY i.create_time desc ";
        sql += str;
        return sql;
    }
}
