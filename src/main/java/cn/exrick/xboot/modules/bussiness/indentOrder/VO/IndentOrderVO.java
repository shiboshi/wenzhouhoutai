package cn.exrick.xboot.modules.bussiness.indentOrder.VO;

import cn.exrick.xboot.common.enums.CheckStatus;
import cn.exrick.xboot.common.vo.CommonVO;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;
import lombok.experimental.Accessors;
import org.springframework.format.annotation.DateTimeFormat;

import java.util.Date;

/**
 * @Author : xiaofei
 * @Date: 2019/8/9
 */
@Data
@Accessors(chain = true)
public class IndentOrderVO extends CommonVO {

    private String orderNum;
    private String checkStatus;
    private String iscomplete;
    private String createBy;
    /**
     *
     */
    private Integer code;



}
