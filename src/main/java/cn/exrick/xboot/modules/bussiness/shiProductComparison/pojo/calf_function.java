package cn.exrick.xboot.modules.bussiness.shiProductComparison.pojo;

import cn.exrick.xboot.base.XbootBaseEntity;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.Table;

/**
 * @author 石博士
 */
@Data
@Entity
@Table(name = "s_calf_function")
@TableName("s_calf_function")
@ApiModel(value = "小腿功能表")
public class calf_function extends XbootBaseEntity {

    private static final long serialVersionUID = 1L;


    @ApiModelProperty("腿部/脚部气囊，手工输入")
    private String foot_airbag;

    @ApiModelProperty("腿部拉伸，0没有，1有，2选配")
    private String stretch;

    @ApiModelProperty("小腿升降，0没有，1有，2选配")
    private String rise_fall;

    @ApiModelProperty("腿部凸点按摩，0没有，1有，2选配")
    private String bump_massage;

    @ApiModelProperty("足部翻转，0没有，1有，2选配")
    private String foot_turn;

    @ApiModelProperty("磁疗功能，手工输入")
    private String magnetic;

    @ApiModelProperty("腿部揉搓，0没有，1有，2选配")
    private String knead;

    @ApiModelProperty("腿部伸缩，0没有，1有，2选配")
    private String telescopic;

    @ApiModelProperty("膝盖/足部加热，0没有，1有，2选配")
    private String foot_heating;

    @ApiModelProperty("足部凸点按摩，0没有，1有，2选配")
    private String foot_bump_massages;

    @ApiModelProperty("足部滚轮刮痧，0没有，1有，2选配")
    private String roller_scraping;

    @ApiModelProperty("腿部/足部振动，0没有，1有，2选配")
    private String foot_vibration;





}