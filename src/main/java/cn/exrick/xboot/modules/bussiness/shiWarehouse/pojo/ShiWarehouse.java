package cn.exrick.xboot.modules.bussiness.shiWarehouse.pojo;

import cn.exrick.xboot.base.XbootBaseEntity;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModelProperty;
import io.swagger.annotations.ApiModel;
import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

/**
 * @author tqr
 */
@Data
@Entity
@Table(name = "t_shi_warehouse")
@TableName("t_shi_warehouse")
@ApiModel(value = "仓库表")
public class ShiWarehouse extends XbootBaseEntity {

    private static final long serialVersionUID = 1L;

    /**
     * 编号
     */
    private String serialNum;

    /**
     * 名称
     */
    private String wName;

    /**
     * 部门id
     * @return
     */
    private String deptId;
}