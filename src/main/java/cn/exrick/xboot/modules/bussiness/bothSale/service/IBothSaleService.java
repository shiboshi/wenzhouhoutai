package cn.exrick.xboot.modules.bussiness.bothSale.service;

import cn.exrick.xboot.common.vo.Result;
import cn.exrick.xboot.modules.base.entity.User;
import cn.exrick.xboot.modules.bussiness.dealerLevel.VO.DealerLevelVO;
import cn.exrick.xboot.modules.bussiness.inventoryOrder.VO.BothSaleVO;
import cn.exrick.xboot.modules.bussiness.product.VO.ProductCommon;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;
import cn.exrick.xboot.modules.bussiness.bothSale.pojo.BothSale;

import java.util.List;

/**
 * 两地销售接口
 * @author tqr
 */
public interface IBothSaleService extends IService<BothSale> {

    Result<BothSale> insertBothSaleCommit(BothSaleVO bothSaleVO,String procDefId);

    IPage<BothSaleVO> getBothSaleCommit(Page initMpPage,String userName , String id);

    Result<BothSale> insertBothSale(String id,String procDefId,String productIds);

    IPage<BothSaleVO> getBothSale(Page initMpPage, String userName, String id);

    Result<BothSale> updateById(BothSaleVO bothSaleVO);

    IPage<BothSaleVO> getBothSaleCommitByUser(Page initMpPage, String id,String code);

    IPage<BothSaleVO> getBothSaleByUser(Page initMpPage, String id,String code);

    List<ProductCommon> getBothSaleDetail(Page initMpPage, String id);

    void choose(String id, String dealerDeptId,String procInstId);

    List<User> getDealer(Page page , String id);
}