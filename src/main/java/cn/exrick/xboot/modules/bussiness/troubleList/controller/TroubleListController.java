package cn.exrick.xboot.modules.bussiness.troubleList.controller;

import cn.exrick.xboot.common.utils.PageUtil;
import cn.exrick.xboot.common.utils.ResultUtil;
import cn.exrick.xboot.common.vo.PageVo;
import cn.exrick.xboot.common.vo.Result;
import cn.exrick.xboot.modules.bussiness.troubleList.pojo.TroubleList;
import cn.exrick.xboot.modules.bussiness.troubleList.service.ITroubleListService;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * @author tqr
 */
@Slf4j
@RestController
@Api(description = "问题表管理接口")
@RequestMapping("/xboot/troubleList")
@Transactional
public class TroubleListController {

    @Autowired
    private ITroubleListService iTroubleListService;

    @RequestMapping(value = "/get/{id}", method = RequestMethod.GET)
    @ApiOperation(value = "通过id获取")
    public Result<TroubleList> get(@PathVariable String id){

        TroubleList troubleList = iTroubleListService.getById(id);
        return new ResultUtil<TroubleList>().setData(troubleList);
    }

    @RequestMapping(value = "/getAll", method = RequestMethod.GET)
    @ApiOperation(value = "获取全部数据")
    public Result<List<TroubleList>> getAll(){

        List<TroubleList> list = iTroubleListService.list();
        return new ResultUtil<List<TroubleList>>().setData(list);
    }

    @RequestMapping(value = "/getByPage", method = RequestMethod.GET)
    @ApiOperation(value = "分页获取")
    public Result<IPage<TroubleList>> getByPage(@ModelAttribute PageVo page){

        IPage<TroubleList> data = iTroubleListService.page(PageUtil.initMpPage(page));
        return new ResultUtil<IPage<TroubleList>>().setData(data);
    }

    @RequestMapping(value = "/insertOrUpdate", method = RequestMethod.POST)
    @ApiOperation(value = "编辑或更新数据")
    public Result<TroubleList> saveOrUpdate(@ModelAttribute TroubleList troubleList){

        if(iTroubleListService.saveOrUpdate(troubleList)){
            return new ResultUtil<TroubleList>().setData(troubleList);
        }
        return new ResultUtil<TroubleList>().setErrorMsg("操作失败");
    }

    @RequestMapping(value = "/delByIds/{ids}", method = RequestMethod.DELETE)
    @ApiOperation(value = "批量通过id删除")
    public Result<Object> delAllByIds(@PathVariable String[] ids){

        for(String id : ids){
            iTroubleListService.removeById(id);
        }
        return new ResultUtil<Object>().setSuccessMsg("批量通过id删除数据成功");
    }
}
