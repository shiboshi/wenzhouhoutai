package cn.exrick.xboot.modules.activiti.service.mybatis;

import cn.exrick.xboot.modules.activiti.dao.mapper.ActProcessMapper;
import cn.exrick.xboot.modules.activiti.entity.ActProcess;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * @Author : xiaofei
 * @Date: 2019/8/23
 */
@Service
public class IActProcessService extends ServiceImpl<ActProcessMapper, ActProcess> {
}
