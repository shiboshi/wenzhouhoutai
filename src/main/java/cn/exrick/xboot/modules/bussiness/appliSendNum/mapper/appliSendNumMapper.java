package cn.exrick.xboot.modules.bussiness.appliSendNum.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import cn.exrick.xboot.modules.bussiness.appliSendNum.pojo.AppliSendNum;

/**
 * 申请发货数据处理层
 * @author fei
 */
public interface appliSendNumMapper extends BaseMapper<AppliSendNum> {

}