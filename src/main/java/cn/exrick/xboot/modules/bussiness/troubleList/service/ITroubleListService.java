package cn.exrick.xboot.modules.bussiness.troubleList.service;

import com.baomidou.mybatisplus.extension.service.IService;
import cn.exrick.xboot.modules.bussiness.troubleList.pojo.TroubleList;

import java.util.List;

/**
 * 问题表接口
 * @author tqr
 */
public interface ITroubleListService extends IService<TroubleList> {

}