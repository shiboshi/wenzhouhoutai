package cn.exrick.xboot.modules.bussiness.shiVO;
import cn.exrick.xboot.common.constant.CommonConstant;
import lombok.Data;

/**
 * 装修申请返回值
 */
@Data
public class ZApplicationOV {

    private static final long serialVersionUID = 1L;


    private String id;

    private String createBy;

    private String createTime;

    private String updateBy;

    private String updateTime;

    private Integer delFlag = CommonConstant.STATUS_NORMAL;


    /**
     * 附件
     */
    private String enclosu;

    /**
     * 备注
     */
    private String remarks;

    /**
     * 申请人id
     */
    private String userId;


    /**
     * 流程定义id
     */
    private String businessId;


    /**
     * 装修申请，审核状态
     */
    private String decorationStatus;


    /**
     * 申请人返回值
     */
    private String username;


    /**
     * 装修申请返回值
     */
    private String shiApplicationStatus;


    /**
     * 申请人部门
     */
    private String titles;;





    /**
     * 完成状态
     */
    private String completionStatus;










}