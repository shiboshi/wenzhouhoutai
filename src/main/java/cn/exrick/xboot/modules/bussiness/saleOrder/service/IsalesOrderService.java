package cn.exrick.xboot.modules.bussiness.saleOrder.service;

import cn.exrick.xboot.common.vo.PageVo;
import cn.exrick.xboot.modules.activiti.entity.ActBusiness;
import cn.exrick.xboot.modules.bussiness.purchaseOrder.VO.PurchaseOrderVO;
import cn.exrick.xboot.modules.bussiness.purchaseOrder.pojo.PurchaseOrder;
import cn.exrick.xboot.modules.bussiness.saleOrder.VO.NeedCheckProcudct;
import cn.exrick.xboot.modules.bussiness.saleOrder.VO.SalesOrderVO;
import cn.exrick.xboot.modules.bussiness.saleOrder.pojo.SalesOrder;
import cn.exrick.xboot.modules.bussiness.sendGoodsOrder.pojo.SendGoodsOrder;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;

import java.math.BigDecimal;
import java.util.List;

/**
 * 销售订单接口
 * @author xiaofei
 */
public interface IsalesOrderService extends IService<SalesOrder> {

    //1.获取销售单列表
    Page<SalesOrderVO> getList(PageVo pageVo, SalesOrderVO salesOrderVO,String userId);

    //2.获取销售单详情
    List<PurchaseOrderVO> getDetail(String salsOrderId);

    //获取当前产品的特价价格
    BigDecimal getproductPrice(String productOrderId);

    SalesOrderVO selectById(String id);

    List<NeedCheckProcudct> getNeedCheck(List<PurchaseOrder> purchaseOrders);

    ActBusiness commitSend(List<PurchaseOrder> purchaseOrders, SalesOrder salesOrder, String userId);

    /**
     * 过滤需要特价审核的
     * @param param
     * @return
     */
    List<PurchaseOrder> filterNeedCheck(String param,String phone);

    Page<SalesOrderVO> getUserList(PageVo pageVo, SalesOrderVO salesOrderVO, String userId);

    void salesSend(SendGoodsOrder sendGoodsOrder,String taskId,String comment,String procInstId);
}