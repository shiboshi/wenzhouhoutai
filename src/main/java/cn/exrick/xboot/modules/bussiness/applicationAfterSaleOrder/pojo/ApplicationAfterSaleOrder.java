package cn.exrick.xboot.modules.bussiness.applicationAfterSaleOrder.pojo;

import cn.exrick.xboot.base.XbootBaseEntity;
import cn.exrick.xboot.common.enums.AfterSaleType;
import cn.exrick.xboot.common.enums.CheckStatus;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModelProperty;
import io.swagger.annotations.ApiModel;
import lombok.Data;
import lombok.experimental.Accessors;

import javax.persistence.Entity;
import javax.persistence.Table;

/**
 * @author xiaofei
 */
@Data
@Entity
@Table(name = "application_after_sale_order")
@TableName("application_after_sale_order")
@ApiModel(value = "申请售后单")
public class ApplicationAfterSaleOrder extends XbootBaseEntity {

    private static final long serialVersionUID = 1L;

    private String indentOrderId;

    private String inventoryIds;

    private AfterSaleType afterSaleType;

    private String businessId;

    private CheckStatus checkStatus;

    private String appliAfterNumb;


}