package cn.exrick.xboot.modules.bussiness.product.controller;

import cn.exrick.xboot.common.utils.CommonUtil;
import cn.exrick.xboot.common.utils.PageUtil;
import cn.exrick.xboot.common.utils.ResultUtil;
import cn.exrick.xboot.common.vo.PageVo;
import cn.exrick.xboot.common.vo.Result;
import cn.exrick.xboot.modules.bussiness.classification.pojo.Classification;
import cn.exrick.xboot.modules.bussiness.classification.service.IClassificationService;
import cn.exrick.xboot.modules.bussiness.product.VO.ProductModelVo;
import cn.exrick.xboot.modules.bussiness.product.VO.ProductOrderExtend;
import cn.exrick.xboot.modules.bussiness.product.pojo.Product;
import cn.exrick.xboot.modules.bussiness.product.pojo.ProductModel;
import cn.exrick.xboot.modules.bussiness.product.pojo.ProductOrder;
import cn.exrick.xboot.modules.bussiness.product.service.IproductModelService;
import cn.exrick.xboot.modules.bussiness.product.service.IproductOrderService;
import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.transaction.annotation.Transactional;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author xiaofei
 */
@Slf4j
@RestController
@Api(description = "产品品号管理接口")
@RequestMapping("/xboot/productModel")
@Transactional
public class productModelController {

    @Autowired
    private IproductModelService iproductModelService;
    @Autowired
    private IClassificationService iClassificationService;
    @Autowired
    private IproductOrderService iproductOrderService;

    @RequestMapping(value = "/get/{id}", method = RequestMethod.GET)
    @ApiOperation(value = "通过id获取")
    public Result<ProductModel> get(@PathVariable String id) {

        ProductModel productModel = iproductModelService.getById(id);
        return new ResultUtil<ProductModel>().setData(productModel);
    }

    @RequestMapping(value = "/getAll", method = RequestMethod.GET)
    @ApiOperation(value = "获取全部数据")
    public Result<List<ProductModel>> getAll() {

        List<ProductModel> list = iproductModelService.list();
        return new ResultUtil<List<ProductModel>>().setData(list);
    }

    @RequestMapping(value = "/getByPage", method = RequestMethod.GET)
    @ApiOperation(value = "分页获取/模糊搜索")
    public Result<Map<String, Object>> getByPage(@ModelAttribute PageVo page, String productModelName) {
        return iproductModelService.getByName(page, productModelName);
    }

    @RequestMapping(value = "/insertOrUpdate", method = RequestMethod.POST)
    @ApiOperation(value = "编辑或更新数据")
    public Result<ProductModel> saveOrUpdate(@ModelAttribute ProductModel productModel) {

        if (iproductModelService.saveOrUpdate(productModel)) {
            return new ResultUtil<ProductModel>().setData(productModel);
        }
        return new ResultUtil<ProductModel>().setErrorMsg("操作失败");
    }

    @RequestMapping(value = "/delByIds", method = RequestMethod.POST)
    @ApiOperation(value = "批量通过id删除")
    public Result<Object> delAllByIds(String idS) {
        String[] ids = idS.split(",");
        for (String id : ids) {
            if(iClassificationService.getBaseMapper().selectList(new QueryWrapper<Classification>().lambda().eq(Classification::getParentId,iproductModelService.getById(id).getKindId())).size()>0){
                return new ResultUtil<Object>().setErrorMsg("不允许删除，该型号下有数据");
            }
            deleteRecursion(id, ids);
        }
        return new ResultUtil<Object>().setSuccessMsg("删除数据成功");
    }

    public void deleteRecursion(String id, String[] ids) {

        // 查询分类表
        ProductModel productModel = iproductModelService.getById(id);
        Classification classification = null;
        if (productModel != null && StrUtil.isNotBlank(productModel.getKindId())) {
            classification = iClassificationService.getById(productModel.getKindId());
            //删除productModel
            iproductModelService.removeById(id);
        }
        // 查询子节点
        if (classification != null) {
            //删除classification
            iClassificationService.removeById(classification.getId());
            //二级节点集合
            List<Classification> childrenDeps = iClassificationService.findByParentIdOrderBySortOrder(classification.getId());
            if (childrenDeps.size() > 0) {
                for (Classification d : childrenDeps) {
                    if (!CommonUtil.judgeIds(d.getId(), ids)) {
                        ProductOrder productOrder = iproductOrderService.getOne(new QueryWrapper<ProductOrder>().lambda().eq(ProductOrder::getKindId, d.getId()).last(" limit 1 "));
                        Classification cf = null;
                        if (productOrder != null && StrUtil.isNotBlank(productOrder.getKindId())) {
                            cf = iClassificationService.getById(productOrder.getKindId());
                            //删除productOrder
                            iproductOrderService.remove(new QueryWrapper<ProductOrder>().lambda().eq(ProductOrder::getKindId, d.getId()));
                        }
                        if (cf != null) {
                            ////删除classification
                            iClassificationService.removeById(cf.getId());
                        }

                    }
                }
            }
        }

    }

    /**
     * 添加产品型号
     */
    @RequestMapping(value = "/insertProductModel", method = RequestMethod.POST)
    @ApiOperation(value = "添加产品")
    public Result<ProductModel> insertProductModel(@ModelAttribute ProductModelVo productModelVo) {

        Result<ProductModel> result = iproductModelService.insertProduct(productModelVo);
        return result;
    }

    /**
     * 修改产品
     */
    @RequestMapping(value = "/updateProductModel", method = RequestMethod.POST)
    @ApiOperation(value = "添加产品")
    public Result<ProductModel> updateProductModel(ProductModelVo productModelVo) {

        Result<ProductModel> result = iproductModelService.updateProduct(productModelVo);
        return result;
    }

}
