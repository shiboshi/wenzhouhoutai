package cn.exrick.xboot.modules.bussiness.putInWarehouseOrder.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import cn.exrick.xboot.modules.bussiness.putInWarehouseOrder.pojo.PutInWarehouseOrder;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * 入库单集合数据处理层
 * @author tqr
 */
@Repository
public interface PutInWarehouseOrderMapper extends BaseMapper<PutInWarehouseOrder> {

}