package cn.exrick.xboot.modules.bussiness.srecharge.pojo;

import cn.exrick.xboot.base.XbootBaseEntity;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.Table;
import java.math.BigDecimal;

/**
 * @author tqr
 */
@Data
@Entity
@Table(name = "t_srecharge")
@TableName("t_srecharge")
@ApiModel(value = "代充值")
public class Srecharge extends XbootBaseEntity {

    private static final long serialVersionUID = 1L;

    /**
     *类型
     */
    private String type;

    /**
     * 备注
     */
    private String remarks;

    /**
     * 申请人id
     */
    private String userId;

    /**
     *充值金额
     */
    private BigDecimal number;

    /**
     *审核状态
     */
    private Long srechargeStatus;

    @ApiModelProperty("业务流程表id")
    private String bussinessId;


    @ApiModelProperty("完成状态")
    private String completionStatus;


    @ApiModelProperty("经销商 id")
    private String dealerLevelId;


    @ApiModelProperty("代充值图片")
    private String rechargePicture;







}






