package cn.exrick.xboot.modules.bussiness.srecharge.controller;

import cn.exrick.xboot.common.utils.PageUtil;
import cn.exrick.xboot.common.utils.ResultUtil;
import cn.exrick.xboot.common.utils.SecurityUtil;
import cn.exrick.xboot.common.vo.PageVo;
import cn.exrick.xboot.common.vo.Result;
import cn.exrick.xboot.modules.activiti.entity.ActBusiness;
import cn.exrick.xboot.modules.activiti.service.ActBusinessService;
import cn.exrick.xboot.modules.base.entity.User;
import cn.exrick.xboot.modules.bussiness.dealerLevel.pojo.DealerLevel;
import cn.exrick.xboot.modules.bussiness.dealerLevel.service.IDealerLevelService;
import cn.exrick.xboot.modules.bussiness.shiRechargeType.pojo.ShiRechargeType;
import cn.exrick.xboot.modules.bussiness.shiRechargeType.service.IShiRechargeTypeService;
import cn.exrick.xboot.modules.bussiness.srecharge.pojo.Srecharge;
import cn.exrick.xboot.modules.bussiness.srecharge.pojo.SrechargeOV;
import cn.exrick.xboot.modules.bussiness.srecharge.service.ISrechargeService;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpSession;
import java.util.List;

/**
 * @author tqr
 */
@Slf4j
@RestController
@Api(description = "代充值管理接口")
@RequestMapping("/xboot/srecharge")
@Transactional
public class SrechargeController {


    @Autowired
    private ISrechargeService iSrechargeService;

    @Autowired
    private IShiRechargeTypeService iShiRechargeTypeService;

    @Autowired
    private SecurityUtil securityUtil;

    @Autowired
    private ActBusinessService actBusinessService;

    @Autowired
    private IDealerLevelService iDealerLevelService;



    @RequestMapping(value = "/get/{id}", method = RequestMethod.POST)
    @ApiOperation(value = "通过id获取")
    public Result<Srecharge> get(@PathVariable String id){
        return new ResultUtil<Srecharge>().setData(iSrechargeService.getById(id));
    }

    @RequestMapping(value = "/getAll", method = RequestMethod.POST)
    @ApiOperation(value = "获取全部数据")
    public Result<List<Srecharge>> getAll(){
        return new ResultUtil<List<Srecharge>>().setData(iSrechargeService.list());
    }

    @RequestMapping(value = "/getByPage", method = RequestMethod.POST)
    @ApiOperation(value = "分页获取")
    public Result<IPage<SrechargeOV>> getByPage(@ModelAttribute PageVo page,String id,HttpSession session, @RequestParam(value="procDefId") String procDefId){
        session.setAttribute("procDefId",procDefId);
        return new ResultUtil<IPage<SrechargeOV>>().setData(iSrechargeService.QuerySrecharge(PageUtil.initMpPage(page),id,securityUtil.getCurrUser().getUsername()));
    }

    @RequestMapping(value = "/insertOrUpdate", method = RequestMethod.POST)
    @ApiOperation(value = "编辑或更新数据")
    public Result<Srecharge> saveOrUpdate(@ModelAttribute Srecharge srecharge,String procDefId, HttpSession session){

           //获取当前登录人 ID
           User u = securityUtil.getCurrUser();
           srecharge.setUserId(u.getId());
           //存入审核状态
           Long srechargestatus = new Long((long)0);
           srecharge.setSrechargeStatus(srechargestatus);
           srecharge.setCompletionStatus("0");
           //保存经销商的 id
           srecharge.setDealerLevelId(srecharge.getDealerLevelId());
        if(iSrechargeService.saveOrUpdate(srecharge)){
            ActBusiness actBusiness = new ActBusiness();
            actBusiness.setProcDefId(session.getAttribute("procDefId").toString());
            //1.保存业务流程表
            actBusiness.setUserId(securityUtil.getCurrUser().getId());
            //2.找到最新的一条业务表数据
            Srecharge srechargeNew = iSrechargeService.getOne(Wrappers.<Srecharge>lambdaQuery()
            .orderByDesc(Srecharge::getId)
            .last("limit 1"));
            actBusiness.setTableId(srechargeNew.getId());
            ActBusiness actBusinessUse = actBusinessService.save(actBusiness);
            //3.更新业务表
                srechargeNew.setBussinessId(actBusinessUse.getId());
            iSrechargeService.updateById(srechargeNew);

            return new ResultUtil<Srecharge>().setData(srecharge);
        }
        return new ResultUtil<Srecharge>().setErrorMsg("操作失败");
    }

    @RequestMapping(value = "/delByIds", method = RequestMethod.DELETE)
    @ApiOperation(value = "批量通过id删除")
    public Result<Object> delAllByIds(String id){
            iSrechargeService.removeById(id);
        return new ResultUtil<Object>().setSuccessMsg("批量通过id删除数据成功");
    }


    @RequestMapping(value = "/getShiRechargeType", method = RequestMethod.POST)
    @ApiOperation(value = "查询充值类型")
    public Result<IPage<ShiRechargeType>> getShiRechargeType(@ModelAttribute PageVo page){
        return new ResultUtil<IPage<ShiRechargeType>>().setData(iShiRechargeTypeService.page(PageUtil.initMpPage(page)));
    }




    @RequestMapping(value = "/getAllDealerLevelName", method = RequestMethod.POST)
    @ApiOperation(value = "查询所有经销商的名称")
    public Result<List<DealerLevel>> getAllDealerLevelName(@ModelAttribute PageVo page){
        List<DealerLevel> list = iDealerLevelService.list();
        return new ResultUtil<List<DealerLevel>>().setData(list);
    }



    @RequestMapping(value = "/getByPageId", method = RequestMethod.POST)
    @ApiOperation(value = "根据ID查询详情")
    public Result<IPage<SrechargeOV>> getByPageId(@ModelAttribute PageVo page,String id){
        return new ResultUtil<IPage<SrechargeOV>>().setData(iSrechargeService.getByPageId(PageUtil.initMpPage(page),id));
    }





















}
