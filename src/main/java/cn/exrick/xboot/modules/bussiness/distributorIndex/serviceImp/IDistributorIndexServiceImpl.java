package cn.exrick.xboot.modules.bussiness.distributorIndex.serviceImp;

import cn.exrick.xboot.common.utils.SecurityUtil;
import cn.exrick.xboot.common.utils.ToolUtil;
import cn.exrick.xboot.common.vo.CommonVO;
import cn.exrick.xboot.modules.base.dao.mapper.DepartmentMapper;
import cn.exrick.xboot.modules.base.dao.mapper.UserMapper;
import cn.exrick.xboot.modules.bussiness.dealerLevel.mapper.DealerLevelMapper;
import cn.exrick.xboot.modules.bussiness.distributorIndex.mapper.DistributorIndexMapper;
import cn.exrick.xboot.modules.bussiness.distributorIndex.pojo.DistributorIndex;
import cn.exrick.xboot.modules.bussiness.distributorIndex.pojo.DistributorIndexVO;
import cn.exrick.xboot.modules.bussiness.distributorIndex.service.IDistributorIndexService;
import cn.exrick.xboot.modules.bussiness.monthlyIndicators.mapper.MonthlyIndicatorsMapper;
import cn.exrick.xboot.modules.bussiness.monthlyIndicators.pojo.MonthlyIndicators;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.parameters.P;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * 经销商指标接口实现
 *
 * @author tqr
 */
@Slf4j
@Service
@Transactional
public class IDistributorIndexServiceImpl extends ServiceImpl<DistributorIndexMapper, DistributorIndex> implements IDistributorIndexService {

    @Autowired
    private DistributorIndexMapper distributorIndexMapper;
    @Autowired
    private UserMapper userMapper;
    @Autowired
    private DepartmentMapper departmentMapper;
    @Autowired
    private MonthlyIndicatorsMapper monthlyIndicatorsMapper;
    @Autowired
    private SecurityUtil securityUtil;
    @Autowired
    private DealerLevelMapper dealerLevelMapper;

    @Override
    public List<DistributorIndexVO> select(Page initMpPage, String deptName) {
        List<DistributorIndexVO> list = new ArrayList<>();
        List<DistributorIndex> dataDistributorIndex = distributorIndexMapper.selectList(new QueryWrapper<DistributorIndex>().lambda().orderByDesc(DistributorIndex::getCreateTime));
        if (ToolUtil.isNotEmpty(dataDistributorIndex)) {
            for (DistributorIndex index : dataDistributorIndex) {
                DistributorIndexVO distributorIndexVO = new DistributorIndexVO();
                ToolUtil.copyProperties(index, distributorIndexVO);
                //查创建人信息
                CommonVO commonVO = userMapper.getUserMessage(index.getCreateBy());
                ToolUtil.copyProperties(commonVO, distributorIndexVO);
                //查询绑定部门名称
                String name = departmentMapper.selectById(index.getDeptId()).getTitle();
                distributorIndexVO.setDeptName(name);
                //查询业绩信息
                MonthlyIndicators monthlyIndicators = monthlyIndicatorsMapper.selectById(index.getMonthlyId());
                distributorIndexVO.setMonthlyIndicators(monthlyIndicators);
                list.add(distributorIndexVO);
            }
        }
        if (ToolUtil.isNotEmpty(deptName)) {
            list = list.stream().filter(d -> (d.getDeptName()).contains(deptName)).collect(Collectors.toList());
        }
        if (ToolUtil.isNotEmpty(securityUtil.getCurrUser().getDepartmentId())) {
            if (!departmentMapper.selectById(securityUtil.getCurrUser().getDepartmentId()).getParentId().equals("0")) {
                List<String> ids = dealerLevelMapper.selectList(new QueryWrapper<>()).stream().map(d -> d.getDeptId()).collect(Collectors.toList());
                if (ids.contains(securityUtil.getCurrUser().getDepartmentId())) {
                    list = list.stream().filter(d -> (d.getDeptId()).equals(securityUtil.getCurrUser().getDepartmentId())).collect(Collectors.toList());
                }
            }
        }
        return list;
    }
}