package cn.exrick.xboot.modules.bussiness.shiProductComparison.serviceImp;

import cn.exrick.xboot.modules.bussiness.shiProductComparison.mapper.head_functionMapper;
import cn.exrick.xboot.modules.bussiness.shiProductComparison.pojo.head_function;
import cn.exrick.xboot.modules.bussiness.shiProductComparison.service.Ihead_functionService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

/**
 * 头部功能接口实现
 * @author 石博士
 */
@Slf4j
@Service
@Transactional
public class Ihead_functionServiceImpl extends ServiceImpl<head_functionMapper, head_function> implements Ihead_functionService {

    @Autowired
    private head_functionMapper head_functionMapper;
}