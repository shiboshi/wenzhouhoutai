package cn.exrick.xboot.modules.bussiness.shiProductComparison.pojo;

import cn.exrick.xboot.base.XbootBaseEntity;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.Table;

/**
 * 石博士
 */
@Data
@Entity
@Table(name = "s_s__product_comparison")
@TableName("s_s__product_comparison")
@ApiModel(value = "产品对比 主表")
public class s_ProductComparison extends XbootBaseEntity {

    private static final long serialVersionUID = 1L;


    @ApiModelProperty("产品名称")
    private String name;

    @ApiModelProperty("头部功能")
    private String heads;

    @ApiModelProperty("肩部功能")
    private String shoulder;

    @ApiModelProperty("背部功能")
    private String backs;

    @ApiModelProperty("臀部功能")
    private String buttocks;

    @ApiModelProperty("扶手功能")
    private String handrail;

    @ApiModelProperty("小腿功能")
    private String calfs;

    @ApiModelProperty("附加功能")
    private String appends;

    @ApiModelProperty("基本参数")
    private String basicParameter;

    @ApiModelProperty("产品规格，关联规格表")
    private String product_specification;

    @ApiModelProperty("产品备注")
    private String remarks;

    @ApiModelProperty("关联产品品号表")
    private String product_order_id;

    @ApiModelProperty("产品名称")
    private String productModelName;







}