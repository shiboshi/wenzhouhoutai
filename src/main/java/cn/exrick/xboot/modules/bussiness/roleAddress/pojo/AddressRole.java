package cn.exrick.xboot.modules.bussiness.roleAddress.pojo;

import cn.exrick.xboot.base.XbootBaseEntity;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModelProperty;
import io.swagger.annotations.ApiModel;
import lombok.Data;
import lombok.experimental.Accessors;

import javax.persistence.Entity;
import javax.persistence.Table;

/**
 * @author fei
 */
@Data
@Entity
@Table(name = "x_address_role")
@TableName("x_address_role")
@ApiModel(value = "角色区域表")
@Accessors(chain = true)
public class AddressRole extends XbootBaseEntity {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty("区域id")
    private String addressId;

    @ApiModelProperty("角色id")
    private String roleId;
}