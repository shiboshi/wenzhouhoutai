package cn.exrick.xboot.modules.bussiness.product.serviceImp;

import cn.exrick.xboot.common.utils.PageUtil;
import cn.exrick.xboot.common.utils.SecurityUtil;
import cn.exrick.xboot.common.utils.ToolUtil;
import cn.exrick.xboot.common.vo.PageVo;
import cn.exrick.xboot.common.vo.Result;
import cn.exrick.xboot.modules.bussiness.classification.mapper.ClassificationMapper;
import cn.exrick.xboot.modules.bussiness.classification.pojo.Classification;
import cn.exrick.xboot.modules.bussiness.product.VO.ProductModelVo;
import cn.exrick.xboot.modules.bussiness.product.mapper.productModelMapper;
import cn.exrick.xboot.modules.bussiness.product.pojo.ProductModel;
import cn.exrick.xboot.modules.bussiness.product.service.IproductModelService;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import lombok.extern.slf4j.Slf4j;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.*;

/**
 * 产品品号接口实现
 *
 * @author xiaofei
 */
@Slf4j
@Service
@Transactional
public class IproductModelServiceImpl extends ServiceImpl<productModelMapper, ProductModel> implements IproductModelService {
    private final static Logger LOGGER = LoggerFactory.getLogger(IproductOrderServiceImpl.class);

    @Autowired
    private productModelMapper productModelMapper;
    @Autowired
    private ClassificationMapper classificationMapper;
    @Autowired
    private SecurityUtil securityUtil;

    @Override
    public Result<Map<String, Object>> getByName(PageVo page, String productModelName) {
        Result<Map<String, Object>> result = new Result<>();
        Map<String, Object> map = new HashMap<>();
        List<ProductModel> list1 = new ArrayList<>();
        if (ToolUtil.isNotEmpty(productModelName)) {
            list1 = PageUtil.listToPage(page, productModelMapper.selectList(new QueryWrapper<ProductModel>().like("product_model_name", productModelName)));
            map.put("total", productModelMapper.selectList(new QueryWrapper<ProductModel>().like("product_model_name", productModelName)).size());
        } else {
            list1 = PageUtil.listToPage(page, productModelMapper.selectList(new QueryWrapper<>()));
            map.put("total", productModelMapper.selectList(new QueryWrapper<>()).size());
        }
        List<ProductModelVo> list = new ArrayList<>();
        for (ProductModel p : list1) {
            ProductModelVo productModelVo = new ProductModelVo();
            ToolUtil.copyProperties(p, productModelVo);
            String productId = null;
            String productName = null;
            if (ToolUtil.isNotEmpty(classificationMapper.selectById(p.getKindId()))) {
                productId = classificationMapper.selectById(p.getKindId()).getParentId();
            }
            if (ToolUtil.isNotEmpty(classificationMapper.selectById(p.getKindId()))) {
                productName = classificationMapper.selectById(p.getKindId()).getParentTitle();
            }
            productModelVo.setProductName(productName);
            productModelVo.setProductId(productId);
            list.add(productModelVo);
        }
        map.put("records", list);
        result.setCode(200);
        result.setSuccess(true);
        result.setMessage("查询成功");
        result.setResult(map);
        return result;
    }

    @Override
    public Result<ProductModel> insertProduct(ProductModelVo productModelVo) {
        Result<ProductModel> result = new Result<>();
        try {
            //添加产品型号
            Classification classification = new Classification();
            classification.setParentId(productModelVo.getProductId());
            classification.setTitle(productModelVo.getProductModelName());
            classification.setIsParent(true);
            classification.setParentTitle(classificationMapper.selectById(productModelVo.getProductId()).getTitle());
            classification.setCreateBy(securityUtil.getCurrUser().getId());
            classification.setCreateTime(new Date());
            classificationMapper.insert(classification);
            //添加产品
            ProductModel productModel = new ProductModel();
            ToolUtil.copyProperties(productModelVo, productModel);
            productModel.setCreateBy(securityUtil.getCurrUser().getId());
            productModel.setCreateTime(new Date());
            productModel.setKindId(classificationMapper.selectOne(new QueryWrapper<Classification>().orderByDesc("create_time").last(" limit 1 ")).getId());
            productModelMapper.insert(productModel);
            result.setCode(200);
            result.setMessage("添加成功");
            result.setSuccess(true);
            result.setResult(productModel);
        } catch (Exception e) {
            LOGGER.error("添加失败：{}", e);
            result.setCode(500);
            result.setMessage("添加失败");
            result.setSuccess(false);
        }

        return result;
    }

    @Override
    public Result<ProductModel> updateProduct(ProductModelVo productModelVo) {
        Result<ProductModel> result = new Result<>();
        try {
            //修改产品型号
            Classification classification = new Classification();
            classification.setParentId(productModelVo.getProductId());
            classification.setTitle(productModelVo.getProductModelName());
            classification.setIsParent(true);
            classification.setParentTitle(classificationMapper.selectById(productModelVo.getProductId()).getTitle());
            classification.setUpdateBy(securityUtil.getCurrUser().getId());
            classification.setUpdateTime(new Date());
            classification.setId(productModelVo.getKindId());
            classificationMapper.updateById(classification);
            //修改产品
            ProductModel productModel = new ProductModel();
            ToolUtil.copyProperties(productModelVo, productModel);
            productModel.setUpdateBy(securityUtil.getCurrUser().getId());
            productModel.setUpdateTime(new Date());
            productModelMapper.updateById(productModel);
            result.setCode(200);
            result.setMessage("修改成功");
            result.setSuccess(true);
        } catch (Exception e) {
            result.setCode(500);
            result.setMessage("修改失败");
            result.setSuccess(false);
        }
        return result;

    }
}
