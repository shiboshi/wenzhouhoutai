package cn.exrick.xboot.modules.wx.VO;

import lombok.Data;
import lombok.experimental.Accessors;

/**
 * @Author : xiaofei
 * @Date: 2019/9/18
 */
@Data
@Accessors(chain =  true)
public class WxUserVO {
    private String userId;
    private String userName;
}
