package cn.exrick.xboot.modules.bussiness.tApplication.pojo;

import lombok.Data;

/**
 * 退款申请返回值
 */
@Data
public class TApplicationVO{

    private static final long serialVersionUID = 1L;


    /**
     * id
     */
    private String id;

    /**
     * 类型，关联类型表refundtype
     */
    private String type;

    /**
     * 退款金额
     */
    private String remoney;

    /**
     * 附件
     */
    private String enclosu;

    /**
     * 备注
     */
    private String remarks;

    //申请人id,为当前登录人id
    private String userId;

    //关联订货单查询
    private String indentOrderId;

    /**
     * 申请状态
     */
    private String tStatus;

    /**
     * 流程id
     */
    private String businessId;





    /**
     * 退款类型返回值
     */
    private String refundTypeName;

    /**
     * 申请人返回值
     */
    private String username;

    /**
     * 订货单编号
     */
    private String orderNum;

    /**
     * 申请状态返回值
     */
    private String shiStatus;


    /**
     * 申请人部门
     */
    private String titles;


    /**
     * 完成状态
     */
    private String completionStatus;



}