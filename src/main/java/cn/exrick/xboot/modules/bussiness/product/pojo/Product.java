package cn.exrick.xboot.modules.bussiness.product.pojo;

import cn.exrick.xboot.base.XbootBaseEntity;
import com.baomidou.mybatisplus.annotation.TableLogic;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModelProperty;
import io.swagger.annotations.ApiModel;
import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

/**
 * @author xiaofei
 */
@Data
@Entity
@Table(name = "x_product")
@TableName("x_product")
@ApiModel(value = "产品大类")
public class Product extends XbootBaseEntity {

    private static final long serialVersionUID = 1L;

    /**
     * 产品名称
     */
    private String productName;

    /**
     * 产品分类
     */
    private String kindId;

}