package cn.exrick.xboot.modules.bussiness.level.pojo;

import cn.exrick.xboot.base.XbootBaseEntity;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModelProperty;
import io.swagger.annotations.ApiModel;
import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.Table;

/**
 * @author tqr
 */
@Data
@Entity
@Table(name = "t_level")
@TableName("t_level")
@ApiModel(value = "等级表")
public class Level extends XbootBaseEntity {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty("等级名称")
    private String levelName;
}