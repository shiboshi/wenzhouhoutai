package cn.exrick.xboot.common.upay.response;
import cn.exrick.xboot.common.upay.exception.UpayException;
/**
 * TODO UPAY平台客户端构造工程，所有交易入口从这里进来
 *
 * @author zhou
 * @date 2019-7-30 09:50:36
 */
public interface UpayClient {

    /**
     * @param request 请求对象
     * @param md5Key  md5Key
     * @param url 请求地址
     * @return
     */
    UpayResponse execute(UpayRequest request, String md5Key, String url) throws UpayException;


    /**
     * @param request    请求对象
     * @param privateKey 私钥
     * @param publicKey  公钥
     * @param url        请求地址
     * @return
     */
    UpayResponse execute(UpayRequest request, String privateKey, String publicKey, String url) throws UpayException;


}
