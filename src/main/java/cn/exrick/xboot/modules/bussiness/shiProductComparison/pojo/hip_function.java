package cn.exrick.xboot.modules.bussiness.shiProductComparison.pojo;

import cn.exrick.xboot.base.XbootBaseEntity;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.Table;

/**
 * @author 石博士
 */
@Data
@Entity
@Table(name = "s_hip_function")
@TableName("s_hip_function")
@ApiModel(value = "臀部功能")
public class hip_function extends XbootBaseEntity {

    private static final long serialVersionUID = 1L;


    @ApiModelProperty("臀部气压，手工输入")
    private String pressure;

    @ApiModelProperty("臀部振动，0没有，1有，2选配")
    private String vibration;

    @ApiModelProperty("机械悬浮式摆臀，0没有，1有，2选配")
    private String swing_the_buttocks;

    @ApiModelProperty("臀部敲击，0没有，1有，2选配")
    private String knocks;






}