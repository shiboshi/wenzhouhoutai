package cn.exrick.xboot.modules.bussiness.channel.service;

import cn.exrick.xboot.modules.bussiness.channel.VO.ChannelVO;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;
import cn.exrick.xboot.modules.bussiness.channel.pojo.Channel;

import java.util.List;

/**
 * 渠道接口
 * @author tqr
 */
public interface IChannelService extends IService<Channel> {

    IPage<ChannelVO> getByPage(Page initMpPage, String channelName);
}