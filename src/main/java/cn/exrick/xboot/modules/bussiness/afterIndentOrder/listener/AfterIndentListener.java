package cn.exrick.xboot.modules.bussiness.afterIndentOrder.listener;

import cn.exrick.xboot.common.enums.CheckStatus;
import cn.exrick.xboot.common.utils.ToolUtil;
import cn.exrick.xboot.config.springContext.SpringContextHolder;
import cn.exrick.xboot.modules.base.service.UserService;
import cn.exrick.xboot.modules.bussiness.afterIndentOrder.pojo.AfterIndentOrder;
import cn.exrick.xboot.modules.bussiness.afterIndentOrder.service.IafterIndentOrderService;
import cn.exrick.xboot.modules.bussiness.applicationAfterSaleOrder.pojo.ApplicationAfterSaleOrder;
import cn.exrick.xboot.modules.bussiness.applicationAfterSaleOrder.service.IapplicationAfterSaleOrderService;
import cn.exrick.xboot.modules.bussiness.dealerLevel.pojo.DealerLevel;
import cn.exrick.xboot.modules.bussiness.dealerLevel.service.IDealerLevelService;
import cn.exrick.xboot.modules.bussiness.inventoryOrder.pojo.InventoryOrder;
import cn.exrick.xboot.modules.bussiness.inventoryOrder.service.IInventoryOrderService;
import cn.exrick.xboot.modules.bussiness.productPrice.pojo.ProductPrice;
import cn.exrick.xboot.modules.bussiness.productPrice.service.IProductPriceService;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import org.activiti.engine.delegate.DelegateExecution;
import org.activiti.engine.delegate.ExecutionListener;

import java.math.BigDecimal;
import java.util.List;

/**
 * @Author : xiaofei
 * @Date: 2019/8/22
 */
public class AfterIndentListener implements ExecutionListener {
    @Override
    public void notify(DelegateExecution delegateExecution) throws Exception {

        String tableId = (String) delegateExecution.getVariable("tableId");
        IafterIndentOrderService iafterIndentOrderService = SpringContextHolder.getBean(IafterIndentOrderService.class);
        IapplicationAfterSaleOrderService iapplicationAfterSaleOrderService = SpringContextHolder.getBean(IapplicationAfterSaleOrderService.class);
        UserService userService = SpringContextHolder.getBean(UserService.class);
        IDealerLevelService iDealerLevelService = SpringContextHolder.getBean(IDealerLevelService.class);
        IInventoryOrderService iInventoryOrderService = SpringContextHolder.getBean(IInventoryOrderService.class);
        if (delegateExecution.getEventName().equals("end")) {
            AfterIndentOrder afterIndentOrder = iafterIndentOrderService.getById(tableId);
            //查询售后申请单
            if (ToolUtil.isNotEmpty(afterIndentOrder)) {
                ApplicationAfterSaleOrder applicationAfterSaleOrder = iapplicationAfterSaleOrderService.getById(afterIndentOrder.getApplicationAfterSaleOrderId());
                if (ToolUtil.isNotEmpty(applicationAfterSaleOrder)) {
                    if (applicationAfterSaleOrder.getAfterSaleType().getCode() == 2) {
                        //如果为退货则删除库存
                        String[] ids = applicationAfterSaleOrder.getInventoryIds().split(",");
                        for (String i : ids) {
                            iInventoryOrderService.getBaseMapper().deleteById(i);
                        }
                        if (ToolUtil.isNotEmpty(afterIndentOrder.getReturnMoney())) {
                            DealerLevel dealerLevel = iDealerLevelService.getBaseMapper().selectOne(new QueryWrapper<DealerLevel>().lambda().eq(DealerLevel::getDeptId, userService.get(afterIndentOrder.getCreateBy()).getDepartmentId()));
                            dealerLevel.setBalance(afterIndentOrder.getReturnMoney());
                            iDealerLevelService.getBaseMapper().updateById(dealerLevel);
                        }
                    }
                }
            }
            afterIndentOrder.setCheckStatus(CheckStatus.sucess);
            iafterIndentOrderService.updateById(afterIndentOrder);
        }
    }
}
