package cn.exrick.xboot.modules.bussiness.troubleList.serviceImp;

import cn.exrick.xboot.modules.bussiness.troubleList.mapper.TroubleListMapper;
import cn.exrick.xboot.modules.bussiness.troubleList.pojo.TroubleList;
import cn.exrick.xboot.modules.bussiness.troubleList.service.ITroubleListService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

/**
 * 问题表接口实现
 * @author tqr
 */
@Slf4j
@Service
@Transactional
public class ITroubleListServiceImpl extends ServiceImpl<TroubleListMapper, TroubleList> implements ITroubleListService {

    @Autowired
    private TroubleListMapper troubleListMapper;
}