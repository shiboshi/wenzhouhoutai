package cn.exrick.xboot.common.upay.request.transaction;


import cn.exrick.xboot.common.upay.enums.BizChannel;
import cn.exrick.xboot.common.upay.enums.BizType;
import cn.exrick.xboot.common.upay.request.RequestBody;
import cn.exrick.xboot.common.upay.utils.StringUtils;

public class PreCreateRequestBody extends RequestBody {
    private String ext_no;
    private String total_amount;
    private String discountable_amount;
    private String undiscountable_amount;
    private String currency = "CNY";
    private String subject = "银联商务浙江";
    private String body;
    private String goods_detail;
    private String timeout_express = "5m";
    private String qr_code_enable = "N";

    public PreCreateRequestBody() {
    }

    @Override
    public boolean validate() {

        if (StringUtils.isEmpty(ext_no)) {
            throw new RuntimeException("商户订单号[ext_no]不可为空");
        }

        if (StringUtils.isEmpty(total_amount)) {
            throw new RuntimeException("订单总金额[total_amount]不可为空");
        }

        if (StringUtils.isEmpty(currency)) {
            throw new RuntimeException("金额币种[currency]不可为空");
        }

        if (StringUtils.isEmpty(subject)) {
            throw new RuntimeException("订单主题[subject]不可为空");
        }

        if (StringUtils.isEmpty(qr_code_enable)) {
            throw new RuntimeException("是否由网关直接生成二维码图片[qr_code_enable]不可为空");
        }

        return true;
    }

    public String getExt_no() {
        return ext_no;
    }

    public void setExt_no(String ext_no) {
        this.ext_no = ext_no;
    }

    public String getTotal_amount() {
        return total_amount;
    }

    public void setTotal_amount(String total_amount) {
        this.total_amount = total_amount;
    }

    public String getDiscountable_amount() {
        return discountable_amount;
    }

    public void setDiscountable_amount(String discountable_amount) {
        this.discountable_amount = discountable_amount;
    }

    public String getUndiscountable_amount() {
        return undiscountable_amount;
    }

    public void setUndiscountable_amount(String undiscountable_amount) {
        this.undiscountable_amount = undiscountable_amount;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public String getBody() {
        return body;
    }

    public void setBody(String body) {
        this.body = body;
    }

    public String getGoods_detail() {
        return goods_detail;
    }

    public void setGoods_detail(String goods_detail) {
        this.goods_detail = goods_detail;
    }

    public String getTimeout_express() {
        return timeout_express;
    }

    public void setTimeout_express(String timeout_express) {
        this.timeout_express = timeout_express;
    }

    public String getQr_code_enable() {
        return qr_code_enable;
    }

    public void setQr_code_enable(String qr_code_enable) {
        this.qr_code_enable = qr_code_enable;
    }

    @Override
    public String acquireBizChannel() {
        return BizChannel.UMSZJ_CHANNEL_WEIXIN.getEn();
    }

    public String acquireWXBizChannel() {
        return BizChannel.UMSZJ_CHANNEL_WEIXIN.getEn();
    }

    @Override
    public String acquireBizType() {
        return BizType.UMSZJ_TRADE_PRECREATE.getEn();
    }

}
