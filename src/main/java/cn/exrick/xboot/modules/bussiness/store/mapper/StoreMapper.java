package cn.exrick.xboot.modules.bussiness.store.mapper;

import cn.exrick.xboot.modules.bussiness.shiVO.StoreVO;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import cn.exrick.xboot.modules.bussiness.store.pojo.Store;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

/**
 * 门店表数据处理层
 * @author tqr
 */
@Repository
public interface StoreMapper extends BaseMapper<Store> {





    /**
     * 查询所有集合
     */
    IPage<StoreVO> QueryStore(Page page, @Param("username") String username);





}