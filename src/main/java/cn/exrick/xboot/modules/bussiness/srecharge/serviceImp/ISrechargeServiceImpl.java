package cn.exrick.xboot.modules.bussiness.srecharge.serviceImp;

import cn.exrick.xboot.modules.bussiness.srecharge.mapper.SrechargeMapper;
import cn.exrick.xboot.modules.bussiness.srecharge.pojo.Srecharge;
import cn.exrick.xboot.modules.bussiness.srecharge.pojo.SrechargeOV;
import cn.exrick.xboot.modules.bussiness.srecharge.service.ISrechargeService;
import cn.exrick.xboot.modules.shiUtil.JedisDelete;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import lombok.extern.slf4j.Slf4j;
import org.apache.ibatis.annotations.Param;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * 代充值接口实现
 * @author tqr
 */
@Slf4j
@Service
@Transactional
public class ISrechargeServiceImpl extends ServiceImpl<SrechargeMapper, Srecharge> implements ISrechargeService {

    @Autowired
    private SrechargeMapper srechargeMapper;



    @Override
    public IPage<Srecharge> getByPage(Page page) {
        IPage<Srecharge> list = srechargeMapper.selectPage(page,new QueryWrapper<>());
        return list;
    }



    /**
     * 查询所有集合业务逻辑层
     */
    public IPage<SrechargeOV> QuerySrecharge(Page page, @Param("id") String id, @Param("username") String username){
        JedisDelete d = new JedisDelete();
        d.Delete();
        if(id.equals("") || id.equals("undefined")){
            id=null;
        }
        return srechargeMapper.QuerySrecharge(page,id,username);
    }



    /**
     * 根据id查询详情
     */
    public IPage<SrechargeOV> getByPageId(Page page, @Param("id") String id){
        JedisDelete d = new JedisDelete();
        d.Delete();
        return srechargeMapper.getByPageId(page,id);
    }








}