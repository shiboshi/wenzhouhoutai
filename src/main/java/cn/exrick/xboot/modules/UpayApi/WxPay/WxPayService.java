package cn.exrick.xboot.modules.UpayApi.WxPay;


import cn.exrick.xboot.modules.UpayApi.test.qrcode.SslUtils;

import com.alibaba.fastjson.JSON;
import net.sf.json.JSONObject;
import org.springframework.stereotype.Service;


import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;

import java.net.HttpURLConnection;
import java.net.URL;
import java.util.*;

import static cn.exrick.xboot.modules.UpayApi.WxPay.Util.jsonToMap;
import static cn.exrick.xboot.modules.UpayApi.WxPay.Util.makeSign;

/**
 * @Author : xiaofei
 * @Date: 2019/9/3
 */
@Service
public class WxPayService {

    public String wxpay(PayVO payVO) {
        //1.生成sign
        JSONObject json = WXPay(payVO);
        Map<String, String> paramsMap = jsonToMap(json);
        json.put("sign", makeSign("sWDb8KhbpsxbX2ACnPzwANsHKfEpQakB3YDtxKPM3RrAbm5Y", paramsMap));
        System.out.println("paramsMap：" + paramsMap);


        //2.打印出请求报文的字符串格式
        String strReqJsonStr = json.toString();
        System.out.println("strReqJsonStr:" + strReqJsonStr);


        //3.调用接口
        String resultStr = null;
        PrintWriter out = null;
        HttpURLConnection httpURLConnection = null;
        Map<String, String> resultMap = new HashMap<String, String>();
        try {
            javax.net.ssl.HttpsURLConnection.setDefaultHostnameVerifier((hostname, sslsession) -> true);
            URL url = new URL("https://qr.chinaums.com/netpay-route-server/api/");
            httpURLConnection = getHttpURLConnection(url);
            if ("https".equalsIgnoreCase(url.getProtocol())) {
                SslUtils.ignoreSsl();
            }

            //发送POST请求参数
            out = new PrintWriter(httpURLConnection.getOutputStream());
            out.write(strReqJsonStr);
            out.flush();

            //读取响应
            if (httpURLConnection.getResponseCode() == HttpURLConnection.HTTP_OK) {
                StringBuffer content = new StringBuffer();
                String tempStr;
                //获取响应参数
                BufferedReader in = new BufferedReader(new InputStreamReader(httpURLConnection.getInputStream()));
                while ((tempStr = in.readLine()) != null) {
                    content.append(tempStr);
                }
                System.out.println("content," + content.toString());

                //转换成json对象
                com.alibaba.fastjson.JSONObject respJson = JSON.parseObject(content.toString());
                String resultCode = respJson.getString("errCode");
                resultMap.put("errCode", resultCode);
                if (resultCode.equals("SUCCESS")) {
                    String billQRCode = (String) respJson.get("billQRCode");
                    resultMap.put("billQRCode", billQRCode);
                    resultMap.put("respStr", respJson.toString());
                } else {
                    resultMap.put("respStr", respJson.toString());
                }
                resultStr = JSONObject.fromObject(resultMap).toString();
            }
        } catch (Exception e) {
            e.printStackTrace();
            resultMap.put("errCode", "HttpURLException");
            resultMap.put("msg", "调用银商接口出现异常：" + e.toString());
            resultStr = JSONObject.fromObject(resultMap).toString();
        } finally {
            if (out != null) {
                out.close();
            }
            httpURLConnection.disconnect();
        }
        return resultStr;
    }

    /**
     * 请求参数
     *
     * @param payVO
     * @return
     */
    public static JSONObject WXPay(PayVO payVO) {
        JSONObject json = new JSONObject();
        json.put("instMid", WxInterface.instMid);
        json.put("merOrderId", Util.genMerOrderId(WxInterface.msgSrcId));
        json.put("mid", WxInterface.mid);
        json.put("tradeType", WxInterface.tradeType);
        json.put("msgSrc", WxInterface.msgSrc);
        json.put("msgSrcId", WxInterface.msgSrcId);
        json.put("msgType", WxInterface.msgType);
        json.put("subOpenId", payVO.getOpenId());
        //json.put("subAppId", WxInterface.subAppId);
        json.put("requestTimestamp", DateUtils.formatNow(DateUtils.YYYY_MM_DD_HH_MM_SS));
        json.put("tid", WxInterface.tid);
        json.put("totalAmount", payVO.getTotalAmount());
        return json;
    }


    /**
     * 获取httpurlConnection
     *
     * @param url
     * @return
     * @throws IOException
     */
    public HttpURLConnection getHttpURLConnection(URL url) throws IOException {

        HttpURLConnection httpURLConnection = (HttpURLConnection) url.openConnection();
        httpURLConnection.setRequestMethod("POST");
        httpURLConnection.setDoInput(true);
        httpURLConnection.setDoOutput(true);
        httpURLConnection.setRequestProperty("Content_Type", "application/json");
        httpURLConnection.setRequestProperty("Accept_Charset", "UTF-8");
        httpURLConnection.setRequestProperty("contentType", "UTF-8");
        return httpURLConnection;
    }

}
