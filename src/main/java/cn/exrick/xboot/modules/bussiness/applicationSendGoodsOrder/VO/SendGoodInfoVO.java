package cn.exrick.xboot.modules.bussiness.applicationSendGoodsOrder.VO;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @Author : xiaofei
 * @Date: 2019/8/31
 */
@Data
public class SendGoodInfoVO {

    /**
     * 关联申请发货单id
     */
    private String id;

    /**
     * 咨询电话
     */
    @ApiModelProperty(value = "咨询电话")
    private String telephone;

    /**
     * 产品图片
     */
    @ApiModelProperty(value = "产品图片")
    private String productPic;

    /**
     * 到货时间
     */
    @ApiModelProperty(value = "到货时间")
    private String arrivalTime;

}
