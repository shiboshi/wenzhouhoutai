package cn.exrick.xboot.modules.bussiness.specialPriceSalesOrder.serviceImp;

import cn.exrick.xboot.common.enums.CheckStatus;
import cn.exrick.xboot.common.enums.OrderType;
import cn.exrick.xboot.common.utils.SecurityUtil;
import cn.exrick.xboot.common.utils.ToolUtil;
import cn.exrick.xboot.common.vo.CommonVO;
import cn.exrick.xboot.modules.activiti.entity.ActBusiness;
import cn.exrick.xboot.modules.activiti.service.ActBusinessService;
import cn.exrick.xboot.modules.base.dao.mapper.UserMapper;
import cn.exrick.xboot.modules.bussiness.purchaseOrder.VO.PurchaseOrderVO;
import cn.exrick.xboot.modules.bussiness.purchaseOrder.pojo.PurchaseOrder;
import cn.exrick.xboot.modules.bussiness.purchaseOrder.service.IpurchaseOrderService;
import cn.exrick.xboot.modules.bussiness.specialPriceSalesOrder.VO.SpecialPriceSalesOrderVO;
import cn.exrick.xboot.modules.bussiness.specialPriceSalesOrder.mapper.specialPriceSalesOrderMapper;
import cn.exrick.xboot.modules.bussiness.specialPriceSalesOrder.pojo.SpecialPriceSalesOrder;
import cn.exrick.xboot.modules.bussiness.specialPriceSalesOrder.service.IspecialPriceSalesOrderService;
import cn.hutool.core.map.MapBuilder;
import cn.hutool.core.map.MapUtil;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.google.common.base.Splitter;
import com.google.common.collect.Lists;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * 特价申请销售订单接口实现
 *
 * @author xiaofei
 */
@Slf4j
@Service
@Transactional
public class IspecialPriceSalesOrderServiceImpl extends ServiceImpl<specialPriceSalesOrderMapper, SpecialPriceSalesOrder> implements IspecialPriceSalesOrderService {

    @Autowired
    private specialPriceSalesOrderMapper specialPriceSalesOrderMapper;
    @Autowired
    private IpurchaseOrderService purchaseOrderService;
    @Autowired
    private SecurityUtil securityUtil;
    @Autowired
    private ActBusinessService actBusinessService;
    @Autowired
    private UserMapper userMapper;

    private static final Splitter SPLITTER = Splitter.on(",").trimResults().omitEmptyStrings();

    @Override
    public ActBusiness insertSecialPriceOrder(List<PurchaseOrder> purchaseOrders, String proDefId,String userId,String phone) {

        String userid = ToolUtil.isNotEmpty(userId)?userId:securityUtil.getCurrUser().getId();
        //插入到特价申请单
        SpecialPriceSalesOrder specialPriceSalesOrder = new SpecialPriceSalesOrder();
        specialPriceSalesOrder.setPurchaseOrderIds(purchaseOrderService.getPurchaseIds(purchaseOrders,userId));
        specialPriceSalesOrder.setCheckStatus(CheckStatus.pengindg);
        specialPriceSalesOrder.setCreateBy(userid);
        specialPriceSalesOrder.setPhoneNumber(phone);
        this.save(specialPriceSalesOrder);

        //添加到业务流程表中
        ActBusiness actBusiness = new ActBusiness();
        actBusiness.setUserId(userid);
        //获取最新的一个特价申请单
        SpecialPriceSalesOrder specialPriceSalesOrderLast = this.getById(specialPriceSalesOrderMapper.selectOne(new QueryWrapper<SpecialPriceSalesOrder>().lambda().orderByDesc(SpecialPriceSalesOrder::getCreateTime).last(" limit 1")));
        actBusiness.setTableId(specialPriceSalesOrderLast.getId());
        actBusiness.setProcDefId(proDefId);
        ActBusiness actBusinessUse = actBusinessService.save(actBusiness);
        //更新特价申请表
        specialPriceSalesOrderLast.setBusinessId(actBusinessUse.getId());
        this.updateById(specialPriceSalesOrder);
        return actBusinessUse;
    }

    @Override
    public List<Map<String,Object>> getMyPassList(String buyerPhone) {
        List<Map<String,Object>> result = Lists.newArrayList();

        List<SpecialPriceSalesOrder> specialPriceSalesOrders = this.getBaseMapper().selectList((Wrappers.<SpecialPriceSalesOrder>lambdaQuery()
                .eq(SpecialPriceSalesOrder::getCheckStatus, CheckStatus.sucess.getCode())
                .eq(SpecialPriceSalesOrder::getPhoneNumber, buyerPhone)));

        if (ToolUtil.isNotEmpty(specialPriceSalesOrders)) {
            specialPriceSalesOrders.stream().forEach(order -> {
                List<String> purchaseOrderIds = Lists.newArrayList(SPLITTER.split(order.getPurchaseOrderIds()));
                List<PurchaseOrder> purchaseOrders = purchaseOrderService.getBaseMapper().selectList(Wrappers.<PurchaseOrder>lambdaQuery()
                        .in(PurchaseOrder::getId, purchaseOrderIds));
                if (ToolUtil.isNotEmpty(purchaseOrders)) {
                    List<Map<String,Object>> idss = purchaseOrders.stream().map(purchaseOrder -> {
                        MapBuilder<String,Object> mapBuilder = MapUtil.builder();
                        mapBuilder.put("id", purchaseOrder.getProductId())
                                .put("price", purchaseOrder.getPrice().doubleValue());
                        return mapBuilder.map();
                    }).collect(Collectors.toList());
                    result.addAll(idss);
                }
            });
            return result;
        }
        return null;
    }

    public static void main(String[] args) {
        BigDecimal a = new BigDecimal(0.001);
        Double b = a.doubleValue();
        System.out.println(b);
    }

    @Override
    public IPage<SpecialPriceSalesOrderVO> getList(Page initMpPage, String userId, String checkStatus) {
        if (ToolUtil.isEmpty(userId)) {
            userId = securityUtil.getCurrUser().getId();
        }
        IPage<SpecialPriceSalesOrderVO> list = specialPriceSalesOrderMapper.getList(initMpPage, userId, checkStatus);
        CommonVO commonVO = userMapper.getUserMessage(userId);
        List<SpecialPriceSalesOrderVO> salesOrderVOS = list.getRecords();
        for (SpecialPriceSalesOrderVO specialPriceSalesOrderVO : salesOrderVOS) {
            if (ToolUtil.isNotEmpty(specialPriceSalesOrderVO.getCheckStatus())) {
                if ("1".equals(specialPriceSalesOrderVO.getCheckStatus())) {
                    specialPriceSalesOrderVO.setCheckStatus("待审核");
                } else if ("2".equals(specialPriceSalesOrderVO.getCheckStatus())) {
                    specialPriceSalesOrderVO.setCheckStatus("审核成功");
                } else if ("3".equals(specialPriceSalesOrderVO.getCheckStatus())) {
                    specialPriceSalesOrderVO.setCheckStatus("审核失败");
                }
            }
        }
        list.setRecords(changeMessage(commonVO, salesOrderVOS));
        return list;
    }

    /**
     * 转移公共类
     *
     * @param commonVO
     * @param salesOrderVOS
     * @return
     */
    public List<SpecialPriceSalesOrderVO> changeMessage(CommonVO commonVO, List<SpecialPriceSalesOrderVO> salesOrderVOS) {
        if (!ToolUtil.isOneEmpty(commonVO, salesOrderVOS)) {
            List<SpecialPriceSalesOrderVO> salesOrderVOSReturn = salesOrderVOS.stream().map(order -> {
                ToolUtil.copyProperties(commonVO, order);
                return order;
            }).collect(Collectors.toList());
            return salesOrderVOSReturn;
        }
        return null;
    }

    @Override
    public List<PurchaseOrderVO> getDetail(String salsOrderId) {
        List<PurchaseOrderVO> purchaseOrders = purchaseOrderService.getPurchaseList(OrderType.specialSaleOrder,salsOrderId);
        return purchaseOrders;
    }
}