package cn.exrick.xboot.modules.bussiness.product.VO;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
@ApiModel(value = "产品型号")
public class ProductModelVo {


    @ApiModelProperty("id")
    private String id;

    @ApiModelProperty("产品大类")
    private String productName;

    @ApiModelProperty("产品大类id")
    private String productId;

    @ApiModelProperty("产品型号")
    private String productModelName;

    @ApiModelProperty("对内型号")
    private String modelNameIn;

    @ApiModelProperty("对外型号")
    private String modelNameOut;

    @ApiModelProperty("分类id")
    private String kindId;


}
