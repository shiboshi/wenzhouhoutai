package cn.exrick.xboot.modules.bussiness.transferOrder.serviceImp;

import cn.exrick.xboot.common.enums.Order;
import cn.exrick.xboot.common.factory.ConstantFactory;
import cn.exrick.xboot.common.utils.SecurityUtil;
import cn.exrick.xboot.common.utils.ToolUtil;
import cn.exrick.xboot.modules.base.dao.mapper.DepartmentMapper;
import cn.exrick.xboot.modules.base.entity.Department;
import cn.exrick.xboot.modules.base.entity.Role;
import cn.exrick.xboot.modules.base.service.DepartmentService;
import cn.exrick.xboot.modules.base.service.mybatis.DepartMentService;
import cn.exrick.xboot.modules.bussiness.inventoryOrder.VO.InventoryVO;
import cn.exrick.xboot.modules.bussiness.inventoryOrder.mapper.InventoryOrderMapper;
import cn.exrick.xboot.modules.bussiness.inventoryOrder.pojo.InventoryOrder;
import cn.exrick.xboot.modules.bussiness.inventoryOrder.pojo.InventoryOrderResult;
import cn.exrick.xboot.modules.bussiness.inventoryOrder.service.IInventoryOrderService;
import cn.exrick.xboot.modules.bussiness.shiRechargeType.mapper.ShiRechargeTypeMapper;
import cn.exrick.xboot.modules.bussiness.shiWarehouse.mapper.ShiWarehouseMapper;
import cn.exrick.xboot.modules.bussiness.shiWarehouse.pojo.ShiWarehouse;
import cn.exrick.xboot.modules.bussiness.shiWarehouse.service.IShiWarehouseService;
import cn.exrick.xboot.modules.bussiness.transferOrder.VO.TransferVO;
import cn.exrick.xboot.modules.bussiness.transferOrder.mapper.transferOrderMapper;
import cn.exrick.xboot.modules.bussiness.transferOrder.pojo.TransferOrder;
import cn.exrick.xboot.modules.bussiness.transferOrder.service.ItransferOrderService;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.core.toolkit.support.SFunction;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.google.common.base.Splitter;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.*;
import java.util.stream.Collectors;

/**
 * 调拨单接口实现
 *
 * @author fei
 */
@Slf4j
@Service
@Transactional
public class ItransferOrderServiceImpl extends ServiceImpl<transferOrderMapper, TransferOrder> implements ItransferOrderService {


    @Autowired
    private SecurityUtil securityUtil;
    @Autowired
    private IInventoryOrderService iInventoryOrderService;
    @Autowired
    private IShiWarehouseService warehouseService;
    @Autowired
    private DepartmentMapper departmentMapper;
    @Autowired
    private transferOrderMapper transferOrderMapper1;
    @Autowired
    private ShiWarehouseMapper shiWarehouseMapper;

    private static final Splitter SPLITTER = Splitter.on(",").trimResults().omitEmptyStrings();

    @Override
    @Transactional(propagation = Propagation.SUPPORTS)
    public void insertTransferOrder(TransferOrder transferOrder) {

        //1.插入订单
        transferOrder.setCreateBy(securityUtil.getCurrUser().getId());
        this.save(transferOrder);

//        //2.修改所选库存的仓库id
//        List<String> inventoryIds = SPLITTER.splitToList(transferOrder.getInventoryIds());
//        inventoryIds.stream().filter(Id -> ToolUtil.isNotEmpty(inventoryOrderService.getById(Id)))
//                .forEach(Id -> {
//                    InventoryOrder inventoryOrder = inventoryOrderService.getById(Id);
//                    inventoryOrder.setWarhouseId(transferOrder.getWarhouseId());
//                    inventoryOrderService.updateById(inventoryOrder);
//                });
    }


    @Override
    public void arrival(String id) {

        //查询调拨单
        TransferOrder transferOrder = transferOrderMapper1.selectById(id);
        if (ToolUtil.isNotEmpty(transferOrder)) {
            //2.修改所选库存的仓库id
            List<String> inventoryIds = SPLITTER.splitToList(transferOrder.getInventoryIds());
            inventoryIds.stream().filter(Id -> ToolUtil.isNotEmpty(iInventoryOrderService.getById(Id)))
                    .forEach(Id -> {
                        InventoryOrder inventoryOrder = iInventoryOrderService.getById(Id);
                        inventoryOrder.setWarhouseId(transferOrder.getWarhouseId());
                        iInventoryOrderService.updateById(inventoryOrder);
                    });
            transferOrder.setStatus(1);
            transferOrderMapper1.updateById(transferOrder);
        }
    }

    @Override
    public List<TransferVO> getList(TransferOrder transferOrder) {
        if (ToolUtil.isNotEmpty(securityUtil.getCurrUser().getRoles())) {
            for (Role role : securityUtil.getCurrUser().getRoles()) {
                if ("ROLE_ADMIN".equals(role.getName())) {
                    Map<SFunction<TransferOrder, ?>, Object> map = Maps.newHashMap();
                    map.put(TransferOrder::getWarhouseId, transferOrder.getWarhouseId());
                    List<TransferVO> transferVOS = new ArrayList<>();
                    List<TransferOrder> TransferOrders = this.list(Wrappers.<TransferOrder>lambdaQuery()
                            .allEq(true, map, false).orderByAsc(TransferOrder::getStatus));
                    if (ToolUtil.isNotEmpty(transferOrder.getWarhouseId())) {
                        TransferOrders = TransferOrders.stream().filter(t -> transferOrder.getWarhouseId().equals(t.getWarhouseId())).collect(Collectors.toList());
                    }
                    if (ToolUtil.isNotEmpty(TransferOrders)) {
                        for (TransferOrder transfer : TransferOrders) {
                            TransferVO transferVO = new TransferVO();
                            ToolUtil.copyProperties(transfer, transferVO);
                            transferVO.setUserName(ConstantFactory.me().getUserName(transfer.getCreateBy()));
                            transferVO.setUserLevel(ConstantFactory.me().getUserLevelName(transfer.getCreateBy()));
                            transferVO.setUserDept(ConstantFactory.me().getUserDeptName(transfer.getCreateBy()));
                            transferVOS.add(transferVO);
                        }
                    }
                    for (TransferVO t : transferVOS) {
                        String name = shiWarehouseMapper.selectById(t.getWarhouseId()).getWName();
                        String oldName = shiWarehouseMapper.selectById(t.getOldwarhouseId()).getWName();
                        t.setOldWarhouseName(oldName);
                        t.setWarhouseName(name);
                    }
                    return transferVOS;
                }
            }
        }
        String deptId = securityUtil.getCurrUser().getDepartmentId();
        if (ToolUtil.isNotEmpty(deptId)) {
            List<TransferVO> transferVOS = getRecursion(deptId, transferOrder);
            transferVOS = transferVOS.stream()
                    .collect(Collectors
                            .collectingAndThen
                                    (Collectors.toCollection(() -> new TreeSet<>(Comparator.comparing(t -> t.getId()))), ArrayList::new)
                    );

            for (TransferVO t : transferVOS) {
                String name = shiWarehouseMapper.selectById(t.getWarhouseId()).getWName();
                String oldName = shiWarehouseMapper.selectById(t.getOldwarhouseId()).getWName();
                t.setOldWarhouseName(oldName);
                t.setWarhouseName(name);
            }
            return transferVOS;
        }
        return null;
    }

    public List<TransferVO> getRecursion(String deptId, TransferOrder transferOrder) {
        //查询该部门所看到的单子
        Map<SFunction<TransferOrder, ?>, Object> map = Maps.newHashMap();
        map.put(TransferOrder::getWarhouseId, transferOrder.getWarhouseId());
        List<TransferVO> transferVOS = new ArrayList<>();
        List<TransferOrder> TransferOrders = this.list(Wrappers.<TransferOrder>lambdaQuery()
                .allEq(true, map, false).orderByAsc(TransferOrder::getStatus));
        if (ToolUtil.isNotEmpty(transferOrder.getWarhouseId())) {
            TransferOrders = TransferOrders.stream().filter(t -> transferOrder.getWarhouseId().equals(t.getWarhouseId())).collect(Collectors.toList());
        }
        if (ToolUtil.isNotEmpty(TransferOrders)) {
            for (TransferOrder transfer : TransferOrders) {
                TransferVO transferVO = new TransferVO();
                ToolUtil.copyProperties(transfer, transferVO);
                transferVO.setUserName(ConstantFactory.me().getUserName(transfer.getCreateBy()));
                transferVO.setUserLevel(ConstantFactory.me().getUserLevelName(transfer.getCreateBy()));
                transferVO.setUserDept(ConstantFactory.me().getUserDeptName(transfer.getCreateBy()));
                transferVOS.add(transferVO);
            }
        }
        transferVOS = transferVOS.stream().filter(t -> shiWarehouseMapper.selectById(t.getWarhouseId()).getDeptId()
                .equals(deptId)).collect(Collectors.toList());
        //查询该部门子集部门
        List<String> ids = departmentMapper.selectList(new QueryWrapper<Department>().lambda().eq(Department::getParentId, deptId))
                .stream()
                .map(department -> department.getId())
                .collect(Collectors.toList());
        if (ids.size() > 0) {
            for (String s : ids) {
                //查询该部门所看到的单子
                Map<SFunction<TransferOrder, ?>, Object> map2 = Maps.newHashMap();
                map.put(TransferOrder::getWarhouseId, transferOrder.getWarhouseId());
                List<TransferVO> transferVOS2 = new ArrayList<>();
                List<TransferOrder> TransferOrders2 = this.list(Wrappers.<TransferOrder>lambdaQuery()
                        .allEq(true, map2, false).orderByAsc(TransferOrder::getStatus));
                if (ToolUtil.isNotEmpty(transferOrder.getWarhouseId())) {
                    TransferOrders = TransferOrders.stream().filter(t -> transferOrder.getWarhouseId().equals(t.getWarhouseId())).collect(Collectors.toList());
                }
                if (ToolUtil.isNotEmpty(TransferOrders)) {
                    for (TransferOrder transfer : TransferOrders) {
                        TransferVO transferVO = new TransferVO();
                        ToolUtil.copyProperties(transfer, transferVO);
                        transferVO.setUserName(ConstantFactory.me().getUserName(transfer.getCreateBy()));
                        transferVO.setUserLevel(ConstantFactory.me().getUserLevelName(transfer.getCreateBy()));
                        transferVO.setUserDept(ConstantFactory.me().getUserDeptName(transfer.getCreateBy()));
                        transferVOS.add(transferVO);
                    }
                }
                transferVOS2 = transferVOS2.stream().filter(t -> shiWarehouseMapper.selectById(t.getWarhouseId()).getDeptId()
                        .equals(s)).collect(Collectors.toList());
                transferVOS.addAll(transferVOS2);
            }
        }
        return transferVOS;
    }

    @Override
    public List<InventoryVO> getDetail(String id,String userId) {

        TransferOrder transferOrder = this.getById(id);
        if (ToolUtil.isNotEmpty(transferOrder)) {
            List<InventoryVO> inventoryVOS = iInventoryOrderService.getInvetoryList(transferOrder.getInventoryIds(),userId);
            return inventoryVOS;
        }
        return null;
    }

    @Override
    public List<Map<String, Object>> getWarhouseList() {

        String departmentId = securityUtil.getCurrUser().getDepartmentId();
        List<Map<String, Object>> result = Lists.newArrayList();

        //1.找出当前经销商部门下面的所有门店部门
        List<Department> departments = departmentMapper.selectList(Wrappers.<Department>lambdaQuery()
                .eq(Department::getParentId, departmentId));

        //2.根据门店部门找到仓库
        if (ToolUtil.isNotEmpty(departments)) {
            departments.stream().forEach(department -> {
                ShiWarehouse shiWarehouse = warehouseService.getOne(Wrappers.<ShiWarehouse>lambdaQuery()
                        .eq(ShiWarehouse::getDeptId, department.getId()));
                if (ToolUtil.isNotEmpty(shiWarehouse)) {
                    Map<String, Object> map = Maps.newHashMap();
                    map.put("id", shiWarehouse.getId());
                    map.put("name", shiWarehouse.getWName());
                    result.add(map);
                }
            });
            return result;
        }
        return null;
    }

}