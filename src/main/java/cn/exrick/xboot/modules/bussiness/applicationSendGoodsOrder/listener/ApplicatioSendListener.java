package cn.exrick.xboot.modules.bussiness.applicationSendGoodsOrder.listener;

import cn.exrick.xboot.common.enums.CheckStatus;
import cn.exrick.xboot.common.enums.PayType;
import cn.exrick.xboot.common.enums.SendStatus;
import cn.exrick.xboot.common.utils.ResultUtil;
import cn.exrick.xboot.common.utils.ToolUtil;
import cn.exrick.xboot.config.springContext.SpringContextHolder;
import cn.exrick.xboot.modules.base.entity.Department;
import cn.exrick.xboot.modules.base.service.UserService;
import cn.exrick.xboot.modules.bussiness.appliSendNum.pojo.AppliSendNum;
import cn.exrick.xboot.modules.bussiness.appliSendNum.service.IappliSendNumService;
import cn.exrick.xboot.modules.bussiness.applicationSendGoodsOrder.pojo.ApplicationSendGoodsOrder;
import cn.exrick.xboot.modules.bussiness.applicationSendGoodsOrder.service.IapplicationSendGoodsOrderService;
import cn.exrick.xboot.modules.bussiness.dealerLevel.pojo.DealerLevel;
import cn.exrick.xboot.modules.bussiness.dealerLevel.service.IDealerLevelService;
import cn.exrick.xboot.modules.bussiness.indentOrder.pojo.IndentOrder;
import cn.exrick.xboot.modules.bussiness.indentOrder.service.IindentOrderService;
import cn.exrick.xboot.modules.bussiness.purchaseOrder.pojo.PurchaseOrder;
import cn.exrick.xboot.modules.bussiness.purchaseOrder.service.IpurchaseOrderService;
import cn.exrick.xboot.modules.bussiness.sendGoodsOrder.service.IsendGoodsOrderService;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import org.activiti.engine.delegate.DelegateExecution;
import org.activiti.engine.delegate.ExecutionListener;

import java.math.BigDecimal;
import java.util.List;

/**
 * @Author : xiaofei
 * @Date: 2019/8/22
 */
public class ApplicatioSendListener implements ExecutionListener {
    @Override
    public void notify(DelegateExecution delegateExecution) throws Exception {

        String tableId = (String) delegateExecution.getVariable("tableId");
        IapplicationSendGoodsOrderService applicationSendGoodsOrderService = SpringContextHolder.getBean(IapplicationSendGoodsOrderService.class);
        IappliSendNumService appliSendNumService = SpringContextHolder.getBean(IappliSendNumService.class);
        IpurchaseOrderService purchaseOrderService = SpringContextHolder.getBean(IpurchaseOrderService.class);
        IsendGoodsOrderService sendGoodsOrderService = SpringContextHolder.getBean(IsendGoodsOrderService.class);
        IindentOrderService indentOrderService = SpringContextHolder.getBean(IindentOrderService.class);
        IDealerLevelService dealerLevelService = SpringContextHolder.getBean(IDealerLevelService.class);
        UserService userService = SpringContextHolder.getBean(UserService.class);

        if (delegateExecution.getEventName().equals("end")){
            ApplicationSendGoodsOrder applicationSendGoodsOrder = applicationSendGoodsOrderService.getById(tableId);
            applicationSendGoodsOrder.setSendStatus(SendStatus.DOING);
            applicationSendGoodsOrder.setCheckStatus(CheckStatus.sucess);
            applicationSendGoodsOrderService.updateById(applicationSendGoodsOrder);

            //同时更新订货单中的已发数量 和减少未发数量
            List<AppliSendNum> appliSendNums = appliSendNumService.list(Wrappers.<AppliSendNum>lambdaQuery()
            .eq(AppliSendNum::getApplicationSendOrderId,tableId));
            if (ToolUtil.isNotEmpty(appliSendNums)){
                appliSendNums.forEach(Number -> {
                    PurchaseOrder purchaseOrder = purchaseOrderService.getById(Number.getPurchaseId());
                    purchaseOrder.setHavaSend(purchaseOrder.getHavaSend().add(new BigDecimal(Number.getSendNum())));
                    purchaseOrder.setNotSend(purchaseOrder.getNotSend().subtract(new BigDecimal(Number.getSendNum())));
                    purchaseOrderService.updateById(purchaseOrder);
                });
            }

            //减少经销商账户余额
            //处理经销商余额
            BigDecimal totalPrice = sendGoodsOrderService.getTotalPrice(applicationSendGoodsOrder.getId());
            //判断支付类型
            IndentOrder indentOrder = indentOrderService.getById(applicationSendGoodsOrder.getIndentOrderId());
            if (ToolUtil.isNotEmpty(indentOrder.getIndentOrderType())) {
                DealerLevel dealerLevel = dealerLevelService.getOne(Wrappers.<DealerLevel>lambdaQuery()
                        .eq(DealerLevel::getDeptId, userService.get(indentOrder.getCreateBy()).getDepartmentId()));
                if (indentOrder.getPayType().equals(PayType.BLANCE)) {
                    dealerLevel.setBalance(dealerLevel.getBalance().subtract(totalPrice));
                    dealerLevelService.updateById(dealerLevel);
                } else if (indentOrder.getPayType().equals(PayType.CRIDIT)) {
                    dealerLevel.setCredit(dealerLevel.getCredit().subtract(totalPrice));
                    dealerLevelService.updateById(dealerLevel);
                }
            }
        }
    }
}
