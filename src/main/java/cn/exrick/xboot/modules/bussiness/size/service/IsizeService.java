package cn.exrick.xboot.modules.bussiness.size.service;

import com.baomidou.mybatisplus.extension.service.IService;
import cn.exrick.xboot.modules.bussiness.size.pojo.size;

import java.util.List;

/**
 * 产品尺寸接口
 * @author tqr
 */
public interface IsizeService extends IService<size> {

}