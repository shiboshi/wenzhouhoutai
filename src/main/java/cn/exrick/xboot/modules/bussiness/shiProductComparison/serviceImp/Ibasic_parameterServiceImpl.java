package cn.exrick.xboot.modules.bussiness.shiProductComparison.serviceImp;

import cn.exrick.xboot.modules.bussiness.shiProductComparison.mapper.basic_parameterMapper;
import cn.exrick.xboot.modules.bussiness.shiProductComparison.pojo.basic_parameter;
import cn.exrick.xboot.modules.bussiness.shiProductComparison.service.Ibasic_parameterService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

/**
 * 基本参数表接口实现
 * @author 石博士
 */
@Slf4j
@Service
@Transactional
public class Ibasic_parameterServiceImpl extends ServiceImpl<basic_parameterMapper, basic_parameter> implements Ibasic_parameterService {

    @Autowired
    private basic_parameterMapper basic_parameterMapper;
}