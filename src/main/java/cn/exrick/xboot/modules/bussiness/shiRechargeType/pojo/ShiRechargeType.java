package cn.exrick.xboot.modules.bussiness.shiRechargeType.pojo;

import cn.exrick.xboot.base.XbootBaseEntity;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModelProperty;
import io.swagger.annotations.ApiModel;
import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

/**
 * @author tqr
 */
@Data
@Entity
@Table(name = "t_shi_recharge_type")
@TableName("t_shi_recharge_type")
@ApiModel(value = "充值分类")
public class ShiRechargeType extends XbootBaseEntity {

    private static final long serialVersionUID = 1L;

    /**
     * 充值类型
     */
    private String types;
}