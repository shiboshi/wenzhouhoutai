package cn.exrick.xboot.modules.bussiness.shiRechargeType.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import cn.exrick.xboot.modules.bussiness.shiRechargeType.pojo.ShiRechargeType;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

/**
 * 充值分类数据处理层
 * @author tqr
 */
@Mapper
public interface ShiRechargeTypeMapper extends BaseMapper<ShiRechargeType> {

}