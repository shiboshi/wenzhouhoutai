package cn.exrick.xboot.modules.wx;

import cn.exrick.xboot.common.constant.ActivitiConstant;
import cn.exrick.xboot.common.enums.CheckStatus;
import cn.exrick.xboot.common.enums.IsComplete;
import cn.exrick.xboot.common.factory.ConstantFactory;
import cn.exrick.xboot.common.utils.*;
import cn.exrick.xboot.common.vo.PageVo;
import cn.exrick.xboot.common.vo.Result;
import cn.exrick.xboot.modules.UpayApi.WxPay.PayVO;
import cn.exrick.xboot.modules.UpayApi.WxPay.ResponseVo;
import cn.exrick.xboot.modules.activiti.entity.ActBusiness;
import cn.exrick.xboot.modules.activiti.service.ActBusinessService;
import cn.exrick.xboot.modules.activiti.service.ActProcessService;
import cn.exrick.xboot.modules.activiti.service.mybatis.IHistoryIdentityService;
import cn.exrick.xboot.modules.activiti.utils.MessageUtil;
import cn.exrick.xboot.modules.activiti.vo.ActProcessVo;
import cn.exrick.xboot.modules.activiti.vo.ProcessNodeVo;
import cn.exrick.xboot.modules.base.dao.mapper.DepartmentMapper;
import cn.exrick.xboot.modules.base.entity.Department;
import cn.exrick.xboot.modules.base.entity.User;
import cn.exrick.xboot.modules.base.service.DepartmentService;
import cn.exrick.xboot.modules.base.service.UserService;
import cn.exrick.xboot.modules.base.vo.UserVO;
import cn.exrick.xboot.modules.bussiness.afterSaleOrder.VO.AfterSaleVO;
import cn.exrick.xboot.modules.bussiness.afterSaleOrder.VO.CheckSaleVO;
import cn.exrick.xboot.modules.bussiness.afterSaleOrder.VO.MaintainCheckVo;
import cn.exrick.xboot.modules.bussiness.afterSaleOrder.pojo.AfterSaleOrder;
import cn.exrick.xboot.modules.bussiness.afterSaleOrder.service.IafterSaleOrderService;
import cn.exrick.xboot.modules.bussiness.applicationSendGoodsOrder.VO.AppliParam;
import cn.exrick.xboot.modules.bussiness.applicationSendGoodsOrder.VO.AppliSendGoodsVO;
import cn.exrick.xboot.modules.bussiness.applicationSendGoodsOrder.VO.FilterVO;
import cn.exrick.xboot.modules.bussiness.applicationSendGoodsOrder.pojo.ApplicationSendGoodsOrder;
import cn.exrick.xboot.modules.bussiness.applicationSendGoodsOrder.service.IapplicationSendGoodsOrderService;
import cn.exrick.xboot.modules.bussiness.classification.VO.ClassificationVo;
import cn.exrick.xboot.modules.bussiness.classification.pojo.Classification;
import cn.exrick.xboot.modules.bussiness.classification.service.IClassificationService;
import cn.exrick.xboot.modules.bussiness.color.VO.ColorVO;
import cn.exrick.xboot.modules.bussiness.color.pojo.Color;
import cn.exrick.xboot.modules.bussiness.color.service.IcolorService;
import cn.exrick.xboot.modules.bussiness.dealerLevel.pojo.DealerLevel;
import cn.exrick.xboot.modules.bussiness.dealerLevel.service.IDealerLevelService;
import cn.exrick.xboot.modules.bussiness.indentOrder.service.IindentOrderService;
import cn.exrick.xboot.modules.bussiness.inventoryOrder.VO.InventoryVO;
import cn.exrick.xboot.modules.bussiness.inventoryOrder.mapper.InventoryOrderMapper;
import cn.exrick.xboot.modules.bussiness.inventoryOrder.pojo.InventoryOrder;
import cn.exrick.xboot.modules.bussiness.inventoryOrder.pojo.InventoryOrderResult;
import cn.exrick.xboot.modules.bussiness.inventoryOrder.service.IInventoryOrderService;
import cn.exrick.xboot.modules.bussiness.product.VO.ProductCommon;
import cn.exrick.xboot.modules.bussiness.product.VO.ProductOrderVo;
import cn.exrick.xboot.modules.bussiness.product.VO.WxProductList;
import cn.exrick.xboot.modules.bussiness.product.VO.WxRequestDTO;
import cn.exrick.xboot.modules.bussiness.product.pojo.ProductModel;
import cn.exrick.xboot.modules.bussiness.product.pojo.ProductOrder;
import cn.exrick.xboot.modules.bussiness.product.service.IproductModelService;
import cn.exrick.xboot.modules.bussiness.product.service.IproductOrderService;
import cn.exrick.xboot.modules.bussiness.productPrice.pojo.ProductPrice;
import cn.exrick.xboot.modules.bussiness.productPrice.service.IProductPriceService;
import cn.exrick.xboot.modules.bussiness.purchaseOrder.VO.PurchaseOrderVO;
import cn.exrick.xboot.modules.bussiness.purchaseOrder.VO.WxPayRequestDTO;
import cn.exrick.xboot.modules.bussiness.purchaseOrder.pojo.PurchaseOrder;
import cn.exrick.xboot.modules.bussiness.purchaseOrder.service.IpurchaseOrderService;
import cn.exrick.xboot.modules.bussiness.saleOrder.VO.NeedCheckProcudct;
import cn.exrick.xboot.modules.bussiness.saleOrder.VO.SalesOrderVO;
import cn.exrick.xboot.modules.bussiness.saleOrder.pojo.SalesOrder;
import cn.exrick.xboot.modules.bussiness.saleOrder.service.IsalesOrderService;
import cn.exrick.xboot.modules.bussiness.sendGoodsOrder.VO.SendGoodsVO;
import cn.exrick.xboot.modules.bussiness.sendGoodsOrder.pojo.SendGoodsOrder;
import cn.exrick.xboot.modules.bussiness.sendGoodsOrder.service.IsendGoodsOrderService;
import cn.exrick.xboot.modules.bussiness.shiWarehouse.pojo.ShiWarehouse;
import cn.exrick.xboot.modules.bussiness.shiWarehouse.service.IShiWarehouseService;
import cn.exrick.xboot.modules.bussiness.shopCart.VO.ShopCartVO;
import cn.exrick.xboot.modules.bussiness.shopCart.pojo.ShopCart;
import cn.exrick.xboot.modules.bussiness.shopCart.service.IShopCartService;
import cn.exrick.xboot.modules.bussiness.specialPriceSalesOrder.service.IspecialPriceSalesOrderService;
import cn.exrick.xboot.modules.bussiness.unit.service.IUnitService;
import cn.exrick.xboot.modules.wx.VO.DebugDetail;
import cn.hutool.core.date.DateUtil;
import cn.hutool.core.util.StrUtil;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.google.common.base.Joiner;
import com.google.common.base.Splitter;
import io.swagger.annotations.ApiOperation;
import lombok.extern.java.Log;
import org.activiti.engine.IdentityService;
import org.activiti.engine.RuntimeService;
import org.activiti.engine.TaskService;
import org.activiti.engine.runtime.ProcessInstance;
import org.activiti.engine.task.Task;
import org.activiti.engine.task.TaskQuery;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import javax.persistence.EntityManager;
import java.math.BigDecimal;
import java.util.*;
import java.util.stream.Collectors;

@RequestMapping("/xboot/wx")
@RestController
@Log
public class WxController {

    @Autowired
    private UserService userService;
    @Autowired
    private IClassificationService iClassificationService;
    @Autowired
    private IproductOrderService iproductOrderService;
    @Autowired
    private DepartmentService departmentService;
    @Autowired
    private IDealerLevelService iDealerLevelService;
    @Autowired
    private IProductPriceService iProductPriceService;
    @Autowired
    private IShopCartService iShopCartService;
    @Autowired
    private IUnitService iUnitService;
    @Autowired
    private IpurchaseOrderService ipurchaseOrderService;
    @Autowired
    private IindentOrderService iindentOrderService;
    @Autowired
    private ActBusinessService actBusinessService;
    @Autowired
    private IspecialPriceSalesOrderService ispecialPriceSalesOrderService;
    @Autowired
    private IInventoryOrderService inventoryOrderService;
    @Autowired
    private IShiWarehouseService shiWarehouseService;
    @Autowired
    private IsendGoodsOrderService IsendGoodsOrderService;
    @Autowired
    private EntityManager entityManager;
    @Autowired
    private IapplicationSendGoodsOrderService applicationSendGoodsOrderService;
    @Autowired
    private StringRedisTemplate redisTemplate;
    @Autowired
    private IdentityService identityService;
    @Autowired
    private IsalesOrderService isalesOrderService;
    @Autowired
    private IsendGoodsOrderService isendGoodsOrderService;
    @Autowired
    private DepartmentMapper departmentMapper;

    @Resource
    private RuntimeService runtimeService;
    @Resource
    private TaskService taskService;

    @Autowired
    private MessageUtil messageUtil;

    @Autowired
    private IafterSaleOrderService iafterSaleOrderService;

    @Autowired
    private SecurityUtil securityUtil;

    @Autowired
    private IapplicationSendGoodsOrderService iapplicationSendGoodsOrderService;

    @Autowired
    private ActProcessService actProcessService;
    @Autowired
    private IDealerLevelService dealerLevelService;
    @Autowired
    private IHistoryIdentityService iHistoryIdentityService;
    @Autowired
    private Wxservice wxservice;
    @Autowired
    private InventoryOrderMapper inventoryOrderMapper;

    @Autowired
    private IcolorService icolorService;

    @Autowired
    private IproductModelService iproductModelService;

    @Autowired
    private cn.exrick.xboot.modules.bussiness.color.mapper.colorMapper colorMapper;


    private Joiner joiner = Joiner.on(",").skipNulls();
    private Splitter splitter = Splitter.on(",").trimResults().omitEmptyStrings();

    /**
     * 用户注册
     *
     * @param userVO
     * @return
     */
    @RequestMapping("/registerUser")
    public Result<Object> register(UserVO userVO) {
        User userInset = new User();
        BeanUtils.copyProperties(userVO, userInset);
        String encryptPass = new BCryptPasswordEncoder().encode(userInset.getPassword());
        userInset.setPassword(encryptPass);
        if (StrUtil.isBlank(userInset.getUsername()) || StrUtil.isBlank(userInset.getPassword())) {
            return new ResultUtil<>().setErrorMsg("缺少必需表单字段");
        }

        if (userService.findByUsername(userInset.getUsername()) != null) {
            return new ResultUtil<>().setErrorMsg("该用户名已被注册");
        }

        if (userService.findByMobile(userInset.getMobile()) != null) {
            return new ResultUtil<>().setErrorMsg("该手机号已被注册");
        }
        //查询序列号
        List<SalesOrder> salesOrder = isalesOrderService.getBaseMapper().selectList(new QueryWrapper<SalesOrder>()
                .lambda().eq(SalesOrder::getBuyerPhone, userVO.getMobile()));
        Boolean b = false;
        for (SalesOrder salesOrderUse : salesOrder) {


            SendGoodsOrder sendGoods = isendGoodsOrderService.getOne(new QueryWrapper<SendGoodsOrder>()
                    .lambda().eq(SendGoodsOrder::getSaleOrderId, salesOrderUse.getId())
                    .eq(SendGoodsOrder::getIsComplete, IsComplete.IS_COMPLETE.getValue()));
            List<Long> ids;
            if (ToolUtil.isNotEmpty(sendGoods)) {
                ids = Arrays.asList(sendGoods.getInventoryOrderIds().split(",")).stream().map(s -> Long.parseLong(s)).collect(Collectors.toList());

                for (Long id : ids) {
                    InventoryOrder inventoryOrder = inventoryOrderService.getById(id);
                    if (inventoryOrder.getSerialNum().contains(userVO.getSerials())) {
                        b = true;
                        break;
                    }
                }
            }
        }

        if (b == true) {
            userInset.setDepartmentId(departmentMapper.selectOne(new QueryWrapper<Department>()
                    .lambda().eq(Department::getTitle, "温州经销商").last(" limit 1 ")).getId());
            userService.save(userInset);
            return new ResultUtil<>().setData(userInset, "注册成功");
        } else {
            return new ResultUtil<>().setErrorMsg("序列号不存在！");
        }

    }
    /*------------------common通·用数据-------------------------------*/

    public Result<List<ClassificationVo>> getValue(List<Classification> classifications) {
        List<ClassificationVo> classficationDTOS = new ArrayList<>();
        if (classifications.size() > 0) {
            classifications.forEach((s) -> {
                ClassificationVo classficationDTO = new ClassificationVo();
                BeanUtils.copyProperties(s, classficationDTO);
                classficationDTO.setIsCheck(false);
                classficationDTOS.add(classficationDTO);
            });
            return new ResultUtil<List<ClassificationVo>>().setData(classficationDTOS);
        } else {
            return new ResultUtil<List<ClassificationVo>>().error(500, "操作失败");
        }

    }

    /**
     * 获取产品分类
     *
     * @return
     */
    @PostMapping("/getCatgory")
    public Result<List<Classification>> getCatoryOne(String classificationId) {

        if (ToolUtil.isEmpty(classificationId)) {
            List<Classification> classificationOneList = iClassificationService.getBaseMapper().selectList(
                    new QueryWrapper<Classification>().lambda().eq(Classification::getParentId, "0")
            );
            return new ResultUtil<List<Classification>>().setData(classificationOneList);

        } else {
            List<Classification> classifications = iClassificationService.getBaseMapper().selectList(
                    new QueryWrapper<Classification>().lambda().eq(Classification::getParentId, classificationId)
            );
            return new ResultUtil<List<Classification>>().setData(classifications);
        }
    }

    /**
     * 获取产品品号列表
     * type 1.经销商 2.门店人员
     *
     * @param wxRequestDTO
     * @return
     */
    @PostMapping("/getPrductOrderList")
    public Result<Object> getPrductOrderList(WxRequestDTO wxRequestDTO) {

        //1.获取当前登录经销商或者门店人员对应的部门
        String dept = "";
        User user = userService.get(wxRequestDTO.getUserId());
        if (ToolUtil.isNotEmpty(user)) {
            if (wxRequestDTO.getType() == 1) {
                dept = user.getDepartmentId();
            } else if (wxRequestDTO.getType() == 2) {
                dept = departmentService.get(user.getDepartmentId()).getParentId();
            } else if (wxRequestDTO.getType() == 3) {
                dept = departmentService.get(departmentService.get(user.getDepartmentId()).getParentId()).getParentId();
            }
        }

        //2.获取当前经销商部门对应的的等级
        DealerLevel dealerLevel = dealerLevelService.getOne(Wrappers.<DealerLevel>lambdaQuery()
                .eq(DealerLevel::getDeptId, dept));

        //3.获取等级对应的产品价格
        if (ToolUtil.isNotEmpty(dealerLevel)) {
            List<ProductPrice> productPrice = iProductPriceService.list(Wrappers.<ProductPrice>lambdaQuery()
                    .eq(ProductPrice::getLevelId, dealerLevel.getLevelId()));
            List<WxProductList> productLists = productPrice.stream().filter(p -> wxservice.filterProduct(p, wxRequestDTO.getClassificationId()) == true)
                    .map(p -> {
                        WxProductList wxProductList = new WxProductList();
                        ProductOrder productOrder = iproductOrderService.getOne(Wrappers.<ProductOrder>lambdaQuery()
                                .eq(ProductOrder::getKindId, p.getProductOrderId()));

                        /**
                         * 查询产品型号表
                         */
                        QueryWrapper<ProductModel> productModelQueryWrapper =new QueryWrapper<>();
                        productModelQueryWrapper.eq("kind_id",wxRequestDTO.getClassificationId());
                        ProductModel ProductModel = iproductModelService.getOne(productModelQueryWrapper);

                        /**
                         * 查询产品颜色 查询 ParentId
                         */
                        QueryWrapper<Color> queryWrapperColor = new QueryWrapper<>();
                        queryWrapperColor.eq("id",productOrder.getColorId());
                        Color color = icolorService.getOne(queryWrapperColor);

                        /**
                         * 二级
                         * 根据 ParentId 查询 父级id 显示 ParentId
                         */
                        QueryWrapper<Color> queryWrapperColor2 = new QueryWrapper<>();
                        queryWrapperColor2.eq("id",color.getParentId());
                        Color color2 = icolorService.getOne(queryWrapperColor2);

                        /**
                         * 三级
                         * 根据 ParentId 查询 父级id 显示 title
                         */
                        QueryWrapper<Color> queryWrapperColor3 = new QueryWrapper<>();
                        queryWrapperColor3.eq("id",color2.getParentId());
                        Color color3 = icolorService.getOne(queryWrapperColor3);

                        /**
                         * 查询产品色号
                         */
                        ColorVO colorVO = colorMapper.getColor(color.getId());

                        if (ToolUtil.isNotEmpty(productOrder)) {
                            wxProductList.setId(productOrder.getId())
                                    .setPicPath(productOrder.getPicPath())
                                    .setProductName(productOrder.getProductOrderName())
                                    .setProductId(productOrder.getId())
                                    .setPrice(wxRequestDTO.getType() == 1 ? p.getProductPrice() : productOrder.getMinSellingPrice())
                                    .setColor(color3.getTitle())
                                    .setModelNameIn(ProductModel.getModelNameIn())
                                    .setModelNameOut(ProductModel.getModelNameOut())
                                    .setColorNumber(colorVO.getColorNumber());
                            return wxProductList;
                        }
                        return null;
                    }).collect(Collectors.toList());
            //查询产品库存数量
            String deptId = dept;
            String condition = null;
            if (ToolUtil.isNotEmpty(deptId)) {
                List<InventoryOrderResult> list1 = getRecursions(deptId, condition);
                list1 = list1.stream().filter(t -> "未出库".equals(t.getSerialNumOut())).collect(Collectors.toList());
                list1 = list1.stream()
                        .collect(Collectors
                                .collectingAndThen
                                        (Collectors.toCollection(
                                                () -> new TreeSet<>(Comparator.comparing(t -> t.getId()))), ArrayList::new)
                        );
                List<InventoryOrderResult> list = list1.stream().
                        collect(Collectors
                                .collectingAndThen
                                        (Collectors.toCollection(
                                                () -> new TreeSet<>(Comparator.comparing(o -> o.getWarhouseId() + "#" + o.getProductOrderId()))), ArrayList::new));
                for (InventoryOrderResult i : list) {
                    Integer count = 0;
                    for (InventoryOrderResult j : list1) {
                        if (i.getWarhouseId().equals(j.getWarhouseId()) && i.getProductOrderId().equals(j.getProductOrderId())) {
                            count += 1;
                        }
                    }
                    i.setAmount(count.longValue());
                }
                if (list.size() > 0) {
                    for (WxProductList w : productLists) {
                        for (InventoryOrderResult i : list) {
                            if (w.getProductId().equals(i.getProductOrderId())) {
                                w.setAmount(i.getAmount());
                            }
                        }
                    }
                }
            }
            //productLists = productLists.stream().filter(l -> ToolUtil.isNotEmpty(l.getAmount())).collect(Collectors.toList());
            return new ResultUtil<>().setData(productLists, "获取列表成功！");
        } else {
            return new ResultUtil<>().setErrorMsg("暂无经销商等级！");
        }
    }

    public List<InventoryOrderResult> getRecursions(String deptId, String condition) {
        //查询该部门所拥有的仓库
        List<InventoryOrderResult> list = inventoryOrderMapper.getByPage(deptId, condition);
        //查询该部门子集部门
        List<String> ids = departmentMapper.selectList(new QueryWrapper<Department>().lambda().eq(Department::getParentId, deptId))
                .stream()
                .map(department -> department.getId())
                .collect(Collectors.toList());
        if (ids.size() > 0) {
            for (String s : ids) {
                //查询该部门所拥有的仓库
                List<InventoryOrderResult> list1 = inventoryOrderMapper.getByPage(s, condition);
                list.addAll(list1);
                List<InventoryOrderResult> list2 = getRecursions(s, condition);
                list.addAll(list2);
            }
        }
        return list;
    }

    /**
     * 获取产品的详情信息
     *
     * @param productOrderId
     * @return
     */
    @PostMapping("/getProductDetail")
    public Result<ProductOrderVo> getProductDetail(String productOrderId) {

        ProductOrderVo productOrderDTOS = iproductOrderService.getAllProductDetail(productOrderId);
        productOrderDTOS.setTimeToMarket(DateUtil.format(DateUtil.parse(productOrderDTOS.getTimeToMarket()),"yyyy-MM-dd"));

        ProductOrder productOrder = iproductOrderService.getById(productOrderId);

        ProductModel productModel = iproductModelService.getOne(Wrappers.<ProductModel>lambdaQuery()
        .eq(ProductModel::getKindId,iClassificationService.getById(productOrder.getKindId()).getParentId()));

        if (ToolUtil.isNotEmpty(productOrderDTOS)) {
            if (ToolUtil.isNotEmpty(productOrderDTOS.getPicPath())) {
                List<String> pic = Arrays.asList(productOrderDTOS.getPicPath().split(","));
                productOrderDTOS.setPicPathList(pic);
            }
           productOrderDTOS.setUnitName(iUnitService.getById(productOrder.getUnit()).getUnitName());
           productOrderDTOS.setUnitMax(iUnitService.getById(productOrder.getUnitMax()).getUnitName());
            /**
             * 对内型号
             */
            productOrderDTOS.setModelNameIn(productModel.getModelNameIn());
            productOrderDTOS.setUnitWeight(productOrder.getUnitWeight().toString());
            productOrderDTOS.setVolume(productOrder.getVolume().toString());
        }
        return new ResultUtil<ProductOrderVo>().setData(productOrderDTOS);
    }

    /**
     * 添加购物车
     * *
     *
     * @param
     * @return
     */
    @RequestMapping("/addCart")
    public Result<ShopCart> addShopCart(String productId, String unitId, String userId) {

        ShopCart shopCartNew = iShopCartService.getOne(new QueryWrapper<ShopCart>()
                .lambda().eq(ShopCart::getProductId, productId).eq(ShopCart::getUserId, userId).last(" limit 1 "));

        if (ToolUtil.isNotEmpty(shopCartNew)) {
            shopCartNew.setAmout(shopCartNew.getAmout() + 1);
            iShopCartService.getBaseMapper().updateById(shopCartNew);
            return new ResultUtil<ShopCart>().setData(shopCartNew);
        } else {
            ShopCart shopCart = new ShopCart();
            shopCart.setProductId(productId);
            shopCart.setUnitId(unitId);
            shopCart.setCreateTime(new Date());
            shopCart.setUserId(userId);
            shopCart.setAmout(1);
            iShopCartService.getBaseMapper().insert(shopCart);
            return new ResultUtil<ShopCart>().setData(shopCart);
        }
    }

    /**
     * 查询购物车
     *
     * @return
     */
    @RequestMapping("/listShopCart")
    public Result<Map<String, Object>> listShopCart(Integer type, String userId) {

        List<ShopCart> shopCarts = iShopCartService.getBaseMapper().selectList(new
                QueryWrapper<ShopCart>().lambda().eq(ShopCart::getUserId, userId).orderByDesc(ShopCart::getCreateTime));
        List<ShopCartVO> shopCartDTOS = new ArrayList<>();
        if (shopCarts.size() > 0) {
            shopCarts.forEach((s) -> {
                ShopCartVO shopCartDTO = new ShopCartVO();
                ToolUtil.copyProperties(s, shopCartDTO);
                //添加产品详细信息
                ProductOrder productOrder = iproductOrderService.getById(s.getProductId());
                if (ToolUtil.isNotEmpty(productOrder)) {
                    Classification three = iClassificationService.getById(productOrder.getKindId());
                    shopCartDTO.setProductOrderName(three.getTitle());
                    Classification two = iClassificationService.getById(three.getParentId());
                    shopCartDTO.setProductModelName(two.getTitle());
                    Classification one = iClassificationService.getById(two.getParentId());
                    shopCartDTO.setProductName(one.getTitle());
                    //获取产品价格
                    if (type == 1) {
                        ProductPrice price = iProductPriceService.getOne(new QueryWrapper<ProductPrice>()
                                .lambda().eq(ProductPrice::getProductOrderId, productOrder.getKindId())
                                .eq(ProductPrice::getLevelId, iDealerLevelService.getOne(new QueryWrapper<DealerLevel>()
                                        .lambda().eq(DealerLevel::getDeptId, userService.get(userId).getDepartmentId())).getLevelId()));
                        shopCartDTO.setPrice(price.getProductPrice());
                    } else {
                        shopCartDTO.setPrice(productOrder.getMinSellingPrice());
                    }
                    //获取产品图片
                    shopCartDTO.setProductPic(productOrder.getPicPath().split(",")[0]);

                    //获取单位名称
                    if (ToolUtil.isNotEmpty(iUnitService.getById(s.getUnitId()))) {
                        shopCartDTO.setUnitName(iUnitService.getById(s.getUnitId()).getUnitName());
                    }
                    shopCartDTO.setAddtime(s.getCreateTime().toString());
                    shopCartDTO.setChecked(false);
                    shopCartDTOS.add(shopCartDTO);
                }
            });
        }

        Map<String, Object> map = new HashMap<>();
        if (departmentService.get(userService.get(userId).getDepartmentId()).getTitle().contains("部门3")) {
            map.put("address", iDealerLevelService.getOne(new QueryWrapper<DealerLevel>().lambda().eq(DealerLevel::getDeptId, userService.get(userId).getDepartmentId())).getAddress());
        } else {
            map.put("address", "暂无地址");
        }

        map.put("shopCartDTOS", shopCartDTOS);
        return new ResultUtil<Map<String, Object>>().setData(map);
    }


    /**
     * 删除购物车
     *
     * @param
     * @return
     */
    @RequestMapping("/deleteShopCard")
    public Result<Object> deleteShopCard(String id) {
        if (ToolUtil.isNotEmpty(id)) {
            iShopCartService.getBaseMapper().deleteById(id);
            return new ResultUtil<>().setSuccessMsg("删除成功");
        } else {
            return new ResultUtil<>().setErrorMsg("插入参数为空");
        }
    }

    /*-----------------------------小程序流程服务-----------------------------------------------------*/

    /**
     * 过滤出需要的流程数据
     *
     * @param actProcessVos
     * @returnF
     */
    public ActProcessVo getFilter(List<ActProcessVo> actProcessVos, String type) {
        List<ActProcessVo> actProcessVoList = actProcessVos.stream().filter(act -> type.equals(act.getDesc())).collect(Collectors.toList());
        return actProcessVoList.get(0);
    }

    /**H
     * 获取redis的流程定义信息
     *
     * @return
     */
    public String getProcDefId(String type) {
        //获取redis的值
        String param = redisTemplate.opsForValue().get(ActivitiConstant.actProcess);
        List<ActProcessVo> actProcessVos = JSON.parseArray(param, ActProcessVo.class);
        if (ToolUtil.isNotEmpty(actProcessVos)) {
            return getFilter(actProcessVos, type).getProceDefId();
        }
        return "";
    }

    /**
     * 获取流程id
     *
     * @param actBusiness
     * @return
     */
    public String startProcess(ActBusiness actBusiness, String userId) {

        identityService.setAuthenticatedUserId(userId);
        actBusiness.getParams().put("tableId", actBusiness.getTableId());
        ProcessInstance processInstance = runtimeService.startProcessInstanceById(actBusiness.getProcDefId(), actBusiness.getId(), actBusiness.getParams());
        List<Task> tasks = taskService.createTaskQuery().processInstanceId(processInstance.getId()).list();
        tasks.forEach(task -> {
            Arrays.stream(actBusiness.getAssignees()).forEach(assi -> {
                taskService.addCandidateUser(task.getId(), assi);
                // 异步发消息
                messageUtil.sendActMessage(assi, ActivitiConstant.MESSAGE_TODO_CONTENT, actBusiness.getSendMessage(),
                        actBusiness.getSendSms(), actBusiness.getSendEmail());
            });
            //设置优先级
            taskService.setPriority(task.getId(), actBusiness.getPriority());
        });
        return processInstance.getId();
    }

    /**
     * 启动流程
     *
     * @param act
     * @param userId
     */
    public void start(ActBusiness act, String userId) {
        String proceInstanceId = startProcess(act, userId);
        act.setProcInstId(proceInstanceId);
        act.setStatus(ActivitiConstant.STATUS_DEALING);
        act.setResult(ActivitiConstant.RESULT_DEALING);
        act.setApplyTime(new Date());
        actBusinessService.update(act);
    }

    public ActBusiness checkUser(ActBusiness actBusiness, String type) {
        ProcessNodeVo processNodeVo = actProcessService.getFirstNode(getProcDefId(type));
        List<User> users = processNodeVo.getUsers();
        List<String> assigneesId = users.stream().map(user -> user.getId()).collect(Collectors.toList());
        String[] ids = new String[assigneesId.size()];
        actBusiness.setAssignees(assigneesId.toArray(ids));
        actBusiness.setPriority(0);
        return actBusiness;
    }


    /*---------------------------订单接口----------------------------------------------*/


    /**
     * 调用支付
     *
     * @param payVO
     * @return
     */
    @PostMapping("/pay")
    public Result<Object> payIndentOrder(@ModelAttribute PayVO payVO) {

        List<PurchaseOrder> purchaseOrders = JSON.parseArray(payVO.getParam(), PurchaseOrder.class);
        //1.判断改订单的总金额是否大于 改经销商的账户余额
        BigDecimal totalPrice = new BigDecimal(purchaseOrders.stream().mapToDouble(Order -> Order.getPrice().multiply(Order.getOrderNumbers()).setScale(2, BigDecimal.ROUND_HALF_UP).doubleValue())
                .sum());
        DealerLevel dealerLevel = dealerLevelService.getOne(Wrappers.<DealerLevel>lambdaQuery()
                .eq(DealerLevel::getDeptId, userService.get(payVO.getUserId()).getDepartmentId()));
        if (totalPrice.compareTo(dealerLevel.getBalance()) == -1 || totalPrice.compareTo(dealerLevel.getBalance()) == 0) {
            dealerLevel.setCredit(dealerLevel.getBalance().subtract(totalPrice));
            dealerLevelService.updateById(dealerLevel);
            return new ResultUtil<>().setErrorMsg(201, "扣除经销商余额！");
        }
        BigDecimal needPay = totalPrice.subtract(dealerLevel.getBalance()).setScale(2, BigDecimal.ROUND_HALF_UP);
        payVO.setTotalAmount(needPay.multiply(new BigDecimal(100)).setScale(0));
        //开始调用支付
        ResponseVo responseVo = wxservice.pay(payVO);
        //刷新经销商额度
        dealerLevel.setBalance(new BigDecimal(0.00));
        dealerLevelService.updateById(dealerLevel);
        return new ResultUtil<>().setData(responseVo, "调用支付！");
    }

    /*--------------------------------------销售订单--------------------------------------------------------*/

    /**
     * 普通调用支付
     *
     * @param payVO
     * @return
     */
    @PostMapping("/paySaleOrder")
    public Result<Object> paySaleOrder(PayVO payVO) {
        List<WxPayRequestDTO> wxPayRequestDTOS = JSONObject.parseArray(payVO.getParam(), WxPayRequestDTO.class);
        BigDecimal totalAmount = wxPayRequestDTOS.stream().map(w -> new BigDecimal(w.getOrderNumbers())
                .multiply(w.getPrice()).setScale(2, BigDecimal.ROUND_HALF_UP))
                .reduce(BigDecimal::add)
                .get()
                .multiply(new BigDecimal(100));
        payVO.setTotalAmount(totalAmount.setScale(0));
        ResponseVo responseVo = wxservice.pay(payVO);
        return new ResultUtil<>().setData(responseVo, "调用支付！");
    }

    /**
     * 判断是否需要特价审核
     *
     * @param param
     * @param phone
     * @return
     */
    @PostMapping("/judgeNeedCheck")
    public Result<Object> judgeNeedCheck(String param, String phone) {
        List<PurchaseOrder> needCheck = isalesOrderService.filterNeedCheck(param, phone);
        if (ToolUtil.isNotEmpty(needCheck)) {
            List<NeedCheckProcudct> needCheckProcudcts = isalesOrderService.getNeedCheck(needCheck);
            return new ResultUtil<>().setErrorMsg(201, "以下商品需要特价审核！", needCheckProcudcts);
        }
        return new ResultUtil<>().setData("不需要特价审核！");
    }

    /**
     * 插入销售定单
     *
     * @param param
     * @param salesOrder
     * @param userId
     * @return
     */
    @PostMapping("/insertSaleOrder")
    public Result<Object> insertSaleOrder(String param, SalesOrder salesOrder, String userId) {

        List<PurchaseOrder> purchaseOrders = JSONObject.parseArray(param, PurchaseOrder.class);
        ActBusiness actBusiness = isalesOrderService.commitSend(purchaseOrders, salesOrder, userId);
        if (ToolUtil.isNotEmpty(actBusiness)) {
            if (ToolUtil.isNotEmpty(actBusiness.getProcDefId())) {
                checkUser(actBusiness, ActivitiConstant.salesOrder);
                start(actBusiness, userId);
            }
            return new ResultUtil<>().setData("插入订单成功！");
        }
        return new ResultUtil<>().setErrorMsg("未知错误！");
    }

    /**
     * 插入特价申请单
     *
     * @param param
     * @param userId
     * @param phone
     * @return
     */
    @PostMapping("/insertSpecialOrder")
    public Result<Object> insertSpecialOrder(String param, String userId, String phone) {
        List<PurchaseOrder> purchaseOrders = JSONObject.parseArray(param, PurchaseOrder.class);
        ActBusiness actBusiness = ispecialPriceSalesOrderService.insertSecialPriceOrder(purchaseOrders, getProcDefId(ActivitiConstant.specialOrder), userId, phone);
        if (ToolUtil.isNotEmpty(actBusiness)) {
            checkUser(actBusiness, ActivitiConstant.specialOrder);
            start(actBusiness, userId);
            return new ResultUtil<>().setData("插入特价申请单成功！");
        }
        return new ResultUtil<>().setErrorMsg("未知错误！");
    }


    /**
     * 插入订货单(支付回调)
     *
     * @param payVO
     * @return
     */
    @PostMapping("/insertIndentOrder")
    public Result<Object> insertIndentOrder(PayVO payVO) {
        //1.生成流程 2.启动流程
        List<PurchaseOrder> purchaseOrdersList = JSON.parseArray(payVO.getParam(), PurchaseOrder.class);
        if (ToolUtil.isNotEmpty(purchaseOrdersList)) {
            ActBusiness actBusiness = iindentOrderService.insertIndentOrder(purchaseOrdersList, getProcDefId(ActivitiConstant.indentOrder), payVO.getUserId(), payVO.getPayType(),payVO.getIndentOrderType());
            //分配审核人
            ActBusiness actBusinessNew = checkUser(actBusiness, ActivitiConstant.indentOrder);
            start(actBusinessNew, payVO.getUserId());
            return new ResultUtil<>().setSuccessMsg("插入订单成功! ");
        }

        //删除购物车
        //1.获取产品ids
        List<String> productIds = purchaseOrdersList.stream().map(PurchaseOrder::getProductId).collect(Collectors.toList());
        productIds.stream().forEach(id -> iShopCartService.remove(Wrappers.<ShopCart>lambdaQuery()
        .eq(ShopCart::getUserId,payVO.getUserId())
        .eq(ShopCart::getProductId,id)));
        return new ResultUtil<>().setErrorMsg("插入失败，插入参数为空！");
    }


    /**
     * 插入申请发货单
     *
     * @param appliParam
     * @return
     */
    @PostMapping("/insertApplicationSendOrder")
    public Result<Object> insertApplicationSendOrder(AppliParam appliParam) {
        List<FilterVO> purchaseOrderFilter = applicationSendGoodsOrderService.filterCanNUm(appliParam);
        if (ToolUtil.isEmpty(purchaseOrderFilter)) {
            appliParam.setProcDefId(getProcDefId(ActivitiConstant.aplicationSendOrder));
            ActBusiness actBusiness = applicationSendGoodsOrderService.insertOrder(appliParam);
            ActBusiness actBusinessNew = checkUser(actBusiness, ActivitiConstant.aplicationSendOrder);
            start(actBusinessNew, appliParam.getUserId());
            return new ResultUtil<>().setSuccessMsg("插入订单成功！");
        } else {
            return new ResultUtil<>().setErrorMsg(201, "以下商品不能提交！！！", purchaseOrderFilter);
        }
    }

    @PostMapping("/getSalesSendList")
    @ApiOperation(value = "获取全部数据")
    public Result<Object> getSalesSendList(@ModelAttribute PageVo pageVo, String id,String code) {
        Map<String, Object> map = new HashMap<>();
        List<SendGoodsVO> list = isendGoodsOrderService.getSalesSendList(id);
        if(ToolUtil.isNotEmpty(code)){
            list = list.stream().filter(l->l.getIsComplete().getValue()==Integer.parseInt(code)).collect(Collectors.toList());
        }
        map.put("total", list.size());
        map.put("result", PageUtil.listToPage(pageVo, list));
        return new ResultUtil<>().setData(map);
    }
    /*-----------------------扫码出库接口---------------------------------------*/

    /**
     * 根据序列号获取库存信息
     *
     * @param serialNum
     * @return
     */
    @PostMapping("/getInventoryVO")
    public Result<List<InventoryVO>> getInventoryVO(String serialNum) {

        List<String> serials = CommonUtil.getSerial(serialNum);
        List<InventoryVO> inventoryVOS = serials.stream().map(s -> {
            InventoryOrder inventoryOrder = inventoryOrderService.getOne(Wrappers.<InventoryOrder>lambdaQuery()
                    .eq(InventoryOrder::getSerialNum, s));
            if (ToolUtil.isNotEmpty(inventoryOrder)) {
                InventoryVO inventoryVO = new InventoryVO();
                ProductCommon productCommon = iproductOrderService.getProductCommon(inventoryOrder.getProductOrderId());
                ToolUtil.copyProperties(productCommon, inventoryVO);
                ToolUtil.copyProperties(inventoryOrder, inventoryVO);
                ShiWarehouse shiWarehouse = shiWarehouseService.getById(inventoryOrder.getWarhouseId());
                if (ToolUtil.isNotEmpty(shiWarehouse)) {
                    inventoryVO.setWarhouseName(shiWarehouse.getWName());
                }
                return inventoryVO;
            }
            return null;
        }).collect(Collectors.toList());
        return new ResultUtil<List<InventoryVO>>().setData(inventoryVOS, "查询成功!");
    }

    /**
     * 插入申请发货单
     *
     * @return
     */
    @PostMapping("/insertAppliSenGoods")
    public Result<Object> insertAppliSenGoods(AppliParam appliParam) {
        //1.生成流程 2.启动流程
        ActBusiness actBusiness = iapplicationSendGoodsOrderService.insertOrder(appliParam);
        start(actBusiness, appliParam.getUserId());
        return new ResultUtil<>().setSuccessMsg("插入订单成功！");
    }

    /**
     * 插入发货单
     *
     * @param sendGoodsOrder
     * @param taskId
     * @return
     */
    @PostMapping("/insertSenGoods")
    public Result<Object> insertSenGoods(SendGoodsOrder sendGoodsOrder, String taskId, String userId) {

        if (!ToolUtil.isOneEmpty(sendGoodsOrder.getInventoryOrderIds(), taskId)) {
            List<String> ids = JSON.parseArray(sendGoodsOrder.getInventoryOrderIds(), String.class);
            sendGoodsOrder.setInventoryOrderIds(joiner.join(ids));
            sendGoodsOrder.setIsComplete(IsComplete.PENDING);
            sendGoodsOrder.setCreateBy(userId);
            IsendGoodsOrderService.save(sendGoodsOrder);
            //同时减少库存
            ids.stream().forEach(id -> inventoryOrderService.removeById(id));
            //运行流程
            Task task = taskService.createTaskQuery().taskId(taskId).singleResult();
            ProcessInstance processInstance = runtimeService.createProcessInstanceQuery().processInstanceId(task.getProcessInstanceId()).singleResult();
            //获取流程定义表
            //获取最新插入的发货单数据
            SendGoodsOrder sendGoodsOrderNew = IsendGoodsOrderService.getOne(Wrappers.<SendGoodsOrder>lambdaQuery()
                    .orderByDesc(SendGoodsOrder::getId)
                    .last("limit 1"));
            ActBusiness actBusiness = actBusinessService.get(processInstance.getBusinessKey());
            actBusiness.setTableId(sendGoodsOrderNew.getId());
            actBusinessService.update(actBusiness);
            taskService.complete(taskId);
            assigessUser(actBusiness, taskId);
            return new ResultUtil<>().setData("插入订单成功！");
        } else {
            return new ResultUtil<>().setErrorMsg("传入参数为空！");
        }
    }


    public void assigessUser(ActBusiness actBusiness, String taskId) {
        // 判断下一个节点

        List<Task> tasks = taskService.createTaskQuery().processInstanceId(actBusiness.getProcInstId()).list();
        if (tasks != null && tasks.size() > 0) {
            for (Task t : tasks) {
                List<User> users = actProcessService.getNode(t.getTaskDefinitionKey()).getUsers();
                for (User user : users) {
                    //根据设置的角色获取审批人
                    taskService.addCandidateUser(t.getId(), user.getId());
                    // 异步发消息
                    messageUtil.sendActMessage(user.getId(), ActivitiConstant.MESSAGE_TODO_CONTENT, actBusiness.getSendMessage(), actBusiness.getSendSms(), actBusiness.getSendEmail());
                }
                if (actBusiness.getPriority() != null) {
                    taskService.setPriority(t.getId(), actBusiness.getPriority());
                }
            }
        }
        // 记录实际审批人员
        iHistoryIdentityService.insert(String.valueOf(SnowFlakeUtil.getFlowIdInstance().nextId()),
                ActivitiConstant.EXECUTOR_TYPE, securityUtil.getCurrUser().getId(), taskId, actBusiness.getProcInstId());
    }


    /**
     * 获取仓库发货人员的待办任务
     *
     * @param pageVo
     * @return
     */
    @PostMapping("/getApplicationSendOrderList")
    public Result<List<AppliSendGoodsVO>> getApplicationSendOrderList(PageVo pageVo, String userId) {

        TaskQuery taskQuery = taskService.createTaskQuery().taskCandidateOrAssigned(userId);
        taskQuery.orderByTaskCreateTime().desc();
        int first = (pageVo.getPageNumber() - 1) * pageVo.getPageSize();
        List<Task> tasks = taskQuery.listPage(first, pageVo.getPageSize());
        if (ToolUtil.isNotEmpty(tasks)) {
            List<AppliSendGoodsVO> appliSendGoodsVOS = tasks.stream().map(task -> {
                AppliSendGoodsVO appliSendGoodsVO = new AppliSendGoodsVO();
                ProcessInstance processInstance = runtimeService.createProcessInstanceQuery().processInstanceId(task.getProcessInstanceId()).singleResult();
                ActBusiness actBusiness = actBusinessService.get(processInstance.getBusinessKey());
                if (ToolUtil.isNotEmpty(actBusiness)) {
                    ApplicationSendGoodsOrder applicationSendGoodsOrder = applicationSendGoodsOrderService.getById(actBusiness.getTableId());
                    if (ToolUtil.isNotEmpty(applicationSendGoodsOrder)) {
                        ToolUtil.copyProperties(applicationSendGoodsOrder, appliSendGoodsVO);
                        String user = applicationSendGoodsOrder.getCreateBy();
                        appliSendGoodsVO.setUserLevel(ConstantFactory.me().getUserLevelName(user));
                        appliSendGoodsVO.setUserLevel(ConstantFactory.me().getUserLevelName(user));
                        appliSendGoodsVO.setUserName(ConstantFactory.me().getUserName(user));
                        appliSendGoodsVO.setTaskId(task.getId());
                    }
                    return appliSendGoodsVO;
                }
                return null;
            }).collect(Collectors.toList());
            return new ResultUtil<List<AppliSendGoodsVO>>().setData(appliSendGoodsVOS, "查询成功!");
        } else {
            return new ResultUtil<List<AppliSendGoodsVO>>().setSuccessMsg("暂无数据！");
        }
    }

    /*---------------------------------------------------------------------------------*/

    //1.

    /**
     * 获取当前申请发货单的详情
     *
     * @param id
     * @return
     */
    @RequestMapping("/getApplicationSendOrder2Detail")
    public Result<List<PurchaseOrderVO>> getApplicationSendOrderDetail(String id) {
        if (ToolUtil.isEmpty(id)) {
            return new ResultUtil<List<PurchaseOrderVO>>().setErrorMsg("出入参数为空！");
        } else {
            List<PurchaseOrderVO> purchaseOrderVOS = applicationSendGoodsOrderService.getDetail(id);
            return new ResultUtil<List<PurchaseOrderVO>>().setData(purchaseOrderVOS, "查询详情成功！");
        }
    }


    /**
     * 维修单
     * 根据当前的登录人和审核状态为2，
     * 审核成功查询销售订单的数据
     */
    @RequestMapping(value = "/QueryAfterSaleOrder", method = RequestMethod.POST)
    @ApiOperation(value = "根据登录人名称状态查询销售订单的数据")
    public Result<List<SalesOrderVO>> QueryAfterSaleOrder(@ModelAttribute PageVo page) {
        List<SalesOrderVO> data = iafterSaleOrderService.QueryAfterSaleOrder(securityUtil.getCurrUser().getUsername());
        return new ResultUtil<List<SalesOrderVO>>().setData(data);
    }


    /*------------------------------小程序消费者售后维修单-----------------------------------------------------*/

    //1.生成订单
    @PostMapping("/insertAfeterSaleOrder")
    public Result<Object> insertAfeterSaleOrder(AfterSaleOrder afterSaleOrder, String userId) {
        if (!filterSerial(afterSaleOrder.getSaleOrderId(), afterSaleOrder.getSerials())) {
            return new ResultUtil<>().setErrorMsg(201, "以下序列号不在此订单中！");
        } else {
            //启动流程
            ActBusiness actBusiness = insertAfterSale(afterSaleOrder, userId);
            if (ToolUtil.isNotEmpty(actBusiness)) {
                checkUser(actBusiness, ActivitiConstant.afterSaleOrder);
                start(actBusiness, userId);
                return new ResultUtil<>().setSuccessMsg("插入订单成功！");
            } else {
                return new ResultUtil<>().setErrorMsg(202, "创建订单失败！");
            }
        }
    }

    /**
     * 过滤出不满足条件的序列号
     *
     * @param saleOrderId
     * @param serial
     * @return
     */
    public boolean filterSerial(String saleOrderId, String serial) {

        if (!ToolUtil.isOneEmpty(saleOrderId, serial)) {

            SalesOrder salesOrder = isalesOrderService.getById(saleOrderId);
            if (ToolUtil.isNotEmpty(salesOrder)) {

                List<InventoryOrder> inventoryOrders = null;
                if (salesOrder.getWarehouseType() == 1) {
                    inventoryOrders = inventoryOrderService.list(Wrappers.<InventoryOrder>lambdaQuery()
                            .in(InventoryOrder::getId, salesOrder.getInventoryOrderIds()));
                } else if (salesOrder.getWarehouseType() == 2) {

                    SendGoodsOrder sendGoodsOrder = isendGoodsOrderService.getOne(Wrappers.<SendGoodsOrder>
                            lambdaQuery().eq(SendGoodsOrder::getSaleOrderId, saleOrderId));
                    if (ToolUtil.isNotEmpty(sendGoodsOrder)) {
                        //2.过滤出当前发货单的所有序列号信息
                        inventoryOrders = inventoryOrderService.list(Wrappers.<InventoryOrder>lambdaQuery()
                                .in(InventoryOrder::getId, splitter.splitToList(sendGoodsOrder.getInventoryOrderIds())));
                    }
                }
                List<String> serialsTotal = inventoryOrders.stream().map(InventoryOrder::getSerialNum).collect(Collectors.toList());
                //3.判断序列号
                if (ToolUtil.isNotEmpty(serialsTotal.stream().filter(s -> s.equals(serial)))) {
                    return true;
                }
            }

        }

        return false;
    }

    /**
     * 插入售后单记录
     *
     * @param afterSaleOrder
     * @return
     */
    public ActBusiness insertAfterSale(AfterSaleOrder afterSaleOrder, String userId) {

        if (ToolUtil.isNotEmpty(afterSaleOrder)) {
            AfterSaleOrder afterSaleOrderUse = new AfterSaleOrder();
            ToolUtil.copyProperties(afterSaleOrder, afterSaleOrderUse);
            if (ToolUtil.isNotEmpty(afterSaleOrder.getSaleOrderId()) && ToolUtil.isNotEmpty(isalesOrderService
                    .getById(afterSaleOrder.getSaleOrderId()))) {
                afterSaleOrderUse.setWarrantyPeriod(DateUtil.offsetMonth(isalesOrderService.getById(afterSaleOrder
                        .getSaleOrderId()).getCreateTime(), 12));
            }
            afterSaleOrderUse.setCreateBy(userId);
            afterSaleOrderUse.setCreateTime(new Date());
            afterSaleOrderUse.setCheckStatus(CheckStatus.pengindg);
            afterSaleOrderUse.setIsComplete(IsComplete.PENDING);
            iafterSaleOrderService.save(afterSaleOrderUse);
            //获取最新的插入记录
            AfterSaleOrder afterSaleOrderLast = iafterSaleOrderService.getOne(Wrappers.<AfterSaleOrder>
                    lambdaQuery().orderByDesc(AfterSaleOrder::getId)
                    .last("limit 1"));

            //生成流程定义实例
            ActBusiness actBusiness = new ActBusiness();
            actBusiness.setTableId(afterSaleOrderLast.getId());
            actBusiness.setUserId(userId);
            actBusiness.setProcDefId(getProcDefId(ActivitiConstant.afterSaleOrder));
            ActBusiness actBusinessReturn = actBusinessService.save(actBusiness);

            //更新业务表的中流程定义id
            afterSaleOrderLast.setBussinessId(actBusinessReturn.getId());
            iafterSaleOrderService.updateById(afterSaleOrderLast);

            return actBusinessReturn;
        }
        return null;
    }

    //2.查询订单
    @PostMapping("/getAfterSaleList")
    public Result<List<AfterSaleVO>> getAfterSaleList(String userId, Integer code) {

        List<AfterSaleOrder> afterSaleOrders = null;
        if (code == 1) {
            afterSaleOrders = iafterSaleOrderService.list(Wrappers.<AfterSaleOrder>lambdaQuery()
                    .eq(AfterSaleOrder::getCreateBy, userId)
                    .eq(AfterSaleOrder::getCheckStatus, CheckStatus.pengindg)
            );
        } else if (code == 2) {
            afterSaleOrders = iafterSaleOrderService.list(Wrappers.<AfterSaleOrder>lambdaQuery()
                    .eq(AfterSaleOrder::getCreateBy, userId)
                    .eq(AfterSaleOrder::getCheckStatus, CheckStatus.sucess)
            );
        }
        List<AfterSaleVO> afterSaleOrdersList = afterSaleOrders.stream().map(order -> {
            AfterSaleVO afterSaleVO = new AfterSaleVO();
            ToolUtil.copyProperties(order, afterSaleVO);
            return afterSaleVO;
        }).collect(Collectors.toList());
        return new ResultUtil<List<AfterSaleVO>>().setData(afterSaleOrdersList, "获取订单成功！");
    }

    /**
     * 获取审批详情信息
     *
     * @param afterSaleOrderId
     * @return
     */
    @PostMapping("/getCheckSaleVO")
    public Result<CheckSaleVO> getCheckSaleVO(String afterSaleOrderId) {

        AfterSaleOrder afterSaleOrder = iafterSaleOrderService.getById(afterSaleOrderId);
        CheckSaleVO checkSaleVO = new CheckSaleVO();
        ToolUtil.copyProperties(afterSaleOrder, checkSaleVO);
        checkSaleVO.setUserName(userService.get(afterSaleOrder.getUserId()).getUsername());
        checkSaleVO.setUserDept(departmentService.get(userService.get(afterSaleOrder.getUserId()).getDepartmentId())
                .getTitle());
        //拼接配件信息
        if (ToolUtil.isNotEmpty(afterSaleOrder.getParets())) {
            StringBuilder stringBuilder = new StringBuilder();
            List<String> pId = splitter.splitToList(afterSaleOrder.getParets());
            List<String> paretss = pId.stream().map(p -> {
                ProductCommon productCommon = iproductOrderService.getProductCommon(p);
                if (ToolUtil.isNotEmpty(productCommon)) {
                    stringBuilder.append(productCommon.getProductName() + "")
                            .append(productCommon.getProductModelName() + "")
                            .append(productCommon.getProductOrderName());
                    return stringBuilder.toString();
                }
                return null;
            }).collect(Collectors.toList());
            checkSaleVO.setParetsName(paretss);
        }
        return new ResultUtil<CheckSaleVO>().setData(checkSaleVO, "获取审核详情成功！");
    }

    //3.交易完成添加评论信息
    @PostMapping("/addComment")
    public Result<Object> addComment(String afterSaleOrderId, String comment) {

        AfterSaleOrder afterSaleOrder = iafterSaleOrderService.getById(afterSaleOrderId);
        if (ToolUtil.isNotEmpty(afterSaleOrder)) {
            afterSaleOrder.setComment(comment);
            afterSaleOrder.setIsComplete(IsComplete.IS_COMPLETE);
            iafterSaleOrderService.updateById(afterSaleOrder);
        }
        return new ResultUtil<>().setSuccessMsg("评论成功！");
    }


    /*------------------------------------维修人员----------------------------------------------------*/
    //1.获取待办任务
    @PostMapping("/getPendingList")
    public Result<Object> getPendingList(PageVo pageVo, String userId) {

        List<Task> tasks = wxservice.getTaskListByUser(pageVo, userId);
        if (ToolUtil.isNotEmpty(tasks)) {
            List<AfterSaleVO> salesOrderVOS = tasks.stream().map(task -> {

                String tableId = wxservice.getTableId(task);
                if (ToolUtil.isNotEmpty(tableId)) {
                    AfterSaleOrder afterSaleOrder = iafterSaleOrderService.getById(tableId);
                    if (ToolUtil.isNotEmpty(afterSaleOrder)) {
                        AfterSaleVO afterSaleVO = new AfterSaleVO();
                        ToolUtil.copyProperties(afterSaleOrder, afterSaleVO);
                        afterSaleVO.setTaskId(task.getId());
                        return afterSaleVO;
                    }
                }
                return null;
            }).collect(Collectors.toList());
            return new ResultUtil<>().setData(salesOrderVOS, "查询待办任务成功！");
        }
        return new ResultUtil<>().setErrorMsg(201, "暂无数据！");
    }

    /**
     * 获取售后详情
     *
     * @param
     * @return
     */
    @PostMapping("/getDebugDetail")
    public Result<Object> getDetail(String id) {
        DebugDetail debugDetail = wxservice.getAfterSaleVoDetail(id);
        if (ToolUtil.isNotEmpty(debugDetail)) {
            return new ResultUtil<>().setData(debugDetail, "获取订单详情成功！");
        }
        return new ResultUtil<>().setErrorMsg("当前产品数据为空！");
    }

    //2.提交任务
    @PostMapping("/commitTask")
    public Result<Object> commitTask(String taskId, MaintainCheckVo maintainCheckVo, String userId) {

        if (!ToolUtil.isOneEmpty(taskId, maintainCheckVo)) {
            Task task = taskService.createTaskQuery().taskId(taskId).singleResult();
            if (ToolUtil.isNotEmpty(task)) {
                String tableId = wxservice.getTableId(task);
                if (ToolUtil.isNotEmpty(tableId)) {
                    AfterSaleOrder afterSaleOrder = iafterSaleOrderService.getById(tableId);
                    if (ToolUtil.isNotEmpty(afterSaleOrder)) {
                        ToolUtil.copyProperties(maintainCheckVo, afterSaleOrder);
                        afterSaleOrder.setUserId(userId);
                        iafterSaleOrderService.updateById(afterSaleOrder);
                    }
                }
                //完成任务
                taskService.complete(taskId);
            }
            return new ResultUtil<>().setSuccessMsg("提交成功！");
        }
        return new ResultUtil<>().setErrorMsg("传入参数为空！");
    }



    /**
     * 商品购买成功之后删除购物车
     */
    @RequestMapping("/deleteShopCardMultiple")
    public Result<Object> deleteShopCardMultiple(String id) {

        //去掉字符串的最后一个点
        String ids = id.substring(0,id.length()-1);

        if (ToolUtil.isNotEmpty(ids)) {

            String[] sourceStrArray = ids.split(",");//分割出来的字符数组
            for (int i = 0; i < sourceStrArray.length; i++) {
                System.out.println("---------id--------"+sourceStrArray[i]);
                iShopCartService.getBaseMapper().deleteById(sourceStrArray[i]);
            }

            return new ResultUtil<>().setSuccessMsg("删除成功");
        } else {
            return new ResultUtil<>().setErrorMsg("插入参数为空");
        }
    }











}

