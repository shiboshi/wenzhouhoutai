package cn.exrick.xboot.modules.bussiness.refundtype.service;

import com.baomidou.mybatisplus.extension.service.IService;
import cn.exrick.xboot.modules.bussiness.refundtype.pojo.refundtype;

import java.util.List;

/**
 * 退款类型接口
 * @author 石博士
 */
public interface IrefundtypeService extends IService<refundtype> {

}