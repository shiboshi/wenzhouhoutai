package cn.exrick.xboot.modules.bussiness.address;

public class AddressProvider {
    /**
     * 产品产品分类SQL
     * @param parentId 父级ID
     * @return
     */
    public String getAddressSql(String parentId) {
        StringBuffer stringBuffer = new StringBuffer();
        stringBuffer.append("select * from t_address where parent_id = ")
                .append(parentId).append(" and del_flag = 0");
        return stringBuffer.toString();
    }
}
