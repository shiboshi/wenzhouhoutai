package cn.exrick.xboot.modules.bussiness.product.serviceImp;

import cn.exrick.xboot.common.utils.CommonUtil;
import cn.exrick.xboot.common.utils.SecurityUtil;
import cn.exrick.xboot.common.utils.ToolUtil;
import cn.exrick.xboot.common.vo.Result;
import cn.exrick.xboot.modules.base.dao.mapper.UserMapper;
import cn.exrick.xboot.modules.base.entity.Department;
import cn.exrick.xboot.modules.base.service.mybatis.DepartMentService;
import cn.exrick.xboot.modules.bussiness.classification.mapper.ClassificationMapper;
import cn.exrick.xboot.modules.bussiness.classification.pojo.Classification;
import cn.exrick.xboot.modules.bussiness.classification.service.IClassificationService;
import cn.exrick.xboot.modules.bussiness.color.pojo.Color;
import cn.exrick.xboot.modules.bussiness.color.service.IcolorService;
import cn.exrick.xboot.modules.bussiness.dealerLevel.pojo.DealerLevel;
import cn.exrick.xboot.modules.bussiness.dealerLevel.service.IDealerLevelService;
import cn.exrick.xboot.modules.bussiness.indentOrder.mapper.IndentOrderMapper;
import cn.exrick.xboot.modules.bussiness.packageSize.pojo.PackageSize;
import cn.exrick.xboot.modules.bussiness.packageSize.service.IPackageSizeService;
import cn.exrick.xboot.modules.bussiness.product.VO.ProductCommon;
import cn.exrick.xboot.modules.bussiness.product.VO.ProductOrderExtend;
import cn.exrick.xboot.modules.bussiness.product.VO.ProductOrderVo;
import cn.exrick.xboot.modules.bussiness.product.mapper.ProductOrderMapper;
import cn.exrick.xboot.modules.bussiness.product.mapper.productModelMapper;
import cn.exrick.xboot.modules.bussiness.product.pojo.ProductModel;
import cn.exrick.xboot.modules.bussiness.product.pojo.ProductOrder;
import cn.exrick.xboot.modules.bussiness.product.service.IproductOrderService;
import cn.exrick.xboot.modules.bussiness.productPrice.pojo.ProductPrice;
import cn.exrick.xboot.modules.bussiness.productPrice.service.IProductPriceService;
import cn.exrick.xboot.modules.bussiness.purchaseOrder.mapper.purchaseOrderMapper;
import cn.exrick.xboot.modules.bussiness.unit.VO.UnitVO;
import cn.exrick.xboot.modules.bussiness.unit.pojo.Unit;
import cn.exrick.xboot.modules.bussiness.unit.service.IUnitService;
import cn.hutool.core.bean.BeanUtil;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.core.toolkit.support.SFunction;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.google.common.base.Joiner;
import com.google.common.base.Splitter;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.lang.Nullable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * 产品型号接口实现
 *
 * @author xiaofei
 */
@Slf4j
@Service
@Transactional
public class IproductOrderServiceImpl extends ServiceImpl<ProductOrderMapper, ProductOrder> implements IproductOrderService {
    private final static Logger LOGGER = LoggerFactory.getLogger(IproductOrderServiceImpl.class);

    @Autowired
    private IClassificationService iClassificationService;
    @Autowired
    private IcolorService icolorService;
    @Autowired
    private IUnitService iUnitService;
    @Autowired
    private ProductOrderMapper productOrderMapper;
    @Autowired
    private SecurityUtil securityUtil;
    @Autowired
    private ClassificationMapper classificationMapper;
    @Autowired
    private IProductPriceService productPriceService;
    @Autowired
    private IDealerLevelService dealerLevelService;
    @Autowired
    private IproductOrderService productOrderService;
    @Autowired
    private IPackageSizeService packageSizeService;
    @Autowired
    private DepartMentService departMentService;
    @Autowired
    private IndentOrderMapper indentOrderMapper;
    @Autowired
    private purchaseOrderMapper prchaseOrderMapper1;
    @Autowired
    private productModelMapper productModelMapper1;
    @Autowired
    private UserMapper userMapper;

    private static final Splitter SPLITTER = Splitter.on(",").trimResults().omitEmptyStrings();
    private static final Joiner JOINER = Joiner.on("/").skipNulls();
    private static SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");

    @Nullable
    @Override
    public ProductCommon getProductCommon(String productOrderId) {

        ProductOrder productOrder = this.getById(productOrderId);

        ProductCommon productCommon = ProductCommon.builder().build();
        //1.获取产品名字信息
        if (ToolUtil.isNotEmpty(productOrder)) {
            Classification classification = iClassificationService.getById(productOrder.getKindId());
            if (ToolUtil.isNotEmpty(classification)) {
                productCommon.setProductOrderName(classification.getTitle());
                Classification classificationTwo = iClassificationService.getById(classification.getParentId());
                if (!CommonUtil.isEmpty(classificationTwo)) {
                    productCommon.setProductModelName(classificationTwo.getTitle());
                    productCommon.setModelNameIn(productModelMapper1.selectList(new QueryWrapper<ProductModel>().lambda().eq(ProductModel::getKindId, classificationTwo.getId())).get(0).getModelNameIn());
                    Classification classificationThree = iClassificationService.getById(classificationTwo.getParentId());
                    if (!CommonUtil.isEmpty(classificationThree)) {
                        productCommon.setProductName(classificationThree.getTitle());
                    }
                }
            }
            //2.获取产品颜色信息
            Color color = icolorService.getById(productOrder.getColorId());
            if (!CommonUtil.isEmpty(color)) {
                productCommon.setMaterial(color.getTitle());

                Color colorTwo = icolorService.getById(color.getParentId());
                if (!CommonUtil.isEmpty(colorTwo)) {
                    productCommon.setColorNumber(colorTwo.getTitle());
                    Color colorThree = icolorService.getById(colorTwo.getParentId());
                    if (!CommonUtil.isEmpty(colorThree)) {
                        productCommon.setColor(colorThree.getTitle());
                    }
                }
            }
            //获取单位信息
            Unit unit = iUnitService.getById(productOrder.getUnit());
            if (ToolUtil.isNotEmpty(unit)) {
                productCommon.setUnitName(unit.getUnitName());
            }
            //最大单位
            Unit unitMax = iUnitService.getById(productOrder.getUnitMax());
            if (ToolUtil.isNotEmpty(unit)) {
                productCommon.setUnitMaxName(unitMax.getUnitName());
            }
            //最小单位
            Unit unitMin = iUnitService.getById(productOrder.getUnitMin());
            if (ToolUtil.isNotEmpty(unit)) {
                productCommon.setUnitMinName(unitMin.getUnitName());
            }
            //获取产品型号


            //基本信息
            BeanUtil.copyProperties(productOrder, productCommon);

            //获取包装尺寸信息
            List<PackageSize> packageSizes = packageSizeService.list(Wrappers.<PackageSize>lambdaQuery()
                    .eq(PackageSize::getProductOrderId, productOrder.getKindId()));
            StringBuilder stringBuilder = new StringBuilder();
            packageSizes.forEach(Size -> stringBuilder.append(Size.getLength()).append("*").append(Size.getWide()).append("*")
                    .append(Size.getTall()).append("\n"));
            productCommon.setPackageSize(stringBuilder.toString());
            //获取图片，报告信息
            if (StringUtils.isNotBlank(productOrder.getPicPath())) {
                List<String> pics = Lists.newArrayList(SPLITTER.split(productOrder.getPicPath()));
                if (!CommonUtil.isEmpty(pics)) {
                    productCommon.setPicPath(pics);
                }
            }

            if (StringUtils.isNotBlank(productOrder.getReportCertificate())) {
                List<String> reports = Lists.newArrayList(SPLITTER.split(productOrder.getReportCertificate()));
                if (!CommonUtil.isEmpty(reports)) {
                    productCommon.setReportCertificate(reports);
                }
            }

            return productCommon;
        } else {
            return null;
        }
    }

    @Override
    public IPage<ProductOrderVo> getAllProduct(Page page, String productModelName) {
        IPage<ProductOrderVo> list = productOrderMapper.getAllProduct(page, productModelName);
        return list;
    }

    @Override
    public List<ProductCommon> getAllProductByDist(String productOrderName, Integer code) {
        //code == 1 是经销商看到的价格 code == 2 是 门店看到的价格
        List<ProductPrice> productPrices = getProductPriceList(code);
        if (ToolUtil.isNotEmpty(productPrices)) {
            List<ProductCommon> productOrderVos = productPrices.stream().map(price -> {
                Map<SFunction<ProductOrder, ?>, Object> map = Maps.newHashMap();
                map.put(ProductOrder::getProductOrderName, productOrderName);

                ProductOrder productOrder = productOrderService.getOne(Wrappers.<ProductOrder>lambdaQuery()
                        .eq(ProductOrder::getKindId, price.getProductOrderId())
                        .allEq(true, map, false));

                if (ToolUtil.isNotEmpty(productOrder)) {
                    ProductCommon productCommon = productOrderService.getProductCommon(productOrder.getId());
                    if (code == 1) {
                        productCommon.setPrice(price.getProductPrice());
                    } else {
                        productCommon.setPrice(productCommon.getUniformSalesPrice());
                    }
                    return productCommon;
                }
                return null;
            }).collect(Collectors.toList());
            return productOrderVos;
        }
        return null;
    }

    /**
     * 1.获取当前登录者的等级
     *
     * @return
     */
    public String getLevel(int code) {

        if (code == 1) {
            DealerLevel dealerLevelOne = dealerLevelService.getOne(Wrappers.<DealerLevel>lambdaQuery()
                    .eq(DealerLevel::getDeptId, securityUtil.getCurrUser().getDepartmentId()));
            if (ToolUtil.isNotEmpty(dealerLevelOne)) {
                return dealerLevelOne.getLevelId();
            }
        } else if (code == 2) {
            Department department = departMentService.getById(securityUtil.getCurrUser().getDepartmentId());
            if (ToolUtil.isNotEmpty(department)) {
                DealerLevel dealerLevelTwo = dealerLevelService.getOne(Wrappers.<DealerLevel>lambdaQuery()
                        .eq(DealerLevel::getDeptId, department.getParentId()));
                if (ToolUtil.isNotEmpty(dealerLevelTwo)) {
                    return dealerLevelTwo.getLevelId();
                } else {
                    DealerLevel dealerLevelTwos = dealerLevelService.getOne(Wrappers.<DealerLevel>lambdaQuery()
                            .eq(DealerLevel::getDeptId, department.getId()));
                    if (ToolUtil.isNotEmpty(dealerLevelTwos)) {
                        return dealerLevelTwos.getLevelId();
                    }
                }
            }
        }
        return null;
    }

    /**
     * 2.获取符合条件的产品价格列表
     *
     * @return
     */
    public List<ProductPrice> getProductPriceList(int code) {
        List<ProductPrice> productPrices = productPriceService.list(Wrappers.<ProductPrice>lambdaQuery()
                .eq(ProductPrice::getLevelId, getLevel(code)));
        if (ToolUtil.isNotEmpty(productPrices)) {
            return productPrices.stream().filter(product -> product.getProductPrice() != null).collect(Collectors.toList());
        }
        return null;
    }

    @Override
    public ProductOrderVo getAllProductDetail(String id) {
        ProductOrderVo productOrderVo = productOrderMapper.getAllProductDetail(id);
        return productOrderVo;
    }

    @Override
    public IPage<UnitVO> getProductUnit(Page initMpPage, String id) {
        IPage<UnitVO> list = productOrderMapper.getProductUnit(initMpPage, id);
        return list;
    }

    @Override
    public Result<ProductOrder> insertProduct(ProductOrderExtend productOrderVo) {
        Result<ProductOrder> result = new Result<>();
        try {
            //添加产品型号
            Classification classification = new Classification();
            classification.setParentId(productOrderVo.getProductModelId());
            classification.setTitle(productOrderVo.getProductOrderName());
            classification.setIsParent(false);
            classification.setParentTitle(classificationMapper.selectById(productOrderVo.getProductModelId()).getTitle());
            classification.setCreateBy(securityUtil.getCurrUser().getId());
            classification.setCreateTime(new Date());
            classificationMapper.insert(classification);
            //添加产品
            ProductOrder productOrder = new ProductOrder();
            ToolUtil.copyProperties(productOrderVo, productOrder);
            productOrder.setCreateBy(securityUtil.getCurrUser().getId());
            productOrder.setCreateTime(new Date());
            productOrder.setFourh(productOrderVo.getFourHQ());
            //productOrder.setTimeToMarket(new Date(productOrderVo.getTimeToMarket()));
            productOrder.setTimeToMarket(simpleDateFormat.parse(productOrderVo.getTimeToMarket()));
            productOrder.setKindId(classificationMapper.selectOne(new QueryWrapper<Classification>().orderByDesc("create_time").last(" limit 1 ")).getId());
            productOrderMapper.insert(productOrder);
            result.setCode(200);
            result.setMessage("添加成功");
            result.setSuccess(true);
            result.setResult(productOrder);
        } catch (Exception e) {
            LOGGER.error("添加失败：{}", e);
            result.setCode(500);
            result.setMessage("添加失败");
            result.setSuccess(false);
        }

        return result;
    }

    @Override
    public Result<ProductOrder> updateProduct(ProductOrderExtend productOrder) {
        Result<ProductOrder> result = new Result<>();
        try {
            //修改产品型号
            Classification classification = new Classification();
            classification.setParentId(productOrder.getProductModelId());
            classification.setTitle(productOrder.getProductOrderName());
            classification.setIsParent(false);
            classification.setParentTitle(classificationMapper.selectById(productOrder.getProductModelId()).getTitle());
            classification.setUpdateBy(securityUtil.getCurrUser().getId());
            classification.setUpdateTime(new Date());
            classification.setId(productOrder.getKindId());
            classificationMapper.updateById(classification);
            //修改产品
            ProductOrder product = new ProductOrder();
            ToolUtil.copyProperties(productOrder, product);
            product.setUpdateBy(securityUtil.getCurrUser().getId());
            product.setUpdateTime(new Date());
            product.setFourh(productOrder.getFourHQ());
            product.setColorId(productOrder.getMaterialId());
            productOrderMapper.updateById(product);
            result.setCode(200);
            result.setMessage("修改成功");
            result.setSuccess(true);
        } catch (Exception e) {
            result.setCode(500);
            result.setMessage("修改失败");
            result.setSuccess(false);
        }

        return result;

    }

    @Override
    public List<ProductCommon> getIndentProduct(String id) {
        String[] ids = indentOrderMapper.selectById(id).getPurchaseOrderIds().split(",");
        String userId = indentOrderMapper.selectById(id).getCreateBy();
        if (ids.length > 0) {
            List<ProductCommon> productOrderVos = new ArrayList<>();
            for (String pid : ids) {
                ProductCommon productCommon = productOrderService.getProductCommon(prchaseOrderMapper1.selectById(pid).getProductId());
                Integer amount = prchaseOrderMapper1.selectById(pid).getOrderNumbers().intValue();
                productCommon.setAmount(amount.toString());
                //获取经销商价格
                String deptId = userMapper.selectById(userId).getDepartmentId();
                String levelId = dealerLevelService.getBaseMapper().selectList(new QueryWrapper<DealerLevel>().lambda().eq(DealerLevel::getDeptId,deptId)).get(0).getLevelId();
                ProductPrice productPrice = productPriceService.getBaseMapper().selectOne(new QueryWrapper<ProductPrice>().lambda().eq(ProductPrice::getLevelId,levelId).eq(ProductPrice::getProductOrderId,productOrderService.getBaseMapper().selectById(prchaseOrderMapper1.selectById(pid).getProductId()).getKindId()));
                if(ToolUtil.isNotEmpty(productPrice)){
                    productCommon.setPrice(productPrice.getProductPrice());
                }
                //查询付款方式
                if(ToolUtil.isNotEmpty(indentOrderMapper.selectById(id).getPayType())){
                    Integer payType = indentOrderMapper.selectById(id).getPayType().getValue();
                    productCommon.setPayType(payType);
                }
                productOrderVos.add(productCommon);

            }
            return productOrderVos;
        }
        return null;
    }
}