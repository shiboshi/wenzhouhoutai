package cn.exrick.xboot.modules.base.service;

import cn.exrick.xboot.modules.base.entity.Department;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * 部门接口
 * @author shi
 */
public interface IDepartmentService extends IService<Department> {


}