package cn.exrick.xboot.modules.bussiness.applicationAfterSaleOrder.sqll;

import cn.exrick.xboot.common.utils.ToolUtil;
import org.apache.ibatis.jdbc.SQL;

import java.util.Map;

/**
 * @Author : xiaofei
 * @Date: 2019/8/16
 */
public class ApplicationSql {

    public  String SelectList(Map<String,Object> map){

        return new SQL(){{
            SELECT("a.id as id,a.appli_after_numb,(case a.check_status WHEN 1 then '待审核' WHEN 2 THEN '审核成功' WHEN 3 THEN '审核失败' END) as checkStatus,a.create_by,i.id as indentOrderId,a.inventory_ids,i.order_num as orderNum,(case a.after_sale_type WHEN 1 then '维修' WHEN 2 THEN '退货' END) as afterSaleType, " +
                    "a.create_time as createTime");
            FROM("application_after_sale_order a");
            LEFT_OUTER_JOIN(" x_indent_order i on a.indent_order_id = i.id");
            if (ToolUtil.isNotEmpty(map.get("orderNum"))){
                WHERE("lOCATE("+map.get("orderNum").toString()+",order_num) >0");
            }
            if (ToolUtil.isNotEmpty(map.get("userId"))){
                WHERE(" a.create_by = '"+map.get("userId")+"'");
            }
            if (ToolUtil.isNotEmpty(map.get("checkStatus"))){
                WHERE(" a.check_status = "+map.get("checkStatus")+"");
            }
            if (ToolUtil.isNotEmpty(map.get("numb"))){
                WHERE(" a.appli_after_numb = "+map.get("numb")+"");
            }
            WHERE(" a.del_flag=0 ");
            ORDER_BY("a.create_time desc");
        }}.toString();

    }

}
