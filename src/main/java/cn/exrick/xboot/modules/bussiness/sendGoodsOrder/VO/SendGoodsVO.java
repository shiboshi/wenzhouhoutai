package cn.exrick.xboot.modules.bussiness.sendGoodsOrder.VO;

import cn.exrick.xboot.common.enums.IsComplete;
import cn.exrick.xboot.common.vo.CommonVO;
import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import java.util.Date;

@ApiModel("发货单")
@Data
public class SendGoodsVO  {
    private String id;
    private String orderNum;
    private String  deptName;
    private String comment;
    private IsComplete isComplete;
    private String createBy;
    private String userLevel;
    private String userName;
    private String applicationId;
    @JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd HH:mm:ss")
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @ApiModelProperty("创建时间")
    private Date createTime;
    /**
     * 咨询电话
     */
    private String telephone;

    /**
     * 产品图片
     */
    private String productPic;

    /**
     * 到货时间
     */
    private String arrivalTime;
}
