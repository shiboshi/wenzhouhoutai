package cn.exrick.xboot.modules.bussiness.shiProductComparison.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import cn.exrick.xboot.modules.bussiness.shiProductComparison.pojo.back_function;

import java.util.List;

/**
 * 背部功能数据处理层
 * @author 石博士
 */
public interface back_functionMapper extends BaseMapper<back_function> {

}