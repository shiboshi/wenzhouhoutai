package cn.exrick.xboot.modules.bussiness.shiProductComparison.service;

import com.baomidou.mybatisplus.extension.service.IService;
import cn.exrick.xboot.modules.bussiness.shiProductComparison.pojo.basic_parameter;

import java.util.List;

/**
 * 基本参数表接口
 * @author 石博士
 */
public interface Ibasic_parameterService extends IService<basic_parameter> {

}