package cn.exrick.xboot.modules.bussiness.shiProductComparison.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import cn.exrick.xboot.modules.bussiness.shiProductComparison.pojo.basic_parameter;

import java.util.List;

/**
 * 基本参数表数据处理层
 * @author 石博士
 */
public interface basic_parameterMapper extends BaseMapper<basic_parameter> {

}