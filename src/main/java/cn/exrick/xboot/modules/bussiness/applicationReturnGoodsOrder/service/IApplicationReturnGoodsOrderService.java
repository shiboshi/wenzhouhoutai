package cn.exrick.xboot.modules.bussiness.applicationReturnGoodsOrder.service;

import cn.exrick.xboot.common.vo.PageVo;
import cn.exrick.xboot.modules.bussiness.applicationReturnGoodsOrder.VO.AppliReturnGoodsVO;
import cn.exrick.xboot.modules.bussiness.applicationSendGoodsOrder.VO.AppliParam;
import cn.exrick.xboot.modules.bussiness.applicationSendGoodsOrder.VO.AppliSendGoodsVO;
import cn.exrick.xboot.modules.bussiness.applicationSendGoodsOrder.pojo.ApplicationSendGoodsOrder;
import cn.exrick.xboot.modules.bussiness.purchaseOrder.VO.PurchaseOrderVO;
import cn.exrick.xboot.modules.bussiness.purchaseOrder.VO.UpdatePurchaseOrderVO;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;
import cn.exrick.xboot.modules.bussiness.applicationReturnGoodsOrder.pojo.ApplicationReturnGoodsOrder;

import java.util.List;

/**
 * 申请退货表接口
 * @author tqr
 */
public interface IApplicationReturnGoodsOrderService extends IService<ApplicationReturnGoodsOrder> {

    List<PurchaseOrderVO> getDetail(String id);

    Page<AppliReturnGoodsVO> getAll(ApplicationReturnGoodsOrder applicationReturnGoodsOrder, PageVo pageVo);

    /**
     * 插入申请退货单
     * @param appliParam
     */
    void insertOrder(AppliParam appliParam);

    AppliReturnGoodsVO selectById(String id);
}