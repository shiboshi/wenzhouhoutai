package cn.exrick.xboot.modules.bussiness.sendGoodsOrder.mapper;

import cn.exrick.xboot.modules.bussiness.sendGoodsOrder.Sql.SendOrderSql;
import cn.exrick.xboot.modules.bussiness.sendGoodsOrder.VO.SendGoodsVO;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import cn.exrick.xboot.modules.bussiness.sendGoodsOrder.pojo.SendGoodsOrder;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.ResultType;
import org.apache.ibatis.annotations.SelectProvider;
import org.springframework.stereotype.Repository;

/**
 * 发货单数据处理层
 * @author xiaofei
 */
@Mapper
@Repository
public interface sendGoodsOrderMapper extends BaseMapper<SendGoodsOrder> {

    @SelectProvider(type = SendOrderSql.class,method = "selectList")
    @ResultType(SendGoodsVO.class)
    Page<SendGoodsVO> getSendGoodsOrder(@Param("initMpPage") Page initMpPage,@Param("orderNum") String orderNum,@Param("userId") String userId,@Param("code")String code,@Param("type") Integer type,@Param("serial")String serial);

   /* @SelectProvider(type = SendOrderSql.class,method = "selectGoodOrderListByApplicationId")
    @ResultType(SendGoodsVO.class)
    Page<SendGoodsVO> selectGoodOrderListByApplicationId(@Param("applicationSendGoodsId") String applicationSendGoodsId);*/

    @SelectProvider(type = SendOrderSql.class,method = "getById")
    SendGoodsVO getById(@Param("id") String id);
}