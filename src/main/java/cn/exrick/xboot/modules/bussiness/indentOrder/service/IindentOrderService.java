package cn.exrick.xboot.modules.bussiness.indentOrder.service;

import cn.exrick.xboot.common.vo.PageVo;
import cn.exrick.xboot.common.vo.Result;
import cn.exrick.xboot.modules.activiti.entity.ActBusiness;
import cn.exrick.xboot.modules.bussiness.indentOrder.VO.IndentOrderDetailVO;
import cn.exrick.xboot.modules.bussiness.indentOrder.VO.IndentOrderVO;
import cn.exrick.xboot.modules.bussiness.indentOrder.VO.InsertVO;
import cn.exrick.xboot.modules.bussiness.purchaseOrder.VO.PurchaseOrderVO;
import cn.exrick.xboot.modules.bussiness.purchaseOrder.pojo.PurchaseOrder;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;
import cn.exrick.xboot.modules.bussiness.indentOrder.pojo.IndentOrder;

import java.util.List;

/**
 * 订货单接口
 * @author xiaofei
 */
public interface IindentOrderService extends IService<IndentOrder> {

    /**
     * 获取订货单列表
     * @param indentOrder
     * @param pageVo
     * @return
     */
    Page<IndentOrderVO> getAll(IndentOrder indentOrder, PageVo pageVo,Integer code,String userId);


    /**
     * 获取当前订单购买产品详情
     * @param indentOrderId
     * @return
     */
    List<PurchaseOrderVO> getIndentOrderDetail(String indentOrderId);


    /**
     * 添加订单
     */
    ActBusiness insertIndentOrder(List<PurchaseOrder> purchaseOrders, String procDefId, String userId, Integer payType,String indentOrderType);

    /**
     * 审批流程里面的表单数据
     * @param indentOrderId
     * @return
     */
    IndentOrderVO formItem(String indentOrderId);


   IndentOrderVO selectById(String id);

    void editIndentOrder(List<PurchaseOrder> purchaseOrders, String procDefId, String userId,String id,Integer payType);

}