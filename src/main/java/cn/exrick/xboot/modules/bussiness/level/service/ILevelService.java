package cn.exrick.xboot.modules.bussiness.level.service;

import cn.exrick.xboot.modules.bussiness.level.VO.LevelVO;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;
import cn.exrick.xboot.modules.bussiness.level.pojo.Level;

import java.util.List;

/**
 * 等级表接口
 * @author tqr
 */
public interface ILevelService extends IService<Level> {

    IPage<LevelVO> getByPage(Page initMpPage, String levelName);
}