package cn.exrick.xboot.modules.bussiness.appliSendNum.service;

import com.baomidou.mybatisplus.extension.service.IService;
import cn.exrick.xboot.modules.bussiness.appliSendNum.pojo.AppliSendNum;

/**
 * 申请发货接口
 * @author fei
 */
public interface IappliSendNumService extends IService<AppliSendNum> {

    void insert(String applicationSendOrderId,String purchaseId,Long num);
}