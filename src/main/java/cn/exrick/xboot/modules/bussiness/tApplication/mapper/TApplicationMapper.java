package cn.exrick.xboot.modules.bussiness.tApplication.mapper;

import cn.exrick.xboot.modules.bussiness.tApplication.pojo.TApplication;
import cn.exrick.xboot.modules.bussiness.tApplication.pojo.TApplicationVO;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

/**
 * 退款申请表数据处理层
 * @author tqr
 */
@Repository
public interface TApplicationMapper extends BaseMapper<TApplication> {


    /**
     * 查询所有集合
     */
    IPage<TApplicationVO> QueryTapplication(Page page, @Param("username") String username);







}