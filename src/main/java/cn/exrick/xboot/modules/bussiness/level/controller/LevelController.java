package cn.exrick.xboot.modules.bussiness.level.controller;

import cn.exrick.xboot.common.utils.PageUtil;
import cn.exrick.xboot.common.utils.ResultUtil;
import cn.exrick.xboot.common.utils.SecurityUtil;
import cn.exrick.xboot.common.vo.PageVo;
import cn.exrick.xboot.common.vo.Result;
import cn.exrick.xboot.modules.bussiness.level.VO.LevelVO;
import cn.exrick.xboot.modules.bussiness.level.pojo.Level;
import cn.exrick.xboot.modules.bussiness.level.service.ILevelService;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import java.util.Date;
import java.util.List;

/**
 * @author tqr
 */
@Slf4j
@RestController
@Api(description = "等级表管理接口")
@RequestMapping("/xboot/level")
@Transactional
public class LevelController {

    @Autowired
    private ILevelService iLevelService;

    @Autowired
    private SecurityUtil securityUtil;


    @RequestMapping(value = "/get/{id}", method = RequestMethod.GET)
    @ApiOperation(value = "通过id获取")
    public Result<Level> get(@PathVariable String id){

        Level level = iLevelService.getById(id);
        return new ResultUtil<Level>().setData(level);
    }

    @RequestMapping(value = "/getAll", method = RequestMethod.GET)
    @ApiOperation(value = "获取全部数据")
    public Result<List<Level>> getAll(){

        List<Level> list = iLevelService.list();
        return new ResultUtil<List<Level>>().setData(list);
    }

    @RequestMapping(value = "/getByPage", method = RequestMethod.POST)
    @ApiOperation(value = "分页获取")
    public Result<IPage<LevelVO>> getByPage(@ModelAttribute PageVo page, String levelName){
        IPage<LevelVO> data = new Page<>();
        if(StringUtils.isNotBlank(levelName)){
            data = iLevelService.getByPage(PageUtil.initMpPage(page),levelName);
        } else {
            data = iLevelService.page(PageUtil.initMpPage(page));
        }
        return new ResultUtil<IPage<LevelVO>>().setData(data);
    }

    @RequestMapping(value = "/insertOrUpdate", method = RequestMethod.POST)
    @ApiOperation(value = "编辑或更新数据")
    public Result<Level> saveOrUpdate(@ModelAttribute Level level){
        String username = securityUtil.getCurrUser().getUsername();
        level.setCreateBy(username);
        level.setUpdateBy(username);
        level.setCreateTime(new Date());
        level.setUpdateTime(new Date());
        if(iLevelService.saveOrUpdate(level)){
            return new ResultUtil<Level>().setData(level);
        }
        return new ResultUtil<Level>().setErrorMsg("操作失败");
    }

    @RequestMapping(value = "/delByIds/{ids}", method = RequestMethod.DELETE)
    @ApiOperation(value = "批量通过id删除")
    public Result<Object> delAllByIds(@PathVariable String[] ids){

        for(String id : ids){
            iLevelService.removeById(id);
        }
        return new ResultUtil<Object>().setSuccessMsg("批量通过id删除数据成功");
    }
}
