package cn.exrick.xboot.modules.UpayApi.test.model;

/**
 * Created by faliny on 2017/8/25.
 */
public class Goods {

    public Goods(String goodsId, String goodsName, Long quantity, Long price, String goodsCategory, String body) {
        this.goodsId = goodsId;
        this.goodsName = goodsName;
        this.quantity = quantity;
        this.price = price;
        this.goodsCategory = goodsCategory;
        this.body = body;
    }

    private String goodsId;
    private String goodsName;
    private Long quantity;
    private Long price;
    private String goodsCategory;
    private String body;

    public String getGoodsId() {
        return goodsId;
    }

    public void setGoodsId(String goodsId) {
        this.goodsId = goodsId;
    }

    public String getGoodsName() {
        return goodsName;
    }

    public void setGoodsName(String goodsName) {
        this.goodsName = goodsName;
    }

    public Long getQuantity() {
        return quantity;
    }

    public void setQuantity(Long quantity) {
        this.quantity = quantity;
    }

    public Long getPrice() {
        return price;
    }

    public void setPrice(Long price) {
        this.price = price;
    }

    public String getGoodsCategory() {
        return goodsCategory;
    }

    public void setGoodsCategory(String goodsCategory) {
        this.goodsCategory = goodsCategory;
    }

    public String getBody() {
        return body;
    }

    public void setBody(String body) {
        this.body = body;
    }
}
