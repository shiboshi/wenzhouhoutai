package cn.exrick.xboot.modules.bussiness.productPrice.service;

import cn.exrick.xboot.modules.bussiness.productPrice.VO.ProductPriceVO;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;
import cn.exrick.xboot.modules.bussiness.productPrice.pojo.ProductPrice;

import java.util.List;

/**
 * 产品价格表接口
 * @author tqr
 */
public interface IProductPriceService extends IService<ProductPrice> {

    IPage<ProductPriceVO> getProductPrice(Page initMpPage, String condition);

    boolean insertPrice(List<ProductPrice> list);

    List<ProductPrice> getByCondition(String productOrderId, String levelId);
}